<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/PROTOCOLO_SISTEMA.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Interface_mensagem.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Mensagem.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Mensagem_token.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Mensagem_generica.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Mensagem_protocolo.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Mensagem_vetor_protocolo.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Mensagem_vetor_token.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/constants.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Helper.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/HelperLog.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Generic_DAO.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/InterfaceSeguranca.php';
include_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/InterfaceSegurancaUsuario.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/protocolo/Interface_protocolo.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/SingletonLog.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Registry.php';

Helper::includePHP(false, 'adm/imports/instancias.php');

Helper::includePHP(false, 'protocolo/in/Protocolo_empresa_hosting.php');

Helper::includePHP(false, 'recursos/classes/class/LockSincronizador.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Estado_sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Estado_sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sincronizacao_mobile_iteracao.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Tipo_operacao_banco.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sistema_tabela.php');

Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sistema_atributo.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Usuario.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud_origem.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud_mobile.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud_mobile_dependente.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Corporacao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Estado_sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Estado_sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sincronizacao_mobile_iteracao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Tipo_operacao_banco.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sistema_tabela.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sistema_sequencia.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sistema_atributo.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Usuario.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud_origem.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud_mobile.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud_mobile_dependente.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Corporacao.php');

include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/DatabaseException.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/QueryException.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Database.php';

Helper::includePHP(false, 'recursos/classes/class/Seguranca.php');
Helper::includePHP(false, 'recursos/php_corporacao/InterfaceConstantDatabaseConfig.php');

include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/ConfiguracaoDatabase.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Database_SQLite.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Database.php';

include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/HelperAPC.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/HelperRedis.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/HelperAPCRedis.php';
include_once $workspaceRaiz.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'classes/Manutencao.php';


Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Crud.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Crud_mobile.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Crud_web.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Arquivo_crud_sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Arquivo_sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Banco.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Banco_sqlite.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Banco_web.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Sincronizador_espelho.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Tabela.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Tabela_web.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_SICOB.php');
Helper::includePHP(false, 'recursos/classes/BO/Database_ponto_eletronico.php');



$pathAbsoluto = str_replace("\\", "/", __DIR__);
$pathRelativo = str_replace($_SERVER["DOCUMENT_ROOT"], "", $pathAbsoluto);

$pathRelativoRaiz = substr($pathRelativo, 0, strrpos($pathRelativo, "recursos"));

$corporacao = "";


$itCorporacoes = null;
$bo = null;
while(true){
    $msg = BO_SICOB::getCorporacoesParaSincronizacaoDoHosting();
    
    if($msg != null && $msg->ok()){
        
        $corporacoes = $msg->mVetorObj;
        
        if(count($corporacoes) > 0){
            $itCorporacoes = $corporacoes;
        }
    }
    $configSincronizadorWebDb = null;
    $configWebDb= null;
    if($itCorporacoes != null && count($itCorporacoes)){
        
        $ultimaHospedagem = null;
        
        for($i = 0 ; $i < count($itCorporacoes); $i++){
            try{
                $jsonObj = $itCorporacoes[$i];

                $idHospedagem = $jsonObj->id_hospedagem;
                $corporacao = $jsonObj->corporacao;
                $idCorporacao = $jsonObj->id_corporacao;

                $msgInit = null;
                if($ultimaHospedagem == null
                    || $ultimaHospedagem != $idHospedagem
                    || $bo == null){

                    ConfiguracaoCorporacaoDinamica::factoryConfiguracaoSite($corporacao);

                    echo "Sincronizando corporacao [$idCorporacao] $corporacao - Novo host: $idHospedagem\n";

                    $configuracaoSite = Registry::get('ConfiguracaoSite');
                    if($configuracaoSite == null){
                        echo "Configuracao site indefinida para a corporacao '$corporacao'";
                        continue;
                    }
                    $idCorporacao = $configuracaoSite->ID_CORPORACAO;
                    $configSincronizadorWebDb = Registry::get('ConfiguracaoDatabasePrincipal');
                    $configWebDb = Registry::get('ConfiguracaoDatabaseSecundario');

                    echo "Config sincronizador web: \n";
                    print_r($configSincronizadorWebDb);
                    echo "Config web: \n";
                    print_r($configWebDb);

                    if($configSincronizadorWebDb == null){
                        $msg = "Config sincronizador web inexistente: '$corporacao'\n";
                        echo $msg;
                        continue;
                    }
                    if($configWebDb == null ||  !isset($configWebDb->host) ){
                        $msg = "Config web inexistente: '$corporacao'\n";
                        echo $msg;
                        continue;
                    }
                    $ultimaHospedagem = $idHospedagem;

                    if(Manutencao::checkIfAssinaturaDaCorporacaoEstaEmManutencao(
                        false , $idCorporacao , false)){
                        echo "Corporacao $corporacao em manutencao!\n";
                        continue;
                    }
                    echo "Executando iteracao da $corporacao...\n";

                    if($bo == null)
                        $bo = new BO_Sincronizacao(
                            $idCorporacao,
                            $corporacao ,
                            $configSincronizadorWebDb,
                            $configWebDb);
                    else {
                        $msgInit = $bo->init(
                            $idCorporacao,
                            $corporacao,
                            $configSincronizadorWebDb,
                            $configWebDb);
                    }

                } else{
                    echo "Sincronizando corporacao [$idCorporacao] $corporacao - Cacheado Host: $idHospedagem\n";
                    $msgInit= $bo->init(
                        $idCorporacao ,
                        $corporacao ,
                        $configSincronizadorWebDb,
                        $configWebDb);
                }
                print_r($msgInit);

                if($msgInit == null || $msgInit->ok()) {
                    $msg = $bo->executaSincronizacaoCompleta();
                    print_r($msg);
                }

                $bo->close();



                if($msgInit == null || $msgInit->ok()){
                    echo("\n\n\n$i\n");

                    sleep(10);

                    $boBS = new BO_Banco_sqlite(
                        $idCorporacao,
                        $corporacao,
                        $configWebDb,
                        $configSincronizadorWebDb);

                    $msg = $boBS->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao();
                }
            }catch(Exception $ex){
                $msg =  new Mensagem(null, null, $ex);
            }

            print_r($msg);
            sleep(2);
        }
    } else {
        echo "N�o temos corpora��es na fila para serem sincronizadas\n";
        sleep(5);
    }
}
//
//if(strlen($corporacao) && strlen($idCorporacao)){
//
//    $i = 1;
//    while(true){
//        //http://127.0.0.1:8080/Cobranca/10003Corporacao/adm/webservice.php?class=Servicos_web&action=getCorporacoesParaSincronizacaoDoHosting&id_empresa_hosting=1
//        //BO_SICOB::getCorporacoesParaSincronizacaoDoHosting()
//        $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
//
//
//
////                if($i == 2){
////                    print_r(Database::$arrLinks);
////                    exit();
////                }
//        //idUltimaSincronizacao - id do SistemaRegistroSincronizadorAndroidParaWeb;
//        //$ultimoIdSincronizacao = Helper::POSTGET("ultimo_id_sincronizacao");
//
//        $msg = $obj->executaSincronizacaoCompleta();
//
//        print_r($msg);
//
//
//        $obj->close();    
//        $obj = null;
//
//
//        echo("\n\n\n$i\n");
//        $i++;
//        sleep(10);
//
//        $bo = new BO_Banco_sqlite($idCorporacao, $corporacao);
//        $msg = $bo->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao();
//        $bo->close();
//        print_r($msg);
//        sleep(2);
//
//    }
//} else {
//    return new Mensagem(
//        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
//        "[lnkerigoer] cicloSincronizacaoCompleta - Parametros invalidos");
//}
