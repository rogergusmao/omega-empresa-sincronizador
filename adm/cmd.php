<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/imports/header_json.php';

ConfiguracaoCorporacaoDinamica::init();

require_once '../recursos/php/funcoes.php';

//include_once '../adm/imports/instancias.php';
include_once '../adm/imports/lingua.php';

include_once '../web_service/protocolo/PROTOCOLO_SISTEMA.php';
include_once '../web_service/protocolo/Interface_mensagem.php';
include_once '../web_service/protocolo/Mensagem.php';
include_once '../web_service/protocolo/Mensagem_token.php';
include_once '../web_service/protocolo/Mensagem_generica.php';
include_once '../web_service/protocolo/Mensagem_protocolo.php';
include_once '../web_service/protocolo/Mensagem_vetor_protocolo.php';
include_once '../web_service/protocolo/Mensagem_vetor_token.php';


include_once '../web_service/Servicos_web.php';


try{
    
    //cmd.php (idCorporacao) (corporacao) (class) (action) (arguments like html)
    if($argc < 5){
        $msg = new Mensagem(
            PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
            "Parametros inválidos. Ex correto: cmd.php (idCorporacao) (corporacao) (class) (action) (arguments like html)");
        echo jsonEncode ($msg);
        exit();
    } else {
        Helper::addPostGet("class", $argv[3]);
        Helper::addPostGet("action", $argv[4]);
        $classe = Helper::GET("class");
        $action = Helper::GET("action");
        Helper::addPostGet("id_corporacao", $argv[1]);
        Helper::addPostGet("corporacao", $argv[2]);
        
    }

    $obj = call_user_func_array(array($classe, "factory"), array());

    

    //---------------------------------
    //*
    //*
    // INICIAR TRANSACAO
    //*
    //*
    //---------------------------------


    
    if($action == "add"){
        $retorno = $obj->__actionAdd();
    }
    elseif($action == "add_ajax"){
        $retorno = $obj->__actionAddAjax();
    }
    elseif($action == "edit"){
        $retorno = $obj->__actionEdit();

    }
    elseif($action == "edit_ajax"){

        $retorno = $obj->__actionEdit();

    }
    elseif($action == "remove"){

       $retorno = $obj->__actionRemove();

    }
    elseif($action == "login"){

        $retorno = $obj->__actionLogin(Helper::POST("txtLogin"), Helper::POST("txtSenha"));

    }
    elseif($action == "logout"){
        $retorno = $obj->__actionLogout();
    }
    else{
        $retorno = call_user_func(array($obj, $action));
    }
} catch (Exception $ex) {
    $retorno = new Mensagem(null, null, $ex);
}
if($retorno != null){

    $log= Registry::get('HelperLog');
    $log->gravarLogEmCasoErro($retorno);

    $strRet = Helper::jsonEncode ($retorno);
    echo $strRet;
}
        
