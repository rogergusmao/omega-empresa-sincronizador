<?php

	Javascript::setRaizDaEstrutura();

    echo Javascript::importarBibliotecaFusionCharts();

    $objBanco = new Database();
    $objEstruturaOrg = new Estrutura_Organizacional();
    $objGrafico = new Grafico("Aporte de Conhecimento Tácito - Contratados");

    $arrComplemento = $objEstruturaOrg->getArrayDePartesParaConsultaSQLRecursiva($objGrafico->arrayNiveisAConsultar, "v.nodo_nivel_estrutura_id_INT", $objGrafico->somenteAreasDaPesquisa, false, false, "nome", $objGrafico->consultarVagas);

    $objConsultas = new ConsultasDosGraficos($arrComplemento);

    $objConsultas->consultaNumeroDeVagasContratadasPorNSPlanejado($objBanco);

    $totalPessoasNenhumaContratado = 0;
    $totalPessoasBaixaContratado = 0;
    $totalPessoasMediaContratado = 0;
    $totalPessoasAltaContratado  = 0;

    while($dados = $objBanco->fetchArray()){

        if($dados[1] == "0")
            $totalPessoasNenhumaContratado = $dados[0];
        elseif($dados[1] == "1")
            $totalPessoasBaixaContratado = $dados[0];
        elseif($dados[1] == "2")
            $totalPessoasMediaContratado = $dados[0];
        elseif($dados[1] == "3")
            $totalPessoasAltaContratado = $dados[0];

    }

    $totalMobilizado = $totalPessoasNenhumaContratado + $totalPessoasBaixaContratado +
                       $totalPessoasMediaContratado + $totalPessoasAltaContratado;



    //NUMERO DE PESSOAS CONTRATADAS (REAL)
    $objConsultas->consultaNumeroDeVagasContratadasPorNSReal($objBanco);

    $totalPessoasNenhumaContratadoReal = 0;
    $totalPessoasBaixaContratadoReal = 0;
    $totalPessoasMediaContratadoReal = 0;
    $totalPessoasAltaContratadoReal = 0;

    while($dados = $objBanco->fetchArray()){

        if($dados[1] == "0")
            $totalPessoasNenhumaContratadoReal = $dados[0];
        elseif($dados[1] == "1")
            $totalPessoasBaixaContratadoReal = $dados[0];
        elseif($dados[1] == "2")
            $totalPessoasMediaContratadoReal = $dados[0];
        elseif($dados[1] == "3")
            $totalPessoasAltaContratadoReal = $dados[0];

    }

    $totalClassificados = $totalPessoasNenhumaContratadoReal + $totalPessoasBaixaContratadoReal +
                          $totalPessoasMediaContratadoReal + $totalPessoasAltaContratadoReal;



    //NUMERO DE VAGAS A CONTRATAR
    $objConsultas->consultaNumeroDeVagasAContratarPorNSPlanejado($objBanco);

    $totalPessoasNenhumaAContratar = 0;
    $totalPessoasBaixaAContratar = 0;
    $totalPessoasMediaAContratar = 0;
    $totalPessoasAltaAContratar = 0;

    while($dados = $objBanco->fetchArray()){

        if($dados[1] == "0")
            $totalPessoasNenhumaAContratar = $dados[0];
        elseif($dados[1] == "1")
            $totalPessoasBaixaAContratar = $dados[0];
        elseif($dados[1] == "2")
            $totalPessoasMediaAContratar = $dados[0];
        elseif($dados[1] == "3")
            $totalPessoasAltaAContratar = $dados[0];

    }


    //ANOS DE EXPERIENCIA: REAL CONTRATADO

    $objConsultas->consultaTempoTotalDeExperienciaRealContratadosEmMesesNoNivelDeSimilaridade(1, $objBanco);

    $arrBaixaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrBaixaSim = Helper::getMatrizLinearToArray($arrBaixaSim);



    $objConsultas->consultaTempoTotalDeExperienciaRealContratadosEmMesesNoNivelDeSimilaridade(2, $objBanco);

    $arrMediaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrMediaSim = Helper::getMatrizLinearToArray($arrMediaSim);



    $objConsultas->consultaTempoTotalDeExperienciaRealContratadosEmMesesNoNivelDeSimilaridade(3, $objBanco);

    $arrAltaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrAltaSim = Helper::getMatrizLinearToArray($arrAltaSim);


    //calculo do total de anos
    $totalAnosBaixaReal = round(array_sum($arrBaixaSim) / 12, 1);
    $totalAnosMediaReal = round(array_sum($arrMediaSim) / 12, 1);
    $totalAnosAltaReal =  round(array_sum($arrAltaSim)  / 12, 1);

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosBaixaReal;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasBaixaContratadoReal;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosMediaReal;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasMediaContratadoReal;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosAltaReal;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasAltaContratadoReal;



    //ANOS DE EXPERIENCIA: PLANEJADO CONTRATADO

    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaContratadosEmMesesNoNivelDeSimilaridade(1, $objBanco);

    $arrBaixaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrBaixaSim = Helper::getMatrizLinearToArray($arrBaixaSim);



    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaContratadosEmMesesNoNivelDeSimilaridade(2, $objBanco);

    $arrMediaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrMediaSim = Helper::getMatrizLinearToArray($arrMediaSim);



    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaContratadosEmMesesNoNivelDeSimilaridade(3, $objBanco);

    $arrAltaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrAltaSim = Helper::getMatrizLinearToArray($arrAltaSim);



    //calculo do total de anos
    $totalAnosBaixaPlanejado = round(array_sum($arrBaixaSim) /12, 1);
    $totalAnosMediaPlanejado = round(array_sum($arrMediaSim) / 12, 1);
    $totalAnosAltaPlanejado =  round(array_sum($arrAltaSim) / 12, 1);

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosBaixaPlanejado;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasBaixaContratado;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosMediaPlanejado;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasMediaContratado;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosAltaPlanejado;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasAltaContratado;



    //ANOS DE EXPERIENCIA: PLANEJADO A CONTRATAR

    //alta similaridade
    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaAContratarEmMesesNoNivelDeSimilaridade(3, $objBanco);

    $arrAltaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrAltaSim = Helper::getMatrizLinearToArray($arrAltaSim);



    //media similaridade
    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaAContratarEmMesesNoNivelDeSimilaridade(2, $objBanco);

    $arrMediaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrMediaSim = Helper::getMatrizLinearToArray($arrMediaSim);



    //baixa similaridade
    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaAContratarEmMesesNoNivelDeSimilaridade(1, $objBanco);

    $arrBaixaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrBaixaSim = Helper::getMatrizLinearToArray($arrBaixaSim);



    //calculo do total de anos
    $totalAnosBaixaAContratar = round(array_sum($arrBaixaSim) /12, 1);
    $totalAnosMediaAContratar = round(array_sum($arrMediaSim) / 12, 1);
    $totalAnosAltaAContratar =  round(array_sum($arrAltaSim) / 12, 1);

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosBaixaAContratar;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasBaixaAContratar;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosMediaAContratar;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasMediaAContratar;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosAltaAContratar;
    Grafico::$numeroPessoasGraficoAporte[] = $totalPessoasAltaAContratar;

    $totalAnosBaixaTudo = $totalAnosBaixaPlanejado + $totalAnosBaixaAContratar;
    $totalAnosMediaTudo = $totalAnosMediaPlanejado + $totalAnosMediaAContratar;
    $totalAnosAltaTudo = $totalAnosAltaPlanejado + $totalAnosAltaAContratar;

    $maximoY = max(array($totalAnosAltaTudo, $totalAnosMediaTudo));

    $maximoYNovo = round($maximoY, -2);

    if($maximoYNovo < $maximoY){

    	$maximoYNovo += 100;

    }

    $maximoY = $maximoYNovo;

    $strPathSWF = Helper::acharRaiz() . "recursos/libs/fusion_charts";

    $strXML  = "<chart bgColor='FFFFFF' outCnvBaseFontColor='666666' caption='Aporte de Conhecimento Tácito'  xAxisName='Alta Similaridade' yAxisName='' numberPrefix='' showValues='0'
                    numVDivLines='10' showAlternateVGridColor='1' AlternateVGridColor='e1f5ff' divLineColor='e1f5ff' vdivLineColor='e1f5ff'  baseFontColor='666666' showLegend='0'
                    showLimits='0' showDivLineValue='0' yAxisMinValue='0' yAxisMaxValue='{$maximoY}' toolTipBgColor='F3F3F3' toolTipBorderColor='666666' canvasBorderColor='666666' canvasBorderThickness='1' showPlotBorder='1' plotFillAlpha='80'>
                    <categories>
                            <category label='' />
                            <category label='' />
                    </categories>
                    <dataset seriesName='Atual' color='B1D1DC' plotBorderColor='B1D1DC'>
                            <set value='{$totalAnosAltaReal}' />
                            <set value='{$totalAnosAltaPlanejado}'/>
                    </dataset>
                    <dataset seriesName='Demanda Futura' color='C8A1D1' plotBorderColor='C8A1D1'>
                            <set  />
                            <set value='{$totalAnosAltaAContratar}'/>
                    </dataset>
                    <styles>
                            <definition>
                                    <style type='animation' name='TrendAnim' param='_alpha' duration='1' start='0' />
                            </definition>
                            <application>
                                    <apply toObject='TRENDLINES' styles='TrendAnim' />
                            </application>
                    </styles>
                </chart>";

    //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    echo renderChart("{$strPathSWF}/StackedBar3D.swf", "", $strXML, "alta", $objGrafico->larguraDoGrafico, 195, false, false);

    echo "<br />";

    $strXML  = "<chart bgColor='FFFFFF' outCnvBaseFontColor='666666' caption=''  xAxisName='Média Similaridade' yAxisName='Anos de Experiência' numberPrefix='' showValues='0'
                    numVDivLines='10' showAlternateVGridColor='1' AlternateVGridColor='e1f5ff' divLineColor='e1f5ff' vdivLineColor='e1f5ff'  baseFontColor='666666' showLegend='0'
                    yAxisMinValue='0' yAxisMaxValue='{$maximoY}' toolTipBgColor='F3F3F3' toolTipBorderColor='666666' canvasBorderColor='666666' canvasBorderThickness='1' showPlotBorder='1' plotFillAlpha='80'>
                    <categories>
                            <category label='' />
                            <category label='' />
                    </categories>
                    <dataset seriesName='Atual' color='B1D1DC' plotBorderColor='B1D1DC'>
                            <set value='{$totalAnosMediaReal}' />
                            <set value='{$totalAnosMediaPlanejado}'/>
                    </dataset>
                    <dataset seriesName='Demanda Futura' color='C8A1D1' plotBorderColor='C8A1D1'>
                            <set  />
                            <set value='{$totalAnosMediaAContratar}'/>
                    </dataset>
                    <styles>
                            <definition>
                                    <style type='animation' name='TrendAnim' param='_alpha' duration='1' start='0' />
                            </definition>
                            <application>
                                    <apply toObject='TRENDLINES' styles='TrendAnim' />
                            </application>
                    </styles>
                </chart>";

    //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    echo renderChart("{$strPathSWF}/StackedBar3D.swf", "", $strXML, "media", $objGrafico->larguraDoGrafico, 200, false, false);

    echo "<br />";

    $realBaixaNenhumaContratados = $totalPessoasBaixaContratadoReal + $totalPessoasNenhumaContratadoReal;
    $realNaoClassificados = $totalMobilizado - $totalClassificados;
    $planejadoBaixaNenhumaContratados = $totalPessoasBaixaContratado + $totalPessoasNenhumaContratado;
    $planejadoAContratar = $totalPessoasBaixaAContratar + $totalPessoasNenhumaAContratar;

    $strXML  = "<chart bgColor='FFFFFF' outCnvBaseFontColor='666666' caption=''  xAxisName='Baixa / Outros' yAxisName='Número de Pessoas' numberPrefix='' showValues='0'
                    numVDivLines='10' showAlternateVGridColor='1' AlternateVGridColor='e1f5ff' divLineColor='e1f5ff' vdivLineColor='e1f5ff'  baseFontColor='666666' showLegend='0'
                    toolTipBgColor='F3F3F3' toolTipBorderColor='666666' canvasBorderColor='666666' canvasBorderThickness='1' showPlotBorder='1' plotFillAlpha='80'>
                    <categories>
                            <category label='' />
                            <category label='' />
                    </categories>
                    <dataset seriesName='Atual' color='B1D1DC' plotBorderColor='B1D1DC'>
                            <set value='{$realBaixaNenhumaContratados}' />
                            <set value='{$planejadoBaixaNenhumaContratados}'/>
                    </dataset>
                    <dataset seriesName='Demanda Futura' color='C8A1D1' plotBorderColor='C8A1D1'>
                            <set  />
                            <set value='{$planejadoAContratar}'/>
                    </dataset>
                    <dataset seriesName='Não Classificados' color='4FADE1' plotBorderColor='C8A1D1'>
                            <set value='{$realNaoClassificados}'/>
                            <set />
                    </dataset>
                    <styles>
                            <definition>
                                    <style type='animation' name='TrendAnim' param='_alpha' duration='1' start='0' />
                            </definition>
                            <application>
                                    <apply toObject='TRENDLINES' styles='TrendAnim' />
                            </application>
                    </styles>
                </chart>";

    //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    echo renderChart("{$strPathSWF}/StackedBar3D.swf", "", $strXML, "baixa", $objGrafico->larguraDoGrafico, 200, false, false);

?>
