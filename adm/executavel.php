<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once '../recursos/php/funcoes_sem_corporacao.php';

require_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'imports/instancias.php';

require_once '../pipeline_sync/MultiRun.php';

try{
    //executavel.php [corporacao] [class] [action] [GET]
    if(php_sapi_name() != 'cli'){
        echo "Esse arquivo so pode ser chamando por linha de comando";
        exit();
    }
    if($argc < 4){
        echo "Chamada inválida. Ex de chamada correta: executavel.php [corporacao] [class] [action] [GET]*. Sendo GET os parametros (opcionais) separados com ','";
        exit();
    }
    $strGet = "";
    $get = array();
    if(count($argv) > 5){
        $strGet = $argv[6];
        $get = explode(",", $strGet);    
    }
    
    $corporacao = $argv[0];
    $classe = $argv[2];
    $action = $argv[4];

    $obj = call_user_func_array(array($classe, "factory"), array());


    //---------------------------------
    //*
    //*
    // INICIAR TRANSACAO
    //*
    //*
    //---------------------------------

    $retorno = null;
    //chama a funcao de nome "$action"
    if(!empty($get))
        $retorno = call_user_func(array($obj, $action, $get));
    else
        $retorno = call_user_func(array($obj, $action));
//        echo "ACTION: $action<br/>";
//        print_r($retorno);

} catch (Exception $ex) {
    $retorno = new Mensagem(null, null, $ex);
}
if($retorno != null){
    $log= Registry::get('HelperLog');
    $log->gravarLogEmCasoErro($retorno);

    $strRet = Helper::jsonEncode ($retorno);
    echo $strRet;
}
        
