<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sincronizacao_mobile
    * DATA DE GERAÇÃO:    10.07.2016
    * ARQUIVO:            sincronizacao_mobile.php
    * TABELA MYSQL:       sincronizacao_mobile
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sincronizacao_mobile();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sincronizacao_mobile"=>"Adicionar nova sincronização mobile",
    					 "list_sincronizacao_mobile"=>"Listar sincronizações mobile");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sincronizacao_mobile">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Sincronizações Mobile</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

