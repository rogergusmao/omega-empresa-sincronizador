<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sincronizacao
    * DATA DE GERAÇÃO:    28.07.2014
    * ARQUIVO:            sincronizacao.php
    * TABELA MYSQL:       sincronizacao
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sincronizacao();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sincronizacao"=>"Adicionar nova sincronização",
    					 "list_sincronizacao"=>"Listar sincronizações");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sincronizacao">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Sincronizações</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

