<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema_tabela
    * DATA DE GERA��O:    23.06.2014
    * ARQUIVO:            sistema_tabela.php
    * TABELA MYSQL:       sistema_tabela
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_tabela();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sistema_tabela"=>"Adicionar nova tabela do sistema",
    					 "list_sistema_tabela"=>"Listar tabelas do sistema");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sistema_tabela">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Tabelas Do Sistema</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

