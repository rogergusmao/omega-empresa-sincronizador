<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sincronizacao_web
    * DATA DE GERAÇÃO:    19.06.2014
    * ARQUIVO:            sincronizacao_web.php
    * TABELA MYSQL:       sincronizacao_web
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sincronizacao_web();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sincronizacao_web"=>"Adicionar nova sincronização web",
    					 "list_sincronizacao_web"=>"Listar sincronizações web");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sincronizacao_web">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Sincronizações Web</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

