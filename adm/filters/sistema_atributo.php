<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema_atributo
    * DATA DE GERA��O:    19.07.2014
    * ARQUIVO:            sistema_atributo.php
    * TABELA MYSQL:       sistema_atributo
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_atributo();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sistema_atributo"=>"Adicionar  atributo",
    					 "list_sistema_atributo"=>"Listar atributos");

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sistema_atributo">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Atributos</legend>

        <table class="tabela_form">


        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

