<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once '../recursos/php/funcoes_sem_corporacao.php';

require_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'imports/instancias.php';

try{
    //executavel.php [corporacao] [class] [action] [GET]
    if(php_sapi_name() != 'cli'){
        echo "Esse arquivo so pode ser chamando por linha de comando";
        exit();
    }
    if($argc < 4){
        echo "Chamada inválida. Ex de chamada correta: executavel.php [corporacao] [class] [action] [GET]*. Sendo GET os parametros (opcionais) separados com ','";
        exit();
    }

    $idProcesso = getmypid();

    $classe = $argv[2];


    $action = $argv[4];
    $idSistemaSihop = $argv[5];
    Helper::criaArquivoComOConteudo(
        InterfaceProcesso::PATH_LOG_PROCESSOS_EM_EXECUCAO($classe,$idSistemaSihop ),
        ";".$idProcesso,
        false);
    $obj = call_user_func_array(array($classe, "factory"), array($idSistemaSihop));
        //chama a funcao de nome "$action"
    $retorno = call_user_func(array($obj, $action), array());

} catch (Exception $ex) {
    $retorno = new Mensagem(null, null, $ex);
}
if($retorno != null){
    $log= Registry::get('HelperLog');
    $log->gravarLogEmCasoErro($retorno);

    $strRet = Helper::jsonEncode ($retorno);
    echo $strRet;
}
        
