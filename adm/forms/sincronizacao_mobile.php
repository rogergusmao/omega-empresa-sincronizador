<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: sincronizacao_mobile
    * DATA DE GERA��O:    10.07.2016
    * ARQUIVO:            sincronizacao_mobile.php
    * TABELA MYSQL:       sincronizacao_mobile
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sincronizacao_mobile();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_sincronizacao_mobile"=>"Adicionar nova sincroniza��o mobile",
    					 "list_sincronizacao_mobile"=>"Listar sincroniza��es mobile");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_sincronizacao_mobile">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar sincroniza��o mobile";

            }
            else{

            	$legend = "Cadastrar sincroniza��o mobile";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_mobile_identificador_INT;
    			$objArg->valor = $obj->getMobile_identificador_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoMobile_identificador_INT($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_mobile_conectado_INT;
    			$objArg->valor = $obj->getMobile_conectado_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoMobile_conectado_INT($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_estado_sincronizacao_mobile_id_INT;
                            $objArg->valor = $obj->getEstado_sincronizacao_mobile_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = false;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("estado_sincronizacao_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllEstado_sincronizacao_mobile($objArg); ?>
                            </td>


                            

    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_inicial_DATETIME;
    			$objArg->valor = $obj->getData_inicial_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoData_inicial_DATETIME($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_final_DATETIME;
    			$objArg->valor = $obj->getData_final_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoData_final_DATETIME($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_resetando_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getResetando_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoResetando_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_sincronizacao_id_INT;
                            $objArg->valor = $obj->getSincronizacao_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = false;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("sincronizacao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllSincronizacao($objArg); ?>
                            </td>


                            

    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_seq_INT;
    			$objArg->valor = $obj->getSeq_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoSeq_INT($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_descricao;
    			$objArg->valor = $obj->getDescricao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoDescricao($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_crud_mobile_ARQUIVO;
    			$objArg->valor = $obj->getCrud_mobile_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoCrud_mobile_ARQUIVO($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_crud_web_para_mobile_ARQUIVO;
    			$objArg->valor = $obj->getCrud_web_para_mobile_ARQUIVO();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoCrud_web_para_mobile_ARQUIVO($objArg); ?>
                            
                        </td>


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_ultimo_crud_id_INT;
                            $objArg->valor = $obj->getUltimo_crud_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = false;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("ultimo_crud_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllUltimo_crud($objArg); ?>
                            </td>


                            			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_ultima_verificacao_DATETIME;
    			$objArg->valor = $obj->getUltima_verificacao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoUltima_verificacao_DATETIME($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_erro;
    			$objArg->valor = $obj->getErro();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoErro($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_erro_sincronizacao_id_INT;
                            $objArg->valor = $obj->getErro_sincronizacao_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = false;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("erro_sincronizacao_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllErro_sincronizacao($objArg); ?>
                            </td>


                            

    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_primeira_vez_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getPrimeira_vez_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoPrimeira_vez_BOOLEAN($objArg); ?></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

