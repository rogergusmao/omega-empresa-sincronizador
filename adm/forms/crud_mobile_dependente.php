<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: crud_mobile_dependente
    * DATA DE GERA��O:    23.06.2014
    * ARQUIVO:            crud_mobile_dependente.php
    * TABELA MYSQL:       crud_mobile_dependente
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Crud_mobile_dependente();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_crud_mobile_dependente"=>"Adicionar novo crud dependente mobile",
    					 "list_crud_mobile_dependente"=>"Listar crud dependente mobile");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_crud_mobile_dependente">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar crud dependente mobile";

            }
            else{

            	$legend = "Cadastrar crud dependente mobile";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_crud_mobile_id_INT;
    			$objArg->valor = $obj->getCrud_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("crud_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllCrud_mobile($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_id_sincronizador_mobile_INT;
    			$objArg->valor = $obj->getId_sincronizador_mobile_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_sincronizador_mobile_INT($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_atributo;
    			$objArg->valor = $obj->getAtributo();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAtributo($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

