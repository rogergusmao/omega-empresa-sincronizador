<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: sistema_tabela
    * DATA DE GERA��O:    23.06.2014
    * ARQUIVO:            sistema_tabela.php
    * TABELA MYSQL:       sistema_tabela
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_tabela();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_sistema_tabela"=>"Adicionar nova tabela do sistema",
    					 "list_sistema_tabela"=>"Listar tabelas do sistema");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_sistema_tabela">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar tabela do sistema";

            }
            else{

            	$legend = "Cadastrar tabela do sistema";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_modificacao_estrutura_DATETIME;
    			$objArg->valor = $obj->getData_modificacao_estrutura_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_modificacao_estrutura_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_frequencia_sincronizador_INT;
    			$objArg->valor = $obj->getFrequencia_sincronizador_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoFrequencia_sincronizador_INT($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_banco_mobile_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getBanco_mobile_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoBanco_mobile_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_banco_web_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getBanco_web_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoBanco_web_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_transmissao_web_para_mobile_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getTransmissao_web_para_mobile_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTransmissao_web_para_mobile_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_transmissao_mobile_para_web_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getTransmissao_mobile_para_web_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTransmissao_mobile_para_web_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tabela_sistema_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getTabela_sistema_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTabela_sistema_BOOLEAN($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_excluida_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getExcluida_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoExcluida_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_atributos_chave_unica;
    			$objArg->valor = $obj->getAtributos_chave_unica();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAtributos_chave_unica($objArg); ?></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

