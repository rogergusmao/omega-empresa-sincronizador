<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: crud_mobile
    * DATA DE GERA��O:    12.07.2014
    * ARQUIVO:            crud_mobile.php
    * TABELA MYSQL:       crud_mobile
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Crud_mobile();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_crud_mobile"=>"Adicionar novo crud mobile",
    					 "list_crud_mobile"=>"Listar crud mobile");

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_crud_mobile">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = "Atualizar crud mobile";

            }
            else{

            	$legend = "Cadastrar crud mobile";

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sistema_tabela_id_INT;
    			$objArg->valor = $obj->getSistema_tabela_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sistema_tabela_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema_tabela($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_id_tabela_mobile_INT;
    			$objArg->valor = $obj->getId_tabela_mobile_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_tabela_mobile_INT($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_id_tabela_web_INT;
    			$objArg->valor = $obj->getId_tabela_web_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_tabela_web_INT($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_id_tabela_web_duplicada_INT;
    			$objArg->valor = $obj->getId_tabela_web_duplicada_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_tabela_web_duplicada_INT($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_id_sincronizador_mobile_INT;
    			$objArg->valor = $obj->getId_sincronizador_mobile_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoId_sincronizador_mobile_INT($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_tipo_operacao_banco_id_INT;
    			$objArg->valor = $obj->getTipo_operacao_banco_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("tipo_operacao_banco_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllTipo_operacao_banco($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_sincronizacao_mobile_id_INT;
    			$objArg->valor = $obj->getSincronizacao_mobile_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("sincronizacao_mobile_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSincronizacao_mobile($objArg); ?>
    			</td>


    			

    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_crud_id_INT;
    			$objArg->valor = $obj->getCrud_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			$obj->addInfoCampos("crud_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllCrud($objArg); ?>
    			</td>


    						</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_processada_BOOLEAN;
    			$objArg->labelTrue = "Sim";
    			$objArg->labelFalse = "N�o";
    			$objArg->valor = $obj->getProcessada_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 80;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoProcessada_BOOLEAN($objArg); ?></td>


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_data_processamento_DATETIME;
    			$objArg->valor = $obj->getData_processamento_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_processamento_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_erro_sql_INT;
    			$objArg->valor = $obj->getErro_sql_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoErro_sql_INT($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>

