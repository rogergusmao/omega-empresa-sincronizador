<?php

class EXTDAO_Sistema_sequencia {

    private function __construct(){


    }

    private static $objBanco = null;
    private static $lockHandler = null;

    private static $IS_UTILIZANDO_LOCK = true;
    public static $VALOR_INVALIDO = -1;


    private static function __getSemaphorePath($nomeTabela){

        $path = ConfiguracaoSite::getPathConteudo().self::getLockName($nomeTabela);
        return $path;

    }



    private static function semaphoreGet($nomeTabela){

        $inicio = microtime();
        $maxIterations = 10;

        if(!self::$IS_UTILIZANDO_LOCK)
            return true;

        if(!self::$IS_UTILIZANDO_APC) {

            if (!is_resource(self::$lockHandler[$nomeTabela]))
                self::$lockHandler[$nomeTabela] = fopen(
                    self::__getSemaphorePath($nomeTabela),
                    'w');
        }
        else{

            $maxIterations = 50;

        }

        for($i=1; $i <= $maxIterations; $i++){

            if(self::$IS_UTILIZANDO_APC){

                $isLocked = apc_exists(self::getLockName($nomeTabela));
                $successLock = !$isLocked;

                if(!$isLocked){

                    $adicionadoComSucesso = apc_add(
                        self::getLockName($nomeTabela),
                        "L",
                        10);

                    //se n�o foi adicionado com sucesso, � pq outra requisi��o j� adicionou antes
                    if (!$adicionadoComSucesso) {

                        $successLock = false;

                    }

                }

            }
            else {

                $successLock = flock(self::$lockHandler[$nomeTabela], LOCK_EX);

            }

            if($successLock) {

                $termino = microtime();
                self::$tempoTotalSemaforo += (($termino - $inicio)*1000);

                return true;

            }

            usleep($i*10000);

        }

        $termino = microtime();
        self::$tempoTotalSemaforo += (($termino - $inicio)*1000);
        return false;

    }

    private static function getLockName($nomeTabela){
        return "lc_".str_replace('.', '_',self::$objBanco->getHost()) ."_".self::$objBanco->getDBName()."_$nomeTabela";
    }
    private static function semaphoreRelease($nomeTabela){

        if(self::$IS_UTILIZANDO_APC){

            apc_delete(self::getLockName($nomeTabela));

        }
        else {

            if (is_resource(self::$lockHandler[$nomeTabela])) {
                flock(self::$lockHandler[$nomeTabela], LOCK_UN);
            }
        }

    }
    public static function gerarId($nomeTabela, $configuracaoDatabase, $totGerar = 1){

        return self::getInstance()->gerarIdInterna($nomeTabela, $configuracaoDatabase, $totGerar);

    }

    public function gerarIdInterna($nomeTabela, $configuracaoDatabase, $totGerar = 1){

        $nomeTabela = strtolower($nomeTabela);

        if(self::$objBanco == null){
            $configuracaoDatabase->novaConexao = true;
            //inicia objeto criando nova conex�o (link) com o banco de dados
            //ir� persistir durante toda a requisi��o
            self::$objBanco = new Database($configuracaoDatabase);

        }

        if(self::semaphoreGet($nomeTabela)) {

            self::$objBanco->iniciarTransacao();
            $msg = self::$objBanco->queryMensagem("UPDATE sistema_sequencia ss1 SET ss1.id_ultimo_INT=ss1.id_ultimo_INT+$totGerar WHERE ss1.tabela='{$nomeTabela}'");

            if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL
                    || $msg->mValor === false)){
                self::$objBanco->rollbackTransacao();
                throw new SequenciaException("Falha ao realizar update da sequ�ncia.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);

            }
            else {

                try {

                    $msg = self::$objBanco->queryMensagem("SELECT id_ultimo_INT FROM sistema_sequencia WHERE tabela='{$nomeTabela}'");

                    if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){
                        self::$objBanco->rollbackTransacao();
                        throw new SequenciaException("Falha ao realizar select na sequ�ncia.", SequenciaException::THROW_FALHA_SELECT_SEQUENCIA);
                    }
                    elseif(self::$objBanco->rows() > 0){

                        $ultimoId = self::$objBanco->getPrimeiraTuplaDoResultSet(0);
                        self::$objBanco->commitTransacao();
                    }
                    else{

                        try {

                            //ultimo id na tabela pesquisada
                            $ultimoId = 1;
                            $msg = self::$objBanco->queryMensagem("SELECT MAX(id) FROM {$nomeTabela}");

                            if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){

                                throw new SequenciaException("Falha ao obter MAX id da tabela {$nomeTabela}.", SequenciaException::THROW_FALHA_SELECT_TABELA);

                            }

                            $ultimoIdNaTabela = self::$objBanco->getPrimeiraTuplaDoResultSet(0);

                            if ($ultimoIdNaTabela)
                                $ultimoId = $ultimoIdNaTabela + $totGerar;



                            $msg = self::$objBanco->queryMensagem("INSERT INTO sistema_sequencia(tabela, id_ultimo_INT) VALUES('{$nomeTabela}', {$ultimoId})");

                            self::$objBanco->commitTransacao();

                            if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){

                                throw new SequenciaException("Falha ao realizar insert na sequ�ncia.", SequenciaException::THROW_FALHA_INSERT_SEQUENCIA);

                            }


                            //libera o lock
                            if (self::$IS_UTILIZANDO_LOCK)
                                self::semaphoreRelease($nomeTabela);

                        } catch (Exception $ex) {
                            self::$objBanco->commitTransacao();

                            if (self::$IS_UTILIZANDO_LOCK)
                                self::semaphoreRelease($nomeTabela);

                            throw $ex;
                        }
                    }

//                    self::$objBanco->commitTransacao();

                    //libera o lock
                    if (self::$IS_UTILIZANDO_LOCK)
                        self::semaphoreRelease($nomeTabela);

                } catch (Exception $ex) {

                    if (self::$IS_UTILIZANDO_LOCK)
                        self::semaphoreRelease($nomeTabela);

                    throw $ex;

                }

            }

        }
        else{

            throw new SequenciaException("Falha ao obter lock da sequ�ncia.", SequenciaException::THROW_FALHA_OBTER_LOCK);

        }

        return $ultimoId;

    }

    public static function close() {
        if(self::$objBanco != null) {

            self::$objBanco->close();
            self::$objBanco = null;
//            unset(self::$objBanco );

        }


    }

    function __destruct() {

        EXTDAO_Sistema_sequencia::close();

    }
}
