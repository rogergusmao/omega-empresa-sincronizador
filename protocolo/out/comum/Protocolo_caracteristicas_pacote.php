<?php
    class Protocolo_caracteristicas_pacote extends Interface_protocolo{
        
        public $totalUsuario;
        public $numeroUsuarioOcupado;
        public $totalEspacoMB;
        public $espacoMBOcupado;
        
        public $totalClinica;
        public $totalEmailMensal;
        public $numeroEmailMensalEnviado;
        public $emailUsuarioPrincipal;
        
        
        public static function constroi($obj){
            $protocolo = new Protocolo_caracteristicas_pacote();
            $protocolo->totalUsuario = $obj["total_usuario"];
            $protocolo->numeroUsuarioOcupado = $obj["numero_usuario_ocupado"];
            
            $protocolo->totalEspacoMB = $obj["total_espaco_mb"];
            $protocolo->espacoMBOcupado = $obj["espaco_mb_ocupado"];
            
            $protocolo->numeroEmailMensalEnviado = $obj["numero_email_mensal_enviado"];
            
            
            $protocolo->totalClinica = $obj["total_clinica"];
            $protocolo->totalEmailMensal = $obj["total_email_mensal"];
            $protocolo->emailUsuarioPrincipal = $obj["email_usuario_principal"];
            
            return $protocolo;
        }

   public function factory($objJson) {
        
        $obj = new Protocolo_caracteristicas_pacote();
        $obj->totalUsuario = $objJson->totalUsuario;
        $obj->totalEspacoMB = $objJson->totalEspacoMB;
        $obj->totalClinica = $objJson->totalClinica;
        $obj->totalEmailMensal = $objJson->totalEmailMensal;
        $obj->emailUsuarioPrincipal = $objJson->emailUsuarioPrincipal;
        $obj->numeroUsuarioOcupado = $objJson->numeroUsuarioOcupado;
        $obj->numeroEmailMensalEnviado = $objJson->numeroEmailEnviado;
        $obj->espacoMBOcupado = $objJson->espacoMBOcupado;
        return $obj;
    }
        
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
