<?php
    class Protocolo_processo_em_aberto extends Interface_protocolo{
        
        
        public $idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
        public $idSincronizacaoMobile;
        public $enviouDadosSincronizacaoMobile;
        
        
        public static function constroi($obj){
            $protocolo = new Protocolo_caracteristicas_pacote();
            $protocolo->idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = $obj["idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb"];
            $protocolo->idSincronizacaoMobile = $obj["idSincronizacaoMobile"];
            $protocolo->enviouDadosSincronizacaoMobile = $obj["enviouDadosSincronizacaoMobile"];
            
            return $protocolo;
        }

        public function factory($objJson) {

             $obj = new Protocolo_caracteristicas_pacote();
             $obj->idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = $objJson->idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
             $obj->idSincronizacaoMobile = $objJson->idSincronizacaoMobile;
             $obj->enviouDadosSincronizacaoMobile = $objJson->enviouDadosSincronizacaoMobile;
             return $obj;
         }
        
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
