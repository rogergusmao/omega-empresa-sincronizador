    
function remarcarSessao(numeroSequencial){
    
    $('#data_DATE_' + numeroSequencial).removeClass('hidden');
    $('#equipamento_id_INT_' + numeroSequencial).removeClass('hidden');
    
    $('#data_DATE_' + numeroSequencial).trigger('change');
    
}

function marcarHorariosAutomaticamente(contadorProcedimento, numeroProcedimentoAtual, opcao){

    var arrayIndicesProcedimentoAtual = new Array();
    var horarioEscolhido = $('#hora_base_automatico' + contadorProcedimento).val();
        
    if(horarioEscolhido.length > 0){

        if(numeroProcedimentoAtual > 0){

            $('td[id^="horario_"]').each(function(index, value){

                var sequenciaIndividual = $(this).attr('id').replace('horario_', '');

                var procedimentoIndividual = $('#procedimento_id_INT_'+ sequenciaIndividual).val();

                if(procedimentoIndividual == numeroProcedimentoAtual){
                    
                    arrayIndicesProcedimentoAtual.push(sequenciaIndividual);

                }

            });

           var i;
           for(i=0; i < arrayIndicesProcedimentoAtual.length; i++){

                $('#horario_' + arrayIndicesProcedimentoAtual[i]).find("td").each(function(index, value){

                    if(!$(this).hasClass('horario_nd') && $(this).text() == horarioEscolhido){

                        $(this).addClass('horario_selecionado');
                        $(this).trigger('click');

                    }
                    else if(!$(this).hasClass('horario_nd')){

                        $(this).addClass('horario_d');

                    }

                });

           }

        }
    
    }
    
}

function marcarDatasAutomaticamente(contadorProcedimento, numeroProcedimentoAtual, opcao){
    
    var arrayIndicesProcedimentoAtual = new Array();
    
    var dataBase = $('#data_base_automatico' + contadorProcedimento).val();
    var dataBaseSQL = false;
    
    if(dataBase.length > 0){
        
        dataBaseSQL = transformarDataParaSQL(dataBase);
        
    }
    
    $('select[id^="data_DATE_"]').each(function(index, value){
        
        if($(this).css('display') != 'none'){

            var sequenciaIndividual = $(this).attr('id').replace('data_DATE_', '');
            var procedimentoIndividual = $('#procedimento_id_INT_'+ sequenciaIndividual).val();

            if(procedimentoIndividual == numeroProcedimentoAtual){

                arrayIndicesProcedimentoAtual.push(sequenciaIndividual);

            }

        }
        
    });
    
    var indiceUltimaSelecao = 0;
    var i;
    for(i=0; i < arrayIndicesProcedimentoAtual.length; i++){

        $('#data_DATE_' + arrayIndicesProcedimentoAtual[i]).find("option").each(function(index, value){
           
            if(transformarDataParaSQL($(this).val()) >= dataBaseSQL){
                              
               if(opcao == 1){
                                      
                   if(index > indiceUltimaSelecao && 
                       ($(this).text().indexOf("Segunda") >= 0 || 
                        $(this).text().indexOf("Quarta") >= 0 ||
                         $(this).text().indexOf("Sexta") >= 0)){
                        
                        $(this).attr('selected', 'selected');
                        $(this).parent().trigger('change');
                        indiceUltimaSelecao = index;
                        return false;
                        
                    }
                    else{
                        
                         $(this).removeAttr('selected');
                        
                    }
                   
               }
               else if(opcao == 2){
                   
                   if(index > indiceUltimaSelecao && 
                       ($(this).text().indexOf("Ter�a") >= 0 || 
                        $(this).text().indexOf("Quinta") >= 0)){
                        
                        $(this).attr('selected', 'selected');
                        $(this).parent().trigger('change');
                        indiceUltimaSelecao = index;
                        return false;
                        
                    }
                    else{
                        
                         $(this).removeAttr('selected');
                        
                    }
                   
               }
               
           } 
                        
        });
           
    }
    
}
    
function escolherEquipamentos(numeroProcedimentoAtual){

    var arrayIndicesProcedimentoAtual = new Array();
    
    $('select[id^="equipamento_id_INT_"]').each(function(index, value){
        
        if($(this).css('display') != 'none'){

            var sequenciaIndividual = $(this).attr('id').replace('equipamento_id_INT_', '');
            var procedimentoIndividual = $('#procedimento_id_INT_'+ sequenciaIndividual).val();

            if(procedimentoIndividual == numeroProcedimentoAtual){

                arrayIndicesProcedimentoAtual.push(sequenciaIndividual);

            }

        }
        
    });
    
    var i;
    for(i=0; i < arrayIndicesProcedimentoAtual.length; i++){

        $('#equipamento_id_INT_' + arrayIndicesProcedimentoAtual[i]).find("option").each(function(index, value){

            if(!$(this).attr('disabled') == undefined && $(this).val() != ""){
                
                $(this).attr('selected', 'selected');
                
            }
            else{
                
                $(this).removeAttr('selected');
                
            }
            
        });
           
    }
    
}
    
function copiarDatasDeOutroProcedimento(numeroProcedimentoAnterior, numeroProcedimentoAtual){

    var arrayIndicesProcedimentoAnterior = new Array();
    var arrayIndicesProcedimentoAtual = new Array();

    if(numeroProcedimentoAnterior > 0 && numeroProcedimentoAtual > 0){
        
        $('select[id^="data_DATE_"]').each(function(index, value){
            
            var sequenciaIndividual = $(this).attr('id').replace('data_DATE_', '');
            var procedimentoIndividual = $('#procedimento_id_INT_'+ sequenciaIndividual).val();
            
            if(procedimentoIndividual == numeroProcedimentoAnterior){
                
                arrayIndicesProcedimentoAnterior.push(sequenciaIndividual);
                
            }
            else if(procedimentoIndividual == numeroProcedimentoAtual){
                
                arrayIndicesProcedimentoAtual.push(sequenciaIndividual);
                
            }
            
        });
      
       var i;
       for(i=0; i < arrayIndicesProcedimentoAnterior.length; i++){
           
           if($('#data_DATE_' + arrayIndicesProcedimentoAnterior[i]).val() != "" && i < arrayIndicesProcedimentoAtual.length){
               
               $('#data_DATE_' + arrayIndicesProcedimentoAtual[i]).val($('#data_DATE_' + arrayIndicesProcedimentoAnterior[i]).val())
               $('#data_DATE_' + arrayIndicesProcedimentoAtual[i]).trigger('change');

           }
           
           
       }

    }
    
}

function copiarHorariosDeOutroProcedimento(numeroProcedimentoAnterior, numeroProcedimentoAtual){

    var arrayIndicesProcedimentoAnterior = new Array();
    var arrayIndicesProcedimentoAtual = new Array();

    if(numeroProcedimentoAnterior > 0 && numeroProcedimentoAtual > 0){
        
        $('td[id^="horario_"]').each(function(index, value){
            
            var sequenciaIndividual = $(this).attr('id').replace('horario_', '');
            
            var procedimentoIndividual = $('#procedimento_id_INT_'+ sequenciaIndividual).val();
            
            if(procedimentoIndividual == numeroProcedimentoAnterior){
                
                arrayIndicesProcedimentoAnterior.push(sequenciaIndividual);
                
            }
            else if(procedimentoIndividual == numeroProcedimentoAtual){
                
                arrayIndicesProcedimentoAtual.push(sequenciaIndividual);
                
            }
                        
        });
      
       var i;
       for(i=0; i < arrayIndicesProcedimentoAnterior.length; i++){
                          
           if(i < arrayIndicesProcedimentoAtual.length){  
               
               var horarioAnterior = $('#hora_escolhida_' + arrayIndicesProcedimentoAnterior[i]).val();
             
                $('#horario_' + arrayIndicesProcedimentoAtual[i]).find("td").each(function(index, value){
                                                                      
                    if(!$(this).hasClass('horario_nd') && $(this).text() == horarioAnterior){
                        
                        $(this).addClass('horario_selecionado');
                        $(this).trigger('click');
                        
                    }
                    else if(!$(this).hasClass('horario_nd')){
                        
                        $(this).addClass('horario_d');
                        
                    }
                    
                });
           
           }
           
       }

    }
    
}
    
$('select[id^="data_DATE_"]').change(function(){

    var mensagemPadrao = "Escolha primeiro a data da sess�o.";
    var data = $(this).val();
    var idVendaCriptografado = $('#id1').val();
    var sequencia = parseInt($(this).attr('id').replace('data_DATE_', ''));
    var procedimento = $('#procedimento_id_INT_'+ sequencia).val();
    var indiceSelecionado = $(this).attr("selectedIndex");
    var validacao = true;      
    var intervaloMinimo = $('#intervalo_minimo_INT_'+ sequencia).val();
    var intervaloMinimoNumerico = 0;

    var diaData = 0;
    var mesData = 0;
    var anoData = 0;
            
    var dataMinima = 0;
    var dataMinimaSQL = 0;

    if(intervaloMinimo.length > 0){
                
        intervaloMinimoNumerico = parseInt(intervaloMinimo);
                
    }
                
    var partesData = data.split("/");
    diaData = parseInt(partesData[0], 10);
    mesData = parseInt(partesData[1], 10)-1;
    anoData = parseInt(partesData[2], 10);
    var objData = new Date(anoData, mesData, diaData+intervaloMinimoNumerico, 0, 0, 0, 0);

    dataMinima = FormatNumberLength(objData.getDate(), 2) + "/" + FormatNumberLength(objData.getMonth()+1, 2) + "/" + objData.getFullYear();
    dataMinimaSQL = transformarDataParaSQL(dataMinima);

    $('select[id^="data_DATE_"]').each(function(index, value){
                
        var sequenciaIndividual = $(this).attr('id').replace('data_DATE_', '');
        sequenciaIndividual = parseInt(sequenciaIndividual);
        var indiceSelecionadoIndividual = $(this).attr("selectedIndex");
        var numeroSessaoIndividual = $('#numero_sessao_' + sequenciaIndividual).val();
        var procedimentoIndividual = $('#procedimento_id_INT_'+ sequenciaIndividual).val();

        if(procedimento == procedimentoIndividual && sequenciaIndividual > sequencia){
                    
            var options = $(this).children("option");
            
            var primeiravez = 1;
                            
            options.each(function(index2, value2){
                
                if($(value2).val().length == 0){
                    
                    return;
                    
                }
                
                var dataCorrenteSQL = transformarDataParaSQL($(value2).val());
                 
                if(dataCorrenteSQL >= dataMinimaSQL){
                                        
                    if(primeiravez == 1){

                        primeiravez = 0;
                        
                    }
                                        
                    $(value2).css('display', 'block');
                    
                }
                else if($(value2).val() != ""){
                    
                    $(value2).css('display', 'none');
                    
                }
                
            });
            
            //$(this).attr('selectedIndex', 0);
                    
        }
                
        if(validacao && procedimento == procedimentoIndividual && sequencia < sequenciaIndividual && indiceSelecionado > indiceSelecionadoIndividual && indiceSelecionadoIndividual != "0"){
                    
            if(confirm("Voc� selecionou uma data para esta sess�o que � maior que a data da sess�o " + numeroSessaoIndividual + ".\nSe continuar, a data da sess�o indicada ser� apagada.\nDeseja continuar?" )){

                $(this).attr("selectedIndex", 0);
                $('#horario_' + sequenciaIndividual).html(mensagemPadrao);
                        
            }
            else{
                        
                $("#data_DATE_" + sequencia).attr("selectedIndex", 0);
                validacao = false;
                        
            }
             
        }
        else if(validacao && procedimento == procedimentoIndividual && sequencia > sequenciaIndividual && indiceSelecionado < indiceSelecionadoIndividual && indiceSelecionadoIndividual != "0"){
                    
            if(confirm("Voc� selecionou uma data para esta sess�o que � menor que a data da sess�o " + numeroSessaoIndividual + ".\nSe continuar, a data da sess�o indicada ser� apagada.\nDeseja continuar?" )){

                $(this).attr("selectedIndex", 0);
                $('#horario_' + sequenciaIndividual).html(mensagemPadrao);
                        
            }
            else{
                        
                $("#data_DATE_" + sequencia).attr("selectedIndex", 0);
                validacao = false;
                        
            }
                    
        }
                
                
    });
            
    if(validacao){

        if(data.length == 10){

            $.ajax({
                url: "ajax.php?ajax_tipo=ajax_forms&ajax_page=venda_procedimento_horario&data=" + data + "&procedimento=" + procedimento + "&i=" + sequencia + "&venda=" + idVendaCriptografado,
                success: function(data){

                    $('#horario_' + sequencia).html(data);
                    eventoDeMarcarHorario();

                }
            }); 

    }
    else{

        $('#horario_' + sequencia).html(mensagemPadrao);

    }
            
}

});  

function transformarDataParaSQL(dataNormal){
    
    var partes = dataNormal.split("/");
    
    if(partes.length == 3){
    
        return "" + partes[2] + "-" + partes[1] + "-" + partes[0] + "";
    
    }
    else{
        
        return "";
        
    }
        
}
        
function eventoDeMarcarHorario(){

    $('td.horario_d').click(function(){
                
        $(this).siblings().removeClass('horario_selecionado');
        $(this).parent().siblings().children().removeClass('horario_selecionado');

        var numeroSequencia = $(this).parents('td').attr('id').replace('horario_', '');

        $(this).addClass('horario_selecionado');

        var equipamentosDisponiveis = $(this).attr('equipamentos');
        var arrEquipamentos = equipamentosDisponiveis.split(",");

        $('#hora_escolhida_' + numeroSequencia).val($(this).html());   
                
        $('#equipamento_id_INT_' + numeroSequencia).children().attr('disabled', 'disabled');
        $('#equipamento_id_INT_' + numeroSequencia).attr('selectedIndex', 0);
                
        $('#equipamento_id_INT_' + numeroSequencia).children().each(function(index, value){
                    
            if(arrEquipamentos.indexOf($(this).val()) > -1){
                        
                $(this).attr('disabled', false);
                        
            }
                    
        });
                
    });
    
}
        
function validarMarcacaoSessoes(formulario){
            
    var retorno = true;
    var numeroDeElementos = $('select[id^="data_DATE_"][value!=""]').size();
            
    $('select[id^="data_DATE_"][value!=""]').each(function(index, value){
                                
        var numeroSequencia = $(this).attr('id').replace('data_DATE_', '');

        if($('#hora_escolhida_' + numeroSequencia).val() == undefined){

            retorno = false;
            return;

        }
        else if($('#hora_escolhida_' + numeroSequencia).val().length != 5){

            retorno = false;
            return;

        }
                                
    });
            
    if(retorno){
                
        $('select[id^="data_DATE_"][value!=""]').each(function(index, value){
               
            var numeroSequencia = $(this).attr('id').replace('data_DATE_', '');

            if($('#equipamento_id_INT_' + numeroSequencia).val() == undefined){

                retorno = false;
                return;

            }
            else if($('#equipamento_id_INT_' + numeroSequencia).val() == ""){

                retorno = false;
                return;

            }                    

        });
                
        if(retorno){
                
            return validarCampos(formulario);
                
        }
        else{
                    
            imprimirMensagemErro("Voc� deve selecionar o equipamento de todas as " + numeroDeElementos + " sess�es");
            return false; 
                    
        }
                
    }
    else{
                
        imprimirMensagemErro("Voc� deve selecionar o hor�rio de todas as " + numeroDeElementos + " sess�es");
        return false;
                
    }
            
}
        
function FormatNumberLength(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}
