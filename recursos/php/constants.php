<?php
define('MODO_BANCO_COM_CORPORACAO', true);
define('BANCO_COM_TRATAMENTO_EXCLUSAO', false);
define('PATH_RELATIVO_PROJETO', 'adm/');
define('IDENTIFICADOR_SISTEMA', 'SCR_');
define('TITULO_PAGINAS_CLIENTE', "WorkOffline Synchronizer");

define('LIMITE_CELULARES_POR_SINCRONIZACAO', 5);

define('PAGINA_INICIAL_PADRAO',"paginas/assinaturas_do_cliente.php");
define('PROJETO', 'SIW');

define('ID_SISTEMA_EMPRESA', 1);
define('ID_SISTEMA_EMPRESA_CORPORACAO', 3);


//id do registro da tabela empresa_hosting@hospedagem relativo ao servidor do sincronizador web  em questão
define ('ID_EMPRESA_HOSTING', 1);



if($_SERVER != null 
    && isset($_SERVER["HTTP_HOST"]) 
    && array_key_exists($_SERVER["HTTP_HOST"], $_SERVER)
){

    if( substr_count($_SERVER["HTTP_HOST"],  "workoffline.com.br") >= 1){
        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "database1.workoffline.com.br");
        define('REDIS_PORT', 7333);

        define('DOMINIO_DE_ACESSO', "http://sync.workoffline.com.br/");
        define('DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hosting.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');
        define('PATH_CURL_CMD', '');

    } else {
        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "127.0.0.1");
        define('REDIS_PORT', 6379);
        if( substr_count($_SERVER["HTTP_HOST"],  "sincronizador.empresa.omegasoftware.com.br") >= 1){

            define('DOMINIO_DE_ACESSO', "http://sincronizador.empresa.omegasoftware.com.br");
            define('DOMINIO_DE_ACESSO_SICOB', "http://empresa.omegasoftware.com.br/site/");
            define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://empresa.omegasoftware.com.br/site/adm/webservice.php?class=Servicos_web&action=");
            define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/webservice.php?class=Servicos_web&action=");

            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');
            define('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');


        }
        elseif(strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp/www/") && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br"){

            define('DOMINIO_DE_ACESSO', "http://127.0.0.1/OmegaEmpresa/PontoEletronico/public_html/");
            define('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/client_area/");
            define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/adm/webservice.php?class=Servicos_web&action=");
            define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/hospedagem/adm/webservice.php?class=Servicos_web&action=");

            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/Trunk/');
            define('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');

        }
    }

} else{

    $nomeMaquina = strtoupper(php_uname("n"));
    if ($nomeMaquina == "LI1047-176.MEMBERS.LINODE.COM")
    {
        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "database1.workoffline.com.br");
        define('REDIS_PORT', 7333);

        define('DOMINIO_DE_ACESSO', "http://sync.workoffline.com.br/");
        define('DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hosting.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');
        define('PATH_CURL_CMD', '');
    }
    else
    {
        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "127.0.0.1");
        define('REDIS_PORT', 6379);

        define('DOMINIO_DE_ACESSO', "http://127.0.0.1/SincronizadorWeb/SW10002Corporacao/");
        define('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/client_area/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/webservice.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
        define('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
    }
}

define('ENDERECO_DE_ACESSO', "http://" + DOMINIO_DE_ACESSO + "/");
define('ENDERECO_DE_ACESSO_SSL', "https://" + DOMINIO_DE_ACESSO + "/");
