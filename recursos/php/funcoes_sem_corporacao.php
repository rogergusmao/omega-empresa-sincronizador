<?php


include_once acharRaizWorkspace(). DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/Funcoes.php';

$singletonFuncoes = Funcoes::getSingleton();

$raizWorkspace = Helper::acharRaizWorkspace();

$raiz = Helper::acharRaiz();

$classe = Helper::POSTGET("class");
$singletonFuncoes->setDiretorios(array(
    array($classe, Helper::acharRaiz()."recursos/classes/", array("class/", "EXTDAO/", "DAO/", "BO/")),
    array($classe, Helper::acharRaiz()."recursos/", array("php_corporacao/")),
    array($classe, Helper::acharRaiz()."recursos/classes/BO/", array("entidade/", "util/")),
    array($classe, Helper::acharRaiz()."web_service/", array('/')),
    array($classe, Helper::acharRaiz()."pipeline_sync/", array('/')),
    array($classe, Helper::acharRaiz()."protocolo/", array("in/", "out/")),
    array($classe, Helper::acharRaiz()."protocolo/out/", array("comum/", "entidade/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS, array("classes/", "php/", "adm_flatty/", "adm_padrao/", "imports/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."classes/", array( "protocolo/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_flatty/", array("imports/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_padrao/", array("imports/"))   
));

