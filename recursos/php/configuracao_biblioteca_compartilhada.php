<?php

class SingletonRaizWorkspace{
    public static $raizWorkspace = null;
}

function acharRaizWorkspace(){
    if(SingletonRaizWorkspace::$raizWorkspace != null)
        return SingletonRaizWorkspace::$raizWorkspace ;
    $pathDir = '';
    $niveis = 0;

    if(php_sapi_name() != 'cli') {

        $pathDir = $_SERVER["SCRIPT_FILENAME"];
        $niveis = substr_count($pathDir,"/");
    }
    else {

        $pathDir = getcwd();
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
            $niveis = substr_count($pathDir,"\\");
        } else {
            $niveis = substr_count($pathDir,"/");
        }

    }


    $root = "";

    for($i = 0; $i < $niveis; $i++)
    {
        if(is_dir("{$root}__workspaceRoot")) {

            SingletonRaizWorkspace::$raizWorkspace  = $root;
            return SingletonRaizWorkspace::$raizWorkspace ;

        }
        else
            $root .= "../";
    }
    SingletonRaizWorkspace::$raizWorkspace = "";
    return SingletonRaizWorkspace::$raizWorkspace ;
}

require_once '../recursos/php/constants.php';
$workspaceRaiz = acharRaizWorkspace();
include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/constants.php';
require_once $workspaceRaiz. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'php/configuracao_corporacao_dinamica.php';
