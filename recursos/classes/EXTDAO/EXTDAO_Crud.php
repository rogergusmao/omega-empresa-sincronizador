<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Crud
* NOME DA CLASSE DAO: DAO_Crud
* DATA DE GERA��O:    19.06.2014
* ARQUIVO:            EXTDAO_Crud.php5
* TABELA MYSQL:       crud
* BANCO DE DADOS:     sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARA��O DA CLASSE
// **********************

class EXTDAO_Crud extends DAO_Crud
{

        public function __construct($db = null, $setLabels = false){

            parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Crud";

        if($id != 0){

            $this->select($id);

        }

        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_sincronizacao_tabela_id_INT = "Tabela";
        $this->label_id_tabela_mobile_INT = "Id do Regisotr na Tabela do Celular";
        $this->label_id_tabela_web_INT = "Id do Registro na Tabela do Web";
        $this->label_tipo_operacao_banco_id_INT = "Tipo de Opera��o do Banco";
        $this->label_crud_origem_id_INT = "Origem";


    }


    public function setDiretorios(){



    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Crud();

    }


    public static function getTotalCrudsDaSincronizacao(
        $idCorporacao, 
        $idSincronizacao, 
        $db = null){

        if($db == null)
            $db = new Database();
        $q = "SELECT COUNT(c.id)
             FROM crud c
             WHERE c.sincronizacao_id_INT = $idSincronizacao
                AND c.corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        $total = $db->getPrimeiraTuplaDoResultSet(0);


        return !is_numeric($total) ? 0 : $total;
    }

    public static function getContainerOperacaoBanco(
        $idCorporacao,
        $idTipoOperacaoBanco,
        $idSistemaTabela,
        $idCrudInicialIncluso,
        $idCrudFinalIncluso,
        $db = null){

        if($db == null)
            $db = new Database();
        $q = "SELECT c.id idCrud,
                 cm.id idCrudMobile, 
                 c.id_tabela_web_INT idTabelaWeb,
                 cm.id_tabela_mobile_INT idTabelaMobile,
                 c.crud_origem_id_INT crudOrigem
             FROM crud c
                LEFT JOIN crud_mobile cm 
                    ON c.crud_origem_id_INT = '".EXTDAO_Crud_origem::MOBILE."' 
                        AND cm.crud_id_INT = c.id
             WHERE c.sistema_tabela_id_INT = $idSistemaTabela
                AND c.tipo_operacao_banco_id_INT = '".$idTipoOperacaoBanco."' 
                AND c.id >= {$idCrudInicialIncluso}
                AND c.id <= {$idCrudFinalIncluso} 
                AND c.corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        $container = Helper::getResultSetToMatriz($db->result,1, 0);

        return $container;
    }

    
    public static function insertCrud($idCorporacao, $idSistemaTabela, 
        $idTabelaWeb, $idTipoOperacaoBanco, $idCrudOrigem, $idSincronizacao, 
        $idSistemaRegistroSincronizador = null, $db = null){
        if($idSistemaRegistroSincronizador == null)
            $idSistemaRegistroSincronizador = "null";
        $query = "INSERT INTO crud ( sistema_tabela_id_INT,id_tabela_web_INT,tipo_operacao_banco_id_INT,crud_origem_id_INT,sincronizacao_id_INT,id_sistema_registro_sincronizador_INT, corporacao_id_INT ) "
            . "VALUES ( $idSistemaTabela, $idTabelaWeb, $idTipoOperacaoBanco, $idCrudOrigem, $idSincronizacao,$idSistemaRegistroSincronizador, $idCorporacao ) ";
        if($db == null) $db = new Database();
        $msg = $db->queryMensagemThrowException($query);
        
        return $msg;
        
    }
    
    
    public static function insertCruds($idCorporacao, 
        $idSistemaTabela, $idsTabelaWeb,
        $idTipoOperacaoBanco, $idCrudOrigem, $idSincronizacao, 
        $idsSistemaRegistroSincronizador = null, $db = null  ){
        
        if($idsTabelaWeb == null || empty($idsTabelaWeb)){
            return false;
        }
        
        $query = "INSERT INTO crud ( sistema_tabela_id_INT,id_tabela_web_INT,"
            . "tipo_operacao_banco_id_INT,crud_origem_id_INT,sincronizacao_id_INT,"
            . "id_sistema_registro_sincronizador_INT, corporacao_id_INT ) "
            . " VALUES  ";
        
        
        if($idsSistemaRegistroSincronizador == null){
            for($i = 0 ; $i < count($idsTabelaWeb); $i++){
                if($i > 0 ) $query .= ", ";
                $query .= "( $idSistemaTabela, {$idsTabelaWeb[$i]}, "
                . "$idTipoOperacaoBanco, $idCrudOrigem, $idSincronizacao,"
                    . "null, $idCorporacao )";
            }    
        } else {
            for($i = 0 ; $i < count($idsTabelaWeb); $i++){
                if($i > 0 ) $query .= ", ";
                $query .= "( $idSistemaTabela, {$idsTabelaWeb[$i]}, "
                . "$idTipoOperacaoBanco, $idCrudOrigem, $idSincronizacao,"
                    . "$idsSistemaRegistroSincronizador[$i], $idCorporacao )";
            }
        }
        
        if($db == null) $db = new Database();
        $msg = $db->queryMensagemThrowException($query);
        
        return $msg;
        
    }

}

    