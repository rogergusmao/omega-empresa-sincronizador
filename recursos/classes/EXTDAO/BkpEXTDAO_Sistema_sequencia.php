<?php


class BkpEXTDAO_Sistema_sequencia {


    private $objBanco = null;
    private $lockHandler = null;
    public $tempoTotalSemaforo = 0;

    private static $IS_UTILIZANDO_LOCK = true;
    private static $IS_UTILIZANDO_APC = true;

    const VALOR_INVALIDO = -1;

    public function __construct($configuracaoDatabase=null){

        if(self::$IS_UTILIZANDO_APC
            && (!function_exists("apc_add")
                || strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'))
            self::$IS_UTILIZANDO_APC = false;

        if(!self::$IS_UTILIZANDO_APC){

            //register_shutdown_function('unlockFiles');

        }

        if($configuracaoDatabase == null){

            //inicia objeto criando nova conex�o (link) com o banco de dados
            //ir� persistir durante toodo o tempo da request
            $this->objBanco = new Database(
                false,
                false,
                false,
                false,
                false,
                true);
        } else {
            $this->objBanco = new Database(
                $configuracaoDatabase,
                false,
                false,
                false,
                false,
                true);

        }

    }



    private function __getSemaphorePath($nomeTabela){

        $path = ConfiguracaoSite::getPathConteudo().$this->getLockName($nomeTabela);
        return $path;

    }

    private function semaphoreGet($nomeTabela){

        $inicio = microtime();
        $maxIterations = 10;

        if(!self::$IS_UTILIZANDO_LOCK)
            return true;

        if(!self::$IS_UTILIZANDO_APC) {

            if (!isset($this->lockHandler[$nomeTabela]) || !is_resource($this->lockHandler[$nomeTabela]))
                $this->lockHandler[$nomeTabela] = fopen(
                    self::__getSemaphorePath($nomeTabela),
                    'w');

        }
        else{

            $maxIterations = 50;

        }

        for($i=1; $i <= $maxIterations; $i++){

            if(self::$IS_UTILIZANDO_APC){

                $isLocked = apc_exists($this->getLockName($nomeTabela));
                $successLock = !$isLocked;

                if(!$isLocked){

                    $adicionadoComSucesso = apc_add(
                        $this->getLockName($nomeTabela),
                        "L",
                        10);

                    //se n�o foi adicionado com sucesso, � pq outra requisi��o j� adicionou antes
                    if (!$adicionadoComSucesso) {

                        $successLock = false;

                    }

                }

            }
            else {

                $successLock = flock($this->lockHandler[$nomeTabela], LOCK_EX);

            }

            if($successLock) {

                $termino = microtime();
                $this->tempoTotalSemaforo += (($termino - $inicio)*1000);

                return true;

            }

            usleep($i*10000);

        }

        $termino = microtime();
        $this->tempoTotalSemaforo += (($termino - $inicio)*1000);
        return false;

    }

    private function getLockName($nomeTabela){
        return "lc_".str_replace('.', '_',$this->objBanco->getHost()) ."_".$this->objBanco->getDBName()."_{$nomeTabela}";
    }

    private function semaphoreRelease($nomeTabela){

        if(self::$IS_UTILIZANDO_APC){

            apc_delete($this->getLockName($nomeTabela));

        }
        else {

            if (is_resource($this->lockHandler[$nomeTabela])) {
                flock($this->lockHandler[$nomeTabela], LOCK_UN);
            }
        }

    }

//    public static function gerarId($nomeTabela, $totGerar = 1){
//
//        return self::getInstance()->gerarIdInterna($nomeTabela, $totGerar);
//
//    }

    public function gerarIdInterna($nomeTabela, $totGerar = 1){

        $nomeTabela = strtolower($nomeTabela);


        if($this->semaphoreGet($nomeTabela)) {

//             CREATE FUNCTION nextval (ptabela VARCHAR(128), totalRegs int(11) ) returns int(11)
//              begin
//              declare id int(11);
//              UPDATE sistema_sequencia SET id_ultimo_INT = LAST_INSERT_ID(id_ultimo_INT + totalRegs) WHERE tabela = ptabela;
//              SELECT LAST_INSERT_ID() into id;
//              return id;
//              end

            //The SELECT statement merely retrieves the identifier information (specific to the current connection).
            // It does not access any table.

            // select nextval('usuario_tipo');


            $this->objBanco->iniciarTransacao();
            $msg = $this->objBanco->queryMensagem("UPDATE sistema_sequencia ss1 SET ss1.id_ultimo_INT=ss1.id_ultimo_INT+$totGerar WHERE ss1.tabela='{$nomeTabela}'");

            if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){
                $this->objBanco->rollbackTransacao();
                throw new SequenciaException("Falha ao realizar update da sequ�ncia.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);

            }
            else {

                try {

                    $msg = $this->objBanco->queryMensagem("SELECT id_ultimo_INT FROM sistema_sequencia WHERE tabela='{$nomeTabela}'");

                    if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){
                        $this->objBanco->rollbackTransacao();
                        throw new SequenciaException("Falha ao realizar select na sequ�ncia.", SequenciaException::THROW_FALHA_SELECT_SEQUENCIA);
                    }
                    elseif($this->objBanco->rows() > 0){

                        $ultimoId = $this->objBanco->getPrimeiraTuplaDoResultSet(0);
                        $this->objBanco->commitTransacao();
                    }
                    else{

                        try {

                            //ultimo id na tabela pesquisada
                            $ultimoId = 1;
                            $msg = $this->objBanco->queryMensagem("SELECT MAX(id) FROM {$nomeTabela}");

                            if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){

                                throw new SequenciaException("Falha ao obter MAX id da tabela {$nomeTabela}.", SequenciaException::THROW_FALHA_SELECT_TABELA);

                            }

                            $ultimoIdNaTabela = $this->objBanco->getPrimeiraTuplaDoResultSet(0);

                            if ($ultimoIdNaTabela)
                                $ultimoId = $ultimoIdNaTabela + $totGerar;



                            $msg = $this->objBanco->queryMensagem("INSERT INTO sistema_sequencia(tabela, id_ultimo_INT) VALUES('{$nomeTabela}', {$ultimoId})");

                            $this->objBanco->commitTransacao();

                            if($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false)){

                                throw new SequenciaException("Falha ao realizar insert na sequ�ncia.", SequenciaException::THROW_FALHA_INSERT_SEQUENCIA);

                            }


                            //libera o lock
                            if (self::$IS_UTILIZANDO_LOCK)
                                $this->semaphoreRelease($nomeTabela);

                        } catch (Exception $ex) {
                            $this->objBanco->commitTransacao();

                            if (self::$IS_UTILIZANDO_LOCK)
                                $this->semaphoreRelease($nomeTabela);

                            throw $ex;
                        }
                    }

//                    $this->objBanco->commitTransacao();

                    //libera o lock
                    if (self::$IS_UTILIZANDO_LOCK)
                        $this->semaphoreRelease($nomeTabela);

                } catch (Exception $ex) {

                    if (self::$IS_UTILIZANDO_LOCK)
                        $this->semaphoreRelease($nomeTabela);

                    throw $ex;

                }

            }

        }
        else{

            throw new SequenciaException("Falha ao obter lock da sequ�ncia.", SequenciaException::THROW_FALHA_OBTER_LOCK);

        }

        return $ultimoId;

    }

    public function close() {

        $this->unlockFiles();

        if($this->objBanco != null) {

            $this->objBanco->close();
            $this->objBanco = null;
//            unset($this->objBanco );

        }

    }

    public function unlockFiles(){

        if(is_array($this->lockHandler)){

            foreach($this->lockHandler as $nomeTabela => $fileHandler){

                if (is_resource($this->lockHandler[$nomeTabela])) {

                    flock($this->lockHandler[$nomeTabela], LOCK_UN);
                    fclose($this->lockHandler[$nomeTabela]);

                }

            }
            unset($this->lockHandler);
            $this->lockHandler = array();
        }

    }


}

