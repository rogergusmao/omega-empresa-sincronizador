<?php


class EXTDAO_Sistema_sequencia {


    private $objBanco = null;


    const VALOR_INVALIDO = -1;

    public function __construct($configuracaoDatabase=null){

        if($configuracaoDatabase == null){

            //inicia objeto criando nova conex�o (link) com o banco de dados
            //ir� persistir durante toodo o tempo da request
            $this->objBanco = new Database(
                false,
                false,
                false,
                false,
                false,
                true);
        } else {
            $this->objBanco = new Database(
                $configuracaoDatabase,
                false,
                false,
                false,
                false,
                true);

        }

    }



//    public static function gerarId($nomeTabela, $totGerar = 1){
//
//        return self::getInstance()->gerarIdInterna($nomeTabela, $totGerar);
//
//    }

    public function bkpgerarIdInterna($nomeTabela, $totGerar = 1){

        $nomeTabela = strtolower($nomeTabela);

        $msg = $this->objBanco->queryMensagem("select nextval('{$nomeTabela}', $totGerar)");

        if($msg != null && $msg->erroBancoForaDoAr()){
            return EXTDAO_Sistema_sequencia::VALOR_INVALIDO;
        } else if($msg != null && $msg->erroComServidor()){
            throw new SequenciaException("Falha ao realizar update da sequ�ncia.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
        }
        else {
            $ultimoId = $this->objBanco->getPrimeiraTuplaDoResultSet(0);
            return $ultimoId;
        }
    }


    public function gerarIdInterna($nomeTabela, $totGerar = 1)
    {

        try{
            $msg = $this->objBanco->queryMensagem("select nextval('{$nomeTabela}', $totGerar)");

            if($msg != null && $msg->erroBancoForaDoAr()){
                return EXTDAO_Sistema_sequencia::VALOR_INVALIDO;
            }else if ($msg != null && $msg->erro())
            {
                throw new SequenciaException("Falha ao realizar update da sequ�ncia '$nomeTabela'.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
            }
            else
            {
                $ultimoId = $this->objBanco->getPrimeiraTuplaDoResultSet(0);
                if($ultimoId==null || $ultimoId == 0){
                    $idn = rand(1, 999);

                    $call = mysqli_prepare($this->objBanco->getLink(), "CALL initNextval(?, ?, @id{$idn})");

                    //Type specification chars
                    //Character	Description
                    //i	corresponding variable has type integer
                    //d	corresponding variable has type double
                    //s	corresponding variable has type string
                    //b	corresponding variable is a blob and will be sent in packets
                    mysqli_stmt_bind_param($call, 'si', $nomeTabela, $totGerar);
                    mysqli_stmt_execute($call);

                    $select = mysqli_query($this->objBanco->getLink(), "SELECT @id{$idn}");

                    //$msg = $this->objBanco->checkLinkState("[v3e5rfg65frg]");
//                    if($msg != null && $msg->erroBancoForaDoAr()){
//                        return EXTDAO_Sistema_sequencia::VALOR_INVALIDO;
//                    }
//                    else if ($msg != null && $msg->erro())
//                    {
//                        throw new SequenciaException("Falha ao realizar update da sequ�ncia '$nomeTabela'.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
//                    }

                    $result = mysqli_fetch_assoc($select);
                    $ultimoId = $result["@id{$idn}"];

                    $select->close();
                }

                return $ultimoId;
            }
        }catch(Exception $ex){
            HelperLog::logErro($ex);
            throw new SequenciaException("Falha ao realizar update da sequ�ncia.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
        }

    }



    public function close() {



        if($this->objBanco != null) {

            $this->objBanco->close();
            $this->objBanco = null;
//            unset($this->objBanco );

        }
    }
}

