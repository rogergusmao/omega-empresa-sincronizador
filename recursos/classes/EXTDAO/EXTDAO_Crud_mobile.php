<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Crud_mobile
* NOME DA CLASSE DAO: DAO_Crud_mobile
* DATA DE GERA��O:    19.06.2014
* ARQUIVO:            EXTDAO_Crud_mobile.php5
* TABELA MYSQL:       crud_mobile
* BANCO DE DADOS:     sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARA��O DA CLASSE
// **********************

class EXTDAO_Crud_mobile extends DAO_Crud_mobile
{

        public function __construct($db = null, $setLabels = false){

            parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Crud_mobile";


        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_sincronizacao_tabela_id_INT = "Tabela";
        $this->label_id_tabela_mobile_INT = "Id do Regisotr na Tabela do Celular";
        $this->label_id_tabela_web_INT = "Id do Registro na Tabela do Web";
        $this->label_tipo_operacao_banco_id_INT = "Tipo de Opera��o do Banco";
        $this->label_sincronizacao_mobile_id_INT = "Sincroniza��o Mobile";


    }

    public function setDiretorios(){



    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Crud_mobile();

    }


    public static function getTabelasDaSincronizacao(
        $idSincronizacao, $idCorporacao, $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT DISTINCT cm.sistema_tabela_id_INT "
            . " FROM sincronizacao s "
            . "     JOIN sincronizacao_mobile sm "
            . "         ON s.id = sm.sincronizacao_id_INT"
            . "     JOIN crud_mobile cm "
            . "         ON cm.sincronizacao_mobile_id_INT = sm.id"
            . " WHERE s.id = $idSincronizacao "
            . "     AND cm.corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        $vetor = Helper::getResultSetToArrayDeUmCampo($db->result);
        return $vetor;
    }

    public static function getTabelasDaSincronizacaoMobile(
        $idSincronizacaoMobile, $idCorporacao, $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT DISTINCT(cm.sistema_tabela_id_INT)"
            . " FROM crud_mobile cm "
            . " WHERE cm.sincronizacao_mobile_id_INT =  ".$idSincronizacaoMobile
            . "     AND cm.corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        $vetor = Helper::getResultSetToArrayDeUmCampo($db->result);
        return $vetor;
    }

    public static function getRemoveNaoVinculadasAoCrud(
        $idSistemaTabela, $idSincronizacao, $idCorporacao, $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT cm.id id, "
            . "     cm.id_tabela_mobile_INT id_tabela_mobile_INT, "
            . "     cm.id_tabela_web_INT id_tabela_web_INT,"
            . "     sm.id id_sincronizacao_mobile "
            . " FROM crud_mobile cm "
            . "     JOIN sincronizacao_mobile sm "
            . "         ON cm.sincronizacao_mobile_id_INT = sm.id "
            . " WHERE cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Remover."' "
            . " AND cm.crud_id_INT IS NULL"
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela"
            . " AND sm.sincronizacao_id_INT = $idSincronizacao "
            . " AND cm.corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        $container = Helper::getResultSetToMatriz($db->result,1, 0);

        return $container;
    }


    public static function getContainerRemove(
        $idSistemaTabela, $idCorporacao, $idSincronizacao, $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT cm.id id, "
            . "     cm.id_tabela_mobile_INT id_tabela_mobile_INT, "
            . "     cm.id_tabela_web_INT id_tabela_web_INT,"
            . "     sm.id id_sincronizacao_mobile "
            . " FROM crud_mobile cm "
            . "     JOIN sincronizacao_mobile sm "
            . "         ON cm.sincronizacao_mobile_id_INT = sm.id "
            . " WHERE cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Remover."' "
            . " AND cm.processada_BOOLEAN = '0'"
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela"
            . " AND sm.sincronizacao_id_INT = $idSincronizacao "
            . " AND cm.corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        $container = Helper::getResultSetToMatriz($db->result,1, 0);

        return $container;
    }

    public static function getContainerInsert(
        $idSistemaTabela,
        $idCorporacao, 
        $idSincronizacao,
        $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT cm.id id, "
            . "     cm.id_tabela_mobile_INT id_tabela_mobile_INT, "
            . "     cm.id_tabela_web_INT id_tabela_web_INT, "
            . "     sm.id id_sincronizacao_mobile, "
            . "     sm.sincronizacao_id_INT id_sincronizacao "
            . " FROM crud_mobile cm "
            . "     JOIN sincronizacao_mobile sm "
            . "         ON cm.sincronizacao_mobile_id_INT = sm.id "
            . " WHERE cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Inserir."' "
            . " AND cm.processada_BOOLEAN = '0'"
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela"
            . " AND sm.sincronizacao_id_INT = $idSincronizacao "
            . " AND cm.corporacao_id_INT = $idCorporacao "
            . " ORDER BY cm.id";

        $db->queryMensagemThrowException($q);
        $container = Helper::getResultSetToMatriz($db->result,1, 0);

        return $container;
    }
    public static function getTabelasContainerInserirEditar(
        $idSincronizacao,
        $idCorporacao, 
        $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT DISTINCT cm.sistema_tabela_id_INT "
            . " FROM crud_mobile cm "
            . "     JOIN sincronizacao_mobile sm "
            . "         ON cm.sincronizacao_mobile_id_INT = sm.id "
            . " WHERE (cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Inserir."' OR  "
            . " cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Editar."') "
            . " AND cm.processada_BOOLEAN = '0'"
            . " AND cm.corporacao_id_INT = $idCorporacao "
            . " AND sm.sincronizacao_id_INT = $idSincronizacao ";

        $db->queryMensagemThrowException($q);
        $container = Helper::getResultSetToMatriz($db->result,1, 0);

        return $container;
    }
    public static function getContainerEdit(
        $idSistemaTabela,
        $idCorporacao, 
        $idSincronizacao,
        $db = null){
        if($db == null)
            $db = new Database();
        $q = "SELECT cm.id id, "
            . "     cm.id_tabela_mobile_INT id_tabela_mobile_INT, "
            . "     cm.id_tabela_web_INT id_tabela_web_INT,"
            . "     sm.id id_sincronizacao_mobile "
            . " FROM crud_mobile cm "
            . "     JOIN sincronizacao_mobile sm "
            . "         ON cm.sincronizacao_mobile_id_INT = sm.id "
            . " WHERE cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Editar."' "
            . " AND cm.processada_BOOLEAN = '0'"
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela" 
            . " AND cm.corporacao_id_INT = $idCorporacao "
            . " AND sm.sincronizacao_id_INT = $idSincronizacao ";
        $db->queryMensagemThrowException($q);
        $container = Helper::getResultSetToMatriz($db->result,1, 0);
        return $container;
    }

    public static function getIdTabelaWebCorrelacionado(
        $idMI, $idSistemaTabelaFk, $idFk, $db, $idCorporacao){
       
        if($db == null)
            $db = new Database();
        
        
        $q2 = "select c.id_tabela_web_INT, cm.id_tabela_web_duplicada_INT
from crud_mobile cm join sistema_tabela st on cm.sistema_tabela_id_INT = st.id
	left join crud c on cm.crud_id_INT = c.id
	join sincronizacao_mobile sm on sm.id = cm.sincronizacao_mobile_id_INT
where cm.id_tabela_mobile_INT= $idFk 
        AND cm.sistema_tabela_id_INT = $idSistemaTabelaFk
         AND sm.mobile_identificador_INT = $idMI 
         AND processada_BOOLEAN = '1'  
         AND cm.corporacao_id_INT = $idCorporacao 
         AND cm.tipo_operacao_banco_id_INT = ".EXTDAO_Tipo_operacao_banco::Inserir
         ." LIMIT 0,1 ";
        
         $msg = $db->queryMensagem($q2);
         if($msg != null && $msg->erro()) return $msg;
         $rs = Helper::getResultSetToMatriz($db->result, 0,  1);
         
         if($rs != null){
            $idTabelaWeb = $rs[0][0];
            if(strlen($idTabelaWeb)){
               return $idTabelaWeb; 
            } else {
                $idTabelaWebDuplicada = $rs[0][1];
                if(strlen($idTabelaWebDuplicada))
                    return $idTabelaWebDuplicada;
            }     
         }
         return null;
    }
    public static function updateProcessada($idCorporacao, $idCrudMobile, 
        $idCrud, $erroSql,
        $processada, $db = null, $idTabelaWebDuplicada = null){
        if($db == null)
            $db = new Database();
        if($erroSql == null)
            $erroSql = "null";
        if($idCrud == null)
            $idCrud = "null";
        if($idTabelaWebDuplicada == null)
            $idTabelaWebDuplicada = "null";
        $q = "UPDATE crud_mobile "
            . " SET crud_id_INT = $idCrud, "
            . "     erro_sql_INT=$erroSql, "
            . "     processada_BOOLEAN=$processada, "
            . "     data_processamento_DATETIME='".Helper::getDiaEHoraAtualSQL()."', "
            . "     id_tabela_web_duplicada_INT = $idTabelaWebDuplicada"
            . " WHERE id = $idCrudMobile AND corporacao_id_INT = $idCorporacao";
        return $db->queryMensagem($q);
    }
    
    static $cacheIdSincronizacaoMobile = null;
    static $cacheIdCrudMobileSM = null;
    
    public static function getIdSincronizacaoMobile(
        $idCrudMobile, $idCorporacao, $db = null){
        
        if(self::$cacheIdSincronizacaoMobile != null 
            && self::$cacheIdCrudMobileSM == $idCrudMobile)
            return self::$cacheIdSincronizacaoMobile;
            
        if($db == null)
            $db = new Database();
        
        $q = "SELECT sincronizacao_mobile_id_INT "
            . " FROM crud_mobile "
            . " WHERE id= $idCrudMobile AND corporacao_id_INT = $idCorporacao";
        $db->queryMensagemThrowException($q);
        self::$cacheIdCrudMobileSM = $idCrudMobile;
        $idSM = $db->getPrimeiraTuplaDoResultSet(0);
        self::$cacheIdSincronizacaoMobile = $idSM;
        return $idSM;
    }
    
    public static function getIdMobileIdentificador($idCrudMobile, $idCorporacao,
        $db = null){
         $q = "SELECT sm.mobile_identificador_INT "
            . " FROM crud_mobile cm "
            . "     JOIN sincronizacao_mobile sm ON cm.sincronizacao_mobile_id_INT = sm.id "
            . " WHERE cm.id = $idCrudMobile AND sm.corporacao_id_INT = $idCorporacao"
            . " LIMIT 0,1 ";
             
        if($db == null)
            $db = new Database();
        $db->queryMensagemThrowException($q);

        $idMI = $db->getPrimeiraTuplaDoResultSet(0);
        return $idMI;
        
        
    }
}

    