<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Categoria
    * NOME DA CLASSE DAO: DAO_Categoria
    * DATA DE GERAÇÃO:    21.12.2013
    * ARQUIVO:            EXTDAO_Categoria.php
    * TABELA MYSQL:       categoria
    * BANCO DE DADOS:     blog_da_ale
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Categoria extends DAO_Categoria
    {

        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Categoria";

          
            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }
        
        public static function getIdCategorias(){
            $q = "SELECT id"
                    . " FROM categoria "
                    . " WHERE blog_BOOLEAN = '1'";
            $db = new Database();
            $db->query($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
            
        }
        
        public function setLabels(){

            $this->label_id = "Id";
            $this->label_cor = "Cor";
            $this->label_nome = "Nome";
            $this->label_titulo = "Título";
            $this->label_subtitulo = "Subtítulo";
            $this->label_corpo_HTML = "Corpo";
            $this->label_icone_ARQUIVO = "ícone";
            $this->label_imagem_fundo_ARQUIVO = "Imagem de Fundo";
            $this->label_icone_padrao = "Ícone padr?o";

        }

        public function setDiretorios(){

			$this->diretorio_icone_ARQUIVO = Helper::acharRaiz()."conteudo/icones/";        //caminho a partir de da raiz, com '/' no final
			$this->diretorio_imagem_fundo_ARQUIVO = Helper::acharRaiz()."conteudo/imagensfundo/";        //caminho a partir de da raiz, com '/' no final


        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Categoria();

        }

        
        
        public function __uploadIcone_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_icone_ARQUIVO;
            $labelCampo = $this->label_icone_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["icone_ARQUIVO{$numRegistro}"];
            
            $objUpload->nome = Helper::getNomeAleatorio()."_". $this->getId().".".Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
            $success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadImagem_fundo_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_imagem_fundo_ARQUIVO;
            $labelCampo = $this->label_imagem_fundo_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["imagem_fundo_ARQUIVO{$numRegistro}"];
            
            $objUpload->nome = Helper::getNomeAleatorio()."_". $this->getId().".".Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
            $success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }

        
	}

    
