<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sincronizacao_mobile
    * NOME DA CLASSE DAO: DAO_Sincronizacao_mobile
    * DATA DE GERA??O:    19.06.2014
    * ARQUIVO:            EXTDAO_Sincronizacao_mobile.php5
    * TABELA MYSQL:       sincronizacao_mobile
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA??O DA CLASSE
    // **********************

    class EXTDAO_Sincronizacao_mobile extends DAO_Sincronizacao_mobile
    {

        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);
            
            $this->nomeClasse = "EXTDAO_Sincronizacao_mobile";


            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }
        
        
//    function select($id)
//    {
//    	$sql =  "SELECT * , "
//            . " UNIX_TIMESTAMP(data_inicial_DATETIME) AS data_inicial_DATETIME_UNIX, "
//            . " UNIX_TIMESTAMP(data_final_DATETIME) AS data_final_DATETIME_UNIX, "
//            . " UNIX_TIMESTAMP(ultima_verificacao_DATETIME) AS ultima_verificacao_DATETIME_UNIX "
//            . " FROM sincronizacao_mobile "
//            . " WHERE id = $id";
//    	$msg=$this->database->queryMensagemThrowException($sql);
//        if($msg != null && $msg->erro()) return $msg;
//    	$row = $this->database->fetchObject($this->database->result);
//
//        $this->id = $row->id;
//
//        $this->mobile_identificador_INT = $row->mobile_identificador_INT;
//
//        $this->mobile_conectado_INT = $row->mobile_conectado_INT;
//
//        $this->estado_sincronizacao_mobile_id_INT = $row->estado_sincronizacao_mobile_id_INT;
//        if($this->estado_sincronizacao_mobile_id_INT && $this->objEstado_sincronizacao_mobile  != null)
//            $this->objEstado_sincronizacao_mobile->select($this->estado_sincronizacao_mobile_id_INT);
//
//        $this->data_inicial_DATETIME = $row->data_inicial_DATETIME;
//        $this->data_inicial_DATETIME_UNIX = $row->data_inicial_DATETIME_UNIX;
//
//        $this->data_final_DATETIME = $row->data_final_DATETIME;
//        $this->data_final_DATETIME_UNIX = $row->data_final_DATETIME_UNIX;
//
//        $this->resetando_BOOLEAN = $row->resetando_BOOLEAN;
//
//        $this->sincronizacao_id_INT = $row->sincronizacao_id_INT;
//        if($this->sincronizacao_id_INT && $this->objSincronizacao != null)
//            $this->objSincronizacao->select($this->sincronizacao_id_INT);
//
//        $this->seq_INT = $row->seq_INT;
//
//        $this->descricao = $row->descricao;
//
//        $this->crud_mobile_ARQUIVO = $row->crud_mobile_ARQUIVO;
//
//        $this->crud_web_para_mobile_ARQUIVO = $row->crud_web_para_mobile_ARQUIVO;
//
//        $this->ultimo_crud_id_INT = $row->ultimo_crud_id_INT;
//        if($this->ultimo_crud_id_INT && $this->objUltimo_crud != null)
//            $this->objUltimo_crud->select($this->ultimo_crud_id_INT);
//
//        $this->ultima_verificacao_DATETIME = $row->ultima_verificacao_DATETIME;
//        $this->ultima_verificacao_DATETIME_UNIX = $row->ultima_verificacao_DATETIME_UNIX;
//
//        $this->erro = $row->erro;
//
//        $this->erro_sincronizacao_id_INT = $row->erro_sincronizacao_id_INT;
//        if($this->erro_sincronizacao_id_INT && $this->objErro_sincronizacao != null)
//            $this->objErro_sincronizacao->select($this->erro_sincronizacao_id_INT);
//
//
//    }
    
    
        public function getUltimasSincronizacoes(
            $idCorporacao, $ultimoIdSincronizacaoExcluso, $db = null){
            if($db == null)
                $db = new Database();
            $q = "SELECT id"
                . " FROM sincronizacao"
                . " WHERE ((id <=  ".$this->getSincronizacao_id_INT();
            if(strlen($ultimoIdSincronizacaoExcluso))
                $q .= " AND id > $ultimoIdSincronizacaoExcluso ";
            $q .= " AND estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::FINALIZADO;
            $q .= " ) ";
            
            
            $q .= " OR id =  ".$this->getSincronizacao_id_INT().") "
                . " AND corporacao_id_INT = $idCorporacao";
            
//            echo $q;
//            exit();;
            
            $db->queryMensagemThrowException($q);
            
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
         public function getIdDaUltimaSincronizacao(
             $idCorporacao, $idMobileIdentificador, $db = null){
            if($db == null)
                $db = new Database();
            $q = "SELECT MAX(sm.sincronizacao_id_INT)"
                . " FROM sincronizacao_mobile sm "
                . "     JOIN sincronizacao s ON sm.sincronizacao_id_INT = s.id "
                . " WHERE sm.mobile_identificador_INT =  ".$idMobileIdentificador
                . "     AND sm.corporacao_id_INT = $idCorporacao "
                . "     AND s.corporacao_id_INT = $idCorporacao "
                . "     AND s.estado_sincronizacao_id_INT IN ("
                .   EXTDAO_Estado_sincronizacao::FINALIZADO.", "
                .   EXTDAO_Estado_sincronizacao::RESETADO.")";
            
            $db->queryMensagemThrowException($q);
            
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public function getTabelasDaSincronizacao($idCorporacao){
            $this->database->queryMensagemThrowException(
                "SELECT DISTINCT c.sistema_tabela_id_INT"
                . "    FROM crud_mobile c "
                . "     WHERE c.sincronizacao_mobile_id_INT = {$this->getId()} "
                . "         AND c.corporacao_id_INT = $idCorporacao ");
            return Helper::getResultSetToArrayDeUmCampo($this->database->result);
            
        }
        public function setLabels(){

			$this->label_id = "Id";
			$this->label__INT = "Mobile Identificador";
			$this->label_estado_sincronizacao_mobile_id_INT = "Estado Sincroniza??o Mobile";
			$this->label_data_inicial_DATETIME = "Data Inicial";
			$this->label_data_final_DATETIME = "Data Final";
			$this->label_resetando_BOOLEAN = "Resetando O Celular?";
			$this->label_sincronizacao_id_INT = "Sincroniza??o";
			$this->label_seq_INT = "Sequ?ncia";
			$this->label_descricao = "Descri??o";
			$this->label_crud_mobile_ARQUIVO = "Crud Mobile";
			$this->label_ultimo_crud_id_INT = "Id do ?ltimo Registro no Crud";


        }

        public function setDiretorios(){

			$this->diretorio_crud_mobile_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final


        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Sincronizacao_mobile();

        }
        public function processaCrudWeb(){
            
        }
        
        public function insereNovoAtributoNasTabelas(){
            
            $this->database->queryMensagemThrowException("SHOW TABLES");
            $ids = Helper::getResultSetToArrayDeUmCampo($this->database->result);
            for($i = 0; $i < count($ids); $i++){
                $t = $ids[$i];
                if(Helper::startsWith($t, "__")){
                    //$q = "ALTER TABLE $t DROP crud_id_INT ";
                    //$q = "ALTER TABLE $t ADD COLUMN crud_id_INT int(11)";
                    $q = "ALTER TABLE $t ADD COLUMN crud_mobile_id_INT int(11)";
                    $this->database->queryMensagemThrowException($q);
                }
            }
//            echo "TERMINOU";
        }

        public function retiraMobileDaFilaDeEspera($idCorporacao){
            $q = "UPDATE sincronizacao_mobile "
                . " SET estado_sincronizacao_mobile_id_INT = ".EXTDAO_Estado_sincronizacao_mobile::MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO
                . " WHERE id = {$this->getId()} "
                . "     AND estado_sincronizacao_mobile_id_INT = ".EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO
                . "     AND corporacao_id_INT = $idCorporacao ";
//            echo $q;
//            exit();
            $msg =  $this->database->queryMensagem($q);
            if($msg != null && $msg->erro()){
                return $msg;
            }
            $q = "SELECT estado_sincronizacao_mobile_id_INT "
                . " FROM sincronizacao_mobile "
                . " WHERE id = {$this->getId()} "
                . "     AND corporacao_id_INT = $idCorporacao ";

            $msg = $this->database->queryMensagem($q);
            if($msg != null && $msg->erro()){
                return $msg;
            }
            $estado = $this->database->getPrimeiraTuplaDoResultSet(0);
            if($estado == EXTDAO_Estado_sincronizacao_mobile::MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO){
                $msg = EXTDAO_Sincronizacao_mobile_iteracao::inserir(
                    $this->database, 
                    $this->getId(), 
                    EXTDAO_Estado_sincronizacao_mobile::MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO);
                return $msg == null ? new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) : $msg;
            }
            else return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO);
        }
        
        public function apagaCrudsMobiles($idCorporacao){
            $q = "DELETE FROM crud_mobile "
                . " WHERE sincronizacao_mobile_id_INT = ".$this->getId()
                . "     AND corporacao_id_INT = $idCorporacao";
            $this->database->queryMensagemThrowException($q);
            
        }
        
        public function processaCrudMobile($idCorporacao, $corporacao){
            try{
                //Apagando cruds mobiles antigos
                $this->apagaCrudsMobiles($idCorporacao);

                $arquivo = $this->getCrud_mobile_ARQUIVO();

                $ultimoIdWeb = null;
                SingletonLog::writeLogStr("processaCrudMobile: $arquivo");

                if(strlen($arquivo)){
                    $pathArquivo = ConfiguracaoSite::getPathConteudo()."sincronizacao_mobile/{$corporacao}/"  ;
                    if(!file_exists($pathArquivo)) Helper::mkdir ($pathArquivo, 0777, true);
                    $pathArquivo .= $arquivo;
                    SingletonLog::writeLogStr("Path: $pathArquivo");
                    $p = fopen($pathArquivo, "r+");


                    if($p != ""){
                        SingletonLog::writeLogStr("Ponteiro OK! ");
                        $strJson = fgets($p);
                        
                        $json = json_decode($strJson);
//                        if($json != null){
//                            SingletonLog::writeLogStr("Json: ".print_r($json, true));
//                        } else {
//                            SingletonLog::writeLogStr("Json: nulo");
//                        }
                        $ultimoIdWeb = $json->ultimoIdWeb;
                        if(!strlen($ultimoIdWeb)) $ultimoIdWeb = null;
                        $vetorCrudMobile = $json->crud_mobile;

                        if(count($vetorCrudMobile)){
                            for($k = 0; $k < count($vetorCrudMobile); $k++){

                               $crudMobile = $vetorCrudMobile[$k];
                               $tabela = $crudMobile->tabela;
                               $tipoOperacao = $crudMobile->tipoOperacao;
                               $idSistemaTabela = EXTDAO_Sistema_tabela::getIdSistemaTabela($tabela, $this->database);

                               if($tipoOperacao == EXTDAO_Tipo_operacao_banco::Remover){
                                   for($i = 0 ; $i < count($crudMobile->ids); $i++){
                                       $idRemovido = $crudMobile->ids[$i];
                                       $idSincronizador =  $crudMobile->idsSincronizador[$i];
                                       $objCrudMobile = new EXTDAO_Crud_mobile($this->database);
                                       $objCrudMobile->setTipo_operacao_banco_id_INT(EXTDAO_Tipo_operacao_banco::Remover);
                                       $objCrudMobile->setSistema_tabela_id_INT($idSistemaTabela);
                                       $objCrudMobile->setId_tabela_web_INT($idRemovido);
                                       $objCrudMobile->setProcessada_BOOLEAN('0');
                                       $objCrudMobile->setSincronizacao_mobile_id_INT( $this->getId());
                                       $objCrudMobile->setId_sincronizador_mobile_INT($idSincronizador);
                                       $objCrudMobile->setCorporacao_id_INT($idCorporacao);
                                       $objCrudMobile->formatarParaSQL();
                                       $objCrudMobile->insert();
                                       $idCrudMobile = $objCrudMobile->getIdDoUltimoRegistroInserido($idCorporacao);
                                   }
                               } else{
                                   //echo "ENTROU";
                                    $strCampos = Helper::arrayToString($crudMobile->campos);
                                    $strInsert = "INSERT INTO __".$tabela." ( ".$strCampos.", crud_mobile_id_INT ) VALUES ";

                                    for($i = 0 ; $i < count($crudMobile->protocolos); $i++){
                                        $protocolo = $crudMobile->protocolos[$i];

                                        $valores = $protocolo->valores;

                                        if(count($valores)){
                                            $objCrudMobile = new EXTDAO_Crud_mobile($this->database);
                                             switch ($tipoOperacao) {

                                                 case EXTDAO_Tipo_operacao_banco::Inserir:
                                                     $objCrudMobile->setId_tabela_mobile_INT($protocolo->id);
                                                     break;

                                                 case EXTDAO_Tipo_operacao_banco::Editar:
                                                     $objCrudMobile->setId_tabela_web_INT($protocolo->id);
                                                     break;
                                                 default:
                                                     $objCrudMobile->setId_tabela_mobile_INT($protocolo->id);
                                                     break;
                                             }
                                             $objCrudMobile->setSistema_tabela_id_INT($idSistemaTabela);
                                             $objCrudMobile->setTipo_operacao_banco_id_INT($tipoOperacao);
                                             $objCrudMobile->setSincronizacao_mobile_id_INT($this->getId());
                                             $objCrudMobile->setId_sincronizador_mobile_INT($protocolo->idSincronizador);
                                             $objCrudMobile->setProcessada_BOOLEAN('0');
                                             $objCrudMobile->setCorporacao_id_INT($idCorporacao);
                                             $objCrudMobile->formatarParaSQL();
                                             $objCrudMobile->insert();
                                             $idCrudMobile = $objCrudMobile->getIdDoUltimoRegistroInserido($idCorporacao);

                                             //Helper::utf8ToIso88591($valores);

                                             $corpo = Helper::arrayToSQL($valores,$this->database);

                                             $consultaInsert = $strInsert." (".$corpo.", '$idCrudMobile') ";

                                             $msg = $this->database->queryMensagemThrowException($consultaInsert);
                                             
                                            $idsDependentes = $protocolo->idsDependentes;
                                            $atributosDependentes = $protocolo->atributosDependentes;
                                            if(count($idsDependentes) && count($atributosDependentes)){
                                                $objCrudMobileDependente = new EXTDAO_Crud_mobile_dependente($this->database);
                                                for($j = 0; $j < count($idsDependentes); $j++){
                                                    $objCrudMobileDependente->setId(null);
                                                    $objCrudMobileDependente->setCrud_mobile_id_INT($idCrudMobile);
                                                    $objCrudMobileDependente->setId_sincronizador_mobile_INT($idsDependentes[$j]);
                                                    $objCrudMobileDependente->setAtributo( $atributosDependentes[$j]);
                                                    $objCrudMobileDependente->formatarParaSQL();
                                                    $objCrudMobileDependente->insert();
                                                }
                                            }
                                        }
                                     }
                               }
                            }
                        }
                    }
                }

                $obj = new EXTDAO_Sincronizacao_mobile($this->database);
                $obj->select($this->getId());
    //            if($ultimoIdWeb != null)
    //                $obj->setUltimo_crud_id_INT ($ultimoIdWeb);    
                $obj->setEstado_sincronizacao_mobile_id_INT(EXTDAO_Estado_sincronizacao_mobile::PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB);
                $obj->setCorporacao_id_INT($idCorporacao);
                $obj->formatarParaSQL();
                $obj->update($this->getId());
                EXTDAO_Sincronizacao_mobile_iteracao::inserir($this->database, $this->getId(), EXTDAO_Estado_sincronizacao_mobile::PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB);
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            } catch(DatabaseException $dbex){
                return new Mensagem_token(
                     PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,
                    null, 
                    null, 
                    $dbex) ;
            }
            catch (Exception $ex) {
                return new Mensagem(null, null, null, $ex);
            }
        }
        
        public static function getMobilesDaSincronizacaoNoEstado(
            $idCorporacao, $idSincronizacao, 
            $idEstadoSincronizacaoMobile, $db = null){
            if($db == null)
                $db = new Database ();
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.sincronizacao_id_INT = $idSincronizacao "
                . "     AND sm.estado_sincronizacao_mobile_id_INT = $idEstadoSincronizacaoMobile "
                . "     AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC";
            
            $db->queryMensagemThrowException($q);
            
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
         public static function getIdsNosEstados(
             $idCorporacao, 
             $idMobileIdentificador, 
             $idsEstadoSincronizacaoMobile, 
             $db = null){
            if($db == null)
                $db = new Database ();
            $strIn = Helper::arrayToString($idsEstadoSincronizacaoMobile);
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.mobile_identificador_INT = $idMobileIdentificador "
                . "     AND sm.estado_sincronizacao_mobile_id_INT IN ($strIn) "
                . "     AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC";
            
            $db->queryMensagemThrowException($q);
            
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        public static function getMobilesDaSincronizacao(
            $idCorporacao, $idSincronizacao, $db = null){
            if($db == null)
                $db = new Database ();
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.sincronizacao_id_INT = $idSincronizacao "
                . "     AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC";
            
            $db->queryMensagemThrowException($q);
            
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
        public static function getMobilesDaSincronizacaoQuePossuemArquivos(
            $idCorporacao, $idSincronizacao, $db = null){
            if($db == null)
                $db = new Database ();
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.sincronizacao_id_INT = $idSincronizacao " 
                    . " AND NOT sm.crud_web_para_mobile_ARQUIVO IS NULL "
                    . " AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC";
            
            $db->queryMensagemThrowException($q);
            
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
          public static function getUltimosCrudsDaSincronizacao(
              $idCorporacao, $idSincronizacao, $db = null){
            if($db == null)
                $db = new Database ();
              $q = "SELECT sm.id, sm.ultimo_crud_id_INT "
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.sincronizacao_id_INT = $idSincronizacao "
                . " AND sm.corporacao_id_INT = $idCorporacao "
                  . " ORDER BY sm.ultima_verificacao_DATETIME DESC";
            
            
            $db->queryMensagemThrowException($q);
            
            $ids = Helper::getResultSetToMatriz($db->result, 1 , 0);
            return $ids;
        }
         public static function getUltimosCrudsDaSincronizacaoNoEstado(
             $idCorporacao, $idSincronizacao, $idEstado, $db = null){
            if($db == null)
                $db = new Database ();
              $q = "SELECT sm.id, sm.ultimo_crud_id_INT "
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.sincronizacao_id_INT = $idSincronizacao "
                . "    AND sm.estado_sincronizacao_mobile_id_INT = $idEstado "
                . "    AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC";
            
            
            $db->queryMensagemThrowException($q);
            
            $ids = Helper::getResultSetToMatriz($db->result, 1 , 0);
            return $ids;
        }
        
        
        
        public static function getMobilesParaNovaSincronizacaoQuePossuemArquivos(
            $idCorporacao, $db = null){
            if($db == null)
                $db = new Database ();
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.estado_sincronizacao_mobile_id_INT = '".EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO."'"
                . "    AND LENGTH( sm.crud_mobile_ARQUIVO) > 0 "
                . "    AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC"
                . " LIMIT 0, ".LIMITE_CELULARES_POR_SINCRONIZACAO;
            
            $db->queryMensagemThrowException($q);
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
        public static function existeMobilesParaNovaSincronizacaoQuePossuemArquivos(
            $idCorporacao, $db = null){
            if($db == null)
                $db = new Database ();
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE "
                . "     sm.estado_sincronizacao_mobile_id_INT = '".EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO."'"
                . "     AND (LENGTH( sm.crud_mobile_ARQUIVO) > 0 OR primeira_vez_BOOLEAN = 1)"
                . "     AND sincronizacao_id_INT IS NULL"
                . "     AND sm.corporacao_id_INT = $idCorporacao "
                . " ORDER BY sm.ultima_verificacao_DATETIME DESC"
                . " LIMIT 0, 1";
            
            $msg = $db->queryMensagem($q);
            if($msg != null && $msg->erro()) return $msg;
            else {
                if($db->rows() > 0){
                    $id = $db->getPrimeiraTuplaDoResultSet(0);
                    return new Mensagem_token(null, null, $id);
                }else 
                    return new Mensagem_token(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
        }
        
        
        public static function getUltimosIdSincronizacaoRelativosAosMobilesQueEstaoNaFilaDeEspera(
            $idCorporacao, $db = null){
            if($db == null)
                $db = new Database ();
            $q ="SELECT MAX(s.id), sm.mobile_identificador_INT
                 FROM sincronizacao_mobile sm JOIN sincronizacao s ON sm.sincronizacao_id_INT = s.id 
                 WHERE s.estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::FINALIZADO." 
                     AND sm.estado_sincronizacao_mobile_id_INT = '".EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO."'
                     AND sm.corporacao_id_INT = $idCorporacao
                 LIMIT 0, ".LIMITE_CELULARES_POR_SINCRONIZACAO." 
                 GROUP BY sm.mobile_identificador_INT";
            
            $db->queryMensagemThrowException($q);
            $ids = Helper::getResultSetToMatriz($db->result);
            return $ids;
        }
        
        
        public static function getMobilesParaNovaSincronizacao(
            $idCorporacao, $db = null){
            if($db == null)
                $db = new Database ();
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.estado_sincronizacao_mobile_id_INT = '".EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO."'"
                . "     AND sm.corporacao_id_INT = $idCorporacao "
                . " LIMIT 0, ".LIMITE_CELULARES_POR_SINCRONIZACAO;
            
            $msg = $db->queryMensagem($q);
            if($msg != null && $msg->erro()) return $msg;
            else if($db->rows() == 0 ) return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            else {
                $ids = Helper::getResultSetToArrayDeUmCampo($db->result);    
                return new Mensagem_vetor_token(null,null, $ids);
            }
            
            
        }
        
        
        
        public static function getIdsNoEstado($idCorporacao, $estado){
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE sm.estado_sincronizacao_mobile_id_INT = $estado " 
                . "     AND sm.corporacao_id_INT = $idCorporacao ";
            $db = new Database();
            $db->queryMensagemThrowException($q);
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
        public static function getIdsDoMobileNoEstado(
            $idCorporacao, $idMobileIdentificador, $estado){
            $q = "SELECT sm.id"
                . " FROM sincronizacao_mobile sm "
                . " WHERE  sm.mobile_identificador_INT = $idMobileIdentificador"
                . "     AND sm.estado_sincronizacao_mobile_id_INT = $estado " 
                . "     AND sm.corporacao_id_INT = $idCorporacao ";
            $db = new Database();
            $db->queryMensagemThrowException($q);
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
            return $ids;
        }
        
        
        public static function geUltimotIdMobile(
            $idCorporacao, $idMobileIdentificador, $db = null){
            $q = "SELECT MAX(sm.id)"
                . " FROM sincronizacao_mobile sm "
                . " WHERE  sm.mobile_identificador_INT = $idMobileIdentificador " 
                . "     AND sm.corporacao_id_INT = $idCorporacao ";
            if($db == null)
            $db = new Database();
            $db->queryMensagemThrowException($q);
            $id = $db->getPrimeiraTuplaDoResultSet(0);
            
            return $id;
        }
        
        public static function getDadosProcessoEmAberto(
            $idCorporacao, $idSincronizacaoMobile, $db = null){
            $q = "SELECT sm.id_ultimo_sinc_android_web_INT idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
                    IF(crud_mobile_arquivo IS NULL or crud_mobile_arquivo = '', 'false', 'true') as enviouDadosSincronizacaoMobile,
                    $idSincronizacaoMobile idSincronizacaoMobile 
                 FROM sincronizacao_mobile sm 
                 WHERE  sm.id = $idSincronizacaoMobile 
                    AND sm.corporacao_id_INT = $idCorporacao ";
            
            if($db == null)
                $db = new Database();
            $db->queryMensagemThrowException($q);
            
            $objs = Helper::getResultSetToMatriz($db->result, 1, 0);
            if(count($objs)){
                return $objs[0];
            }
            else return null;
        }
        
        
        public static function geIdSincronizacao(
            $idCorporacao, $idSincronizacaoMobile, $db = null){
            $q = "SELECT sincronizacao_id_INT"
                . " FROM sincronizacao_mobile sm "
                . " WHERE  sm.id = $idSincronizacaoMobile "
                . "     AND sm.corporacao_id_INT = $idCorporacao ";
            if($db == null)
                $db = new Database();
            $db->queryMensagemThrowException($q);
            $id = $db->getPrimeiraTuplaDoResultSet(0);
            
            return $id;
        }
        
        public function downloadDadosSincronizacaoMobile($corporacao){
            $configuracaoSite = Registry::get('ConfiguracaoSite');

            $dirUpload  = $configuracaoSite->PATH_CONTEUDO."sincronizacao_mobile/{$corporacao}/";
            if(!file_exists($dirUpload) )
                Helper::mkdir($dirUpload, 0777, true);
            
            $nomeArquivo = $this->getCrud_web_para_mobile_ARQUIVO();
            if(!strlen($nomeArquivo))
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, 
                    "N?o existe arquivo da sincronizacao mobile");
            $pathArquivo = $dirUpload.$nomeArquivo;
            if(!file_exists($pathArquivo))
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_ARQUIVO_SINCRONIZACAO_INEXISTENTE,
                    "Arquivo de sincroniza??o inexistente: $pathArquivo");
            $objDownload = new Download($pathArquivo);
            $fp = $objDownload->df_download();
            if ($fp){
                return null;
            }
            else
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
                    "[ksjadnciwa]Falha durante o download do arquivo: $pathArquivo");
        }
        
        public function getCrudsRemocao($idSistemaTabela, $idCorporacao){
             $q = "SELECT      cm.crud_id_INT idCrud,  "
            . "     cm.id_tabela_web_INT idTabelaWeb,  "
            . "     cm.id_sincronizador_mobile_INT idSincronizadorMobile,  "
            . "     cm.id idCrudMobile,  "
            . "     cm.erro_sql_INT erroSql  "
            . " FROM crud_mobile cm "
            . "    LEFT JOIN crud c "
            . "         ON c.id = cm.crud_id_INT "
            . " WHERE cm.sincronizacao_mobile_id_INT = {$this->getId()} "
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND cm.tipo_operacao_banco_id_INT = ".EXTDAO_Tipo_operacao_banco::Remover." "
            . " AND cm.processada_BOOLEAN = '1' "
            . " AND cm.corporacao_id_INT= $idCorporacao "
            . " AND c.corporacao_id_INT = $idCorporacao " 
            . " ORDER BY cm.sistema_tabela_id_INT, cm.id ";
            $this->database->queryMensagemThrowException($q);
            return Helper::getResultSetToMatriz($this->database->result, 1, 0);
            
        }
        
        public function getCrudsInsercao($idSistemaTabela, $idCorporacao){
            //ADICIONE OS CAMPOS POR BAIXO NO SELECT
            //O PROJETO ANDROID OMEGA EMPRESA LE NESSA ORDEM
            //QUALQUER ALTERACAO GERA EXCECAO
             $q = "SELECT      cm.crud_id_INT idCrud,  " 
            . "     cm.id_tabela_mobile_INT idTabelaMobile,  "
            . "     cm.id_sincronizador_mobile_INT idSincronizadorMobile,  "
            . "     c.id_tabela_web_INT idTabelaWeb,  "
            . "     cm.id_tabela_web_INT idTabelaWebCrudMobile,  "     
            . "     cm.id_tabela_web_duplicada_INT idTabelaWebDuplicada,  "
            . "     cm.id idCrudMobile,  "
            . "     cm.sistema_tabela_id_INT idSistemaTabela,"
            . "     cm.erro_sql_INT erroSql,  "
            . "     c.id_tabela_web_INT idTabelaWebCrud  "
            . " FROM crud_mobile cm "
            . "    LEFT JOIN crud c"
            . "         ON c.id = cm.crud_id_INT "
            . " WHERE cm.sincronizacao_mobile_id_INT = {$this->getId()} "
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND cm.tipo_operacao_banco_id_INT = ".EXTDAO_Tipo_operacao_banco::Inserir." "
            . " AND cm.processada_BOOLEAN = '1'" 
            . " AND cm.corporacao_id_INT= $idCorporacao "
            . " AND c.corporacao_id_INT = $idCorporacao " 
            . " ORDER BY cm.sistema_tabela_id_INT, cm.id ";
            $this->database->queryMensagemThrowException($q);
            return Helper::getResultSetToMatriz($this->database->result, 1, 0);
            
        }
        
        public function getCrudsEdicao($idSistemaTabela, $idCorporacao){
            //ADICIONE OS CAMPOS POR BAIXO NO SELECT
            //O PROJETO ANDROID OMEGA EMPRESA LE NESSA ORDEM
            //QUALQUER ALTERACAO GERA EXCECAO
             $q = "SELECT cm.crud_id_INT idCrud,  "
            . "    c.id_tabela_web_INT idTabelaWeb,  "
            . "    cm.id_tabela_web_INT idTabelaWebCrudMobile,  "
            . "     cm.id_tabela_web_duplicada_INT idTabelaWebDuplicada,  "
            . "     cm.id_sincronizador_mobile_INT idSincronizadorMobile,  "
            . "     cm.id idCrudMobile,  "
            . "     cm.erro_sql_INT erroSql,"
            . "     c.id_tabela_web_INT idTabelaWebCrud  "
            . " FROM crud_mobile cm "
            . "    LEFT JOIN crud c"
            . "         ON c.id = cm.crud_id_INT "
            . " WHERE cm.sincronizacao_mobile_id_INT = {$this->getId()} "
            . " AND cm.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND cm.tipo_operacao_banco_id_INT = ".EXTDAO_Tipo_operacao_banco::Editar." " 
            . " AND cm.processada_BOOLEAN = '1'" 
            . " AND cm.corporacao_id_INT= $idCorporacao "
            . " AND c.corporacao_id_INT = $idCorporacao " 
            . " ORDER BY cm.sistema_tabela_id_INT, cm.id ";
            $this->database->queryMensagemThrowException($q);
            return Helper::getResultSetToMatriz($this->database->result, 1, 0);
            
        }
                                             
        public static function existeMobileNaFilaComOBancoUltrapassado($idCorporacao, $db = null){

            
            $q = "SELECT 1
FROM sincronizacao_mobile sm 
WHERE sm.estado_sincronizacao_mobile_id_INT = ".EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO."
        AND sm.corporacao_id_INT = $idCorporacao
	AND (
		SELECT sm2.id
		FROM sincronizacao_mobile sm2 
			JOIN sincronizacao s2
				ON sm2.sincronizacao_id_INT = s2.id
		WHERE sm2.mobile_identificador_INT = sm.mobile_identificador_INT
                AND sm2.corporacao_id_INT = $idCorporacao
                AND s2.corporacao_id_INT = $idCorporacao
		AND sm2.id = (  
                    SELECT MAX(sm3.id) 
                    FROM sincronizacao_mobile sm3 
                    WHERE sm3.id < sm.id 
                        AND sm3.mobile_identificador_INT = sm2.mobile_identificador_INT
                        AND sm3.corporacao_id_INT = $idCorporacao
                    )
		AND sm2.estado_sincronizacao_mobile_id_INT != '".EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL."'
		AND (SELECT s3.quantidade_crud_processado_INT
                    FROM sincronizacao s3
                    WHERE s3.id > s2.id 
                        AND s3.corporacao_id_INT = $idCorporacao
                        AND s3.quantidade_crud_processado_INT > 0
                        AND s3.estado_sincronizacao_id_INT = '".EXTDAO_Estado_sincronizacao::FINALIZADO."'
                    LIMIT 0,1 
                    ) > 0
		ORDER BY id DESC 
		LIMIT 0, 1
	) > 0
LIMIT 0, 1;";
            
            if($db == null)
                $db = new Database();
            $msg = $db->queryMensagem($q);
            if($msg != null && $msg->erro()) return $msg;
            else if($db->rows() == 0) return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            else {
                return new Mensagem(null);
            }
        }
        
        
        function setErro($val)
        {
            
        }
        
//    public function formatarParaSQL(){
//
//		if($this->mobile_identificador_INT == ""){
//
//			$this->mobile_identificador_INT = "null";
//
//		}
//
//		if($this->mobile_conectado_INT == ""){
//
//			$this->mobile_conectado_INT = "null";
//
//		}
//
//		if($this->estado_sincronizacao_mobile_id_INT == ""){
//
//			$this->estado_sincronizacao_mobile_id_INT = "null";
//
//		}
//
//		if($this->resetando_BOOLEAN == ""){
//
//			$this->resetando_BOOLEAN = "null";
//
//		}
//
//		if($this->sincronizacao_id_INT == ""){
//
//			$this->sincronizacao_id_INT = "null";
//
//		}
//
//		if($this->seq_INT == ""){
//
//			$this->seq_INT = "null";
//
//		}
//
//		if($this->ultimo_crud_id_INT == ""){
//
//			$this->ultimo_crud_id_INT = "null";
//
//		}
//
//		if($this->erro_sincronizacao_id_INT == ""){
//
//			$this->erro_sincronizacao_id_INT = "null";
//
//		}
//
//		if($this->primeira_vez_BOOLEAN == ""){
//
//			$this->primeira_vez_BOOLEAN = "null";
//
//		}
//
//
//		if($this->id_primeiro_sinc_android_web_INT == ""){
//
//			$this->id_primeiro_sinc_android_web_INT = "null";
//
//		}
//
//		if($this->id_ultimo_sinc_android_web_INT == ""){
//
//			$this->id_ultimo_sinc_android_web_INT = "null";
//
//		}
//
//
//		if($this->corporacao_id_INT == ""){
//
//			$this->corporacao_id_INT = "null";
//
//		}
//
//                $val = $this->erro;
//                if(strlen($val)){
//                    $val = Helper::substring( $val, 512);
//
//                    $val  = $this->database->realEscapeString($val);
//                    $this->erro = $val ;
//                }
//
//
//
//	$this->data_inicial_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicial_DATETIME);
//	$this->data_final_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_final_DATETIME);
//	$this->ultima_verificacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->ultima_verificacao_DATETIME);
//
//
//    }
//
    
        public function getIdentificador(){
            return "Sincronizacao {$this->getSincronizacao_id_INT()}. idSM: ". $this->id;
        }
        
        
        
        public static function updateEstadoSincronizacaoMobile(
            $idSincronizacaoMobile, 
            $idCorporacao, 
            $idEstadoSincronizacaoMobile, 
            $db = null){
            
            if($db == null) $db = new Database();
            $q = "UPDATE sincronizacao_mobile "
                . " SET estado_sincronizacao_mobile_id_INT = ".$idEstadoSincronizacaoMobile
                  ." WHERE id = $idSincronizacaoMobile AND corporacao_id_INT = $idCorporacao";
            $msg = $db->queryMensagem($q);
            if($msg != null && $msg->erro()){
                return $msg;
            }
            $msg = EXTDAO_Sincronizacao_mobile_iteracao::inserir(
                $db, 
                $idSincronizacaoMobile, 
                $idEstadoSincronizacaoMobile);
            return $msg == null
                ? new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                : $msg;
        }

        public static function updateEstadosSincronizacoesMobiles(
            $idsSincronizacoesMobiles, 
            $idCorporacao, 
            $idEstadoSincronizacaoMobile, 
            $db = null){
            
            if($db == null) $db = new Database();
            
            $strIn = Helper::arrayToString($idsSincronizacoesMobiles, ',');
            $q = "UPDATE sincronizacao_mobile "
                . " SET estado_sincronizacao_mobile_id_INT = ".$idEstadoSincronizacaoMobile
                  ." WHERE id in ($strIn) AND corporacao_id_INT = $idCorporacao";
            $msg = $db->queryMensagem($q);
            if($msg != null && $msg->erro()){
                return $msg;
            }
            $msg = EXTDAO_Sincronizacao_mobile_iteracao::inserirLista(
                $db, 
                $strIn, 
                $idEstadoSincronizacaoMobile);
            return $msg;
        }

	}

    