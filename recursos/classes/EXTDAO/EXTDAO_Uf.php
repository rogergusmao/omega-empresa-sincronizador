<?php //@@NAO_MODIFICAR
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Uf
    * NOME DA CLASSE DAO: DAO_Uf
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            EXTDAO_Uf.php
    * TABELA MYSQL:       uf
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    *
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    
    class EXTDAO_Uf extends DAO_Uf
    {

        public function __construct($db=null, $setLabels=false){
        
            parent::__construct($db);
        
            	$this->nomeClasse = "EXTDAO_Uf";

            if($setLabels){
            
                $this->setLabels();
            
            }
            
            $this->setDiretorios();
            $this->setDimensoesImagens();
            
                    
        }
        
        public function setLabels(){
        
			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_sigla = "Sigla";
			$this->label_dataCadastro = "Data de Cadastro";
			$this->label_dataEdicao = "Data de Edição";

        
        }
        
        public function setDiretorios(){
                
        
        
        }
        
        public function setDimensoesImagens(){
                
     
        
        }
        
        public function factory(){
                
            return new EXTDAO_Uf();        
        
        }
        
	}
    
    
