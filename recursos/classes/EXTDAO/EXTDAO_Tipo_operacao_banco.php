<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_operacao_banco
    * NOME DA CLASSE DAO: DAO_Tipo_operacao_banco
    * DATA DE GERA��O:    19.06.2014
    * ARQUIVO:            EXTDAO_Tipo_operacao_banco.php
    * TABELA MYSQL:       tipo_operacao_banco
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class EXTDAO_Tipo_operacao_banco extends DAO_Tipo_operacao_banco
    {
        const Editar=3;
	const Inserir = 2;
	const Remover = 1;

        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Tipo_operacao_banco";

            

            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Tipo_operacao_banco();

        }

	}

    