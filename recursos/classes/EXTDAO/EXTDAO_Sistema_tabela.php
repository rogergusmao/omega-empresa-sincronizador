<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Sistema_tabela
* NOME DA CLASSE DAO: DAO_Sistema_tabela
* DATA DE GERAÇÃO:    21.06.2014
* ARQUIVO:            EXTDAO_Sistema_tabela.php5
* TABELA MYSQL:       sistema_tabela
* BANCO DE DADOS:     sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Sistema_tabela extends DAO_Sistema_tabela
{
    static $hashTabelas = array();

    public function __construct($db=null, $setLabels=false){

        parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Sistema_tabela";


        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }
    public function getAtributosChaveUnica(){
        $strAtributos = $this->getAtributos_chave_unica();
        if(strlen($strAtributos)){

            $attrs = explode(",", $strAtributos);
            for($i = 0 ; $i < count($attrs); $i++){
                $attrs[$i] = trim( $attrs[$i]);
            }
            return $attrs;
        }
        return null;
    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_nome = "Nome";
        $this->label_data_modificacao_estrutura_DATETIME = "Data de Modificação da Estrutura";
        $this->label_frequencia_sincronizador_INT = "Frequência do Sincronizador";
        $this->label_banco_mobile_BOOLEAN = "Banco Mobile?";
        $this->label_banco_web_BOOLEAN = "Banco Web?";
        $this->label_transmissao_web_para_mobile_BOOLEAN = "Transmissao Web Para Mobile?";
        $this->label_transmissao_mobile_para_web_BOOLEAN = "Transmissão Mobile Para Web?";
        $this->label_tabela_sistema_BOOLEAN = "Tabela do Sistema?";
        $this->label_excluida_BOOLEAN = "Excluida?";


    }

    public function setDiretorios(){



    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Sistema_tabela();

    }



    public static function getIdSistemaTabela($nomeTabela,  $db = null){
        if($db == null){
            $db = new Database();
        }
        if(isset(EXTDAO_Sistema_tabela::$hashTabelas[$nomeTabela])){
            return EXTDAO_Sistema_tabela::$hashTabelas[$nomeTabela];
        }

        $db->queryMensagemThrowException("SELECT id"
            . " FROM sistema_tabela"
            . " WHERE nome LIKE '$nomeTabela'");
        $id = $db->getPrimeiraTuplaDoResultSet(0);
        if(!strlen($id)) return null;
        else {
            EXTDAO_Sistema_tabela::$hashTabelas[$nomeTabela] = $id;
            return $id;
        }
    }



    //web_para_mobile => true
    public static function getTabelasDownload($db = null){

        $q = "SELECT DISTINCT(st.id)"
            . " FROM sistema_tabela st"
            . " WHERE st.transmissao_web_para_mobile_BOOLEAN = '1' "
            . " AND st.frequencia_sincronizador_INT = '-1' ";
        if($db ==null){
            $db = new Database();
        }
        $db->queryMensagemThrowException($q);
        $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
        return $ids;
    }
    //mobille_para_web => true
    public static function getTabelasUpload($db = null){

        $q = "SELECT st.id, st.nome"
            . " FROM sistema_tabela st"
            . " WHERE st.transmissao_mobile_para_web_BOOLEAN = '1' "
            . " AND st.frequencia_sincronizador_INT = '-1' ";
        if($db ==null){
            $db = new Database();
        }
        $db->queryMensagemThrowException($q);
        $ids = Helper::getResultSetToMatriz($db->result, 1, 0);
        return $ids;
    }
    static $hash = array();
    public static function getNomeDaTabela($idSistemaTabela, $db=null){
        if(isset(EXTDAO_Sistema_tabela::$hash[$idSistemaTabela] ))
            return EXTDAO_Sistema_tabela::$hash[$idSistemaTabela];
        if($db == null)
            $db=new Database();
        $db->queryMensagemThrowException("SELECT nome "
            . " FROM sistema_tabela"
            . " WHERE id=$idSistemaTabela");
        $nome= $db->getPrimeiraTuplaDoResultSet(0);
        EXTDAO_Sistema_tabela::$hash[$idSistemaTabela] = $nome;
        return $nome;
    }


    public static function getIdDaSistemaTabela($pNomeTabela){
        if(strlen($pNomeTabela) > 0 ){

            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT ts.id as id
                              FROM sistema_tabela ts
                              WHERE ts.nome = '".Helper::formatarCampoTextoHTMLParaSQL($pNomeTabela)."' ";


            $objBanco->queryMensagemThrowException($query);

            $id = $objBanco->getPrimeiraTuplaDoResultSet("id");
//                    $objBanco->close();
            //o usuario ja eh existente
            if (strlen($id)) {

                return $id;
            }
            else return null;
        }
    }
}

    
