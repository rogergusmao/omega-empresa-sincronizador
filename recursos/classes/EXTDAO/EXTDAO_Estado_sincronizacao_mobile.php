<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Estado_sincronizacao_mobile
* NOME DA CLASSE DAO: DAO_Estado_sincronizacao_mobile
* DATA DE GERAÇÃO:    19.06.2014
* ARQUIVO:            EXTDAO_Estado_sincronizacao_mobile.php5
* TABELA MYSQL:       estado_sincronizacao_mobile
* BANCO DE DADOS:     sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Estado_sincronizacao_mobile extends DAO_Estado_sincronizacao_mobile
{

    //PROCESSO 1 [OPCIONAL][WEB]
    const MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO = 10;

    //PROCESSO 1 [WEB]
    const FILA_ESPERA_PARA_SINCRONIZACAO = 1;


    //PROCESSO 2 [WEB]
    const INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO = 2;
    //PROCESSO 3 [WEB]
    const PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB = 3;
    //PROCESSO 4 [WEB] [OPCIONAL NO FLUXO]
    const OCORREU_UM_ERRO_FATAL = -3;
    const OCORREU_UM_ERRO_LEVE_QUE_SO_ENVIARA_OS_DADOS_LOCAIS_QUANDO_RESETAR = 4;
    //PROCESSO 5 [WEB]
    const AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE = 5;
    //PROCESSO 6 [MOBILE]
    const AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE = 6;

    //PROCESSO 7 [WEB]
    const FINALIZADO = 7;

    const RESETADO = 8;

    const CANCELADO = 9;

    public function __construct($db=null, $setLabels=false){

        parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Estado_sincronizacao_mobile";

   
        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_nome = "Nome";


    }

    public function setDiretorios(){



    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Estado_sincronizacao_mobile();

    }



}

    
