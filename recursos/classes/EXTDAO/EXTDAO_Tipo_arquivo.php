<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_arquivo
    * NOME DA CLASSE DAO: DAO_Tipo_arquivo
    * DATA DE GERAÇÃO:    21.12.2013
    * ARQUIVO:            EXTDAO_Tipo_arquivo.php
    * TABELA MYSQL:       tipo_arquivo
    * BANCO DE DADOS:     blog_da_ale
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_arquivo extends DAO_Tipo_arquivo
    {
        
        const FOTO = 1;
        const VIDEO = 2;

        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Tipo_arquivo";

           

            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Tipo_arquivo();

        }

	}

    
