<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sincronizacao
    * NOME DA CLASSE DAO: DAO_Sincronizacao
    * DATA DE GERAÇÃO:    19.06.2014
    * ARQUIVO:            EXTDAO_Sincronizacao.php5
    * TABELA MYSQL:       sincronizacao
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sincronizacao extends DAO_Sincronizacao
    {

        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Sincronizacao";


            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }
        
        public function setLabels(){

			$this->label_id = "Id";
			$this->label_data_inicial_DATETIME = "Data Inicial";
			$this->label_data_final_DATETIME = "Data Final";
			$this->label_limite_mobile_INT = "Limite de Telefones";
			$this->label_estado_sincronizacao_id_INT = "Estado Sincroização";
			$this->label_primeiro_crud_id_INT = "Id do Primeiro Registro no Crud";
			$this->label_ultimo_crud_id_INT = "Id do último Registro no Crud";
			$this->label_seq_INT = "Sequência";
			$this->label_crud_ARQUIVO = "Arquivo do Crud";


        }

        public function setDiretorios(){

			$this->diretorio_crud_ARQUIVO = "";        //caminho a partir de da raiz, com '/' no final


        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Sincronizacao();

        }
        
       
        
        public static function atualizaEstado(
                    $idCorporacao,
                    $idSincronizacao, 
                    $estado, 
                    $db){
            if($db == null)
                $db = new Database();
            
            if($estado == EXTDAO_Estado_sincronizacao::FINALIZADO){
                $totalCruds = EXTDAO_Crud::getTotalCrudsDaSincronizacao(
                    $idCorporacao, $idSincronizacao, $db);
                $objSincronizacao = new EXTDAO_Sincronizacao($db);
                $objSincronizacao->select($idSincronizacao);
                $objSincronizacao->setQuantidade_crud_processado_INT($totalCruds);
                $objSincronizacao->formatarParaSQL();
                $objSincronizacao->update($objSincronizacao->getId());
            }
            
            $q = "UPDATE sincronizacao "
                . " SET estado_sincronizacao_id_INT = $estado "
                . " WHERE id = $idSincronizacao AND corporacao_id_INT = $idCorporacao";
            return $db->queryMensagemThrowException($q);
        }
            
        public static function finalizaProcessamentoDaSincronizacao(
                    $idCorporacao,
                    $idSincronizacao, 
                    $novoEstado,
                    Database $db = null){

            try{
                if($db == null) {
                    $db = new Database();
                }
                $db->iniciarTransacao();

                $q = "UPDATE sincronizacao "
                    . " SET processando_BOOLEAN = '0', "
                    . "     data_ultimo_processamento_DATETIME = '".Helper::getDiaEHoraAtualSQL()."', "
                    . "     estado_sincronizacao_id_INT = $novoEstado "
                    . " WHERE id = '$idSincronizacao' AND corporacao_id_INT = $idCorporacao";
                $msg = $db->queryMensagem($q);
                SingletonLog::writeLogStr("finalizaProcessamentoDaSincronizacao $idSincronizacao. Novo estado $novoEstado. ", $msg );
                if($msg == null){
                    $db->commitTransacao();
                    return $msg;
                }
                else{
                    if($db !=null)$db->rollbackTransacao();
                    return $msg;
                }
            }catch(Exception $ex){
                return new Mensagem(null, null, $ex);
            }
        }
        
        public static function getTotalCrudAPartirDaSincronizacao($idCorporacao, $idSincronizacao, $db = null){
            $q = "SELECT SUM(s.quantidade_crud_processado_INT) "
             . "    FROM sincronizacao s "
             . "    WHERE s.id > $idSincronizacao AND s.corporacao_id_INT = $idCorporacao";
         if($db == null)
         $db = new Database();
         $db->queryMensagemThrowException($q);
         $id = $db->getPrimeiraTuplaDoResultSet(0);
         return !is_numeric($id) ? 0 : $id;
        }
        public static function getIdDaUltimaSincronizacaoFinalizadaSejaSucessoOuComErro( $idCorporacao, $db = null){
         $q = "SELECT MAX(s.id) "
             . "    FROM sincronizacao s "
             . "    WHERE s.corporacao_id_INT = $idCorporacao "
             . "        AND s.estado_sincronizacao_id_INT IN "
             . "    (".EXTDAO_Estado_sincronizacao::FINALIZADO.", "
             .EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO.","
             . EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO_MOBILE.","
             .EXTDAO_Estado_sincronizacao::RESETADO. ") ";
         if($db == null)
         $db = new Database();
         $db->queryMensagemThrowException($q);
         $id = $db->getPrimeiraTuplaDoResultSet(0);
         return $id;
        }
        public static function getUltimoIdSistemaRegistroSincronizadorWebParaAndroidDeUmaSincronizacao(
            $idCorporacao,
            $idLimiteSuperiorSincronizacao,
            Database $db = null){

            if(!is_numeric($idLimiteSuperiorSincronizacao)) $idLimiteSuperiorSincronizacao = 0;
            $q = "SELECT MAX(s.id_ultimo_sistema_registro_sincronizador_INT) "
             . "    FROM sincronizacao s "
             . "    WHERE ";
            $where = "";

            if(is_numeric($idLimiteSuperiorSincronizacao))
                $where .=    " s.id < $idLimiteSuperiorSincronizacao ";

            if(strlen($where))
                $where .= " AND ";

            $where .= " s.corporacao_id_INT = $idCorporacao 
                        AND s.id_ultimo_sistema_registro_sincronizador_INT > 0";

            if($db == null)
               $db = new Database();
            $db->queryMensagemThrowException($q.$where );
            $id = $db->getPrimeiraTuplaDoResultSet(0);
            return $id;
        }
        public static function getUltimoIdSistemaRegistroSincronizadorDaWeb($id, $db = null){
         $q = "SELECT s.id_ultimo_sistema_registro_sincronizador_INT "
             . "    FROM sincronizacao s "
             . "    WHERE s.id = $id ";
         
         if($db == null)
            $db = new Database();
         $db->queryMensagemThrowException($q);
         $id = $db->getPrimeiraTuplaDoResultSet(0);
         return $id;
        }
        
        public static function getIdDaUltimaSincronizacaoComErro( $idCorporacao, $db = null){
         $q = "SELECT MAX(s.id) "
             . "    FROM sincronizacao s LEFT JOIN sincronizacao_mobile sm ON s.id = sm.sincronizacao_id_INT "
             . "    WHERE (s.estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO_MOBILE
             . "        OR sm.estado_sincronizacao_mobile_id_INT = ". EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL.") "
             . "   AND sm.corporacao_id_INT = $idCorporacao";
         if($db == null)
         $db = new Database();
         $db->queryMensagemThrowException($q);
         $id = $db->getPrimeiraTuplaDoResultSet(0);
         return $id;
        }
        
        public static function getIdNoEstado($idCorporacao, $estado, $db = null){
         $q = "SELECT s.id"
             . " FROM sincronizacao s "
             . " WHERE s.corporacao_id_INT = $idCorporacao";
         $strIn = "";
         if(is_array($estado)){
              $strIn = Helper::arrayToString($estado, ",");
            $q .= "    AND s.estado_sincronizacao_id_INT IN ($strIn) ";
         } else {
            $q .= "    AND s.estado_sincronizacao_id_INT = $estado ";
         }
         
         
         if($db == null) 
             $db = new Database();
         $db->queryMensagemThrowException($q);
         
         if($db->rows() == 0) 
             return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
         
         $id = $db->getPrimeiraTuplaDoResultSet(0);
         return new Mensagem_token(null, null, $id);
        }
        
        
        public static function getEstadoDaSincronizacao($idSincronizacao, $db = null){
         $q = "SELECT s.estado_sincronizacao_id_INT"
             . " FROM sincronizacao s"
             . " WHERE s.id = $idSincronizacao";
         
         
         if($db == null) $db = new Database();
         $db->queryMensagemThrowException($q);
         $id = $db->getPrimeiraTuplaDoResultSet(0);
         return $id;
        }
        
        public static function iniciaEtapaSePossivel(
            $idCorporacao,
            $novoEstado, 
            $estadosAnteriores, 
            $db = null,
            $estadosQueNaoPodemEstarSendoExecutadosNoMesmoInstante = null){
            if($db == null)
                $db = new Database();
            $strIn = "";
            if(is_array($estadosAnteriores)){
                 $strIn = Helper::arrayToString($estadosAnteriores, ",");
            } else {
                $strIn = $estadosAnteriores;
            }
            $strDatetime = Helper::getDiaEHoraAtualSQL();
            $q = "UPDATE sincronizacao  
                    SET processando_BOOLEAN = '1', 
                    data_ultimo_processamento_DATETIME = '$strDatetime'";
            if($novoEstado != null)
                    $q .= ", estado_sincronizacao_id_INT = '$novoEstado' ";

            $q .=" WHERE estado_sincronizacao_id_INT IN ($strIn)
                AND processando_BOOLEAN = '0'
                AND corporacao_id_INT = $idCorporacao
                "; 
            if($estadosQueNaoPodemEstarSendoExecutadosNoMesmoInstante  != null){
                $strIn2 = "";
                if(is_array($estadosQueNaoPodemEstarSendoExecutadosNoMesmoInstante))
                    $strIn2 = Helper::arrayToString ($estadosQueNaoPodemEstarSendoExecutadosNoMesmoInstante, ", ");
                else $strIn2 = $estadosQueNaoPodemEstarSendoExecutadosNoMesmoInstante;
                $q.= " AND (
                    SELECT COUNT(*) 
                    FROM (
                        SELECT COUNT(s2.id) 
                        FROM sincronizacao s2 
                        WHERE 
                            s2.id != id 
                            AND s2.processando_BOOLEAN = '1'
                            AND s2.estado_sincronizacao_id_INT IN ($strIn2)
                             AND s2.corporacao_id_INT = $idCorporacao
                        LIMIT 0,1
                    ) s3
                ) = 0 ";
            }

            $q .= " ORDER BY id ASC";
            $msg= $db->queryMensagem($q);
//            print_r($msg);
            if($msg == null ){
                if($novoEstado != null)
                    $msg = self::getIdNoEstado($idCorporacao, $novoEstado, $db);
                else 
                    $msg = self::getIdNoEstado($idCorporacao,$estadosAnteriores, $db);
                
                return $msg;
                
            } else return $msg;
        }
        
        
        
        public function getTabelasDaSincronizacao(){
            $this->database->queryMensagemThrowException("SELECT DISTINCT c.sistema_tabela_id_INT"
                . "    FROM crud c "
                . "     WHERE c.sincronizacao_id_INT = {$this->getId()} ");
                
            return Helper::getResultSetToArrayDeUmCampo($this->database->result);
            
        }
        
        public function getCrudsDaSincronizacao($idSistemaTabela, $idTipoOperacao){
             $q = "SELECT c.id, "
            . "     c.tipo_operacao_banco_id_INT tipoOperacao, "
            . "     c.crud_origem_id_INT crudOrigem, "
            . "     c.id_tabela_web_INT idTabelaWeb, "
            . "     sm.mobile_identificador_INT idMobileIdentificador "
            . " FROM crud c "
            . "     LEFT JOIN crud_mobile cm "
            . "         ON c.id = cm.crud_id_INT "
            . "     LEFT JOIN sincronizacao_mobile sm "
            . "         ON sm.id = cm.sincronizacao_mobile_id_INT "
            . " WHERE c.sincronizacao_id_INT = {$this->getId()} "
            . " AND c.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND c.tipo_operacao_banco_id_INT = $idTipoOperacao " 
            . " ORDER BY c.sistema_tabela_id_INT, c.id ";
            $this->database->queryMensagemThrowException($q);
            return Helper::getResultSetToMatriz($this->database->result, 1, 0);
            
        }
        
        public static function getUltimasSincronizacoes($ultimoIdSincronizacaoExcluso, $idAtualSincronizacaoExcluso, $db){
            $q = "SELECT id"
                . " FROM sincronizacao"
                . " WHERE id > $ultimoIdSincronizacaoExcluso "
                . " AND id < $idAtualSincronizacaoExcluso "
                . " AND estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::FINALIZADO;
            $db->queryMensagemThrowException($q);
            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }
        
        public static function getEstadoSincronizacao($idSinc, $db = null){
            $q = "SELECT estado_sincronizacao_id_INT "
                . " FROM sincronizacao "
                . " WHERE id=  $idSinc ";
            if($db == null) $db =new Database();
            $db->queryMensagemThrowException($q);
            return $db->getPrimeiraTuplaDoResultSet(0);
        }
        
        public function getCrudsRemocao( $idSistemaTabela){
             $q = "SELECT  c.id idCrud, "
                 . " c.id_tabela_web_INT idTabelaWeb,"
            . "     sm.mobile_identificador_INT idMobileIdentificador,"
            . "  c.crud_origem_id_INT idCrudOrigem,"
            . "     c.id_sistema_registro_sincronizador_INT idSistemaRegistroSincronizador, "
            . "  c.sistema_tabela_id_INT"
            . " FROM crud c "
            . "     LEFT JOIN crud_mobile cm "
            . "         ON c.id = cm.crud_id_INT "
            . "     LEFT JOIN sincronizacao_mobile sm "
            . "         ON sm.id = cm.sincronizacao_mobile_id_INT "
            . " WHERE c.sincronizacao_id_INT = {$this->getId()} "
            . " AND c.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND c.tipo_operacao_banco_id_INT = ".EXTDAO_Tipo_operacao_banco::Remover." "
            . " ORDER BY c.sistema_tabela_id_INT, c.id ";
            
            $this->database->queryMensagemThrowException($q);
            return Helper::getResultSetToMatriz($this->database->result, 1, 0);
            
        }
        
        
        
         public function downloadDadosSincronizacao($corporacao){

             if($this->getEstado_sincronizacao_id_INT() == EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO
                 || $this->getEstado_sincronizacao_id_INT() == EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO_MOBILE
             ){

                 $expre = I18N::getExpression("A sincronizacao {0} apresentou problemas",$this->getId());

                 return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SINCRONIZACAO,
                     $expre );
             }

            $configuracaoSite = Registry::get('ConfiguracaoSite');
            $dirUpload  = $configuracaoSite->PATH_CONTEUDO."sincronizacao_mobile/{$corporacao}/";
            if(!file_exists($dirUpload))
                Helper::mkdir($dirUpload, 0777, true);
            $nomeArquivo = $this->getCrud_ARQUIVO();
            
            
            $pathArquivo = $dirUpload.$nomeArquivo;
            if(strlen($pathArquivo) == 0 || !file_exists($pathArquivo))
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_ARQUIVO_SINCRONIZACAO_INEXISTENTE, "Arquivo de sincronização inexistente: $pathArquivo");
//            echo "entrou: ".$this->getId();
//            exit();
            $objDownload = new Download($pathArquivo);
            $fp = $objDownload->df_download();
            
            if ($fp){
                return null;
            }
            else
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "[lknmnvief]Falha durante o download do arquivo $pathArquivo.");
            

        }
        
        
        public function formatarParaSQL(){

            if($this->limite_mobile_INT == ""){

                    $this->limite_mobile_INT = "null";

            }

            if($this->estado_sincronizacao_id_INT == ""){

                    $this->estado_sincronizacao_id_INT = "null";

            }

            if($this->seq_INT == ""){

                    $this->seq_INT = "null";

            }

            if($this->id_ultimo_sistema_registro_sincronizador_INT == ""){

                    $this->id_ultimo_sistema_registro_sincronizador_INT = "null";

            }

            if($this->quantidade_crud_processado_INT == ""){

                    $this->quantidade_crud_processado_INT = "null";

            }

            if($this->processando_BOOLEAN == ""){

                    $this->processando_BOOLEAN = "null";

            }
			
			if($this->corporacao_id_INT == ""){

				$this->corporacao_id_INT = "null";

			}

            $val = $this->erro;
            if(strlen($val)){
                $val = Helper::substring( $val, 512);
                $val  = $this->database->realEscapeString($val);
                $this->erro = $val ;
            }

            $this->data_inicial_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicial_DATETIME); 
            $this->data_final_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_final_DATETIME); 
            $this->data_ultimo_processamento_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ultimo_processamento_DATETIME); 
            
            

        }
        
        public function getIdentificador(){
            return "Sincronizacao: ". $this->id.".";;
        }
        
	}

    
