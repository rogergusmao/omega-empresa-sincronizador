<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Sistema_atributo
* NOME DA CLASSE DAO: DAO_Sistema_atributo
* DATA DE GERAÇÃO:    19.07.2014
* ARQUIVO:            EXTDAO_Sistema_atributo.php5
* TABELA MYSQL:       sistema_atributo
* BANCO DE DADOS:     sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Sistema_atributo extends DAO_Sistema_atributo
{

    public function __construct($db=null, $setLabels=false){

        parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Sistema_atributo";


        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_nome = "Nome";
        $this->label_sistema_tabela_id_INT = "Sistema Tabela";
        $this->label_tipo_sql = "Tipo Sql";
        $this->label_tamanho_INT = "Tamanho";
        $this->label_decimal_INT = "Decimal";
        $this->label_not_null_BOOLEAN = "Not Null";
        $this->label_primary_key_BOOLEAN = "Chave Primária?";
        $this->label_auto_increment_BOOLEAN = "Auto Incremento";
        $this->label_valor_default = "Valor Default";
        $this->label_fk_sistema_tabela_id_INT = "Tabela Fk";
        $this->label_atributo_fk = "Atributo Fk";
        $this->label_fk_nome = "Nome";
        $this->label_update_tipo_fk = "Update Tipo Fk";
        $this->label_delete_tipo_fk = "Delete Tipo Fk";
        $this->label_label = "Label";
        $this->label_unique_BOOLEAN = "única?";
        $this->label_unique_nome = "Nome Chave única";
        $this->label_seq_INT = "Sequência";


    }

    public function setDiretorios(){



    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Sistema_atributo();

    }

    public static function getAtributosQueApontamParaATabela($idSistemaTabela, $db){
        $q = "SELECT id, nome, atributo_fk, update_tipo_fk, delete_tipo_fk, fk_sistema_tabela_id_INT "
            . " FROM sistema_atributo"
            . " WHERE fk_sistema_tabela_id_INT = '{$idSistemaTabela}' "
            . "     AND fk_sistema_tabela_id_INT IS NOT NULL  ";
        $db->queryMensagemThrowException($q);
        return Helper::getResultSetToMatriz($db->result);
    }

    public static function getAtributosFkDaTabela($idSistemaTabela, $db = null){
        if($db == null)
            $db = new Database();
         if(Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk == null)
                    Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk = array();
                
        if(!isset(Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk[$db->getDBName()] ))
            Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk[$db->getDBName()] = array();
        else if(isset(Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk[$db->getDBName()][$idSistemaTabela] )) 
            return Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk[$db->getDBName()][$idSistemaTabela];
          $q = "SELECT id, nome, atributo_fk, update_tipo_fk, delete_tipo_fk, fk_sistema_tabela_id_INT "
            . " FROM sistema_atributo"
            . " WHERE sistema_tabela_id_INT = '{$idSistemaTabela}' "
            . "     AND fk_sistema_tabela_id_INT IS NOT NULL  ";
        $db->queryMensagemThrowException($q);
        $vetor = Helper::getResultSetToMatriz($db->result);
        Database::$cacheDatabaseIsSistemaTabelaPorAtributosFk[$db->getDBName()][$idSistemaTabela] = $vetor;
        return $vetor;
    }
    
    public static function formataAtributosFk($idSistemaTabela, $atributos, $atributosFk, $idCache){
       
        
        if(Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados == null)
            Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados = array();
                
        if(! isset(Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados[$idCache]))
            Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados[$idCache] = array();
        else if(isset(Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados[$idCache][$idSistemaTabela] ))
            return Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados[$idCache][$idSistemaTabela];
        
        $vetor = array();
        for ($j = 0; $j < count($atributos); $j++) {
//            echo $attr;
            $attr = $atributos[$j];
            $vetor[$j] = null;
            for($i = 0 ; $i < count($atributosFk); $i++){
                if($attr == $atributosFk[$i]['nome']){
                    $vetor[$j] = $atributosFk[$i];
                    break;
                }
            }
        }
        Database::$cacheDatabaseIdSistemaTabelaPorAtributosFkFormatados[$idCache][$idSistemaTabela] = $vetor;
        return $vetor;
    }

}

    
