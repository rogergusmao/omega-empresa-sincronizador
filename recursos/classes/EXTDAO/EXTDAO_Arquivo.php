<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Arquivo
* NOME DA CLASSE DAO: DAO_Arquivo
* DATA DE GERAÇÃO:    21.12.2013
* ARQUIVO:            EXTDAO_Arquivo.php5
* TABELA MYSQL:       arquivo
* BANCO DE DADOS:     blog_da_ale
* -------------------------------------------------------
*
*/

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Arquivo extends DAO_Arquivo
{

    public function __construct($db=null, $setLabels=false){

        parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Arquivo";

        if($id != 0){

            $this->select($id);

        }

        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_nome_ARQUIVO = "Nome";
        $this->label_noticia_id_INT = "Notícia";
        $this->label_tipo_arquivo_id_INT = "Tipo de Arquivo";
        $this->label_legenda = "Legenda";


    }

    public function setDiretorios(){
        $configuracaoSite = Registry::get('ConfiguracaoSite');

        $this->diretorio_nome_ARQUIVO = $configuracaoSite->PATH_CONTEUDO."conteudo/imgs/arquivo/";        //caminho a partir de da raiz, com '/' no final


    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Arquivo();

    }



    public function __uploadNome_ARQUIVO($numRegistro=1, $urlErro){

        $dirUpload  = $this->diretorio_nome_ARQUIVO;
        $labelCampo = $this->label_nome_ARQUIVO;

        $objUpload = new Upload();

        $objUpload->arrPermitido = "";
        $objUpload->tamanhoMax = "";
        $objUpload->file = $_FILES["nome_ARQUIVO{$numRegistro}"];
        $nomeDoArquivoDeEntrada = $objUpload->file["name"];
        $objUpload->nome = Helper::getNomeAleatorio()."_". $this->getId().".".Helper::getExtensaoDoArquivo($objUpload->file["name"]);


        $objUpload->uploadPath = $dirUpload;
        $success = $objUpload->uploadFile();

        if(!$success){

            $_SESSION["erro"] = true;
            $this->createSession();
            return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
            exit();

        }
        else{

            return array($objUpload->nome, $dirUpload);

        }

    }


    public function delete($id)
    {
        $nomeArquivo = $this->getNome_ARQUIVO();
        if(strlen($nomeArquivo)){
            $pathArquivo = $this->diretorio_nome_ARQUIVO.$nomeArquivo;
            if(file_exists($pathArquivo))
                unlink ($pathArquivo);
        }
        $sql = "DELETE FROM arquivo WHERE id = $id;";
        $this->database->queryMensagemThrowException($sql);

    }


}

    
