<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Corporacao
    * NOME DA CLASSE DAO: DAO_Corporacao
    * DATA DE GERAÇÃO:    13.02.2017
    * ARQUIVO:            EXTDAO_Corporacao.php5
    * TABELA MYSQL:       corporacao
    * BANCO DE DADOS:     sincronizador_web_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Corporacao extends DAO_Corporacao
    {

        public function __construct($db = null, $setLabels = false){

            parent::__construct($db);

            $this->nomeClasse = "EXTDAO_Corporacao";
            if($setLabels)
            $this->setLabels();

            $this->setDiretorios();
            $this->setDimensoesImagens();
        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";
			$this->label_nome_normalizado = "Nome Normalizado";
			$this->label_usuario_dropbox = "Usuário Dropbox";
			$this->label_senha_dropbox = "Senha Dropbox";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Corporacao();

        }

        
        public static function cadastra($idCorporacao, $corporacao){
            $db = null;
            try {
                ConfiguracaoCorporacaoDinamica::clearConfiguracaoSiteFromCache($corporacao);
                $db = new Database();
                $idExistente = EXTDAO_Corporacao::getIdCorporacao($corporacao, $db);
                if(!empty($idExistente)){
                    return new Mensagem(null, "Sucesso! J� tinhamos o registro em quest�o cadastradao. [0] - $idExistente");
                }


                $db->iniciarTransacao();
                $q = null;
                if(EXTDAO_Corporacao::existeIdCorporacao($db, $idCorporacao)){
                    $q = "UPDATE corporacao SET nome = '$corporacao', nome_normalizado='{$corporacao}' WHERE id={$idCorporacao}";
                } else {
                    $q = "INSERT INTO corporacao (id, nome, nome_normalizado)"
                        . " VALUES ($idCorporacao, '$corporacao', '$corporacao')";
                }

                $msg = $db->queryMensagem($q);
                $msgSucesso = "Corporacao cadastrada com sucesso. $q. Banco: ".$db->getDBName();
                if($msg != null && $msg->erro()){
                    if($db !=null)$db->rollbackTransacao();
                    if($msg->mValor == Database::CHAVE_DUPLICADA){
                        
                        return new Mensagem(null, "Sucesso! J� tinhamos o registro em quest�o cadastradao. [A] - ".$msgSucesso);        
                    }
                    return $msg;
                }
                //$dataUltimaMigracao = EXTDAO_Corporacao::updateDataUltimaMigracao($db);
                $db->commitTransacao();
                //return new Mensagem_token(null, "[B] - ".$msgSucesso, $dataUltimaMigracao);
                return new Mensagem_token(null, "[B] - ".$msgSucesso);
            } catch (Exception $exc) {
                if($db != null)
                    $db->rollbackTransacao();
                return new Mensagem(null,null,$exc);
            }

        }



        public static function getDataUltimaMigracao($db , $idCorporacao){
            $q = "SELECT ultima_migracao_DATETIME FROM corporacao WHERE id = $idCorporacao";

            $db->query($q);
            $data = Helper::getResultSetToPrimeiroResultado($db->result);

            return $data;

        }

        public static function updateDataUltimaMigracao($db){

            $data = Helper::getDiaEHoraAtualSQL();
            $q = "UPDATE corporacao SET ultima_migracao_DATETIME = '$data'";
            $db->query($q);
            return $data;
        }

        public static function procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao){
            $ult = EXTDAO_Corporacao::getDataUltimaMigracao($db, $idCorporacao);

            if(!empty($ult) && $ult > $ultimaMigracao){

                HelperLog::logTemp("A estrutura foi migrada e a atual data � $ult > $ultimaMigracao");
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ESTRUTURA_MIGRADA,
                        "A estrutura foi migrada e a atual data � $ult > $ultimaMigracao",
                        $ult);
            }
            return null;
        }

        public static function cadastraNaReserva($idCorporacao, $corporacao){
            try {

                $db = new Database();


                $q = "UPDATE corporacao SET nome = '$corporacao', nome_normalizado='$corporacao' WHERE id = $idCorporacao";
                $msg = $db->queryMensagem($q);

                //$dataUltimaMigracao = EXTDAO_Corporacao::updateDataUltimaMigracao($db);

                //return new Mensagem_token(null, "[B] - ".$msgSucesso, $dataUltimaMigracao);
                return $msg;
            } catch (Exception $exc) {
                return new Mensagem(null,null,$exc);
            }

        }


        public static function getIdCorporacao($nome, $db = null)
        {
            if (strlen($nome) > 0) {
                if ($db == null)
                    $objBanco = new Database();
                else $objBanco = $db;

                //checa a existencia do funcionario
                $query = "SELECT u.id as id
                 FROM corporacao u
                 WHERE u.nome = '$nome' ";


                $objBanco->query($query);
                $id = $objBanco->getPrimeiraTuplaDoResultSet(0);
//                $objBanco->close();
                //o usuario ja eh existente
                if ($id) {

                    return $id;
                } else
                    return null;
            }
        }

        public static function updateNome(Database $db, $idCorporacao, $nomeCorporacao){
            $q = "UPDATE corporacao 
              SET nome='$nomeCorporacao', nome_normalizado='$nomeCorporacao'
              WHERE id=$idCorporacao";
            return $db->queryMensagem($q);
        }

        public static function existeIdCorporacao(Database $db, $id){
            $db->query("SELECT 1 FROM corporacao WHERE id=$id");
            return $db->getPrimeiraTuplaDoResultSet(0) == 1 ? true : false;
        }
	}

    
