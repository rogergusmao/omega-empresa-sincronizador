<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Crud_mobile_dependente
* NOME DA CLASSE DAO: DAO_Crud_mobile_dependente
* DATA DE GERA��O:    19.06.2014
* ARQUIVO:            EXTDAO_Crud_mobile_dependente.php5
* TABELA MYSQL:       crud_mobile_dependente
* BANCO DE DADOS:     sincronizador_web
* -------------------------------------------------------
*
*/

// **********************
// DECLARA��O DA CLASSE
// **********************

class EXTDAO_Crud_mobile_dependente extends DAO_Crud_mobile_dependente
{

    public function __construct($db=null, $setLabels=false){

        parent::__construct($db);

        $this->nomeClasse = "EXTDAO_Crud_mobile_dependente";


        if($setLabels){

            $this->setLabels();

        }

        $this->setDiretorios();
        $this->setDimensoesImagens();


    }

    public function setLabels(){

        $this->label_id = "Id";
        $this->label_crud_id_INT = "Crud";
        $this->label_fk_crud_id_INT = "Fk Crud";


    }

    public function setDiretorios(){



    }

    public function setDimensoesImagens(){



    }

    public function factory(){

        return new EXTDAO_Crud_mobile_dependente();

    }

    public static function getCrudsDependentesDoCrudMobile(
        $idCorporacao, 
        $idCrudMobile, 
        $idSincronizacaoMobile, 
        $db = null, 
        $tiposOperacoes = null){

        if($db == null)
            $db = new Database();
        $q = "SELECT cm.id idCrudMobile, "
            . " cm.tipo_operacao_banco_id_INT tipoOperacao, "
            . " cmd.id_sincronizador_mobile_INT idSincronizadorMobile, "
            . " cmd.atributo, "
            . " st.nome tabela"
            . " FROM crud_mobile_dependente cmd "
            . "     JOIN crud_mobile cm"
            . "         ON cmd.crud_mobile_id_INT = cm.id "
            . "     JOIN sistema_tabela st"
            . "         ON cm.sistema_tabela_id_INT = st.id "
            . " WHERE cm.sincronizacao_mobile_id_INT = $idSincronizacaoMobile "
            . " AND cmd.crud_mobile_id_INT = $idCrudMobile"
            . " AND cm.corporacao_id_INT = $idCorporacao";
        if(count($tiposOperacoes )){
            $q .= " AND cm.tipo_operacao_banco_id_INT IN (".Helper::arrayToString($tiposOperacoes).") ";

        }
        $q .= " ORDER BY cmd.id ";
//            echo $q;

        $db->query($q);
        
        $vetor = Helper::getResultSetToMatriz($db->result, 1, 0);
        return $vetor;
    }
    public static function getCrudsDependentes(
        $idCorporacao,
        $idSincronizadorMobile, 
        $idCrudMobile, 
        $db = null, 
        $tiposOperacoes = null){

        if($db == null)
            $db = new Database();
        $q = "SELECT
	cm2.id idCrudMobile,
	cm2.tipo_operacao_banco_id_INT tipoOperacao,
	cmd.atributo,
	st.nome tabela
FROM crud_mobile_dependente cmd
JOIN crud_mobile cm ON cmd.id_sincronizador_mobile_INT = cm.id_sincronizador_mobile_INT
JOIN crud_mobile cm2 ON cm2.id = cmd.crud_mobile_id_INT
JOIN sistema_tabela st ON cm2.sistema_tabela_id_INT = st.id
WHERE cm.id = $idCrudMobile AND cm2.sincronizacao_mobile_id_INT = cm.sincronizacao_mobile_id_INT 
      AND cm.corporacao_id_INT = $idCorporacao 
      AND cm2.corporacao_id_INT = $idCorporacao ";
        if(count($tiposOperacoes )){
            $q .= " AND cm2.tipo_operacao_banco_id_INT IN (".Helper::arrayToString($tiposOperacoes).") ";

        }
        $q .= " ORDER BY cmd.id ";
//            echo $q;

        $db->query($q);
        
        $vetor = Helper::getResultSetToMatriz($db->result, 1, 0);
        return $vetor;
    }



}

    