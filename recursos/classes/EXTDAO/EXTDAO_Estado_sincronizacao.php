<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Estado_sincronizacao
    * NOME DA CLASSE DAO: DAO_Estado_sincronizacao
    * DATA DE GERAÇÃO:    19.06.2014
    * ARQUIVO:            EXTDAO_Estado_sincronizacao.php
    * TABELA MYSQL:       estado_sincronizacao
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Estado_sincronizacao extends DAO_Estado_sincronizacao
    {
        
        //PROCESSO 1
        const INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO = 1; 
        //PROCESSO 2
        const PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB = 2;
        //PROCESSO 3
        const ERRO_SINCRONIZACAO_MOBILE = 3;
        //PROCESSO 4
        const ERRO_SINCRONIZACAO = 4;
        //PROCESSO 5
        const INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO = 5;
        //PROCESSO 6
        const GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR = 6;
        //PROCESSO 7
        const FINALIZADO = 7;
        
        const RESETADO = 8;


        
        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Estado_sincronizacao";

          

            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Estado_sincronizacao();

        }

	}

    
