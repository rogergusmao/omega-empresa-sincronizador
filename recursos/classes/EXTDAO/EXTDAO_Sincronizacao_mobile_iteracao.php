<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sincronizacao_mobile_iteracao
    * NOME DA CLASSE DAO: DAO_Sincronizacao_mobile_iteracao
    * DATA DE GERAÇÃO:    08.10.2016
    * ARQUIVO:            EXTDAO_Sincronizacao_mobile_iteracao.php5
    * TABELA MYSQL:       sincronizacao_mobile_iteracao
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sincronizacao_mobile_iteracao extends DAO_Sincronizacao_mobile_iteracao
    {
        const ENUM_ORIGEM_ANDROID = 'A';
        const ENUM_ORIGEM_WEB = 'W';
        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Sincronizacao_mobile_iteracao";

           
            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_sincronizacao_mobile_id_INT = "Sincronizacao Mobile";
			$this->label_estado_sincronizacao_mobile_id_INT = "Estado";
			$this->label_ocorrencia_DATETIME = "Data de Ocorrencia";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Sincronizacao_mobile_iteracao();

        }
        public static function inserirLista(
            $db,
            $strIdsSincronizadorMobile,
            $estado){
            $str = "";
           
            if(is_array($strIdsSincronizadorMobile))
                    $str = Helper::arrayToString ($strIdsSincronizadorMobile, ",");
            else $str = $strIdsSincronizadorMobile;

            $q = "SELECT id "
                . " FROM sincronizacao_mobile "
                . " WHERE id in ($str) "
                . " AND estado_sincronizacao_mobile_id_INT = $estado";
            $msg =  $db->queryMensagem($q);
            if($msg != null && $msg->erro()){
                return $msg;
            }
            $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
                            
            
            
            
            if(count($ids)){
                
                $qInsert = "INSERT INTO sincronizacao_mobile_iteracao ("
                . "sincronizacao_mobile_id_INT, "
                . "estado_sincronizacao_mobile_id_INT,"
                . "ocorrencia_DATETIME) "
                . "VALUES  ";
                $strDate = Helper::getDiaEHoraAtualSQL();
                for($i = 0 ; $i < count($ids); $i++){
                    if($i > 0 ) $qInsert .= ", ";
                    $qInsert .= "( {$ids[$i]}, ".$estado.", '".$strDate."')" ;
                }
                
                $msg = $db->queryMensagem($qInsert);
               
                if($msg != null && $msg->erro())return $msg;
            }
            
        }
        public static function inserir(
            Database $db,
            $idSM,
            $idEstadoSincronizadorMobile,
            $strData = null){

            if($strData == null){
                $strData = Helper::getDiaEHoraAtualSQL();
            }
            $origem = Helper::POSTGET("mobile_identificador") != null 
                ? EXTDAO_Sincronizacao_mobile_iteracao::ENUM_ORIGEM_ANDROID 
                : EXTDAO_Sincronizacao_mobile_iteracao::ENUM_ORIGEM_WEB;
            
            $q = "INSERT INTO sincronizacao_mobile_iteracao ( "
                . "sincronizacao_mobile_id_INT, "
                . "estado_sincronizacao_mobile_id_INT, "
                . "ocorrencia_DATETIME, "
                . "enum_origem_chamada) "
                . "VALUES ( {$idSM}, ".$idEstadoSincronizadorMobile.", '".$strData."', '$origem')";
            $msg = $db->queryMensagem($q);
            if($msg != null && $msg->erro()){
                return $msg;
            }    
            return null;
        }

	}

    
