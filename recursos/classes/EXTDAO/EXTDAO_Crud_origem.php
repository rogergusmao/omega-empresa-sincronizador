<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Crud_origem
    * NOME DA CLASSE DAO: DAO_Crud_origem
    * DATA DE GERA��O:    19.06.2014
    * ARQUIVO:            EXTDAO_Crud_origem.php
    * TABELA MYSQL:       crud_origem
    * BANCO DE DADOS:     sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class EXTDAO_Crud_origem extends DAO_Crud_origem
    {
        const MOBILE = 1;
        const WEB = 2;

        public function __construct($db=null, $setLabels=false){

            parent::__construct($db);

            	$this->nomeClasse = "EXTDAO_Crud_origem";


            if($setLabels){

                $this->setLabels();

            }

            $this->setDiretorios();
            $this->setDimensoesImagens();


        }

        public function setLabels(){

			$this->label_id = "Id";
			$this->label_nome = "Nome";


        }

        public function setDiretorios(){



        }

        public function setDimensoesImagens(){



        }

        public function factory(){

            return new EXTDAO_Crud_origem();

        }

	}

    