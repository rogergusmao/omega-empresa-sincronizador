<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sincronizacao_mobile
    * DATA DE GERAÇÃO: 19.02.2017
    * ARQUIVO:         DAO_Sincronizacao_mobile.php
    * TABELA MYSQL:    sincronizacao_mobile
    * BANCO DE DADOS:  sincronizador_web_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Sincronizacao_mobile extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $mobile_identificador_INT;
	public $mobile_conectado_INT;
	public $estado_sincronizacao_mobile_id_INT;
	public $objEstado_sincronizacao_mobile;
	public $data_inicial_DATETIME;
	public $data_final_DATETIME;
	public $resetando_BOOLEAN;
	public $sincronizacao_id_INT;
	public $objSincronizacao;
	public $seq_INT;
	public $descricao;
	public $crud_mobile_ARQUIVO;
	public $crud_web_para_mobile_ARQUIVO;
	public $ultimo_crud_id_INT;
	public $objUltimo_crud;
	public $ultima_verificacao_DATETIME;
	public $erro;
	public $erro_sincronizacao_id_INT;
	public $objErro_sincronizacao;
	public $primeira_vez_BOOLEAN;
	public $id_primeiro_sinc_android_web_INT;
	public $id_ultimo_sinc_android_web_INT;
	public $corporacao_id_INT;
	public $objCorporacao;


    public $nomeEntidade;

	public $data_inicial_DATETIME_UNIX;
	public $data_final_DATETIME_UNIX;
	public $ultima_verificacao_DATETIME_UNIX;


    

	public $label_id;
	public $label_mobile_identificador_INT;
	public $label_mobile_conectado_INT;
	public $label_estado_sincronizacao_mobile_id_INT;
	public $label_data_inicial_DATETIME;
	public $label_data_final_DATETIME;
	public $label_resetando_BOOLEAN;
	public $label_sincronizacao_id_INT;
	public $label_seq_INT;
	public $label_descricao;
	public $label_crud_mobile_ARQUIVO;
	public $label_crud_web_para_mobile_ARQUIVO;
	public $label_ultimo_crud_id_INT;
	public $label_ultima_verificacao_DATETIME;
	public $label_erro;
	public $label_erro_sincronizacao_id_INT;
	public $label_primeira_vez_BOOLEAN;
	public $label_id_primeiro_sinc_android_web_INT;
	public $label_id_ultimo_sinc_android_web_INT;
	public $label_corporacao_id_INT;


	public $diretorio_crud_mobile_ARQUIVO;
	public $diretorio_crud_web_para_mobile_ARQUIVO;




    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sincronizacao_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
	function getObjEXTDAO_Estado_sincronizacao_mobile(){
	if($this->objEstado_sincronizacao_mobile ==null)
		$this->objEstado_sincronizacao_mobile = new EXTDAO_Estado_sincronizacao_mobile($this->getDatabase());
	return $this->objEstado_sincronizacao_mobile ;
	}
	function getObjEXTDAO_Sincronizacao(){
	if($this->objSincronizacao ==null)
		$this->objSincronizacao = new EXTDAO_Sincronizacao($this->getDatabase());
	return $this->objSincronizacao ;
	}
	function getObjEXTDAO_Ultimo_crud(){
	if($this->objUltimo_crud ==null)
		$this->objUltimo_crud = new EXTDAO_Ultimo_crud($this->getDatabase());
	return $this->objUltimo_crud ;
	}
	function getObjEXTDAO_Erro_sincronizacao(){
	if($this->objErro_sincronizacao ==null)
		$this->objErro_sincronizacao = new EXTDAO_Erro_sincronizacao($this->getDatabase());
	return $this->objErro_sincronizacao ;
	}
	function getObjEXTDAO_Corporacao(){
	if($this->objCorporacao ==null)
		$this->objCorporacao = new EXTDAO_Corporacao($this->getDatabase());
	return $this->objCorporacao ;
	}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllEstado_sincronizacao_mobile($objArgumentos){

		$objArgumentos->nome="estado_sincronizacao_mobile_id_INT";
		$objArgumentos->id="estado_sincronizacao_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Estado_sincronizacao_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSincronizacao($objArgumentos){

		$objArgumentos->nome="sincronizacao_id_INT";
		$objArgumentos->id="sincronizacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Sincronizacao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllUltimo_crud($objArgumentos){

		$objArgumentos->nome="ultimo_crud_id_INT";
		$objArgumentos->id="ultimo_crud_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Ultimo_crud()->getComboBox($objArgumentos);

	}

public function getComboBoxAllErro_sincronizacao($objArgumentos){

		$objArgumentos->nome="erro_sincronizacao_id_INT";
		$objArgumentos->id="erro_sincronizacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Erro_sincronizacao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCorporacao($objArgumentos){

		$objArgumentos->nome="corporacao_id_INT";
		$objArgumentos->id="corporacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Corporacao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Crud_mobile_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_mobile_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_mobile_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_mobile_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Crud_web_para_mobile_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_web_para_mobile_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_web_para_mobile_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_web_para_mobile_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Crud_mobile_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_mobile_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_mobile_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_mobile_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Crud_web_para_mobile_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_web_para_mobile_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_web_para_mobile_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_web_para_mobile_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Crud_mobile_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_mobile_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_mobile_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_mobile_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Crud_web_para_mobile_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_web_para_mobile_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_web_para_mobile_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_web_para_mobile_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sincronizacao_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sincronizacao_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMOÇÃO DO ARQUIVO Crud_mobile_ARQUIVO
            $pathArquivo = $this->diretorio_crud_mobile_ARQUIVO . $this->getCrud_mobile_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMOÇÃO DO ARQUIVO Crud_web_para_mobile_ARQUIVO
            $pathArquivo = $this->diretorio_crud_web_para_mobile_ARQUIVO . $this->getCrud_web_para_mobile_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadCrud_mobile_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_crud_mobile_ARQUIVO;
            $labelCampo = $this->label_crud_mobile_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["crud_mobile_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadCrud_web_para_mobile_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_crud_web_para_mobile_ARQUIVO;
            $labelCampo = $this->label_crud_web_para_mobile_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["crud_web_para_mobile_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_2." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getMobile_identificador_INT()
    {
    	return $this->mobile_identificador_INT;
    }
    
    public function getMobile_conectado_INT()
    {
    	return $this->mobile_conectado_INT;
    }
    
    public function getEstado_sincronizacao_mobile_id_INT()
    {
    	return $this->estado_sincronizacao_mobile_id_INT;
    }
    
    function getData_inicial_DATETIME_UNIX()
    {
    	return $this->data_inicial_DATETIME_UNIX;
    }
    
    public function getData_inicial_DATETIME()
    {
    	return $this->data_inicial_DATETIME;
    }
    
    function getData_final_DATETIME_UNIX()
    {
    	return $this->data_final_DATETIME_UNIX;
    }
    
    public function getData_final_DATETIME()
    {
    	return $this->data_final_DATETIME;
    }
    
    public function getResetando_BOOLEAN()
    {
    	return $this->resetando_BOOLEAN;
    }
    
    public function getSincronizacao_id_INT()
    {
    	return $this->sincronizacao_id_INT;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    public function getDescricao()
    {
    	return $this->descricao;
    }
    
    public function getCrud_mobile_ARQUIVO()
    {
    	return $this->crud_mobile_ARQUIVO;
    }
    
    public function getCrud_web_para_mobile_ARQUIVO()
    {
    	return $this->crud_web_para_mobile_ARQUIVO;
    }
    
    public function getUltimo_crud_id_INT()
    {
    	return $this->ultimo_crud_id_INT;
    }
    
    function getUltima_verificacao_DATETIME_UNIX()
    {
    	return $this->ultima_verificacao_DATETIME_UNIX;
    }
    
    public function getUltima_verificacao_DATETIME()
    {
    	return $this->ultima_verificacao_DATETIME;
    }
    
    public function getErro()
    {
    	return $this->erro;
    }
    
    public function getErro_sincronizacao_id_INT()
    {
    	return $this->erro_sincronizacao_id_INT;
    }
    
    public function getPrimeira_vez_BOOLEAN()
    {
    	return $this->primeira_vez_BOOLEAN;
    }
    
    public function getId_primeiro_sinc_android_web_INT()
    {
    	return $this->id_primeiro_sinc_android_web_INT;
    }
    
    public function getId_ultimo_sinc_android_web_INT()
    {
    	return $this->id_ultimo_sinc_android_web_INT;
    }
    
    public function getCorporacao_id_INT()
    {
    	return $this->corporacao_id_INT;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setMobile_identificador_INT($val)
    {
    	$this->mobile_identificador_INT =  $val;
    }
    
    function setMobile_conectado_INT($val)
    {
    	$this->mobile_conectado_INT =  $val;
    }
    
    function setEstado_sincronizacao_mobile_id_INT($val)
    {
    	$this->estado_sincronizacao_mobile_id_INT =  $val;
    }
    
    function setData_inicial_DATETIME($val)
    {
    	$this->data_inicial_DATETIME =  $val;
    }
    
    function setData_final_DATETIME($val)
    {
    	$this->data_final_DATETIME =  $val;
    }
    
    function setResetando_BOOLEAN($val)
    {
    	$this->resetando_BOOLEAN =  $val;
    }
    
    function setSincronizacao_id_INT($val)
    {
    	$this->sincronizacao_id_INT =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    
    function setDescricao($val)
    {
    	$this->descricao =  $val;
    }
    
    function setCrud_mobile_ARQUIVO($val)
    {
    	$this->crud_mobile_ARQUIVO =  $val;
    }
    
    function setCrud_web_para_mobile_ARQUIVO($val)
    {
    	$this->crud_web_para_mobile_ARQUIVO =  $val;
    }
    
    function setUltimo_crud_id_INT($val)
    {
    	$this->ultimo_crud_id_INT =  $val;
    }
    
    function setUltima_verificacao_DATETIME($val)
    {
    	$this->ultima_verificacao_DATETIME =  $val;
    }
    
    function setErro($val)
    {
    	$this->erro =  $val;
    }
    
    function setErro_sincronizacao_id_INT($val)
    {
    	$this->erro_sincronizacao_id_INT =  $val;
    }
    
    function setPrimeira_vez_BOOLEAN($val)
    {
    	$this->primeira_vez_BOOLEAN =  $val;
    }
    
    function setId_primeiro_sinc_android_web_INT($val)
    {
    	$this->id_primeiro_sinc_android_web_INT =  $val;
    }
    
    function setId_ultimo_sinc_android_web_INT($val)
    {
    	$this->id_ultimo_sinc_android_web_INT =  $val;
    }
    
    function setCorporacao_id_INT($val)
    {
    	$this->corporacao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_inicial_DATETIME) AS data_inicial_DATETIME_UNIX, UNIX_TIMESTAMP(data_final_DATETIME) AS data_final_DATETIME_UNIX, UNIX_TIMESTAMP(ultima_verificacao_DATETIME) AS ultima_verificacao_DATETIME_UNIX FROM sincronizacao_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && ($msg->erro() || $msg->resultadoVazio()))
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->mobile_identificador_INT = $row->mobile_identificador_INT;
        
        $this->mobile_conectado_INT = $row->mobile_conectado_INT;
        
        $this->estado_sincronizacao_mobile_id_INT = $row->estado_sincronizacao_mobile_id_INT;
        if(isset($this->objEstado_sincronizacao_mobile))
			$this->objEstado_sincronizacao_mobile->select($this->estado_sincronizacao_mobile_id_INT);

        $this->data_inicial_DATETIME = $row->data_inicial_DATETIME;
        $this->data_inicial_DATETIME_UNIX = $row->data_inicial_DATETIME_UNIX;

        $this->data_final_DATETIME = $row->data_final_DATETIME;
        $this->data_final_DATETIME_UNIX = $row->data_final_DATETIME_UNIX;

        $this->resetando_BOOLEAN = $row->resetando_BOOLEAN;
        
        $this->sincronizacao_id_INT = $row->sincronizacao_id_INT;
        if(isset($this->objSincronizacao))
			$this->objSincronizacao->select($this->sincronizacao_id_INT);

        $this->seq_INT = $row->seq_INT;
        
        $this->descricao = $row->descricao;
        
        $this->crud_mobile_ARQUIVO = $row->crud_mobile_ARQUIVO;
        
        $this->crud_web_para_mobile_ARQUIVO = $row->crud_web_para_mobile_ARQUIVO;
        
        $this->ultimo_crud_id_INT = $row->ultimo_crud_id_INT;
        if(isset($this->objUltimo_crud))
			$this->objUltimo_crud->select($this->ultimo_crud_id_INT);

        $this->ultima_verificacao_DATETIME = $row->ultima_verificacao_DATETIME;
        $this->ultima_verificacao_DATETIME_UNIX = $row->ultima_verificacao_DATETIME_UNIX;

        $this->erro = $row->erro;
        
        $this->erro_sincronizacao_id_INT = $row->erro_sincronizacao_id_INT;
        if(isset($this->objErro_sincronizacao))
			$this->objErro_sincronizacao->select($this->erro_sincronizacao_id_INT);

        $this->primeira_vez_BOOLEAN = $row->primeira_vez_BOOLEAN;
        
        $this->id_primeiro_sinc_android_web_INT = $row->id_primeiro_sinc_android_web_INT;
        
        $this->id_ultimo_sinc_android_web_INT = $row->id_ultimo_sinc_android_web_INT;
        
        $this->corporacao_id_INT = $row->corporacao_id_INT;
        if(isset($this->objCorporacao))
			$this->objCorporacao->select($this->corporacao_id_INT);


    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sincronizacao_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sincronizacao_mobile ( mobile_identificador_INT,mobile_conectado_INT,estado_sincronizacao_mobile_id_INT,data_inicial_DATETIME,data_final_DATETIME,resetando_BOOLEAN,sincronizacao_id_INT,seq_INT,descricao,crud_mobile_ARQUIVO,crud_web_para_mobile_ARQUIVO,ultimo_crud_id_INT,ultima_verificacao_DATETIME,erro,erro_sincronizacao_id_INT,primeira_vez_BOOLEAN,id_primeiro_sinc_android_web_INT,id_ultimo_sinc_android_web_INT,corporacao_id_INT ) VALUES ( $this->mobile_identificador_INT,$this->mobile_conectado_INT,$this->estado_sincronizacao_mobile_id_INT,$this->data_inicial_DATETIME,$this->data_final_DATETIME,$this->resetando_BOOLEAN,$this->sincronizacao_id_INT,$this->seq_INT,'$this->descricao','$this->crud_mobile_ARQUIVO','$this->crud_web_para_mobile_ARQUIVO',$this->ultimo_crud_id_INT,$this->ultima_verificacao_DATETIME,'$this->erro',$this->erro_sincronizacao_id_INT,$this->primeira_vez_BOOLEAN,$this->id_primeiro_sinc_android_web_INT,$this->id_ultimo_sinc_android_web_INT,$this->corporacao_id_INT )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoMobile_identificador_INT(){ 

		return "mobile_identificador_INT";

	}

	public function nomeCampoMobile_conectado_INT(){ 

		return "mobile_conectado_INT";

	}

	public function nomeCampoEstado_sincronizacao_mobile_id_INT(){ 

		return "estado_sincronizacao_mobile_id_INT";

	}

	public function nomeCampoData_inicial_DATETIME(){ 

		return "data_inicial_DATETIME";

	}

	public function nomeCampoData_final_DATETIME(){ 

		return "data_final_DATETIME";

	}

	public function nomeCampoResetando_BOOLEAN(){ 

		return "resetando_BOOLEAN";

	}

	public function nomeCampoSincronizacao_id_INT(){ 

		return "sincronizacao_id_INT";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}

	public function nomeCampoDescricao(){ 

		return "descricao";

	}

	public function nomeCampoCrud_mobile_ARQUIVO(){ 

		return "crud_mobile_ARQUIVO";

	}

	public function nomeCampoCrud_web_para_mobile_ARQUIVO(){ 

		return "crud_web_para_mobile_ARQUIVO";

	}

	public function nomeCampoUltimo_crud_id_INT(){ 

		return "ultimo_crud_id_INT";

	}

	public function nomeCampoUltima_verificacao_DATETIME(){ 

		return "ultima_verificacao_DATETIME";

	}

	public function nomeCampoErro(){ 

		return "erro";

	}

	public function nomeCampoErro_sincronizacao_id_INT(){ 

		return "erro_sincronizacao_id_INT";

	}

	public function nomeCampoPrimeira_vez_BOOLEAN(){ 

		return "primeira_vez_BOOLEAN";

	}

	public function nomeCampoId_primeiro_sinc_android_web_INT(){ 

		return "id_primeiro_sinc_android_web_INT";

	}

	public function nomeCampoId_ultimo_sinc_android_web_INT(){ 

		return "id_ultimo_sinc_android_web_INT";

	}

	public function nomeCampoCorporacao_id_INT(){ 

		return "corporacao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoMobile_identificador_INT($objArguments){

		$objArguments->nome = "mobile_identificador_INT";
		$objArguments->id = "mobile_identificador_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoMobile_conectado_INT($objArguments){

		$objArguments->nome = "mobile_conectado_INT";
		$objArguments->id = "mobile_conectado_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoEstado_sincronizacao_mobile_id_INT($objArguments){

		$objArguments->nome = "estado_sincronizacao_mobile_id_INT";
		$objArguments->id = "estado_sincronizacao_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoData_inicial_DATETIME($objArguments){

		$objArguments->nome = "data_inicial_DATETIME";
		$objArguments->id = "data_inicial_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_final_DATETIME($objArguments){

		$objArguments->nome = "data_final_DATETIME";
		$objArguments->id = "data_final_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoResetando_BOOLEAN($objArguments){

		$objArguments->nome = "resetando_BOOLEAN";
		$objArguments->id = "resetando_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoSincronizacao_id_INT($objArguments){

		$objArguments->nome = "sincronizacao_id_INT";
		$objArguments->id = "sincronizacao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoDescricao($objArguments){

		$objArguments->nome = "descricao";
		$objArguments->id = "descricao";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoCrud_mobile_ARQUIVO($objArguments){

		$objArguments->nome = "crud_mobile_ARQUIVO";
		$objArguments->id = "crud_mobile_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoCrud_web_para_mobile_ARQUIVO($objArguments){

		$objArguments->nome = "crud_web_para_mobile_ARQUIVO";
		$objArguments->id = "crud_web_para_mobile_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoUltimo_crud_id_INT($objArguments){

		$objArguments->nome = "ultimo_crud_id_INT";
		$objArguments->id = "ultimo_crud_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoUltima_verificacao_DATETIME($objArguments){

		$objArguments->nome = "ultima_verificacao_DATETIME";
		$objArguments->id = "ultima_verificacao_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoErro($objArguments){

		$objArguments->nome = "erro";
		$objArguments->id = "erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoErro_sincronizacao_id_INT($objArguments){

		$objArguments->nome = "erro_sincronizacao_id_INT";
		$objArguments->id = "erro_sincronizacao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoPrimeira_vez_BOOLEAN($objArguments){

		$objArguments->nome = "primeira_vez_BOOLEAN";
		$objArguments->id = "primeira_vez_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoId_primeiro_sinc_android_web_INT($objArguments){

		$objArguments->nome = "id_primeiro_sinc_android_web_INT";
		$objArguments->id = "id_primeiro_sinc_android_web_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_ultimo_sinc_android_web_INT($objArguments){

		$objArguments->nome = "id_ultimo_sinc_android_web_INT";
		$objArguments->id = "id_ultimo_sinc_android_web_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorporacao_id_INT($objArguments){

		$objArguments->nome = "corporacao_id_INT";
		$objArguments->id = "corporacao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->mobile_identificador_INT == ""){

			$this->mobile_identificador_INT = "null";

		}

		if($this->mobile_conectado_INT == ""){

			$this->mobile_conectado_INT = "null";

		}

		if($this->estado_sincronizacao_mobile_id_INT == ""){

			$this->estado_sincronizacao_mobile_id_INT = "null";

		}

		if($this->resetando_BOOLEAN == ""){

			$this->resetando_BOOLEAN = "null";

		}

		if($this->sincronizacao_id_INT == ""){

			$this->sincronizacao_id_INT = "null";

		}

		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}

		if($this->ultimo_crud_id_INT == ""){

			$this->ultimo_crud_id_INT = "null";

		}

		if($this->erro_sincronizacao_id_INT == ""){

			$this->erro_sincronizacao_id_INT = "null";

		}

		if($this->primeira_vez_BOOLEAN == ""){

			$this->primeira_vez_BOOLEAN = "null";

		}

		if($this->id_primeiro_sinc_android_web_INT == ""){

			$this->id_primeiro_sinc_android_web_INT = "null";

		}

		if($this->id_ultimo_sinc_android_web_INT == ""){

			$this->id_ultimo_sinc_android_web_INT = "null";

		}

		if($this->corporacao_id_INT == ""){

			$this->corporacao_id_INT = "null";

		}



	$this->data_inicial_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicial_DATETIME); 
	$this->data_final_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_final_DATETIME); 
	$this->ultima_verificacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->ultima_verificacao_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_inicial_DATETIME = $this->formatarDataTimeParaExibicao($this->data_inicial_DATETIME); 
	$this->data_final_DATETIME = $this->formatarDataTimeParaExibicao($this->data_final_DATETIME); 
	$this->ultima_verificacao_DATETIME = $this->formatarDataTimeParaExibicao($this->ultima_verificacao_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["mobile_identificador_INT"] = $this->mobile_identificador_INT; 
		$_SESSION["mobile_conectado_INT"] = $this->mobile_conectado_INT; 
		$_SESSION["estado_sincronizacao_mobile_id_INT"] = $this->estado_sincronizacao_mobile_id_INT; 
		$_SESSION["data_inicial_DATETIME"] = $this->data_inicial_DATETIME; 
		$_SESSION["data_final_DATETIME"] = $this->data_final_DATETIME; 
		$_SESSION["resetando_BOOLEAN"] = $this->resetando_BOOLEAN; 
		$_SESSION["sincronizacao_id_INT"] = $this->sincronizacao_id_INT; 
		$_SESSION["seq_INT"] = $this->seq_INT; 
		$_SESSION["descricao"] = $this->descricao; 
		$_SESSION["crud_mobile_ARQUIVO"] = $this->crud_mobile_ARQUIVO; 
		$_SESSION["crud_web_para_mobile_ARQUIVO"] = $this->crud_web_para_mobile_ARQUIVO; 
		$_SESSION["ultimo_crud_id_INT"] = $this->ultimo_crud_id_INT; 
		$_SESSION["ultima_verificacao_DATETIME"] = $this->ultima_verificacao_DATETIME; 
		$_SESSION["erro"] = $this->erro; 
		$_SESSION["erro_sincronizacao_id_INT"] = $this->erro_sincronizacao_id_INT; 
		$_SESSION["primeira_vez_BOOLEAN"] = $this->primeira_vez_BOOLEAN; 
		$_SESSION["id_primeiro_sinc_android_web_INT"] = $this->id_primeiro_sinc_android_web_INT; 
		$_SESSION["id_ultimo_sinc_android_web_INT"] = $this->id_ultimo_sinc_android_web_INT; 
		$_SESSION["corporacao_id_INT"] = $this->corporacao_id_INT; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["mobile_identificador_INT"]);
		unset($_SESSION["mobile_conectado_INT"]);
		unset($_SESSION["estado_sincronizacao_mobile_id_INT"]);
		unset($_SESSION["data_inicial_DATETIME"]);
		unset($_SESSION["data_final_DATETIME"]);
		unset($_SESSION["resetando_BOOLEAN"]);
		unset($_SESSION["sincronizacao_id_INT"]);
		unset($_SESSION["seq_INT"]);
		unset($_SESSION["descricao"]);
		unset($_SESSION["crud_mobile_ARQUIVO"]);
		unset($_SESSION["crud_web_para_mobile_ARQUIVO"]);
		unset($_SESSION["ultimo_crud_id_INT"]);
		unset($_SESSION["ultima_verificacao_DATETIME"]);
		unset($_SESSION["erro"]);
		unset($_SESSION["erro_sincronizacao_id_INT"]);
		unset($_SESSION["primeira_vez_BOOLEAN"]);
		unset($_SESSION["id_primeiro_sinc_android_web_INT"]);
		unset($_SESSION["id_ultimo_sinc_android_web_INT"]);
		unset($_SESSION["corporacao_id_INT"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->mobile_identificador_INT = $this->formatarDados($_SESSION["mobile_identificador_INT{$numReg}"]); 
		$this->mobile_conectado_INT = $this->formatarDados($_SESSION["mobile_conectado_INT{$numReg}"]); 
		$this->estado_sincronizacao_mobile_id_INT = $this->formatarDados($_SESSION["estado_sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->data_inicial_DATETIME = $this->formatarDados($_SESSION["data_inicial_DATETIME{$numReg}"]); 
		$this->data_final_DATETIME = $this->formatarDados($_SESSION["data_final_DATETIME{$numReg}"]); 
		$this->resetando_BOOLEAN = $this->formatarDados($_SESSION["resetando_BOOLEAN{$numReg}"]); 
		$this->sincronizacao_id_INT = $this->formatarDados($_SESSION["sincronizacao_id_INT{$numReg}"]); 
		$this->seq_INT = $this->formatarDados($_SESSION["seq_INT{$numReg}"]); 
		$this->descricao = $this->formatarDados($_SESSION["descricao{$numReg}"]); 
		$this->crud_mobile_ARQUIVO = $this->formatarDados($_SESSION["crud_mobile_ARQUIVO{$numReg}"]); 
		$this->crud_web_para_mobile_ARQUIVO = $this->formatarDados($_SESSION["crud_web_para_mobile_ARQUIVO{$numReg}"]); 
		$this->ultimo_crud_id_INT = $this->formatarDados($_SESSION["ultimo_crud_id_INT{$numReg}"]); 
		$this->ultima_verificacao_DATETIME = $this->formatarDados($_SESSION["ultima_verificacao_DATETIME{$numReg}"]); 
		$this->erro = $this->formatarDados($_SESSION["erro{$numReg}"]); 
		$this->erro_sincronizacao_id_INT = $this->formatarDados($_SESSION["erro_sincronizacao_id_INT{$numReg}"]); 
		$this->primeira_vez_BOOLEAN = $this->formatarDados($_SESSION["primeira_vez_BOOLEAN{$numReg}"]); 
		$this->id_primeiro_sinc_android_web_INT = $this->formatarDados($_SESSION["id_primeiro_sinc_android_web_INT{$numReg}"]); 
		$this->id_ultimo_sinc_android_web_INT = $this->formatarDados($_SESSION["id_ultimo_sinc_android_web_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_SESSION["corporacao_id_INT{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->mobile_identificador_INT = $this->formatarDados($_POST["mobile_identificador_INT{$numReg}"]); 
		$this->mobile_conectado_INT = $this->formatarDados($_POST["mobile_conectado_INT{$numReg}"]); 
		$this->estado_sincronizacao_mobile_id_INT = $this->formatarDados($_POST["estado_sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->data_inicial_DATETIME = $this->formatarDados($_POST["data_inicial_DATETIME{$numReg}"]); 
		$this->data_final_DATETIME = $this->formatarDados($_POST["data_final_DATETIME{$numReg}"]); 
		$this->resetando_BOOLEAN = $this->formatarDados($_POST["resetando_BOOLEAN{$numReg}"]); 
		$this->sincronizacao_id_INT = $this->formatarDados($_POST["sincronizacao_id_INT{$numReg}"]); 
		$this->seq_INT = $this->formatarDados($_POST["seq_INT{$numReg}"]); 
		$this->descricao = $this->formatarDados($_POST["descricao{$numReg}"]); 
		$this->crud_mobile_ARQUIVO = $this->formatarDados($_POST["crud_mobile_ARQUIVO{$numReg}"]); 
		$this->crud_web_para_mobile_ARQUIVO = $this->formatarDados($_POST["crud_web_para_mobile_ARQUIVO{$numReg}"]); 
		$this->ultimo_crud_id_INT = $this->formatarDados($_POST["ultimo_crud_id_INT{$numReg}"]); 
		$this->ultima_verificacao_DATETIME = $this->formatarDados($_POST["ultima_verificacao_DATETIME{$numReg}"]); 
		$this->erro = $this->formatarDados($_POST["erro{$numReg}"]); 
		$this->erro_sincronizacao_id_INT = $this->formatarDados($_POST["erro_sincronizacao_id_INT{$numReg}"]); 
		$this->primeira_vez_BOOLEAN = $this->formatarDados($_POST["primeira_vez_BOOLEAN{$numReg}"]); 
		$this->id_primeiro_sinc_android_web_INT = $this->formatarDados($_POST["id_primeiro_sinc_android_web_INT{$numReg}"]); 
		$this->id_ultimo_sinc_android_web_INT = $this->formatarDados($_POST["id_ultimo_sinc_android_web_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_POST["corporacao_id_INT{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->mobile_identificador_INT = $this->formatarDados($_GET["mobile_identificador_INT{$numReg}"]); 
		$this->mobile_conectado_INT = $this->formatarDados($_GET["mobile_conectado_INT{$numReg}"]); 
		$this->estado_sincronizacao_mobile_id_INT = $this->formatarDados($_GET["estado_sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->data_inicial_DATETIME = $this->formatarDados($_GET["data_inicial_DATETIME{$numReg}"]); 
		$this->data_final_DATETIME = $this->formatarDados($_GET["data_final_DATETIME{$numReg}"]); 
		$this->resetando_BOOLEAN = $this->formatarDados($_GET["resetando_BOOLEAN{$numReg}"]); 
		$this->sincronizacao_id_INT = $this->formatarDados($_GET["sincronizacao_id_INT{$numReg}"]); 
		$this->seq_INT = $this->formatarDados($_GET["seq_INT{$numReg}"]); 
		$this->descricao = $this->formatarDados($_GET["descricao{$numReg}"]); 
		$this->crud_mobile_ARQUIVO = $this->formatarDados($_GET["crud_mobile_ARQUIVO{$numReg}"]); 
		$this->crud_web_para_mobile_ARQUIVO = $this->formatarDados($_GET["crud_web_para_mobile_ARQUIVO{$numReg}"]); 
		$this->ultimo_crud_id_INT = $this->formatarDados($_GET["ultimo_crud_id_INT{$numReg}"]); 
		$this->ultima_verificacao_DATETIME = $this->formatarDados($_GET["ultima_verificacao_DATETIME{$numReg}"]); 
		$this->erro = $this->formatarDados($_GET["erro{$numReg}"]); 
		$this->erro_sincronizacao_id_INT = $this->formatarDados($_GET["erro_sincronizacao_id_INT{$numReg}"]); 
		$this->primeira_vez_BOOLEAN = $this->formatarDados($_GET["primeira_vez_BOOLEAN{$numReg}"]); 
		$this->id_primeiro_sinc_android_web_INT = $this->formatarDados($_GET["id_primeiro_sinc_android_web_INT{$numReg}"]); 
		$this->id_ultimo_sinc_android_web_INT = $this->formatarDados($_GET["id_ultimo_sinc_android_web_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_GET["corporacao_id_INT{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	$upd="";
	if(isset($tipo["mobile_identificador_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "mobile_identificador_INT = $this->mobile_identificador_INT, ";

	} 

	if(isset($tipo["mobile_conectado_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "mobile_conectado_INT = $this->mobile_conectado_INT, ";

	} 

	if(isset($tipo["estado_sincronizacao_mobile_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "estado_sincronizacao_mobile_id_INT = $this->estado_sincronizacao_mobile_id_INT, ";

	} 

	if(isset($tipo["data_inicial_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_inicial_DATETIME = $this->data_inicial_DATETIME, ";

	} 

	if(isset($tipo["data_final_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_final_DATETIME = $this->data_final_DATETIME, ";

	} 

	if(isset($tipo["resetando_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "resetando_BOOLEAN = $this->resetando_BOOLEAN, ";

	} 

	if(isset($tipo["sincronizacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sincronizacao_id_INT = $this->sincronizacao_id_INT, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

	if(isset($tipo["descricao{$numReg}"]) || $tipo == "vazio"){

		$upd.= "descricao = '$this->descricao', ";

	} 

	if(isset($tipo["crud_mobile_ARQUIVO{$numReg}"]) || $tipo == "vazio"){

		$upd.= "crud_mobile_ARQUIVO = '$this->crud_mobile_ARQUIVO', ";

	} 

	if(isset($tipo["crud_web_para_mobile_ARQUIVO{$numReg}"]) || $tipo == "vazio"){

		$upd.= "crud_web_para_mobile_ARQUIVO = '$this->crud_web_para_mobile_ARQUIVO', ";

	} 

	if(isset($tipo["ultimo_crud_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "ultimo_crud_id_INT = $this->ultimo_crud_id_INT, ";

	} 

	if(isset($tipo["ultima_verificacao_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "ultima_verificacao_DATETIME = $this->ultima_verificacao_DATETIME, ";

	} 

	if(isset($tipo["erro{$numReg}"]) || $tipo == "vazio"){

		$upd.= "erro = '$this->erro', ";

	} 

	if(isset($tipo["erro_sincronizacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "erro_sincronizacao_id_INT = $this->erro_sincronizacao_id_INT, ";

	} 

	if(isset($tipo["primeira_vez_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "primeira_vez_BOOLEAN = $this->primeira_vez_BOOLEAN, ";

	} 

	if(isset($tipo["id_primeiro_sinc_android_web_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_primeiro_sinc_android_web_INT = $this->id_primeiro_sinc_android_web_INT, ";

	} 

	if(isset($tipo["id_ultimo_sinc_android_web_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_ultimo_sinc_android_web_INT = $this->id_ultimo_sinc_android_web_INT, ";

	} 

	if(isset($tipo["corporacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "corporacao_id_INT = $this->corporacao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sincronizacao_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    ?>
