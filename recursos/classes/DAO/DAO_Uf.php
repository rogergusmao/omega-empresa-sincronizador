<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Uf
    * DATA DE GERA��O: 21.10.2010
    * ARQUIVO:         DAO_Uf.php
    * TABELA MYSQL:    uf
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Uf extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

    	public $id;
	public $nome;
	public $sigla;
	public $dataCadastro;
	public $dataEdicao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_sigla;
	public $label_dataCadastro;
	public $label_dataEdicao;






    // **********************
    // M�TODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "estado";
    	$this->nomeTabela = "uf";
    	$this->campoId = "id";
    	$this->campoLabel = "nome";



    }

    public function valorCampoLabel(){

    	return $this->getNome();

    }

    



	 public function __actionAdd(){

            $mensagemSucesso = "O estado foi cadastrado com sucesso.";



            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

            
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "O estado foi cadastrado com sucesso.";



            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);



                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

            
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "O estado foi modificado com sucesso.";



            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));


            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

            
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "O estado foi exclu�do com sucesso.";


            $urlSuccess = Helper::getUrlAction("list_uf", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_uf", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

        

        return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // M�TODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getSigla()
    {
    	return $this->sigla;
    }
    
    public function getDataCadastro()
    {
    	return $this->dataCadastro;
    }
    
    public function getDataEdicao()
    {
    	return $this->dataEdicao;
    }
    
    // **********************
    // M�TODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setSigla($val)
    {
    	$this->sigla =  $val;
    }
    
    function setDataCadastro($val)
    {
    	$this->dataCadastro =  $val;
    }
    
    function setDataEdicao($val)
    {
    	$this->dataEdicao =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM uf WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->sigla = $row->sigla;
        
        $this->dataCadastro = $row->dataCadastro;
        
        $this->dataEdicao = $row->dataEdicao;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM uf WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO uf ( nome,sigla,dataCadastro,dataEdicao ) VALUES ( '$this->nome','$this->sigla',NOW(),NOW() )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoSigla(){ 

		return "sigla";

	}

	public function nomeCampoDataCadastro(){ 

		return "dataCadastro";

	}

	public function nomeCampoDataEdicao(){ 

		return "dataEdicao";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSigla($objArguments){

		$objArguments->nome = "sigla";
		$objArguments->id = "sigla";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoDataCadastro($objArguments){

		$objArguments->nome = "dataCadastro";
		$objArguments->id = "dataCadastro";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoDataEdicao($objArguments){

		$objArguments->nome = "dataEdicao";
		$objArguments->id = "dataEdicao";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){



	$this->dataCadastro = $this->formatarDataTimeParaComandoSQL($this->dataCadastro); 
	$this->dataEdicao = $this->formatarDataTimeParaComandoSQL($this->dataEdicao); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->dataCadastro = $this->formatarDataTimeParaExibicao($this->dataCadastro); 
	$this->dataEdicao = $this->formatarDataTimeParaExibicao($this->dataEdicao); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["nome"] = $this->nome; 
		$_SESSION["sigla"] = $this->sigla; 
		$_SESSION["dataCadastro"] = $this->dataCadastro; 
		$_SESSION["dataEdicao"] = $this->dataEdicao; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["nome"]);
		unset($_SESSION["sigla"]);
		unset($_SESSION["dataCadastro"]);
		unset($_SESSION["dataEdicao"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]); 
		$this->sigla = $this->formatarDados($_SESSION["sigla{$numReg}"]); 
		$this->dataCadastro = $this->formatarDados($_SESSION["dataCadastro{$numReg}"]); 
		$this->dataEdicao = $this->formatarDados($_SESSION["dataEdicao{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_POST["nome{$numReg}"]); 
		$this->sigla = $this->formatarDados($_POST["sigla{$numReg}"]); 
		$this->dataCadastro = $this->formatarDados($_POST["dataCadastro{$numReg}"]); 
		$this->dataEdicao = $this->formatarDados($_POST["dataEdicao{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_GET["nome{$numReg}"]); 
		$this->sigla = $this->formatarDados($_GET["sigla{$numReg}"]); 
		$this->dataCadastro = $this->formatarDados($_GET["dataCadastro{$numReg}"]); 
		$this->dataEdicao = $this->formatarDados($_GET["dataEdicao{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["nome{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome = '$this->nome', ";

	} 

	if(isset($tipo["sigla{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sigla = '$this->sigla', ";

	} 

	if(isset($tipo["dataCadastro{$numReg}"]) || $tipo == "vazio"){

		$upd.= "dataCadastro = '$this->dataCadastro', ";

	} 

		$upd.= "dataEdicao = NOW(), ";

	if(isset($tipo["dataEdicao{$numReg}"]) || $tipo == "vazio"){

		$upd.= "dataEdicao = '$this->dataEdicao', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE uf SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>