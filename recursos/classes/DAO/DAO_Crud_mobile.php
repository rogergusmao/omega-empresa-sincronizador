<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Crud_mobile
    * DATA DE GERAÇÃO: 19.02.2017
    * ARQUIVO:         DAO_Crud_mobile.php
    * TABELA MYSQL:    crud_mobile
    * BANCO DE DADOS:  sincronizador_web_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Crud_mobile extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_tabela_id_INT;
	public $objSistema_tabela;
	public $id_tabela_mobile_INT;
	public $id_tabela_web_INT;
	public $id_tabela_web_duplicada_INT;
	public $id_sincronizador_mobile_INT;
	public $tipo_operacao_banco_id_INT;
	public $objTipo_operacao_banco;
	public $sincronizacao_mobile_id_INT;
	public $objSincronizacao_mobile;
	public $crud_id_INT;
	public $objCrud;
	public $processada_BOOLEAN;
	public $data_processamento_DATETIME;
	public $erro_sql_INT;
	public $corporacao_id_INT;
	public $objCorporacao;


    public $nomeEntidade;

	public $data_processamento_DATETIME_UNIX;


    

	public $label_id;
	public $label_sistema_tabela_id_INT;
	public $label_id_tabela_mobile_INT;
	public $label_id_tabela_web_INT;
	public $label_id_tabela_web_duplicada_INT;
	public $label_id_sincronizador_mobile_INT;
	public $label_tipo_operacao_banco_id_INT;
	public $label_sincronizacao_mobile_id_INT;
	public $label_crud_id_INT;
	public $label_processada_BOOLEAN;
	public $label_data_processamento_DATETIME;
	public $label_erro_sql_INT;
	public $label_corporacao_id_INT;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "crud_mobile";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
	function getObjEXTDAO_Sistema_tabela(){
	if($this->objSistema_tabela ==null)
		$this->objSistema_tabela = new EXTDAO_Sistema_tabela($this->getDatabase());
	return $this->objSistema_tabela ;
	}
	function getObjEXTDAO_Tipo_operacao_banco(){
	if($this->objTipo_operacao_banco ==null)
		$this->objTipo_operacao_banco = new EXTDAO_Tipo_operacao_banco($this->getDatabase());
	return $this->objTipo_operacao_banco ;
	}
	function getObjEXTDAO_Sincronizacao_mobile(){
	if($this->objSincronizacao_mobile ==null)
		$this->objSincronizacao_mobile = new EXTDAO_Sincronizacao_mobile($this->getDatabase());
	return $this->objSincronizacao_mobile ;
	}
	function getObjEXTDAO_Crud(){
	if($this->objCrud ==null)
		$this->objCrud = new EXTDAO_Crud($this->getDatabase());
	return $this->objCrud ;
	}
	function getObjEXTDAO_Corporacao(){
	if($this->objCorporacao ==null)
		$this->objCorporacao = new EXTDAO_Corporacao($this->getDatabase());
	return $this->objCorporacao ;
	}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_tabela($objArgumentos){

		$objArgumentos->nome="sistema_tabela_id_INT";
		$objArgumentos->id="sistema_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Sistema_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Tipo_operacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSincronizacao_mobile($objArgumentos){

		$objArgumentos->nome="sincronizacao_mobile_id_INT";
		$objArgumentos->id="sincronizacao_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Sincronizacao_mobile()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCrud($objArgumentos){

		$objArgumentos->nome="crud_id_INT";
		$objArgumentos->id="crud_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Crud()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCorporacao($objArgumentos){

		$objArgumentos->nome="corporacao_id_INT";
		$objArgumentos->id="corporacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Corporacao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_crud_mobile", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_crud_mobile", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_tabela_id_INT()
    {
    	return $this->sistema_tabela_id_INT;
    }
    
    public function getId_tabela_mobile_INT()
    {
    	return $this->id_tabela_mobile_INT;
    }
    
    public function getId_tabela_web_INT()
    {
    	return $this->id_tabela_web_INT;
    }
    
    public function getId_tabela_web_duplicada_INT()
    {
    	return $this->id_tabela_web_duplicada_INT;
    }
    
    public function getId_sincronizador_mobile_INT()
    {
    	return $this->id_sincronizador_mobile_INT;
    }
    
    public function getTipo_operacao_banco_id_INT()
    {
    	return $this->tipo_operacao_banco_id_INT;
    }
    
    public function getSincronizacao_mobile_id_INT()
    {
    	return $this->sincronizacao_mobile_id_INT;
    }
    
    public function getCrud_id_INT()
    {
    	return $this->crud_id_INT;
    }
    
    public function getProcessada_BOOLEAN()
    {
    	return $this->processada_BOOLEAN;
    }
    
    function getData_processamento_DATETIME_UNIX()
    {
    	return $this->data_processamento_DATETIME_UNIX;
    }
    
    public function getData_processamento_DATETIME()
    {
    	return $this->data_processamento_DATETIME;
    }
    
    public function getErro_sql_INT()
    {
    	return $this->erro_sql_INT;
    }
    
    public function getCorporacao_id_INT()
    {
    	return $this->corporacao_id_INT;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_tabela_id_INT($val)
    {
    	$this->sistema_tabela_id_INT =  $val;
    }
    
    function setId_tabela_mobile_INT($val)
    {
    	$this->id_tabela_mobile_INT =  $val;
    }
    
    function setId_tabela_web_INT($val)
    {
    	$this->id_tabela_web_INT =  $val;
    }
    
    function setId_tabela_web_duplicada_INT($val)
    {
    	$this->id_tabela_web_duplicada_INT =  $val;
    }
    
    function setId_sincronizador_mobile_INT($val)
    {
    	$this->id_sincronizador_mobile_INT =  $val;
    }
    
    function setTipo_operacao_banco_id_INT($val)
    {
    	$this->tipo_operacao_banco_id_INT =  $val;
    }
    
    function setSincronizacao_mobile_id_INT($val)
    {
    	$this->sincronizacao_mobile_id_INT =  $val;
    }
    
    function setCrud_id_INT($val)
    {
    	$this->crud_id_INT =  $val;
    }
    
    function setProcessada_BOOLEAN($val)
    {
    	$this->processada_BOOLEAN =  $val;
    }
    
    function setData_processamento_DATETIME($val)
    {
    	$this->data_processamento_DATETIME =  $val;
    }
    
    function setErro_sql_INT($val)
    {
    	$this->erro_sql_INT =  $val;
    }
    
    function setCorporacao_id_INT($val)
    {
    	$this->corporacao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_processamento_DATETIME) AS data_processamento_DATETIME_UNIX FROM crud_mobile WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && ($msg->erro() || $msg->resultadoVazio()))
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->sistema_tabela_id_INT = $row->sistema_tabela_id_INT;
        if(isset($this->objSistema_tabela))
			$this->objSistema_tabela->select($this->sistema_tabela_id_INT);

        $this->id_tabela_mobile_INT = $row->id_tabela_mobile_INT;
        
        $this->id_tabela_web_INT = $row->id_tabela_web_INT;
        
        $this->id_tabela_web_duplicada_INT = $row->id_tabela_web_duplicada_INT;
        
        $this->id_sincronizador_mobile_INT = $row->id_sincronizador_mobile_INT;
        
        $this->tipo_operacao_banco_id_INT = $row->tipo_operacao_banco_id_INT;
        if(isset($this->objTipo_operacao_banco))
			$this->objTipo_operacao_banco->select($this->tipo_operacao_banco_id_INT);

        $this->sincronizacao_mobile_id_INT = $row->sincronizacao_mobile_id_INT;
        if(isset($this->objSincronizacao_mobile))
			$this->objSincronizacao_mobile->select($this->sincronizacao_mobile_id_INT);

        $this->crud_id_INT = $row->crud_id_INT;
        if(isset($this->objCrud))
			$this->objCrud->select($this->crud_id_INT);

        $this->processada_BOOLEAN = $row->processada_BOOLEAN;
        
        $this->data_processamento_DATETIME = $row->data_processamento_DATETIME;
        $this->data_processamento_DATETIME_UNIX = $row->data_processamento_DATETIME_UNIX;

        $this->erro_sql_INT = $row->erro_sql_INT;
        
        $this->corporacao_id_INT = $row->corporacao_id_INT;
        if(isset($this->objCorporacao))
			$this->objCorporacao->select($this->corporacao_id_INT);


    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM crud_mobile WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO crud_mobile ( sistema_tabela_id_INT,id_tabela_mobile_INT,id_tabela_web_INT,id_tabela_web_duplicada_INT,id_sincronizador_mobile_INT,tipo_operacao_banco_id_INT,sincronizacao_mobile_id_INT,crud_id_INT,processada_BOOLEAN,data_processamento_DATETIME,erro_sql_INT,corporacao_id_INT ) VALUES ( $this->sistema_tabela_id_INT,$this->id_tabela_mobile_INT,$this->id_tabela_web_INT,$this->id_tabela_web_duplicada_INT,$this->id_sincronizador_mobile_INT,$this->tipo_operacao_banco_id_INT,$this->sincronizacao_mobile_id_INT,$this->crud_id_INT,$this->processada_BOOLEAN,$this->data_processamento_DATETIME,$this->erro_sql_INT,$this->corporacao_id_INT )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_tabela_id_INT(){ 

		return "sistema_tabela_id_INT";

	}

	public function nomeCampoId_tabela_mobile_INT(){ 

		return "id_tabela_mobile_INT";

	}

	public function nomeCampoId_tabela_web_INT(){ 

		return "id_tabela_web_INT";

	}

	public function nomeCampoId_tabela_web_duplicada_INT(){ 

		return "id_tabela_web_duplicada_INT";

	}

	public function nomeCampoId_sincronizador_mobile_INT(){ 

		return "id_sincronizador_mobile_INT";

	}

	public function nomeCampoTipo_operacao_banco_id_INT(){ 

		return "tipo_operacao_banco_id_INT";

	}

	public function nomeCampoSincronizacao_mobile_id_INT(){ 

		return "sincronizacao_mobile_id_INT";

	}

	public function nomeCampoCrud_id_INT(){ 

		return "crud_id_INT";

	}

	public function nomeCampoProcessada_BOOLEAN(){ 

		return "processada_BOOLEAN";

	}

	public function nomeCampoData_processamento_DATETIME(){ 

		return "data_processamento_DATETIME";

	}

	public function nomeCampoErro_sql_INT(){ 

		return "erro_sql_INT";

	}

	public function nomeCampoCorporacao_id_INT(){ 

		return "corporacao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_tabela_id_INT($objArguments){

		$objArguments->nome = "sistema_tabela_id_INT";
		$objArguments->id = "sistema_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_tabela_mobile_INT($objArguments){

		$objArguments->nome = "id_tabela_mobile_INT";
		$objArguments->id = "id_tabela_mobile_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_tabela_web_INT($objArguments){

		$objArguments->nome = "id_tabela_web_INT";
		$objArguments->id = "id_tabela_web_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_tabela_web_duplicada_INT($objArguments){

		$objArguments->nome = "id_tabela_web_duplicada_INT";
		$objArguments->id = "id_tabela_web_duplicada_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_sincronizador_mobile_INT($objArguments){

		$objArguments->nome = "id_sincronizador_mobile_INT";
		$objArguments->id = "id_sincronizador_mobile_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_banco_id_INT";
		$objArguments->id = "tipo_operacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSincronizacao_mobile_id_INT($objArguments){

		$objArguments->nome = "sincronizacao_mobile_id_INT";
		$objArguments->id = "sincronizacao_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCrud_id_INT($objArguments){

		$objArguments->nome = "crud_id_INT";
		$objArguments->id = "crud_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProcessada_BOOLEAN($objArguments){

		$objArguments->nome = "processada_BOOLEAN";
		$objArguments->id = "processada_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoData_processamento_DATETIME($objArguments){

		$objArguments->nome = "data_processamento_DATETIME";
		$objArguments->id = "data_processamento_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoErro_sql_INT($objArguments){

		$objArguments->nome = "erro_sql_INT";
		$objArguments->id = "erro_sql_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorporacao_id_INT($objArguments){

		$objArguments->nome = "corporacao_id_INT";
		$objArguments->id = "corporacao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_tabela_id_INT == ""){

			$this->sistema_tabela_id_INT = "null";

		}

		if($this->id_tabela_mobile_INT == ""){

			$this->id_tabela_mobile_INT = "null";

		}

		if($this->id_tabela_web_INT == ""){

			$this->id_tabela_web_INT = "null";

		}

		if($this->id_tabela_web_duplicada_INT == ""){

			$this->id_tabela_web_duplicada_INT = "null";

		}

		if($this->id_sincronizador_mobile_INT == ""){

			$this->id_sincronizador_mobile_INT = "null";

		}

		if($this->tipo_operacao_banco_id_INT == ""){

			$this->tipo_operacao_banco_id_INT = "null";

		}

		if($this->sincronizacao_mobile_id_INT == ""){

			$this->sincronizacao_mobile_id_INT = "null";

		}

		if($this->crud_id_INT == ""){

			$this->crud_id_INT = "null";

		}

		if($this->processada_BOOLEAN == ""){

			$this->processada_BOOLEAN = "null";

		}

		if($this->erro_sql_INT == ""){

			$this->erro_sql_INT = "null";

		}

		if($this->corporacao_id_INT == ""){

			$this->corporacao_id_INT = "null";

		}



	$this->data_processamento_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_processamento_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_processamento_DATETIME = $this->formatarDataTimeParaExibicao($this->data_processamento_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["sistema_tabela_id_INT"] = $this->sistema_tabela_id_INT; 
		$_SESSION["id_tabela_mobile_INT"] = $this->id_tabela_mobile_INT; 
		$_SESSION["id_tabela_web_INT"] = $this->id_tabela_web_INT; 
		$_SESSION["id_tabela_web_duplicada_INT"] = $this->id_tabela_web_duplicada_INT; 
		$_SESSION["id_sincronizador_mobile_INT"] = $this->id_sincronizador_mobile_INT; 
		$_SESSION["tipo_operacao_banco_id_INT"] = $this->tipo_operacao_banco_id_INT; 
		$_SESSION["sincronizacao_mobile_id_INT"] = $this->sincronizacao_mobile_id_INT; 
		$_SESSION["crud_id_INT"] = $this->crud_id_INT; 
		$_SESSION["processada_BOOLEAN"] = $this->processada_BOOLEAN; 
		$_SESSION["data_processamento_DATETIME"] = $this->data_processamento_DATETIME; 
		$_SESSION["erro_sql_INT"] = $this->erro_sql_INT; 
		$_SESSION["corporacao_id_INT"] = $this->corporacao_id_INT; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["sistema_tabela_id_INT"]);
		unset($_SESSION["id_tabela_mobile_INT"]);
		unset($_SESSION["id_tabela_web_INT"]);
		unset($_SESSION["id_tabela_web_duplicada_INT"]);
		unset($_SESSION["id_sincronizador_mobile_INT"]);
		unset($_SESSION["tipo_operacao_banco_id_INT"]);
		unset($_SESSION["sincronizacao_mobile_id_INT"]);
		unset($_SESSION["crud_id_INT"]);
		unset($_SESSION["processada_BOOLEAN"]);
		unset($_SESSION["data_processamento_DATETIME"]);
		unset($_SESSION["erro_sql_INT"]);
		unset($_SESSION["corporacao_id_INT"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->sistema_tabela_id_INT = $this->formatarDados($_SESSION["sistema_tabela_id_INT{$numReg}"]); 
		$this->id_tabela_mobile_INT = $this->formatarDados($_SESSION["id_tabela_mobile_INT{$numReg}"]); 
		$this->id_tabela_web_INT = $this->formatarDados($_SESSION["id_tabela_web_INT{$numReg}"]); 
		$this->id_tabela_web_duplicada_INT = $this->formatarDados($_SESSION["id_tabela_web_duplicada_INT{$numReg}"]); 
		$this->id_sincronizador_mobile_INT = $this->formatarDados($_SESSION["id_sincronizador_mobile_INT{$numReg}"]); 
		$this->tipo_operacao_banco_id_INT = $this->formatarDados($_SESSION["tipo_operacao_banco_id_INT{$numReg}"]); 
		$this->sincronizacao_mobile_id_INT = $this->formatarDados($_SESSION["sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->crud_id_INT = $this->formatarDados($_SESSION["crud_id_INT{$numReg}"]); 
		$this->processada_BOOLEAN = $this->formatarDados($_SESSION["processada_BOOLEAN{$numReg}"]); 
		$this->data_processamento_DATETIME = $this->formatarDados($_SESSION["data_processamento_DATETIME{$numReg}"]); 
		$this->erro_sql_INT = $this->formatarDados($_SESSION["erro_sql_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_SESSION["corporacao_id_INT{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->sistema_tabela_id_INT = $this->formatarDados($_POST["sistema_tabela_id_INT{$numReg}"]); 
		$this->id_tabela_mobile_INT = $this->formatarDados($_POST["id_tabela_mobile_INT{$numReg}"]); 
		$this->id_tabela_web_INT = $this->formatarDados($_POST["id_tabela_web_INT{$numReg}"]); 
		$this->id_tabela_web_duplicada_INT = $this->formatarDados($_POST["id_tabela_web_duplicada_INT{$numReg}"]); 
		$this->id_sincronizador_mobile_INT = $this->formatarDados($_POST["id_sincronizador_mobile_INT{$numReg}"]); 
		$this->tipo_operacao_banco_id_INT = $this->formatarDados($_POST["tipo_operacao_banco_id_INT{$numReg}"]); 
		$this->sincronizacao_mobile_id_INT = $this->formatarDados($_POST["sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->crud_id_INT = $this->formatarDados($_POST["crud_id_INT{$numReg}"]); 
		$this->processada_BOOLEAN = $this->formatarDados($_POST["processada_BOOLEAN{$numReg}"]); 
		$this->data_processamento_DATETIME = $this->formatarDados($_POST["data_processamento_DATETIME{$numReg}"]); 
		$this->erro_sql_INT = $this->formatarDados($_POST["erro_sql_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_POST["corporacao_id_INT{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->sistema_tabela_id_INT = $this->formatarDados($_GET["sistema_tabela_id_INT{$numReg}"]); 
		$this->id_tabela_mobile_INT = $this->formatarDados($_GET["id_tabela_mobile_INT{$numReg}"]); 
		$this->id_tabela_web_INT = $this->formatarDados($_GET["id_tabela_web_INT{$numReg}"]); 
		$this->id_tabela_web_duplicada_INT = $this->formatarDados($_GET["id_tabela_web_duplicada_INT{$numReg}"]); 
		$this->id_sincronizador_mobile_INT = $this->formatarDados($_GET["id_sincronizador_mobile_INT{$numReg}"]); 
		$this->tipo_operacao_banco_id_INT = $this->formatarDados($_GET["tipo_operacao_banco_id_INT{$numReg}"]); 
		$this->sincronizacao_mobile_id_INT = $this->formatarDados($_GET["sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->crud_id_INT = $this->formatarDados($_GET["crud_id_INT{$numReg}"]); 
		$this->processada_BOOLEAN = $this->formatarDados($_GET["processada_BOOLEAN{$numReg}"]); 
		$this->data_processamento_DATETIME = $this->formatarDados($_GET["data_processamento_DATETIME{$numReg}"]); 
		$this->erro_sql_INT = $this->formatarDados($_GET["erro_sql_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_GET["corporacao_id_INT{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_tabela_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sistema_tabela_id_INT = $this->sistema_tabela_id_INT, ";

	} 

	if(isset($tipo["id_tabela_mobile_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_tabela_mobile_INT = $this->id_tabela_mobile_INT, ";

	} 

	if(isset($tipo["id_tabela_web_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_tabela_web_INT = $this->id_tabela_web_INT, ";

	} 

	if(isset($tipo["id_tabela_web_duplicada_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_tabela_web_duplicada_INT = $this->id_tabela_web_duplicada_INT, ";

	} 

	if(isset($tipo["id_sincronizador_mobile_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_sincronizador_mobile_INT = $this->id_sincronizador_mobile_INT, ";

	} 

	if(isset($tipo["tipo_operacao_banco_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "tipo_operacao_banco_id_INT = $this->tipo_operacao_banco_id_INT, ";

	} 

	if(isset($tipo["sincronizacao_mobile_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sincronizacao_mobile_id_INT = $this->sincronizacao_mobile_id_INT, ";

	} 

	if(isset($tipo["crud_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "crud_id_INT = $this->crud_id_INT, ";

	} 

	if(isset($tipo["processada_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "processada_BOOLEAN = $this->processada_BOOLEAN, ";

	} 

	if(isset($tipo["data_processamento_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_processamento_DATETIME = $this->data_processamento_DATETIME, ";

	} 

	if(isset($tipo["erro_sql_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "erro_sql_INT = $this->erro_sql_INT, ";

	} 

	if(isset($tipo["corporacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "corporacao_id_INT = $this->corporacao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE crud_mobile SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    ?>
