<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Categoria
    * DATA DE GERAÇÃO: 23.12.2013
    * ARQUIVO:         DAO_Categoria.php
    * TABELA MYSQL:    categoria
    * BANCO DE DADOS:  blog_da_ale
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Categoria extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $cor;
	public $nome;
	public $titulo;
	public $subtitulo;
	public $corpo_HTML;
	public $icone_ARQUIVO;
	public $imagem_fundo_ARQUIVO;
	public $icone_padrao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_cor;
	public $label_nome;
	public $label_titulo;
	public $label_subtitulo;
	public $label_corpo_HTML;
	public $label_icone_ARQUIVO;
	public $label_imagem_fundo_ARQUIVO;
	public $label_icone_padrao;


	public $diretorio_icone_ARQUIVO;
	public $diretorio_imagem_fundo_ARQUIVO;




    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "categoria";
    	$this->campoId = "id";
    	$this->campoLabel = "nome";



    }

    public function valorCampoLabel(){

    	return $this->getNome();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Icone_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Imagem_fundo_ARQUIVO
            if(Helper::verificarUploadArquivo("imagem_fundo_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadImagem_fundo_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "imagem_fundo_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Icone_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Imagem_fundo_ARQUIVO
            if(Helper::verificarUploadArquivo("imagem_fundo_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadImagem_fundo_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "imagem_fundo_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Icone_ARQUIVO
            if(Helper::verificarUploadArquivo("icone_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadIcone_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "icone_ARQUIVO", $arquivo[0]);

                }

            }

                
            //UPLOAD DO ARQUIVO Imagem_fundo_ARQUIVO
            if(Helper::verificarUploadArquivo("imagem_fundo_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadImagem_fundo_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "imagem_fundo_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_categoria", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_categoria", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMOÇÃO DO ARQUIVO Icone_ARQUIVO
            $pathArquivo = $this->diretorio_icone_ARQUIVO . $this->getIcone_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            //REMOÇÃO DO ARQUIVO Imagem_fundo_ARQUIVO
            $pathArquivo = $this->diretorio_imagem_fundo_ARQUIVO . $this->getImagem_fundo_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadIcone_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_icone_ARQUIVO;
            $labelCampo = $this->label_icone_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["icone_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           

        public function __uploadImagem_fundo_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_imagem_fundo_ARQUIVO;
            $labelCampo = $this->label_imagem_fundo_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["imagem_fundo_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_2." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getCor()
    {
    	return $this->cor;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getTitulo()
    {
    	return $this->titulo;
    }
    
    public function getSubtitulo()
    {
    	return $this->subtitulo;
    }
    
    public function getCorpo_HTML()
    {
    	return $this->corpo_HTML;
    }
    
    public function getIcone_ARQUIVO()
    {
    	return $this->icone_ARQUIVO;
    }
    
    public function getImagem_fundo_ARQUIVO()
    {
    	return $this->imagem_fundo_ARQUIVO;
    }
    
    public function getIcone_padrao()
    {
    	return $this->icone_padrao;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setCor($val)
    {
    	$this->cor =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setTitulo($val)
    {
    	$this->titulo =  $val;
    }
    
    function setSubtitulo($val)
    {
    	$this->subtitulo =  $val;
    }
    
    function setCorpo_HTML($val)
    {
    	$this->corpo_HTML =  $val;
    }
    
    function setIcone_ARQUIVO($val)
    {
    	$this->icone_ARQUIVO =  $val;
    }
    
    function setImagem_fundo_ARQUIVO($val)
    {
    	$this->imagem_fundo_ARQUIVO =  $val;
    }
    
    function setIcone_padrao($val)
    {
    	$this->icone_padrao =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM categoria WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->cor = $row->cor;
        
        $this->nome = $row->nome;
        
        $this->titulo = $row->titulo;
        
        $this->subtitulo = $row->subtitulo;
        
        $this->corpo_HTML = $row->corpo_HTML;
        
        $this->icone_ARQUIVO = $row->icone_ARQUIVO;
        
        $this->imagem_fundo_ARQUIVO = $row->imagem_fundo_ARQUIVO;
        
        $this->icone_padrao = $row->icone_padrao;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM categoria WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO categoria ( cor,nome,titulo,subtitulo,corpo_HTML,icone_ARQUIVO,imagem_fundo_ARQUIVO,icone_padrao ) VALUES ( '$this->cor','$this->nome','$this->titulo','$this->subtitulo','$this->corpo_HTML','$this->icone_ARQUIVO','$this->imagem_fundo_ARQUIVO','$this->icone_padrao' )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoCor(){ 

		return "cor";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoTitulo(){ 

		return "titulo";

	}

	public function nomeCampoSubtitulo(){ 

		return "subtitulo";

	}

	public function nomeCampoCorpo_HTML(){ 

		return "corpo_HTML";

	}

	public function nomeCampoIcone_ARQUIVO(){ 

		return "icone_ARQUIVO";

	}

	public function nomeCampoImagem_fundo_ARQUIVO(){ 

		return "imagem_fundo_ARQUIVO";

	}

	public function nomeCampoIcone_padrao(){ 

		return "icone_padrao";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoCor($objArguments){

		$objArguments->nome = "cor";
		$objArguments->id = "cor";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoTitulo($objArguments){

		$objArguments->nome = "titulo";
		$objArguments->id = "titulo";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSubtitulo($objArguments){

		$objArguments->nome = "subtitulo";
		$objArguments->id = "subtitulo";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoCorpo_HTML($objArguments){

		$objArguments->nome = "corpo_HTML";
		$objArguments->id = "corpo_HTML";

	}

	public function imprimirCampoIcone_ARQUIVO($objArguments){

		$objArguments->nome = "icone_ARQUIVO";
		$objArguments->id = "icone_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoImagem_fundo_ARQUIVO($objArguments){

		$objArguments->nome = "imagem_fundo_ARQUIVO";
		$objArguments->id = "imagem_fundo_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoIcone_padrao($objArguments){

		$objArguments->nome = "icone_padrao";
		$objArguments->id = "icone_padrao";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){



$this->corpo_HTML = htmlentities($this->corpo_HTML, ENT_QUOTES);


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->corpo_HTML = html_entity_decode($this->corpo_HTML); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["cor"] = $this->cor; 
		$_SESSION["nome"] = $this->nome; 
		$_SESSION["titulo"] = $this->titulo; 
		$_SESSION["subtitulo"] = $this->subtitulo; 
		$_SESSION["corpo_HTML"] = $this->corpo_HTML; 
		$_SESSION["icone_ARQUIVO"] = $this->icone_ARQUIVO; 
		$_SESSION["imagem_fundo_ARQUIVO"] = $this->imagem_fundo_ARQUIVO; 
		$_SESSION["icone_padrao"] = $this->icone_padrao; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["cor"]);
		unset($_SESSION["nome"]);
		unset($_SESSION["titulo"]);
		unset($_SESSION["subtitulo"]);
		unset($_SESSION["corpo_HTML"]);
		unset($_SESSION["icone_ARQUIVO"]);
		unset($_SESSION["imagem_fundo_ARQUIVO"]);
		unset($_SESSION["icone_padrao"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->cor = $this->formatarDados($_SESSION["cor{$numReg}"]); 
		$this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]); 
		$this->titulo = $this->formatarDados($_SESSION["titulo{$numReg}"]); 
		$this->subtitulo = $this->formatarDados($_SESSION["subtitulo{$numReg}"]); 
		$this->corpo_HTML = $this->formatarDados($_SESSION["corpo_HTML{$numReg}"]); 
		$this->icone_ARQUIVO = $this->formatarDados($_SESSION["icone_ARQUIVO{$numReg}"]); 
		$this->imagem_fundo_ARQUIVO = $this->formatarDados($_SESSION["imagem_fundo_ARQUIVO{$numReg}"]); 
		$this->icone_padrao = $this->formatarDados($_SESSION["icone_padrao{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->cor = $this->formatarDados($_POST["cor{$numReg}"]); 
		$this->nome = $this->formatarDados($_POST["nome{$numReg}"]); 
		$this->titulo = $this->formatarDados($_POST["titulo{$numReg}"]); 
		$this->subtitulo = $this->formatarDados($_POST["subtitulo{$numReg}"]); 
		$this->corpo_HTML = $this->formatarDados($_POST["corpo_HTML{$numReg}"]); 
		$this->icone_ARQUIVO = $this->formatarDados($_POST["icone_ARQUIVO{$numReg}"]); 
		$this->imagem_fundo_ARQUIVO = $this->formatarDados($_POST["imagem_fundo_ARQUIVO{$numReg}"]); 
		$this->icone_padrao = $this->formatarDados($_POST["icone_padrao{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->cor = $this->formatarDados($_GET["cor{$numReg}"]); 
		$this->nome = $this->formatarDados($_GET["nome{$numReg}"]); 
		$this->titulo = $this->formatarDados($_GET["titulo{$numReg}"]); 
		$this->subtitulo = $this->formatarDados($_GET["subtitulo{$numReg}"]); 
		$this->corpo_HTML = $this->formatarDados($_GET["corpo_HTML{$numReg}"]); 
		$this->icone_ARQUIVO = $this->formatarDados($_GET["icone_ARQUIVO{$numReg}"]); 
		$this->imagem_fundo_ARQUIVO = $this->formatarDados($_GET["imagem_fundo_ARQUIVO{$numReg}"]); 
		$this->icone_padrao = $this->formatarDados($_GET["icone_padrao{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["cor{$numReg}"]) || $tipo == "vazio"){

		$upd.= "cor = '$this->cor', ";

	} 

	if(isset($tipo["nome{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome = '$this->nome', ";

	} 

	if(isset($tipo["titulo{$numReg}"]) || $tipo == "vazio"){

		$upd.= "titulo = '$this->titulo', ";

	} 

	if(isset($tipo["subtitulo{$numReg}"]) || $tipo == "vazio"){

		$upd.= "subtitulo = '$this->subtitulo', ";

	} 

	if(isset($tipo["corpo_HTML{$numReg}"]) || $tipo == "vazio"){

		$upd.= "corpo_HTML = '$this->corpo_HTML', ";

	} 

	if(isset($tipo["icone_ARQUIVO{$numReg}"]) || $tipo == "vazio"){

		$upd.= "icone_ARQUIVO = '$this->icone_ARQUIVO', ";

	} 

	if(isset($tipo["imagem_fundo_ARQUIVO{$numReg}"]) || $tipo == "vazio"){

		$upd.= "imagem_fundo_ARQUIVO = '$this->imagem_fundo_ARQUIVO', ";

	} 

	if(isset($tipo["icone_padrao{$numReg}"]) || $tipo == "vazio"){

		$upd.= "icone_padrao = '$this->icone_padrao', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE categoria SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>
