<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sincronizacao_mobile_iteracao
    * DATA DE GERAÇÃO: 08.10.2016
    * ARQUIVO:         DAO_Sincronizacao_mobile_iteracao.php5
    * TABELA MYSQL:    sincronizacao_mobile_iteracao
    * BANCO DE DADOS:  sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Sincronizacao_mobile_iteracao extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $sincronizacao_mobile_id_INT;
	public $objSincronizacao_mobile;
	public $estado_sincronizacao_mobile_id_INT;
	public $objEstado_sincronizacao_mobile;
	public $ocorrencia_DATETIME;


    public $nomeEntidade;

	public $ocorrencia_DATETIME_UNIX;


    

	public $label_id;
	public $label_sincronizacao_mobile_id_INT;
	public $label_estado_sincronizacao_mobile_id_INT;
	public $label_ocorrencia_DATETIME;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sincronizacao_mobile_iteracao";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

		$this->objSincronizacao_mobile = new EXTDAO_Sincronizacao_mobile();
		$this->objEstado_sincronizacao_mobile = new EXTDAO_Estado_sincronizacao_mobile();


    }

    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSincronizacao_mobile($objArgumentos){

		$objArgumentos->nome="sincronizacao_mobile_id_INT";
		$objArgumentos->id="sincronizacao_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->objSincronizacao_mobile->getComboBox($objArgumentos);

	}

public function getComboBoxAllEstado_sincronizacao_mobile($objArgumentos){

		$objArgumentos->nome="estado_sincronizacao_mobile_id_INT";
		$objArgumentos->id="estado_sincronizacao_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->objEstado_sincronizacao_mobile->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sincronizacao_mobile_iteracao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sincronizacao_mobile_iteracao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSincronizacao_mobile_id_INT()
    {
    	return $this->sincronizacao_mobile_id_INT;
    }
    
    public function getEstado_sincronizacao_mobile_id_INT()
    {
    	return $this->estado_sincronizacao_mobile_id_INT;
    }
    
    function getOcorrencia_DATETIME_UNIX()
    {
    	return $this->ocorrencia_DATETIME_UNIX;
    }
    
    public function getOcorrencia_DATETIME()
    {
    	return $this->ocorrencia_DATETIME;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSincronizacao_mobile_id_INT($val)
    {
    	$this->sincronizacao_mobile_id_INT =  $val;
    }
    
    function setEstado_sincronizacao_mobile_id_INT($val)
    {
    	$this->estado_sincronizacao_mobile_id_INT =  $val;
    }
    
    function setOcorrencia_DATETIME($val)
    {
    	$this->ocorrencia_DATETIME =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(ocorrencia_DATETIME) AS ocorrencia_DATETIME_UNIX FROM sincronizacao_mobile_iteracao WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->sincronizacao_mobile_id_INT = $row->sincronizacao_mobile_id_INT;
        if($this->sincronizacao_mobile_id_INT)
			$this->objSincronizacao_mobile->select($this->sincronizacao_mobile_id_INT);

        $this->estado_sincronizacao_mobile_id_INT = $row->estado_sincronizacao_mobile_id_INT;
        if($this->estado_sincronizacao_mobile_id_INT)
			$this->objEstado_sincronizacao_mobile->select($this->estado_sincronizacao_mobile_id_INT);

        $this->ocorrencia_DATETIME = $row->ocorrencia_DATETIME;
        $this->ocorrencia_DATETIME_UNIX = $row->ocorrencia_DATETIME_UNIX;


    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sincronizacao_mobile_iteracao WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sincronizacao_mobile_iteracao ( sincronizacao_mobile_id_INT,estado_sincronizacao_mobile_id_INT,ocorrencia_DATETIME ) VALUES ( $this->sincronizacao_mobile_id_INT,$this->estado_sincronizacao_mobile_id_INT,$this->ocorrencia_DATETIME )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSincronizacao_mobile_id_INT(){ 

		return "sincronizacao_mobile_id_INT";

	}

	public function nomeCampoEstado_sincronizacao_mobile_id_INT(){ 

		return "estado_sincronizacao_mobile_id_INT";

	}

	public function nomeCampoOcorrencia_DATETIME(){ 

		return "ocorrencia_DATETIME";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSincronizacao_mobile_id_INT($objArguments){

		$objArguments->nome = "sincronizacao_mobile_id_INT";
		$objArguments->id = "sincronizacao_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoEstado_sincronizacao_mobile_id_INT($objArguments){

		$objArguments->nome = "estado_sincronizacao_mobile_id_INT";
		$objArguments->id = "estado_sincronizacao_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoOcorrencia_DATETIME($objArguments){

		$objArguments->nome = "ocorrencia_DATETIME";
		$objArguments->id = "ocorrencia_DATETIME";

		return $this->campoDataTime($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sincronizacao_mobile_id_INT == ""){

			$this->sincronizacao_mobile_id_INT = "null";

		}

		if($this->estado_sincronizacao_mobile_id_INT == ""){

			$this->estado_sincronizacao_mobile_id_INT = "null";

		}



	$this->ocorrencia_DATETIME = $this->formatarDataTimeParaComandoSQL($this->ocorrencia_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->ocorrencia_DATETIME = $this->formatarDataTimeParaExibicao($this->ocorrencia_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["sincronizacao_mobile_id_INT"] = $this->sincronizacao_mobile_id_INT; 
		$_SESSION["estado_sincronizacao_mobile_id_INT"] = $this->estado_sincronizacao_mobile_id_INT; 
		$_SESSION["ocorrencia_DATETIME"] = $this->ocorrencia_DATETIME; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["sincronizacao_mobile_id_INT"]);
		unset($_SESSION["estado_sincronizacao_mobile_id_INT"]);
		unset($_SESSION["ocorrencia_DATETIME"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->sincronizacao_mobile_id_INT = $this->formatarDados($_SESSION["sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->estado_sincronizacao_mobile_id_INT = $this->formatarDados($_SESSION["estado_sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->ocorrencia_DATETIME = $this->formatarDados($_SESSION["ocorrencia_DATETIME{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->sincronizacao_mobile_id_INT = $this->formatarDados($_POST["sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->estado_sincronizacao_mobile_id_INT = $this->formatarDados($_POST["estado_sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->ocorrencia_DATETIME = $this->formatarDados($_POST["ocorrencia_DATETIME{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->sincronizacao_mobile_id_INT = $this->formatarDados($_GET["sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->estado_sincronizacao_mobile_id_INT = $this->formatarDados($_GET["estado_sincronizacao_mobile_id_INT{$numReg}"]); 
		$this->ocorrencia_DATETIME = $this->formatarDados($_GET["ocorrencia_DATETIME{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["sincronizacao_mobile_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sincronizacao_mobile_id_INT = $this->sincronizacao_mobile_id_INT, ";

	} 

	if(isset($tipo["estado_sincronizacao_mobile_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "estado_sincronizacao_mobile_id_INT = $this->estado_sincronizacao_mobile_id_INT, ";

	} 

	if(isset($tipo["ocorrencia_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "ocorrencia_DATETIME = $this->ocorrencia_DATETIME, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sincronizacao_mobile_iteracao SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>
