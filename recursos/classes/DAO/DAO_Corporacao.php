<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Corporacao
    * DATA DE GERAÇÃO: 19.02.2017
    * ARQUIVO:         DAO_Corporacao.php
    * TABELA MYSQL:    corporacao
    * BANCO DE DADOS:  sincronizador_web_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Corporacao extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $nome;
	public $nome_normalizado;
	public $usuario_dropbox;
	public $senha_dropbox;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome;
	public $label_nome_normalizado;
	public $label_usuario_dropbox;
	public $label_senha_dropbox;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "corporacao";
    	$this->campoId = "id";
    	$this->campoLabel = "nome";

    }
	


    public function valorCampoLabel(){

    	return $this->getNome();

    }

    

        

	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_corporacao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_corporacao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome()
    {
    	return $this->nome;
    }
    
    public function getNome_normalizado()
    {
    	return $this->nome_normalizado;
    }
    
    public function getUsuario_dropbox()
    {
    	return $this->usuario_dropbox;
    }
    
    public function getSenha_dropbox()
    {
    	return $this->senha_dropbox;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome($val)
    {
    	$this->nome =  $val;
    }
    
    function setNome_normalizado($val)
    {
    	$this->nome_normalizado =  $val;
    }
    
    function setUsuario_dropbox($val)
    {
    	$this->usuario_dropbox =  $val;
    }
    
    function setSenha_dropbox($val)
    {
    	$this->senha_dropbox =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM corporacao WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && ($msg->erro() || $msg->resultadoVazio()))
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->nome = $row->nome;
        
        $this->nome_normalizado = $row->nome_normalizado;
        
        $this->usuario_dropbox = $row->usuario_dropbox;
        
        $this->senha_dropbox = $row->senha_dropbox;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM corporacao WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO corporacao ( nome,nome_normalizado,usuario_dropbox,senha_dropbox ) VALUES ( '$this->nome','$this->nome_normalizado','$this->usuario_dropbox','$this->senha_dropbox' )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome(){ 

		return "nome";

	}

	public function nomeCampoNome_normalizado(){ 

		return "nome_normalizado";

	}

	public function nomeCampoUsuario_dropbox(){ 

		return "usuario_dropbox";

	}

	public function nomeCampoSenha_dropbox(){ 

		return "senha_dropbox";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome($objArguments){

		$objArguments->nome = "nome";
		$objArguments->id = "nome";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoNome_normalizado($objArguments){

		$objArguments->nome = "nome_normalizado";
		$objArguments->id = "nome_normalizado";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoUsuario_dropbox($objArguments){

		$objArguments->nome = "usuario_dropbox";
		$objArguments->id = "usuario_dropbox";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoSenha_dropbox($objArguments){

		$objArguments->nome = "senha_dropbox";
		$objArguments->id = "senha_dropbox";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["nome"] = $this->nome; 
		$_SESSION["nome_normalizado"] = $this->nome_normalizado; 
		$_SESSION["usuario_dropbox"] = $this->usuario_dropbox; 
		$_SESSION["senha_dropbox"] = $this->senha_dropbox; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["nome"]);
		unset($_SESSION["nome_normalizado"]);
		unset($_SESSION["usuario_dropbox"]);
		unset($_SESSION["senha_dropbox"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]); 
		$this->nome_normalizado = $this->formatarDados($_SESSION["nome_normalizado{$numReg}"]); 
		$this->usuario_dropbox = $this->formatarDados($_SESSION["usuario_dropbox{$numReg}"]); 
		$this->senha_dropbox = $this->formatarDados($_SESSION["senha_dropbox{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_POST["nome{$numReg}"]); 
		$this->nome_normalizado = $this->formatarDados($_POST["nome_normalizado{$numReg}"]); 
		$this->usuario_dropbox = $this->formatarDados($_POST["usuario_dropbox{$numReg}"]); 
		$this->senha_dropbox = $this->formatarDados($_POST["senha_dropbox{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->nome = $this->formatarDados($_GET["nome{$numReg}"]); 
		$this->nome_normalizado = $this->formatarDados($_GET["nome_normalizado{$numReg}"]); 
		$this->usuario_dropbox = $this->formatarDados($_GET["usuario_dropbox{$numReg}"]); 
		$this->senha_dropbox = $this->formatarDados($_GET["senha_dropbox{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	$upd="";
	if(isset($tipo["nome{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome = '$this->nome', ";

	} 

	if(isset($tipo["nome_normalizado{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome_normalizado = '$this->nome_normalizado', ";

	} 

	if(isset($tipo["usuario_dropbox{$numReg}"]) || $tipo == "vazio"){

		$upd.= "usuario_dropbox = '$this->usuario_dropbox', ";

	} 

	if(isset($tipo["senha_dropbox{$numReg}"]) || $tipo == "vazio"){

		$upd.= "senha_dropbox = '$this->senha_dropbox', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE corporacao SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    ?>
