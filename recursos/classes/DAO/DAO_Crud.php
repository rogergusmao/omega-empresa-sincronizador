<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Crud
    * DATA DE GERAÇÃO: 19.02.2017
    * ARQUIVO:         DAO_Crud.php
    * TABELA MYSQL:    crud
    * BANCO DE DADOS:  sincronizador_web_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Crud extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $sistema_tabela_id_INT;
	public $objSistema_tabela;
	public $id_tabela_web_INT;
	public $tipo_operacao_banco_id_INT;
	public $objTipo_operacao_banco;
	public $crud_origem_id_INT;
	public $objCrud_origem;
	public $sincronizacao_id_INT;
	public $objSincronizacao;
	public $id_sistema_registro_sincronizador_INT;
	public $corporacao_id_INT;
	public $objCorporacao;


    public $nomeEntidade;



    

	public $label_id;
	public $label_sistema_tabela_id_INT;
	public $label_id_tabela_web_INT;
	public $label_tipo_operacao_banco_id_INT;
	public $label_crud_origem_id_INT;
	public $label_sincronizacao_id_INT;
	public $label_id_sistema_registro_sincronizador_INT;
	public $label_corporacao_id_INT;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "crud";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
	function getObjEXTDAO_Sistema_tabela(){
	if($this->objSistema_tabela ==null)
		$this->objSistema_tabela = new EXTDAO_Sistema_tabela($this->getDatabase());
	return $this->objSistema_tabela ;
	}
	function getObjEXTDAO_Tipo_operacao_banco(){
	if($this->objTipo_operacao_banco ==null)
		$this->objTipo_operacao_banco = new EXTDAO_Tipo_operacao_banco($this->getDatabase());
	return $this->objTipo_operacao_banco ;
	}
	function getObjEXTDAO_Crud_origem(){
	if($this->objCrud_origem ==null)
		$this->objCrud_origem = new EXTDAO_Crud_origem($this->getDatabase());
	return $this->objCrud_origem ;
	}
	function getObjEXTDAO_Sincronizacao(){
	if($this->objSincronizacao ==null)
		$this->objSincronizacao = new EXTDAO_Sincronizacao($this->getDatabase());
	return $this->objSincronizacao ;
	}
	function getObjEXTDAO_Corporacao(){
	if($this->objCorporacao ==null)
		$this->objCorporacao = new EXTDAO_Corporacao($this->getDatabase());
	return $this->objCorporacao ;
	}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllSistema_tabela($objArgumentos){

		$objArgumentos->nome="sistema_tabela_id_INT";
		$objArgumentos->id="sistema_tabela_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Sistema_tabela()->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_operacao_banco($objArgumentos){

		$objArgumentos->nome="tipo_operacao_banco_id_INT";
		$objArgumentos->id="tipo_operacao_banco_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Tipo_operacao_banco()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCrud_origem($objArgumentos){

		$objArgumentos->nome="crud_origem_id_INT";
		$objArgumentos->id="crud_origem_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Crud_origem()->getComboBox($objArgumentos);

	}

public function getComboBoxAllSincronizacao($objArgumentos){

		$objArgumentos->nome="sincronizacao_id_INT";
		$objArgumentos->id="sincronizacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Sincronizacao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCorporacao($objArgumentos){

		$objArgumentos->nome="corporacao_id_INT";
		$objArgumentos->id="corporacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Corporacao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_crud", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_crud", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getSistema_tabela_id_INT()
    {
    	return $this->sistema_tabela_id_INT;
    }
    
    public function getId_tabela_web_INT()
    {
    	return $this->id_tabela_web_INT;
    }
    
    public function getTipo_operacao_banco_id_INT()
    {
    	return $this->tipo_operacao_banco_id_INT;
    }
    
    public function getCrud_origem_id_INT()
    {
    	return $this->crud_origem_id_INT;
    }
    
    public function getSincronizacao_id_INT()
    {
    	return $this->sincronizacao_id_INT;
    }
    
    public function getId_sistema_registro_sincronizador_INT()
    {
    	return $this->id_sistema_registro_sincronizador_INT;
    }
    
    public function getCorporacao_id_INT()
    {
    	return $this->corporacao_id_INT;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setSistema_tabela_id_INT($val)
    {
    	$this->sistema_tabela_id_INT =  $val;
    }
    
    function setId_tabela_web_INT($val)
    {
    	$this->id_tabela_web_INT =  $val;
    }
    
    function setTipo_operacao_banco_id_INT($val)
    {
    	$this->tipo_operacao_banco_id_INT =  $val;
    }
    
    function setCrud_origem_id_INT($val)
    {
    	$this->crud_origem_id_INT =  $val;
    }
    
    function setSincronizacao_id_INT($val)
    {
    	$this->sincronizacao_id_INT =  $val;
    }
    
    function setId_sistema_registro_sincronizador_INT($val)
    {
    	$this->id_sistema_registro_sincronizador_INT =  $val;
    }
    
    function setCorporacao_id_INT($val)
    {
    	$this->corporacao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM crud WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && ($msg->erro() || $msg->resultadoVazio()))
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->sistema_tabela_id_INT = $row->sistema_tabela_id_INT;
        if(isset($this->objSistema_tabela))
			$this->objSistema_tabela->select($this->sistema_tabela_id_INT);

        $this->id_tabela_web_INT = $row->id_tabela_web_INT;
        
        $this->tipo_operacao_banco_id_INT = $row->tipo_operacao_banco_id_INT;
        if(isset($this->objTipo_operacao_banco))
			$this->objTipo_operacao_banco->select($this->tipo_operacao_banco_id_INT);

        $this->crud_origem_id_INT = $row->crud_origem_id_INT;
        if(isset($this->objCrud_origem))
			$this->objCrud_origem->select($this->crud_origem_id_INT);

        $this->sincronizacao_id_INT = $row->sincronizacao_id_INT;
        if(isset($this->objSincronizacao))
			$this->objSincronizacao->select($this->sincronizacao_id_INT);

        $this->id_sistema_registro_sincronizador_INT = $row->id_sistema_registro_sincronizador_INT;
        
        $this->corporacao_id_INT = $row->corporacao_id_INT;
        if(isset($this->objCorporacao))
			$this->objCorporacao->select($this->corporacao_id_INT);


    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM crud WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO crud ( sistema_tabela_id_INT,id_tabela_web_INT,tipo_operacao_banco_id_INT,crud_origem_id_INT,sincronizacao_id_INT,id_sistema_registro_sincronizador_INT,corporacao_id_INT ) VALUES ( $this->sistema_tabela_id_INT,$this->id_tabela_web_INT,$this->tipo_operacao_banco_id_INT,$this->crud_origem_id_INT,$this->sincronizacao_id_INT,$this->id_sistema_registro_sincronizador_INT,$this->corporacao_id_INT )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoSistema_tabela_id_INT(){ 

		return "sistema_tabela_id_INT";

	}

	public function nomeCampoId_tabela_web_INT(){ 

		return "id_tabela_web_INT";

	}

	public function nomeCampoTipo_operacao_banco_id_INT(){ 

		return "tipo_operacao_banco_id_INT";

	}

	public function nomeCampoCrud_origem_id_INT(){ 

		return "crud_origem_id_INT";

	}

	public function nomeCampoSincronizacao_id_INT(){ 

		return "sincronizacao_id_INT";

	}

	public function nomeCampoId_sistema_registro_sincronizador_INT(){ 

		return "id_sistema_registro_sincronizador_INT";

	}

	public function nomeCampoCorporacao_id_INT(){ 

		return "corporacao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoSistema_tabela_id_INT($objArguments){

		$objArguments->nome = "sistema_tabela_id_INT";
		$objArguments->id = "sistema_tabela_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_tabela_web_INT($objArguments){

		$objArguments->nome = "id_tabela_web_INT";
		$objArguments->id = "id_tabela_web_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_operacao_banco_id_INT($objArguments){

		$objArguments->nome = "tipo_operacao_banco_id_INT";
		$objArguments->id = "tipo_operacao_banco_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCrud_origem_id_INT($objArguments){

		$objArguments->nome = "crud_origem_id_INT";
		$objArguments->id = "crud_origem_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSincronizacao_id_INT($objArguments){

		$objArguments->nome = "sincronizacao_id_INT";
		$objArguments->id = "sincronizacao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_sistema_registro_sincronizador_INT($objArguments){

		$objArguments->nome = "id_sistema_registro_sincronizador_INT";
		$objArguments->id = "id_sistema_registro_sincronizador_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCorporacao_id_INT($objArguments){

		$objArguments->nome = "corporacao_id_INT";
		$objArguments->id = "corporacao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->sistema_tabela_id_INT == ""){

			$this->sistema_tabela_id_INT = "null";

		}

		if($this->id_tabela_web_INT == ""){

			$this->id_tabela_web_INT = "null";

		}

		if($this->tipo_operacao_banco_id_INT == ""){

			$this->tipo_operacao_banco_id_INT = "null";

		}

		if($this->crud_origem_id_INT == ""){

			$this->crud_origem_id_INT = "null";

		}

		if($this->sincronizacao_id_INT == ""){

			$this->sincronizacao_id_INT = "null";

		}

		if($this->id_sistema_registro_sincronizador_INT == ""){

			$this->id_sistema_registro_sincronizador_INT = "null";

		}

		if($this->corporacao_id_INT == ""){

			$this->corporacao_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["sistema_tabela_id_INT"] = $this->sistema_tabela_id_INT; 
		$_SESSION["id_tabela_web_INT"] = $this->id_tabela_web_INT; 
		$_SESSION["tipo_operacao_banco_id_INT"] = $this->tipo_operacao_banco_id_INT; 
		$_SESSION["crud_origem_id_INT"] = $this->crud_origem_id_INT; 
		$_SESSION["sincronizacao_id_INT"] = $this->sincronizacao_id_INT; 
		$_SESSION["id_sistema_registro_sincronizador_INT"] = $this->id_sistema_registro_sincronizador_INT; 
		$_SESSION["corporacao_id_INT"] = $this->corporacao_id_INT; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["sistema_tabela_id_INT"]);
		unset($_SESSION["id_tabela_web_INT"]);
		unset($_SESSION["tipo_operacao_banco_id_INT"]);
		unset($_SESSION["crud_origem_id_INT"]);
		unset($_SESSION["sincronizacao_id_INT"]);
		unset($_SESSION["id_sistema_registro_sincronizador_INT"]);
		unset($_SESSION["corporacao_id_INT"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->sistema_tabela_id_INT = $this->formatarDados($_SESSION["sistema_tabela_id_INT{$numReg}"]); 
		$this->id_tabela_web_INT = $this->formatarDados($_SESSION["id_tabela_web_INT{$numReg}"]); 
		$this->tipo_operacao_banco_id_INT = $this->formatarDados($_SESSION["tipo_operacao_banco_id_INT{$numReg}"]); 
		$this->crud_origem_id_INT = $this->formatarDados($_SESSION["crud_origem_id_INT{$numReg}"]); 
		$this->sincronizacao_id_INT = $this->formatarDados($_SESSION["sincronizacao_id_INT{$numReg}"]); 
		$this->id_sistema_registro_sincronizador_INT = $this->formatarDados($_SESSION["id_sistema_registro_sincronizador_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_SESSION["corporacao_id_INT{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->sistema_tabela_id_INT = $this->formatarDados($_POST["sistema_tabela_id_INT{$numReg}"]); 
		$this->id_tabela_web_INT = $this->formatarDados($_POST["id_tabela_web_INT{$numReg}"]); 
		$this->tipo_operacao_banco_id_INT = $this->formatarDados($_POST["tipo_operacao_banco_id_INT{$numReg}"]); 
		$this->crud_origem_id_INT = $this->formatarDados($_POST["crud_origem_id_INT{$numReg}"]); 
		$this->sincronizacao_id_INT = $this->formatarDados($_POST["sincronizacao_id_INT{$numReg}"]); 
		$this->id_sistema_registro_sincronizador_INT = $this->formatarDados($_POST["id_sistema_registro_sincronizador_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_POST["corporacao_id_INT{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->sistema_tabela_id_INT = $this->formatarDados($_GET["sistema_tabela_id_INT{$numReg}"]); 
		$this->id_tabela_web_INT = $this->formatarDados($_GET["id_tabela_web_INT{$numReg}"]); 
		$this->tipo_operacao_banco_id_INT = $this->formatarDados($_GET["tipo_operacao_banco_id_INT{$numReg}"]); 
		$this->crud_origem_id_INT = $this->formatarDados($_GET["crud_origem_id_INT{$numReg}"]); 
		$this->sincronizacao_id_INT = $this->formatarDados($_GET["sincronizacao_id_INT{$numReg}"]); 
		$this->id_sistema_registro_sincronizador_INT = $this->formatarDados($_GET["id_sistema_registro_sincronizador_INT{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_GET["corporacao_id_INT{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	$upd="";
	if(isset($tipo["sistema_tabela_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sistema_tabela_id_INT = $this->sistema_tabela_id_INT, ";

	} 

	if(isset($tipo["id_tabela_web_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_tabela_web_INT = $this->id_tabela_web_INT, ";

	} 

	if(isset($tipo["tipo_operacao_banco_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "tipo_operacao_banco_id_INT = $this->tipo_operacao_banco_id_INT, ";

	} 

	if(isset($tipo["crud_origem_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "crud_origem_id_INT = $this->crud_origem_id_INT, ";

	} 

	if(isset($tipo["sincronizacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "sincronizacao_id_INT = $this->sincronizacao_id_INT, ";

	} 

	if(isset($tipo["id_sistema_registro_sincronizador_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_sistema_registro_sincronizador_INT = $this->id_sistema_registro_sincronizador_INT, ";

	} 

	if(isset($tipo["corporacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "corporacao_id_INT = $this->corporacao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE crud SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    ?>
