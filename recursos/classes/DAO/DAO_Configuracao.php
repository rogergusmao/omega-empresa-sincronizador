<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Configuracao
    * DATA DE GERAÇÃO: 06.04.2011
    * ARQUIVO:         DAO_Configuracao.php
    * TABELA MYSQL:    configuracao
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Configuracao extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $numero_niveis_estrutura_org_INT;
	public $nome_projeto;


    public $nomeEntidade;



    

	public $label_id;
	public $label_numero_niveis_estrutura_org_INT;
	public $label_nome_projeto;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "configuração";
    	$this->nomeTabela = "configuracao";
    	$this->campoId = "id";
    	$this->campoLabel = "nome_projeto";



    }

    public function valorCampoLabel(){

    	return $this->getNome_projeto();

    }

    



	 public function __actionAdd(){

            $mensagemSucesso = "A configuração foi cadastrada com sucesso.";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

            
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "A configuração foi cadastrada com sucesso.";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);



                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

            
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "A configuração foi modificada com sucesso.";



            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));


            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

            
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "A configuração foi excluída com sucesso.";


            $urlSuccess = Helper::getUrlAction("list_configuracao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_configuracao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

        

        return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNumero_niveis_estrutura_org_INT()
    {
    	return $this->numero_niveis_estrutura_org_INT;
    }
    
    public function getNome_projeto()
    {
    	return $this->nome_projeto;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNumero_niveis_estrutura_org_INT($val)
    {
    	$this->numero_niveis_estrutura_org_INT =  $val;
    }
    
    function setNome_projeto($val)
    {
    	$this->nome_projeto =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM configuracao WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->numero_niveis_estrutura_org_INT = $row->numero_niveis_estrutura_org_INT;
        
        $this->nome_projeto = $row->nome_projeto;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM configuracao WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO configuracao ( numero_niveis_estrutura_org_INT,nome_projeto ) VALUES ( $this->numero_niveis_estrutura_org_INT,'$this->nome_projeto' )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNumero_niveis_estrutura_org_INT(){ 

		return "numero_niveis_estrutura_org_INT";

	}

	public function nomeCampoNome_projeto(){ 

		return "nome_projeto";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNumero_niveis_estrutura_org_INT($objArguments){

		$objArguments->nome = "numero_niveis_estrutura_org_INT";
		$objArguments->id = "numero_niveis_estrutura_org_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoNome_projeto($objArguments){

		$objArguments->nome = "nome_projeto";
		$objArguments->id = "nome_projeto";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->numero_niveis_estrutura_org_INT == ""){

			$this->numero_niveis_estrutura_org_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["numero_niveis_estrutura_org_INT"] = $this->numero_niveis_estrutura_org_INT; 
		$_SESSION["nome_projeto"] = $this->nome_projeto; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["numero_niveis_estrutura_org_INT"]);
		unset($_SESSION["nome_projeto"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->numero_niveis_estrutura_org_INT = $this->formatarDados($_SESSION["numero_niveis_estrutura_org_INT{$numReg}"]); 
		$this->nome_projeto = $this->formatarDados($_SESSION["nome_projeto{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->numero_niveis_estrutura_org_INT = $this->formatarDados($_POST["numero_niveis_estrutura_org_INT{$numReg}"]); 
		$this->nome_projeto = $this->formatarDados($_POST["nome_projeto{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->numero_niveis_estrutura_org_INT = $this->formatarDados($_GET["numero_niveis_estrutura_org_INT{$numReg}"]); 
		$this->nome_projeto = $this->formatarDados($_GET["nome_projeto{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["numero_niveis_estrutura_org_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "numero_niveis_estrutura_org_INT = $this->numero_niveis_estrutura_org_INT, ";

	} 

	if(isset($tipo["nome_projeto{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome_projeto = '$this->nome_projeto', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE configuracao SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>
