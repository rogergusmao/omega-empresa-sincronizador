<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Arquivo
    * DATA DE GERAÇÃO: 21.12.2013
    * ARQUIVO:         DAO_Arquivo.php
    * TABELA MYSQL:    arquivo
    * BANCO DE DADOS:  blog_da_ale
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Arquivo extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $nome_ARQUIVO;
	public $noticia_id_INT;
	public $objNoticia;
	public $tipo_arquivo_id_INT;
	public $objTipo_arquivo;
	public $legenda;


    public $nomeEntidade;



    

	public $label_id;
	public $label_nome_ARQUIVO;
	public $label_noticia_id_INT;
	public $label_tipo_arquivo_id_INT;
	public $label_legenda;


	public $diretorio_nome_ARQUIVO;




    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "arquivo";
    	$this->campoId = "id";
    	$this->campoLabel = "nome_ARQUIVO";

		$this->objNoticia = new EXTDAO_Noticia();
		$this->objTipo_arquivo = new EXTDAO_Tipo_arquivo();


    }

    public function valorCampoLabel(){

    	return $this->getNome_ARQUIVO();

    }

    

        public function getComboBoxAllNoticia($objArgumentos){

		$objArgumentos->nome="noticia_id_INT";
		$objArgumentos->id="noticia_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->objNoticia->getComboBox($objArgumentos);

	}

public function getComboBoxAllTipo_arquivo($objArgumentos){

		$objArgumentos->nome="tipo_arquivo_id_INT";
		$objArgumentos->id="tipo_arquivo_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->objTipo_arquivo->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Nome_ARQUIVO
            if(Helper::verificarUploadArquivo("nome_ARQUIVO{$i}")){
                
                if(is_array($arquivo = $this->__uploadNome_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "nome_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Nome_ARQUIVO
            if(Helper::verificarUploadArquivo("nome_ARQUIVO{$i}")){

                
                if(is_array($arquivo = $this->__uploadNome_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "nome_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Nome_ARQUIVO
            if(Helper::verificarUploadArquivo("nome_ARQUIVO{$i}")){
                if(is_array($arquivo = $this->__uploadNome_ARQUIVO($i, $urlErro))){
                    $this->updateCampo($this->getId(), "nome_ARQUIVO", $arquivo[0]);
                }
            }
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_arquivo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_arquivo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMOÇÃO DO ARQUIVO Nome_ARQUIVO
            $pathArquivo = $this->diretorio_nome_ARQUIVO . $this->getNome_ARQUIVO();
            
            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadNome_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_nome_ARQUIVO;
            $labelCampo = $this->label_nome_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["nome_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $objUpload->file;
            
            $objUpload->uploadPath = $dirUpload;
            $success = $objUpload->uploadFile();
            
            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getNome_ARQUIVO()
    {
    	return $this->nome_ARQUIVO;
    }
    
    public function getNoticia_id_INT()
    {
    	return $this->noticia_id_INT;
    }
    
    public function getTipo_arquivo_id_INT()
    {
    	return $this->tipo_arquivo_id_INT;
    }
    
    public function getLegenda()
    {
    	return $this->legenda;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setNome_ARQUIVO($val)
    {
    	$this->nome_ARQUIVO =  $val;
    }
    
    function setNoticia_id_INT($val)
    {
    	$this->noticia_id_INT =  $val;
    }
    
    function setTipo_arquivo_id_INT($val)
    {
    	$this->tipo_arquivo_id_INT =  $val;
    }
    
    function setLegenda($val)
    {
    	$this->legenda =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM arquivo WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->nome_ARQUIVO = $row->nome_ARQUIVO;
        
        $this->noticia_id_INT = $row->noticia_id_INT;
        if($this->noticia_id_INT)
			$this->objNoticia->select($this->noticia_id_INT);

        $this->tipo_arquivo_id_INT = $row->tipo_arquivo_id_INT;
        if($this->tipo_arquivo_id_INT)
			$this->objTipo_arquivo->select($this->tipo_arquivo_id_INT);

        $this->legenda = $row->legenda;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM arquivo WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO arquivo ( nome_ARQUIVO,noticia_id_INT,tipo_arquivo_id_INT,legenda ) VALUES ( '$this->nome_ARQUIVO',$this->noticia_id_INT,$this->tipo_arquivo_id_INT,'$this->legenda' )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoNome_ARQUIVO(){ 

		return "nome_ARQUIVO";

	}

	public function nomeCampoNoticia_id_INT(){ 

		return "noticia_id_INT";

	}

	public function nomeCampoTipo_arquivo_id_INT(){ 

		return "tipo_arquivo_id_INT";

	}

	public function nomeCampoLegenda(){ 

		return "legenda";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoNome_ARQUIVO($objArguments){

		$objArguments->nome = "nome_ARQUIVO";
		$objArguments->id = "nome_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoNoticia_id_INT($objArguments){

		$objArguments->nome = "noticia_id_INT";
		$objArguments->id = "noticia_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoTipo_arquivo_id_INT($objArguments){

		$objArguments->nome = "tipo_arquivo_id_INT";
		$objArguments->id = "tipo_arquivo_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoLegenda($objArguments){

		$objArguments->nome = "legenda";
		$objArguments->id = "legenda";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->noticia_id_INT == ""){

			$this->noticia_id_INT = "null";

		}

		if($this->tipo_arquivo_id_INT == ""){

			$this->tipo_arquivo_id_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["nome_ARQUIVO"] = $this->nome_ARQUIVO; 
		$_SESSION["noticia_id_INT"] = $this->noticia_id_INT; 
		$_SESSION["tipo_arquivo_id_INT"] = $this->tipo_arquivo_id_INT; 
		$_SESSION["legenda"] = $this->legenda; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["nome_ARQUIVO"]);
		unset($_SESSION["noticia_id_INT"]);
		unset($_SESSION["tipo_arquivo_id_INT"]);
		unset($_SESSION["legenda"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->nome_ARQUIVO = $this->formatarDados($_SESSION["nome_ARQUIVO{$numReg}"]); 
		$this->noticia_id_INT = $this->formatarDados($_SESSION["noticia_id_INT{$numReg}"]); 
		$this->tipo_arquivo_id_INT = $this->formatarDados($_SESSION["tipo_arquivo_id_INT{$numReg}"]); 
		$this->legenda = $this->formatarDados($_SESSION["legenda{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->nome_ARQUIVO = $this->formatarDados($_POST["nome_ARQUIVO{$numReg}"]); 
		$this->noticia_id_INT = $this->formatarDados($_POST["noticia_id_INT{$numReg}"]); 
		$this->tipo_arquivo_id_INT = $this->formatarDados($_POST["tipo_arquivo_id_INT{$numReg}"]); 
		$this->legenda = $this->formatarDados($_POST["legenda{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->nome_ARQUIVO = $this->formatarDados($_GET["nome_ARQUIVO{$numReg}"]); 
		$this->noticia_id_INT = $this->formatarDados($_GET["noticia_id_INT{$numReg}"]); 
		$this->tipo_arquivo_id_INT = $this->formatarDados($_GET["tipo_arquivo_id_INT{$numReg}"]); 
		$this->legenda = $this->formatarDados($_GET["legenda{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {
	if(isset($tipo["nome_ARQUIVO{$numReg}"]) || $tipo == "vazio"){

		$upd.= "nome_ARQUIVO = '$this->nome_ARQUIVO', ";

	} 

	if(isset($tipo["noticia_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "noticia_id_INT = $this->noticia_id_INT, ";

	} 

	if(isset($tipo["tipo_arquivo_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "tipo_arquivo_id_INT = $this->tipo_arquivo_id_INT, ";

	} 

	if(isset($tipo["legenda{$numReg}"]) || $tipo == "vazio"){

		$upd.= "legenda = '$this->legenda', ";

	} 

        $upd = substr($upd, 0, -2);

    	$sql = " UPDATE arquivo SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);

    }
    

    } // classe: fim

    ?>
