<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sincronizacao
    * DATA DE GERAÇÃO: 19.02.2017
    * ARQUIVO:         DAO_Sincronizacao.php
    * TABELA MYSQL:    sincronizacao
    * BANCO DE DADOS:  sincronizador_web_corporacao
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Sincronizacao extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $data_inicial_DATETIME;
	public $data_final_DATETIME;
	public $limite_mobile_INT;
	public $estado_sincronizacao_id_INT;
	public $objEstado_sincronizacao;
	public $seq_INT;
	public $crud_ARQUIVO;
	public $erro;
	public $id_ultimo_sistema_registro_sincronizador_INT;
	public $quantidade_crud_processado_INT;
	public $processando_BOOLEAN;
	public $data_ultimo_processamento_DATETIME;
	public $corporacao_id_INT;
	public $objCorporacao;


    public $nomeEntidade;

	public $data_inicial_DATETIME_UNIX;
	public $data_final_DATETIME_UNIX;
	public $data_ultimo_processamento_DATETIME_UNIX;


    

	public $label_id;
	public $label_data_inicial_DATETIME;
	public $label_data_final_DATETIME;
	public $label_limite_mobile_INT;
	public $label_estado_sincronizacao_id_INT;
	public $label_seq_INT;
	public $label_crud_ARQUIVO;
	public $label_erro;
	public $label_id_ultimo_sistema_registro_sincronizador_INT;
	public $label_quantidade_crud_processado_INT;
	public $label_processando_BOOLEAN;
	public $label_data_ultimo_processamento_DATETIME;
	public $label_corporacao_id_INT;


	public $diretorio_crud_ARQUIVO;




    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($db=null)
    {

    	parent::__construct($db);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "sincronizacao";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

    }
	
	function getObjEXTDAO_Estado_sincronizacao(){
	if($this->objEstado_sincronizacao ==null)
		$this->objEstado_sincronizacao = new EXTDAO_Estado_sincronizacao($this->getDatabase());
	return $this->objEstado_sincronizacao ;
	}
	function getObjEXTDAO_Corporacao(){
	if($this->objCorporacao ==null)
		$this->objCorporacao = new EXTDAO_Corporacao($this->getDatabase());
	return $this->objCorporacao ;
	}


    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllEstado_sincronizacao($objArgumentos){

		$objArgumentos->nome="estado_sincronizacao_id_INT";
		$objArgumentos->id="estado_sincronizacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Estado_sincronizacao()->getComboBox($objArgumentos);

	}

public function getComboBoxAllCorporacao($objArgumentos){

		$objArgumentos->nome="corporacao_id_INT";
		$objArgumentos->id="corporacao_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->getObjEXTDAO_Corporacao()->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Crud_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
            //UPLOAD DO ARQUIVO Crud_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_ARQUIVO", $arquivo[0]);

                }

            }

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
            //UPLOAD DO ARQUIVO Crud_ARQUIVO
            if(Helper::verificarUploadArquivo("crud_ARQUIVO{$i}")){

                if(is_array($arquivo = $this->__uploadCrud_ARQUIVO($i, $urlErro))){

                    $this->updateCampo($this->getId(), "crud_ARQUIVO", $arquivo[0]);

                }

            }

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_sincronizacao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sincronizacao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            //REMOÇÃO DO ARQUIVO Crud_ARQUIVO
            $pathArquivo = $this->diretorio_crud_ARQUIVO . $this->getCrud_ARQUIVO();

            if(file_exists($pathArquivo)){

                unlink($pathArquivo);

            }

                

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        

        public function __uploadCrud_ARQUIVO($numRegistro=1, $urlErro){

            $dirUpload  = $this->diretorio_crud_ARQUIVO;
            $labelCampo = $this->label_crud_ARQUIVO;

            $objUpload = new Upload();

            $objUpload->arrPermitido = "";
            $objUpload->tamanhoMax = "";
            $objUpload->file = $_FILES["crud_ARQUIVO{$numRegistro}"];
            $objUpload->nome = $this->getId() . "_1." . Helper::getExtensaoDoArquivo($objUpload->file["name"]);
            $objUpload->uploadPath = $dirUpload;
			$success = $objUpload->uploadFile();

            if(!$success){

                $_SESSION["erro"] = true;
                $this->createSession();
                return array("location: $urlErro&msgErro=Erro no upload do arquivo $labelCampo:" . $objUpload->erro);
                exit();

            }
            else{

                return array($objUpload->nome, $dirUpload);

            }

        }
           


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    function getData_inicial_DATETIME_UNIX()
    {
    	return $this->data_inicial_DATETIME_UNIX;
    }
    
    public function getData_inicial_DATETIME()
    {
    	return $this->data_inicial_DATETIME;
    }
    
    function getData_final_DATETIME_UNIX()
    {
    	return $this->data_final_DATETIME_UNIX;
    }
    
    public function getData_final_DATETIME()
    {
    	return $this->data_final_DATETIME;
    }
    
    public function getLimite_mobile_INT()
    {
    	return $this->limite_mobile_INT;
    }
    
    public function getEstado_sincronizacao_id_INT()
    {
    	return $this->estado_sincronizacao_id_INT;
    }
    
    public function getSeq_INT()
    {
    	return $this->seq_INT;
    }
    
    public function getCrud_ARQUIVO()
    {
    	return $this->crud_ARQUIVO;
    }
    
    public function getErro()
    {
    	return $this->erro;
    }
    
    public function getId_ultimo_sistema_registro_sincronizador_INT()
    {
    	return $this->id_ultimo_sistema_registro_sincronizador_INT;
    }
    
    public function getQuantidade_crud_processado_INT()
    {
    	return $this->quantidade_crud_processado_INT;
    }
    
    public function getProcessando_BOOLEAN()
    {
    	return $this->processando_BOOLEAN;
    }
    
    function getData_ultimo_processamento_DATETIME_UNIX()
    {
    	return $this->data_ultimo_processamento_DATETIME_UNIX;
    }
    
    public function getData_ultimo_processamento_DATETIME()
    {
    	return $this->data_ultimo_processamento_DATETIME;
    }
    
    public function getCorporacao_id_INT()
    {
    	return $this->corporacao_id_INT;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setData_inicial_DATETIME($val)
    {
    	$this->data_inicial_DATETIME =  $val;
    }
    
    function setData_final_DATETIME($val)
    {
    	$this->data_final_DATETIME =  $val;
    }
    
    function setLimite_mobile_INT($val)
    {
    	$this->limite_mobile_INT =  $val;
    }
    
    function setEstado_sincronizacao_id_INT($val)
    {
    	$this->estado_sincronizacao_id_INT =  $val;
    }
    
    function setSeq_INT($val)
    {
    	$this->seq_INT =  $val;
    }
    
    function setCrud_ARQUIVO($val)
    {
    	$this->crud_ARQUIVO =  $val;
    }
    
    function setErro($val)
    {
    	$this->erro =  $val;
    }
    
    function setId_ultimo_sistema_registro_sincronizador_INT($val)
    {
    	$this->id_ultimo_sistema_registro_sincronizador_INT =  $val;
    }
    
    function setQuantidade_crud_processado_INT($val)
    {
    	$this->quantidade_crud_processado_INT =  $val;
    }
    
    function setProcessando_BOOLEAN($val)
    {
    	$this->processando_BOOLEAN =  $val;
    }
    
    function setData_ultimo_processamento_DATETIME($val)
    {
    	$this->data_ultimo_processamento_DATETIME =  $val;
    }
    
    function setCorporacao_id_INT($val)
    {
    	$this->corporacao_id_INT =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT * , UNIX_TIMESTAMP(data_inicial_DATETIME) AS data_inicial_DATETIME_UNIX, UNIX_TIMESTAMP(data_final_DATETIME) AS data_final_DATETIME_UNIX, UNIX_TIMESTAMP(data_ultimo_processamento_DATETIME) AS data_ultimo_processamento_DATETIME_UNIX FROM sincronizacao WHERE id = $id;";
    	$msg = $this->database->queryMensagem($sql);
	if($msg != null && ($msg->erro() || $msg->resultadoVazio()))
	return $msg;

    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->data_inicial_DATETIME = $row->data_inicial_DATETIME;
        $this->data_inicial_DATETIME_UNIX = $row->data_inicial_DATETIME_UNIX;

        $this->data_final_DATETIME = $row->data_final_DATETIME;
        $this->data_final_DATETIME_UNIX = $row->data_final_DATETIME_UNIX;

        $this->limite_mobile_INT = $row->limite_mobile_INT;
        
        $this->estado_sincronizacao_id_INT = $row->estado_sincronizacao_id_INT;
        if(isset($this->objEstado_sincronizacao))
			$this->objEstado_sincronizacao->select($this->estado_sincronizacao_id_INT);

        $this->seq_INT = $row->seq_INT;
        
        $this->crud_ARQUIVO = $row->crud_ARQUIVO;
        
        $this->erro = $row->erro;
        
        $this->id_ultimo_sistema_registro_sincronizador_INT = $row->id_ultimo_sistema_registro_sincronizador_INT;
        
        $this->quantidade_crud_processado_INT = $row->quantidade_crud_processado_INT;
        
        $this->processando_BOOLEAN = $row->processando_BOOLEAN;
        
        $this->data_ultimo_processamento_DATETIME = $row->data_ultimo_processamento_DATETIME;
        $this->data_ultimo_processamento_DATETIME_UNIX = $row->data_ultimo_processamento_DATETIME_UNIX;

        $this->corporacao_id_INT = $row->corporacao_id_INT;
        if(isset($this->objCorporacao))
			$this->objCorporacao->select($this->corporacao_id_INT);


    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM sincronizacao WHERE id = $id";
	$msg = $this->database->queryMensagem($sql);
	return $msg;

    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO sincronizacao ( data_inicial_DATETIME,data_final_DATETIME,limite_mobile_INT,estado_sincronizacao_id_INT,seq_INT,crud_ARQUIVO,erro,id_ultimo_sistema_registro_sincronizador_INT,quantidade_crud_processado_INT,processando_BOOLEAN,data_ultimo_processamento_DATETIME,corporacao_id_INT ) VALUES ( $this->data_inicial_DATETIME,$this->data_final_DATETIME,$this->limite_mobile_INT,$this->estado_sincronizacao_id_INT,$this->seq_INT,'$this->crud_ARQUIVO','$this->erro',$this->id_ultimo_sistema_registro_sincronizador_INT,$this->quantidade_crud_processado_INT,$this->processando_BOOLEAN,$this->data_ultimo_processamento_DATETIME,$this->corporacao_id_INT )";
    		$msg = $this->database->queryMensagem($sql);
	if($msg != null && $msg->erro()) return $msg;

    	
	return $msg;

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoData_inicial_DATETIME(){ 

		return "data_inicial_DATETIME";

	}

	public function nomeCampoData_final_DATETIME(){ 

		return "data_final_DATETIME";

	}

	public function nomeCampoLimite_mobile_INT(){ 

		return "limite_mobile_INT";

	}

	public function nomeCampoEstado_sincronizacao_id_INT(){ 

		return "estado_sincronizacao_id_INT";

	}

	public function nomeCampoSeq_INT(){ 

		return "seq_INT";

	}

	public function nomeCampoCrud_ARQUIVO(){ 

		return "crud_ARQUIVO";

	}

	public function nomeCampoErro(){ 

		return "erro";

	}

	public function nomeCampoId_ultimo_sistema_registro_sincronizador_INT(){ 

		return "id_ultimo_sistema_registro_sincronizador_INT";

	}

	public function nomeCampoQuantidade_crud_processado_INT(){ 

		return "quantidade_crud_processado_INT";

	}

	public function nomeCampoProcessando_BOOLEAN(){ 

		return "processando_BOOLEAN";

	}

	public function nomeCampoData_ultimo_processamento_DATETIME(){ 

		return "data_ultimo_processamento_DATETIME";

	}

	public function nomeCampoCorporacao_id_INT(){ 

		return "corporacao_id_INT";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoData_inicial_DATETIME($objArguments){

		$objArguments->nome = "data_inicial_DATETIME";
		$objArguments->id = "data_inicial_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoData_final_DATETIME($objArguments){

		$objArguments->nome = "data_final_DATETIME";
		$objArguments->id = "data_final_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoLimite_mobile_INT($objArguments){

		$objArguments->nome = "limite_mobile_INT";
		$objArguments->id = "limite_mobile_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoEstado_sincronizacao_id_INT($objArguments){

		$objArguments->nome = "estado_sincronizacao_id_INT";
		$objArguments->id = "estado_sincronizacao_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoSeq_INT($objArguments){

		$objArguments->nome = "seq_INT";
		$objArguments->id = "seq_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoCrud_ARQUIVO($objArguments){

		$objArguments->nome = "crud_ARQUIVO";
		$objArguments->id = "crud_ARQUIVO";

		return $this->campoArquivo($objArguments);

	}

	public function imprimirCampoErro($objArguments){

		$objArguments->nome = "erro";
		$objArguments->id = "erro";

		return $this->campoTexto($objArguments);

	}

	public function imprimirCampoId_ultimo_sistema_registro_sincronizador_INT($objArguments){

		$objArguments->nome = "id_ultimo_sistema_registro_sincronizador_INT";
		$objArguments->id = "id_ultimo_sistema_registro_sincronizador_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoQuantidade_crud_processado_INT($objArguments){

		$objArguments->nome = "quantidade_crud_processado_INT";
		$objArguments->id = "quantidade_crud_processado_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoProcessando_BOOLEAN($objArguments){

		$objArguments->nome = "processando_BOOLEAN";
		$objArguments->id = "processando_BOOLEAN";

		return $this->campoBoolean($objArguments);

	}

	public function imprimirCampoData_ultimo_processamento_DATETIME($objArguments){

		$objArguments->nome = "data_ultimo_processamento_DATETIME";
		$objArguments->id = "data_ultimo_processamento_DATETIME";

		return $this->campoDataTime($objArguments);

	}

	public function imprimirCampoCorporacao_id_INT($objArguments){

		$objArguments->nome = "corporacao_id_INT";
		$objArguments->id = "corporacao_id_INT";

		return $this->campoInteiro($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->limite_mobile_INT == ""){

			$this->limite_mobile_INT = "null";

		}

		if($this->estado_sincronizacao_id_INT == ""){

			$this->estado_sincronizacao_id_INT = "null";

		}

		if($this->seq_INT == ""){

			$this->seq_INT = "null";

		}

		if($this->id_ultimo_sistema_registro_sincronizador_INT == ""){

			$this->id_ultimo_sistema_registro_sincronizador_INT = "null";

		}

		if($this->quantidade_crud_processado_INT == ""){

			$this->quantidade_crud_processado_INT = "null";

		}

		if($this->processando_BOOLEAN == ""){

			$this->processando_BOOLEAN = "null";

		}

		if($this->corporacao_id_INT == ""){

			$this->corporacao_id_INT = "null";

		}



	$this->data_inicial_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicial_DATETIME); 
	$this->data_final_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_final_DATETIME); 
	$this->data_ultimo_processamento_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ultimo_processamento_DATETIME); 


    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){

	$this->data_inicial_DATETIME = $this->formatarDataTimeParaExibicao($this->data_inicial_DATETIME); 
	$this->data_final_DATETIME = $this->formatarDataTimeParaExibicao($this->data_final_DATETIME); 
	$this->data_ultimo_processamento_DATETIME = $this->formatarDataTimeParaExibicao($this->data_ultimo_processamento_DATETIME); 


    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["data_inicial_DATETIME"] = $this->data_inicial_DATETIME; 
		$_SESSION["data_final_DATETIME"] = $this->data_final_DATETIME; 
		$_SESSION["limite_mobile_INT"] = $this->limite_mobile_INT; 
		$_SESSION["estado_sincronizacao_id_INT"] = $this->estado_sincronizacao_id_INT; 
		$_SESSION["seq_INT"] = $this->seq_INT; 
		$_SESSION["crud_ARQUIVO"] = $this->crud_ARQUIVO; 
		$_SESSION["erro"] = $this->erro; 
		$_SESSION["id_ultimo_sistema_registro_sincronizador_INT"] = $this->id_ultimo_sistema_registro_sincronizador_INT; 
		$_SESSION["quantidade_crud_processado_INT"] = $this->quantidade_crud_processado_INT; 
		$_SESSION["processando_BOOLEAN"] = $this->processando_BOOLEAN; 
		$_SESSION["data_ultimo_processamento_DATETIME"] = $this->data_ultimo_processamento_DATETIME; 
		$_SESSION["corporacao_id_INT"] = $this->corporacao_id_INT; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["data_inicial_DATETIME"]);
		unset($_SESSION["data_final_DATETIME"]);
		unset($_SESSION["limite_mobile_INT"]);
		unset($_SESSION["estado_sincronizacao_id_INT"]);
		unset($_SESSION["seq_INT"]);
		unset($_SESSION["crud_ARQUIVO"]);
		unset($_SESSION["erro"]);
		unset($_SESSION["id_ultimo_sistema_registro_sincronizador_INT"]);
		unset($_SESSION["quantidade_crud_processado_INT"]);
		unset($_SESSION["processando_BOOLEAN"]);
		unset($_SESSION["data_ultimo_processamento_DATETIME"]);
		unset($_SESSION["corporacao_id_INT"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->data_inicial_DATETIME = $this->formatarDados($_SESSION["data_inicial_DATETIME{$numReg}"]); 
		$this->data_final_DATETIME = $this->formatarDados($_SESSION["data_final_DATETIME{$numReg}"]); 
		$this->limite_mobile_INT = $this->formatarDados($_SESSION["limite_mobile_INT{$numReg}"]); 
		$this->estado_sincronizacao_id_INT = $this->formatarDados($_SESSION["estado_sincronizacao_id_INT{$numReg}"]); 
		$this->seq_INT = $this->formatarDados($_SESSION["seq_INT{$numReg}"]); 
		$this->crud_ARQUIVO = $this->formatarDados($_SESSION["crud_ARQUIVO{$numReg}"]); 
		$this->erro = $this->formatarDados($_SESSION["erro{$numReg}"]); 
		$this->id_ultimo_sistema_registro_sincronizador_INT = $this->formatarDados($_SESSION["id_ultimo_sistema_registro_sincronizador_INT{$numReg}"]); 
		$this->quantidade_crud_processado_INT = $this->formatarDados($_SESSION["quantidade_crud_processado_INT{$numReg}"]); 
		$this->processando_BOOLEAN = $this->formatarDados($_SESSION["processando_BOOLEAN{$numReg}"]); 
		$this->data_ultimo_processamento_DATETIME = $this->formatarDados($_SESSION["data_ultimo_processamento_DATETIME{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_SESSION["corporacao_id_INT{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->data_inicial_DATETIME = $this->formatarDados($_POST["data_inicial_DATETIME{$numReg}"]); 
		$this->data_final_DATETIME = $this->formatarDados($_POST["data_final_DATETIME{$numReg}"]); 
		$this->limite_mobile_INT = $this->formatarDados($_POST["limite_mobile_INT{$numReg}"]); 
		$this->estado_sincronizacao_id_INT = $this->formatarDados($_POST["estado_sincronizacao_id_INT{$numReg}"]); 
		$this->seq_INT = $this->formatarDados($_POST["seq_INT{$numReg}"]); 
		$this->crud_ARQUIVO = $this->formatarDados($_POST["crud_ARQUIVO{$numReg}"]); 
		$this->erro = $this->formatarDados($_POST["erro{$numReg}"]); 
		$this->id_ultimo_sistema_registro_sincronizador_INT = $this->formatarDados($_POST["id_ultimo_sistema_registro_sincronizador_INT{$numReg}"]); 
		$this->quantidade_crud_processado_INT = $this->formatarDados($_POST["quantidade_crud_processado_INT{$numReg}"]); 
		$this->processando_BOOLEAN = $this->formatarDados($_POST["processando_BOOLEAN{$numReg}"]); 
		$this->data_ultimo_processamento_DATETIME = $this->formatarDados($_POST["data_ultimo_processamento_DATETIME{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_POST["corporacao_id_INT{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->data_inicial_DATETIME = $this->formatarDados($_GET["data_inicial_DATETIME{$numReg}"]); 
		$this->data_final_DATETIME = $this->formatarDados($_GET["data_final_DATETIME{$numReg}"]); 
		$this->limite_mobile_INT = $this->formatarDados($_GET["limite_mobile_INT{$numReg}"]); 
		$this->estado_sincronizacao_id_INT = $this->formatarDados($_GET["estado_sincronizacao_id_INT{$numReg}"]); 
		$this->seq_INT = $this->formatarDados($_GET["seq_INT{$numReg}"]); 
		$this->crud_ARQUIVO = $this->formatarDados($_GET["crud_ARQUIVO{$numReg}"]); 
		$this->erro = $this->formatarDados($_GET["erro{$numReg}"]); 
		$this->id_ultimo_sistema_registro_sincronizador_INT = $this->formatarDados($_GET["id_ultimo_sistema_registro_sincronizador_INT{$numReg}"]); 
		$this->quantidade_crud_processado_INT = $this->formatarDados($_GET["quantidade_crud_processado_INT{$numReg}"]); 
		$this->processando_BOOLEAN = $this->formatarDados($_GET["processando_BOOLEAN{$numReg}"]); 
		$this->data_ultimo_processamento_DATETIME = $this->formatarDados($_GET["data_ultimo_processamento_DATETIME{$numReg}"]); 
		$this->corporacao_id_INT = $this->formatarDados($_GET["corporacao_id_INT{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	$upd="";
	if(isset($tipo["data_inicial_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_inicial_DATETIME = $this->data_inicial_DATETIME, ";

	} 

	if(isset($tipo["data_final_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_final_DATETIME = $this->data_final_DATETIME, ";

	} 

	if(isset($tipo["limite_mobile_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "limite_mobile_INT = $this->limite_mobile_INT, ";

	} 

	if(isset($tipo["estado_sincronizacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "estado_sincronizacao_id_INT = $this->estado_sincronizacao_id_INT, ";

	} 

	if(isset($tipo["seq_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "seq_INT = $this->seq_INT, ";

	} 

	if(isset($tipo["crud_ARQUIVO{$numReg}"]) || $tipo == "vazio"){

		$upd.= "crud_ARQUIVO = '$this->crud_ARQUIVO', ";

	} 

	if(isset($tipo["erro{$numReg}"]) || $tipo == "vazio"){

		$upd.= "erro = '$this->erro', ";

	} 

	if(isset($tipo["id_ultimo_sistema_registro_sincronizador_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_ultimo_sistema_registro_sincronizador_INT = $this->id_ultimo_sistema_registro_sincronizador_INT, ";

	} 

	if(isset($tipo["quantidade_crud_processado_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "quantidade_crud_processado_INT = $this->quantidade_crud_processado_INT, ";

	} 

	if(isset($tipo["processando_BOOLEAN{$numReg}"]) || $tipo == "vazio"){

		$upd.= "processando_BOOLEAN = $this->processando_BOOLEAN, ";

	} 

	if(isset($tipo["data_ultimo_processamento_DATETIME{$numReg}"]) || $tipo == "vazio"){

		$upd.= "data_ultimo_processamento_DATETIME = $this->data_ultimo_processamento_DATETIME, ";

	} 

	if(isset($tipo["corporacao_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "corporacao_id_INT = $this->corporacao_id_INT, ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE sincronizacao SET $upd WHERE id = $id ";

	$msg = $this->database->queryMensagem($sql);
	return $msg;



    
    }
    

    } // classe: fim

    ?>
