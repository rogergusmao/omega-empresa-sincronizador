<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Crud_mobile_dependente
    * DATA DE GERAÇÃO: 23.06.2014
    * ARQUIVO:         DAO_Crud_mobile_dependente.php
    * TABELA MYSQL:    crud_mobile_dependente
    * BANCO DE DADOS:  sincronizador_web
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Crud_mobile_dependente extends Generic_DAO
    {


    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

	public $id;
	public $crud_mobile_id_INT;
	public $objCrud_mobile;
	public $id_sincronizador_mobile_INT;
	public $atributo;


    public $nomeEntidade;



    

	public $label_id;
	public $label_crud_mobile_id_INT;
	public $label_id_sincronizador_mobile_INT;
	public $label_atributo;






    // **********************
    // MÉTODO CONSTRUTOR
    // **********************

    public function __construct($niveisRaiz=2)
    {

    	parent::__construct($niveisRaiz);

    	$this->nomeEntidade = "";
    	$this->nomeTabela = "crud_mobile_dependente";
    	$this->campoId = "id";
    	$this->campoLabel = "id";

		$this->objCrud_mobile = new EXTDAO_Crud_mobile();


    }

    public function valorCampoLabel(){

    	return $this->getId();

    }

    

        public function getComboBoxAllCrud_mobile($objArgumentos){

		$objArgumentos->nome="crud_mobile_id_INT";
		$objArgumentos->id="crud_mobile_id_INT";
		$objArgumentos->valueReplaceId=false;

		return $this->objCrud_mobile->getComboBox($objArgumentos);

	}



	 public function __actionAdd(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionAddAjax(){

            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);

                

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();

                
    
        	}

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionEdit(){

            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for($i=1; $i <= $numeroRegistros; $i++){

                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                
                
    
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");

        }

        public function __actionRemove(){

            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_crud_mobile_dependente", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_crud_mobile_dependente", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);

        }

        


    // **********************
    // MÉTODOS GETTER's
    // **********************

    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getCrud_mobile_id_INT()
    {
    	return $this->crud_mobile_id_INT;
    }
    
    public function getId_sincronizador_mobile_INT()
    {
    	return $this->id_sincronizador_mobile_INT;
    }
    
    public function getAtributo()
    {
    	return $this->atributo;
    }
    
    // **********************
    // MÉTODOS SETTER's
    // **********************

    
    function setId($val)
    {
    	$this->id =  $val;
    }
    
    function setCrud_mobile_id_INT($val)
    {
    	$this->crud_mobile_id_INT =  $val;
    }
    
    function setId_sincronizador_mobile_INT($val)
    {
    	$this->id_sincronizador_mobile_INT =  $val;
    }
    
    function setAtributo($val)
    {
    	$this->atributo =  $val;
    }
    

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    	$sql =  "SELECT *  FROM crud_mobile_dependente WHERE id = $id;";
    	$this->database->query($sql);
    	$result = $this->database->result;
    	$row = $this->database->fetchObject($result);

    
        $this->id = $row->id;
        
        $this->crud_mobile_id_INT = $row->crud_mobile_id_INT;
        if($this->crud_mobile_id_INT)
			$this->objCrud_mobile->select($this->crud_mobile_id_INT);

        $this->id_sincronizador_mobile_INT = $row->id_sincronizador_mobile_INT;
        
        $this->atributo = $row->atributo;
        

    }
    

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    	$sql = "DELETE FROM crud_mobile_dependente WHERE id = $id;";
    	$this->database->query($sql);
    
    }
    
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    	$this->id = ""; //limpar chave com autoincremento

    	$sql = "INSERT INTO crud_mobile_dependente ( crud_mobile_id_INT,id_sincronizador_mobile_INT,atributo ) VALUES ( $this->crud_mobile_id_INT,$this->id_sincronizador_mobile_INT,'$this->atributo' )";
    	$this->database->query($sql);
    	

    }
    

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

	public function nomeCampoId(){ 

		return "id";

	}

	public function nomeCampoCrud_mobile_id_INT(){ 

		return "crud_mobile_id_INT";

	}

	public function nomeCampoId_sincronizador_mobile_INT(){ 

		return "id_sincronizador_mobile_INT";

	}

	public function nomeCampoAtributo(){ 

		return "atributo";

	}




    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

	public function imprimirCampoCrud_mobile_id_INT($objArguments){

		$objArguments->nome = "crud_mobile_id_INT";
		$objArguments->id = "crud_mobile_id_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoId_sincronizador_mobile_INT($objArguments){

		$objArguments->nome = "id_sincronizador_mobile_INT";
		$objArguments->id = "id_sincronizador_mobile_INT";

		return $this->campoInteiro($objArguments);

	}

	public function imprimirCampoAtributo($objArguments){

		$objArguments->nome = "atributo";
		$objArguments->id = "atributo";

		return $this->campoTexto($objArguments);

	}




    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

		if($this->crud_mobile_id_INT == ""){

			$this->crud_mobile_id_INT = "null";

		}

		if($this->id_sincronizador_mobile_INT == ""){

			$this->id_sincronizador_mobile_INT = "null";

		}





    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
    //****************************************************************************

    public function formatarParaExibicao(){



    }

    
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

		$_SESSION["id"] = $this->id; 
		$_SESSION["crud_mobile_id_INT"] = $this->crud_mobile_id_INT; 
		$_SESSION["id_sincronizador_mobile_INT"] = $this->id_sincronizador_mobile_INT; 
		$_SESSION["atributo"] = $this->atributo; 


    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

		unset($_SESSION["id"]);
		unset($_SESSION["crud_mobile_id_INT"]);
		unset($_SESSION["id_sincronizador_mobile_INT"]);
		unset($_SESSION["atributo"]);


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession($numReg){

		$this->id = $this->formatarDados($_SESSION["id{$numReg}"]); 
		$this->crud_mobile_id_INT = $this->formatarDados($_SESSION["crud_mobile_id_INT{$numReg}"]); 
		$this->id_sincronizador_mobile_INT = $this->formatarDados($_SESSION["id_sincronizador_mobile_INT{$numReg}"]); 
		$this->atributo = $this->formatarDados($_SESSION["atributo{$numReg}"]); 


    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost($numReg){

		$this->id = $this->formatarDados($_POST["id{$numReg}"]); 
		$this->crud_mobile_id_INT = $this->formatarDados($_POST["crud_mobile_id_INT{$numReg}"]); 
		$this->id_sincronizador_mobile_INT = $this->formatarDados($_POST["id_sincronizador_mobile_INT{$numReg}"]); 
		$this->atributo = $this->formatarDados($_POST["atributo{$numReg}"]); 


    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet($numReg){

		$this->id = $this->formatarDados($_GET["id{$numReg}"]); 
		$this->crud_mobile_id_INT = $this->formatarDados($_GET["crud_mobile_id_INT{$numReg}"]); 
		$this->id_sincronizador_mobile_INT = $this->formatarDados($_GET["id_sincronizador_mobile_INT{$numReg}"]); 
		$this->atributo = $this->formatarDados($_GET["atributo{$numReg}"]); 


    }
    
    // **********************
    // UPDATE
    // **********************

    public function update($id, $tipo = "vazio", $numReg=1)
    {

	if(isset($tipo["crud_mobile_id_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "crud_mobile_id_INT = $this->crud_mobile_id_INT, ";

	} 

	if(isset($tipo["id_sincronizador_mobile_INT{$numReg}"]) || $tipo == "vazio"){

		$upd.= "id_sincronizador_mobile_INT = $this->id_sincronizador_mobile_INT, ";

	} 

	if(isset($tipo["atributo{$numReg}"]) || $tipo == "vazio"){

		$upd.= "atributo = '$this->atributo', ";

	} 

		$upd = substr($upd, 0, -2);

    	$sql = " UPDATE crud_mobile_dependente SET $upd WHERE id = $id ";

    	$result = $this->database->query($sql);


    
    }
    

    } // classe: fim

    ?>
