<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Sincronizacao
 *
 * @author home
 */
class BO_Sincronizacao
{
    //put your code here

    public $db;
    public $objSincronizacao;
    public $objSincronizacaoMobile;
    public $idCorporacao;
    public $corporacao;

    public $configWeb;
    public $configSincronizadorWeb;


    public function __construct(
        $idCorporacao = null,
        $corporacao = null,
        $configSincronizadorWeb = false,
        $configWeb = false
    )
    {
        if($idCorporacao != null && $corporacao != null)
            $this->init(
                $idCorporacao,
                $corporacao,
                $configSincronizadorWeb,
                $configWeb
            );
        else if($idCorporacao != null )
            $this->initByIdCorporacao($idCorporacao  );

    }

    public function initByIdCorporacao($idCorporacao )
    {
        try {
            SingletonLog::close();
            $this->idCorporacao = $idCorporacao;
            $corporacao = BOSicobComum::getNomeCorporacao($idCorporacao);
            if($corporacao == null) return null;
            ConfiguracaoCorporacaoDinamica::factoryConfiguracaoSite($corporacao);

            $this->configWeb = Registry::get('ConfiguracaoDatabaseSecundario');
            $this->configSincronizadorWeb = Registry::get('ConfiguracaoDatabasePrincipal');

            $this->corporacao = $corporacao;

            $this->db = new Database($this->configSincronizadorWeb );

            $this->objSincronizacao = new EXTDAO_Sincronizacao($this->db);
            $this->objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($this->db);

            SingletonLog::init();
            return null;
        } catch (DatabaseException $dbex) {


            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);

            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);

            return $msg;
        } catch (Exception $ex) {
            $msg = new Mensagem(null, null, $ex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        }
    }

    public function init(
        $idCorporacao = null,
        $corporacao = null,
        $configSincronizadorWeb = false,
        $configWeb = false)
    {
        try {
            SingletonLog::close();

            if ($idCorporacao != null
                && $corporacao != null
            ) {
                $this->configWeb = $configWeb;
                $this->configSincronizadorWeb = $configSincronizadorWeb;
                $this->idCorporacao = $idCorporacao;
                $this->corporacao = $corporacao;

                $this->db = new Database($configSincronizadorWeb);

                $this->objSincronizacao = new EXTDAO_Sincronizacao($this->db);
                $this->objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($this->db);
            }
            SingletonLog::init();
            return null;
        } catch (DatabaseException $dbex) {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);

            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);

            return $msg;
        } catch (Exception $ex) {
            $msg = new Mensagem(null, null, $ex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        }
    }

    public function close()
    {
        if ($this->db != null) {
            $this->db->close();
            $this->db = null;
        }
    }

    public function processo1($executarEmApenasUmaIteracao)
    {
        try {

            $msg = $this->processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar();

            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            //ACESSO_NEGADO
            if (!$msg->ok()) {
                return $msg;
            } else {
                $idSincronizacao = $msg->mValor;
                //TODO para fim de depuracao, exportamos o banco ao fim do processo de sincrnoizacao
//                if($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
//                    $nomeArquivo = "{$corporacao}_{$idSincronizacao}_inicio_".NOME_BANCO_DE_DADOS_WEB.".sql";
//                    $path = "conteudo/sincronizacao_mobile/$nomeArquivo";
//                    $dbWeb = new Database(NOME_BANCO_DE_DADOS_WEB);
//                    $dbWeb->dumpSql($path);
//                }
                //INSERE AS OPERACOES NO BANCO ESPELHO
                $msg = $this->processaArquivosSincronizacaoMobile($idSincronizacao, $executarEmApenasUmaIteracao);

                //INCOMPLETA ou OPERACAO_REALIZADA_COM_SUCESSO ou RESULTADO_VAZIO
                return $msg;
            }

        } catch (DatabaseException $dbex) {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);

            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);

            return $msg;
        } catch (Exception $ex) {
            $msg = new Mensagem(null, null, $ex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        }

    }

    public function processo2()
    {
        $objSS = null;
        try {
            $objSS = new EXTDAO_Sistema_sequencia($this->configWeb);
            Registry::add($objSS);

            //EXECUTA AS OPERACOES DO BANCO ESPELHO NO WEB
            $msg = $this->executaCrudMobileDoBancoEspelhoNoBancoWeb();
            //        echo "<br/>PASSO 3<br/>";
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);

            if (!$msg->erro()) {
                $msg = $this->processoInsereOperacoesDoBancoWebNoBancoEspelho();
                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
            }
            $objSS->close();
            Registry::remove('EXTDAO_Sistema_sequencia');
            return $msg;
        } catch (DatabaseException $dbex) {
            $objSS->close();
            Registry::remove('EXTDAO_Sistema_sequencia');
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        } catch (Exception $ex) {
            $objSS->close();
            Registry::remove('EXTDAO_Sistema_sequencia');
            $msg = new Mensagem(null, null, $ex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        }

    }

    public function executaSincronizacaoCompleta()
    {
        $lock = false;
        try {

            if (!LockSincronizador::semaphoreGet($this->idCorporacao))
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "O processo de sincroniza��o da corporacao " . $this->idCorporacao . " est� retido.");
            $lock = true;
            $relatorio = "Processo 1...";
            // id vindo da tabela da web sistema_registro_sincronizador
            $executarEmApenasUmaIteracao = true;
            SingletonLog::init();
            //        ini_set('memory_limit', '-1');
            //INSERE OS MOBILES
            //set_time_limit(10 * 60);
            ini_set('max_execution_time', -1);
            SingletonLog::writeLogStr("SincronizacaoCompleta::processo1 - inicio");

            $msg = $this->processo1($executarEmApenasUmaIteracao);

            SingletonLog::writeLogStr("SincronizacaoCompleta::processo1 - fim. ", $msg);
            $relatorio .= "Processo 1 Finalizado! " . print_r($msg, true);
            if ($msg->erro()) {
                SingletonLog::close();
                LockSincronizador::semaphoreRelease($this->idCorporacao);
                return $msg;
            } else if (
                $msg->mCodRetorno != PROTOCOLO_SISTEMA::RESULTADO_VAZIO
                && $msg->mCodRetorno != PROTOCOLO_SISTEMA::ACESSO_NEGADO
                && !$executarEmApenasUmaIteracao
            ) {
                SingletonLog::close();
                LockSincronizador::semaphoreRelease($this->idCorporacao);
                //OU processou um arquivo mobile = OPERACAO_REALIZDA_COM_SUCESSO OU OPERACAO_INCOMPLETA
                //(ainda existem mais arquivos de mobile para serem processados)
                return $msg;
            }
            //        echo "processo 2<br/>";
            //EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao($idSincronizacao, $this->db);

            SingletonLog::writeLogStr("SincronizacaoCompleta::processo2 - inicio");
            $relatorio .= "Processo 2...";

            $msg = $this->processo2();

            $relatorio .= "Processo 2 Finalizado! " . print_r($msg, true);
            SingletonLog::writeLogStr("SincronizacaoCompleta::processo2 - fim. ", $msg);
            if ($msg->erro()) {
                SingletonLog::close();
                LockSincronizador::semaphoreRelease($this->idCorporacao);
                return $msg;
            } else if ($msg->mCodRetorno != PROTOCOLO_SISTEMA::RESULTADO_VAZIO
                && $msg->mCodRetorno != PROTOCOLO_SISTEMA::ACESSO_NEGADO
                && !$executarEmApenasUmaIteracao
            ) {
                SingletonLog::close();
                LockSincronizador::semaphoreRelease($this->idCorporacao);
                return $msg;
            }
            SingletonLog::writeLogStr("SincronizacaoCompleta::processo3 - inicio");
            $relatorio .= "Processo 3 ... ";
            $msg = $this->processo3();
            $relatorio .= "Processo 3 Finalizado! " . print_r($msg, true);
            SingletonLog::writeLogStr("SincronizacaoCompleta::processo3 - fim. ", $msg);

            SingletonLog::close();
            LockSincronizador::semaphoreRelease($this->idCorporacao);
            return new Mensagem(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                $relatorio);
        } catch (DatabaseException $ex2) {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $ex2);
            SingletonLog::writeLogStr("BANCO FORA DO AR", $msg);
            SingletonLog::close();
            if ($lock) LockSincronizador::semaphoreRelease($this->idCorporacao);
            return $msg;
        } catch (Exception $ex) {
            $msg = new Mensagem(null, null, $ex);
            SingletonLog::writeLogStr("Exception: ", $msg);

            SingletonLog::close();

            if ($lock) LockSincronizador::semaphoreRelease($this->idCorporacao);
            return $msg;
        }
    }

    public function processoInsereOperacoesDoBancoWebNoBancoEspelho()
    {


        $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
            $this->idCorporacao,
            null,
            EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
            $this->db);
        if ($msg->erro())
            return $msg;

        if (!$msg->ok()) {
            $primeiraMsg = print_r($msg, true);
            $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
                $this->idCorporacao,
                EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
                EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                $this->db,
                array(
                    EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
                    EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB

                )
            );
            if ($msg->erro())
                return $msg;
            else if($msg->resultadoVazio()){
//                EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
//                    $this->idCorporacao,
//                    $idSincronizacao,
//                    EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR,
//                    $this->db);
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Talvez tenha o processo 3 a ser executado. Primeira msg: {$primeiraMsg}. Segunda msg: ".print_r($msg, true));
            }
            else if (!$msg->ok())
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "Id sinc nulo. N�o existe condicional suficiente para executar a tarefa INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO. ".print_r($msg, true));
            else {
                $idSincronizacao = $msg->mValor;
            }
        } else {
            $idSincronizacao = $msg->mValor;
        }
        $msg = $this->insereCrudsWeb($idSincronizacao);
        return $msg;
    }

    public function processo3()
    {
        try {

            //$msg = $this->nextSincronizacaoNoEstado(EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB);

            $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
                $this->idCorporacao,
                null,
                EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR,
                $this->db);

            if ($msg->erro())
                return $msg;

            if (!$msg->ok()) {
                $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
                    $this->idCorporacao,
                    EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR,
                    EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
                    $this->db,
                    array(EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR));
                if ($msg->erro())
                    return $msg;
                else if (!$msg->ok())
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                        "ID sinc 2 nulo. N�o existe condicional suficiente para executar a tarefa INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO");
                else {
                    $idSincronizacao = $msg->mValor;
                }

            } else {
                $idSincronizacao = $msg->mValor;
            }

            $msg = $this->geraArquivoCrudDaSincronizacao($idSincronizacao);
            //TODO para fim de depuracao, exportamos o banco ao fim do processo de sincrnoizacao
//            if($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
//                $nomeArquivo = "{$corporacao}_{$idSincronizacao}_fim_".NOME_BANCO_DE_DADOS_WEB.".sql";
//                $path = "conteudo/sincronizacao_mobile/$nomeArquivo";
//                $dbWeb = new Database(NOME_BANCO_DE_DADOS_WEB);
//                $dbWeb->dumpSql($path);
//            }
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
            //        $msg = $this->nextSincronizacaoNoEstado(EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR);
            //        if($msg->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) return $msg;
            //        return $this->geraArquivoCrudDaSincronizacao($corporacao);
        } catch (DatabaseException $dbex) {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        } catch (Exception $ex) {
            $msg = new Mensagem(null, null, $ex);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            return $msg;
        }
    }

    public function executaCrudMobileDoBancoEspelhoNoBancoWeb()
    {

        //$msg = $this->nextSincronizacaoNoEstado(EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB);

        $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
            $this->idCorporacao,
            null,
            EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB,
            $this->db);

        if ($msg->erro())
            return $msg;

        if (!$msg->ok()) {
            SingletonLog::writeLogStr("N�o existe sincronizacao "
                . " no estado PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB");
            $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
                $this->idCorporacao,
                EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO,
                $this->db,
                array(
                    EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
                    EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB
                )
            );

            if ($msg->erro())
                return $msg;
            else if (!$msg->ok()) {

                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "N�o existe condicional suficiente para executar a tarefa INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO");
            } else {

                $idSincronizacao = $msg->mValor;
                SingletonLog::writeLogStr("Nova sincronizacao $idSincronizacao "
                    . " no estado PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB");
            }
        } else {
            $idSincronizacao = $msg->mValor;
            SingletonLog::writeLogStr("Retomando a sincronizacao $idSincronizacao "
                . " no estado PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB");
        }


        $msg = $this->executandoCrudMobileDaSincronizacao($idSincronizacao);

        return $msg;

    }




    //PROCESSO 1
    //EXTDAO_Estado_sincronizacao::ADICIONANDO_CELULARES
    public function iniciaSincronizacao()
    {
        //id vindo da tabela da web sistema_registro_sincronizador
        try {

            $msg = EXTDAO_Sincronizacao_mobile::existeMobilesParaNovaSincronizacaoQuePossuemArquivos(
                $this->idCorporacao, $this->db);

            if ($msg != null && $msg->erro())
                return $msg;
            else if ($msg->ok()) {
                $existeSMComArquivo = $msg->mValor;
            } else {
                $existeSMComArquivo = false;
            }

//            echo "EXISTE $existeSMComArquivo. ".print_r($msg, true);
            $strExiste = $existeSMComArquivo ? "1" : "0";

            SingletonLog::writeLogStr("iniciaSincronizacao. Existe Sincronizacao Mobile com arq: $strExiste ");
            $forcar = false;
            $idsSM = null;
            if (!$existeSMComArquivo) {
                $boBancoWeb = new BO_Banco_web (
                    $this->idCorporacao,
                    $this->corporacao,
                    $this->configWeb,
                    $this->configSincronizadorWeb);
                $msgBW = $boBancoWeb->init();
                if ($msgBW != null && $msgBW->erro()) return $msgBW;
                $msgCWS = $boBancoWeb->existemCrudsWebParaSincronizar();
                $boBancoWeb->dbWeb->close();
                if ($msgCWS->erro())
                    return $msgCWS;
                if ($msgCWS->resultadoVazio()) {
                    SingletonLog::writeLogStr("Nao existem cruds web para serem sincronizados.");
                    $existe = EXTDAO_Sincronizacao_mobile::existeMobileNaFilaComOBancoUltrapassado($this->idCorporacao, $this->db);
                    if ($existe->erro()) return $existe;
                    if ($existe->resultadoVazio()) {
                        SingletonLog::writeLogStr("Nao temos nem mobiles que acabaram de sincronizar, entao nao abriremos uma nova sincronizacao.");
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::SEM_DADOS_PARA_SEREM_SINCRONIZADOS,
                            "Nenhum dado web ou mobile para serem sincronizados.");
                    } else {
                        $forcar = true;
                        SingletonLog::writeLogStr("Existe mobile que acabou de sincronizar e esta com o banco atrasado.");
                    }
                } else {
                    SingletonLog::writeLogStr("Existe crud web para sincronizar.");
                    $forcar = true;
                }
            } else
                $forcar = true;
            //exit();
            if ($idsSM == null) {

                $msg = EXTDAO_Sincronizacao_mobile::getMobilesParaNovaSincronizacao(
                    $this->idCorporacao, $this->db);

                if ($msg->erro())
                    return $msg;
                else if ($msg->ok())
                    $idsSM = $msg->mVetor;
            }
            if ($idsSM == null)
                $idsSM = array();

            if (count($idsSM) || $forcar) {
                SingletonLog::writeLogStr("Mobiles da nova sincronizacao: " . print_r($idsSM, true));
                $this->db->iniciarTransacao();
                $this->objSincronizacao = new EXTDAO_Sincronizacao($this->db);
                $nowSQL = Helper::getDiaEHoraAtualSQL();
                $this->objSincronizacao->setData_inicial_DATETIME($nowSQL);
                $this->objSincronizacao->setEstado_sincronizacao_id_INT(EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO);
                $this->objSincronizacao->setLimite_mobile_INT(LIMITE_CELULARES_POR_SINCRONIZACAO);
                $this->objSincronizacao->setData_ultimo_processamento_DATETIME($nowSQL);
                $this->objSincronizacao->setProcessando_BOOLEAN("0");
                $this->objSincronizacao->setCorporacao_id_INT($this->idCorporacao);

                $this->objSincronizacao->formatarParaSQL();
                $this->objSincronizacao->insert();
                $idSincronizacao = $this->objSincronizacao->getIdDoUltimoRegistroInserido($this->idCorporacao);
                SingletonLog::writeLogStr("Nova sincronizacao de id $idSincronizacao");
                $ids = "";
                $tot = count($idsSM);

                if ($tot > 0) {
                    $ids = Helper::arrayToString($idsSM, ',');
                    $q = "UPDATE sincronizacao_mobile "
                        . " SET sincronizacao_id_INT = '{$idSincronizacao}', "
                        . "     estado_sincronizacao_mobile_id_INT = '" . EXTDAO_Estado_sincronizacao_mobile::INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO . "'  "
                        . " WHERE id IN ($ids) "
                        . "     AND estado_sincronizacao_mobile_id_INT = '" . EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO . "' ";

                    $msg = $this->db->queryMensagem($q);
                    if ($msg != null && $msg->erro()) return $msg;
                    SingletonLog::writeLogStr("Resultado da atualizacao dos estados sincronizacao_mobile", $msg);

                    //                print_r($msg);
                    $msg = EXTDAO_Sincronizacao_mobile_iteracao::inserirLista(
                        $this->db,
                        $ids,
                        EXTDAO_Estado_sincronizacao_mobile::INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO);
                    if ($msg != null && $msg->erro()) {
                        $this->db->rollbackTransacao();
                        return $msg;
                    }
                } else {
                    SingletonLog::writeLogStr("Sincronizacao sem mobiles");
                }

                $this->db->commitTransacao();
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Novo processo de sincronizacao iniciado.",
                    $idSincronizacao);

            } else {
                SingletonLog::writeLogStr("Nao existem mobiles para serem sincronizados");
                return new Mensagem_token(PROTOCOLO_SISTEMA::SEM_DADOS_PARA_SEREM_SINCRONIZADOS,
                    "Fila de celulares para o processo de sincroniza��o est� vazia.");
            }
        } catch (DatabaseException $dbex) {
            $this->db->rollbackTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
        } catch (Exception $exc) {
            $this->db->rollbackTransacao();
            return new Mensagem(null, null, $exc);
        }
    }

    private function processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar(){
        SingletonLog::writeLogStr("Tenta iniciar uma nova sincronizacao para o estado: INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO");

        $msg = EXTDAO_Sincronizacao::iniciaEtapaSePossivel(
            $this->idCorporacao,
            null,
            EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO,
            $this->db);
        SingletonLog::writeLogStr("INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO: ".print_r($msg,true));
        if ($msg->erro())
            return $msg;

        if (!$msg->ok()) {

            SingletonLog::writeLogStr("Procurando id sincronizacao no estado INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO");

            $msg = EXTDAO_Sincronizacao::getIdNoEstado(
                $this->idCorporacao,
                EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO,
                $this->db);
            SingletonLog::writeLogStr("Resultado da busca INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO: ".print_r($msg, true));
            if ($msg->erro()) {

                return $msg;
            } else if ($msg->ok()) {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "Existe um processo de sincronizacao no estado "
                    . EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO
                    . " temos que aguardar o seu fim");
            } else {
                SingletonLog::writeLogStr("iniciaSincronizacao...");

                //A SINCRONIZACAO INICIA AQUI!!!
                $msg = $this->iniciaSincronizacao();

                SingletonLog::writeLogStr("iniciaSincronizacao: ".print_r($msg, true));

                return $msg;
            }
        } else {

            $idSincronizacaoAux = $msg->mValor;
            HelperLog::logSincronizacao("finalizaProcessamentoDaSincronizacao:PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB...");
            $msg = EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
                $this->idCorporacao,
                $idSincronizacaoAux,
                EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                $this->db);

            HelperLog::logSincronizacao("finalizaProcessamentoDaSincronizacao: ".print_r($msg, true));
            if ($msg != null && $msg->erro()) return $msg;

            return new Mensagem_token(null, $msg->mMensagem, $idSincronizacaoAux);

        }
    }

    //PROCESSO 2
    //EXTDAO_Estado_sincronizacao::PROCESSANDO_CRUD_MOBILE
    //EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_PROCESSAMENTO_SINCRONIZADOR
    private function processaArquivosSincronizacaoMobile(
        $idSincronizacao,
        $executarEmApenasUmaIteracao = true)
    {
        $idsSincronizacaoMobile = EXTDAO_Sincronizacao_mobile::getMobilesDaSincronizacaoNoEstado(
            $this->idCorporacao,
            $idSincronizacao,
            EXTDAO_Estado_sincronizacao_mobile::INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO,
            $this->db);

        $countSM = count($idsSincronizacaoMobile);
        if ($countSM > 0) {
            SingletonLog::writeLogStr("processaArquivosSincronizacaoMobile. Id Sincronizacao: $idSincronizacao. Total de mobiles nessa sincronizacao: $countSM.");
            for ($i = 0; $i < count($idsSincronizacaoMobile); $i++) {
                $idSM = $idsSincronizacaoMobile[$i];
                $this->objSincronizacaoMobile->select($idSM);

                $msg = $this->objSincronizacaoMobile->processaCrudMobile($this->idCorporacao, $this->corporacao);
                if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR) {
                    return $msg;
                }
                if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
                    $this->setErroSincronizacao($idSincronizacao, $msg->mMensagem);
                    return $msg;
                } else {

                    if ($countSM == 1) {
                        $msg = new Mensagem_token(
                            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                            "A sincronizacao_mobile '$idSM' foi processada com sucesso. A sincroniza��o passou para o proximo estado.",
                            $idSincronizacao);
                        break;
                    } else {
                        $msg = new Mensagem_token(
                            PROTOCOLO_SISTEMA::OPERACAO_INCOMPLETA,
                            "A sincronizacao_mobile '$idSM' foi processada com sucesso. Mas ainda existem sincronizacao_mobile's a serem processadas.",
                            count($idsSincronizacaoMobile) - 1);
                        if (!$executarEmApenasUmaIteracao) {
                            break;
                        }
                    }
                }
            }

            EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
                $this->idCorporacao,
                $idSincronizacao,
                EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                $this->db);

            return $msg;
        } else {

            EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
                $this->idCorporacao,
                $idSincronizacao,
                EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                $this->db);

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                "No processo de sincroniza��o '{$idSincronizacao}' n�o existem mobiles no estado: INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO) .",
                $idSincronizacao);
        }
    }


    public function getMenorCrud($mobiles)
    {
        $menorIdCrud = null;
        $index = null;
        for ($i = 0; $i < count($mobiles); $i++) {
            $mobile = $mobiles[$i];
            if ($menorIdCrud == null || $mobile['ultimo_crud_id_INT'] < $menorIdCrud) {
                $menorIdCrud = $mobile['ultimo_crud_id_INT'];
                $index = $i;
            }
        }

        return $menorIdCrud;
    }

    private function setErroSincronizacao(
        $idSincronizacao,
        $msg = null,
        $idEstadoErro = EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO)
    {
        $db = null;
        try {
            $db = new Database($this->configSincronizadorWeb);

            $db->iniciarTransacao();

            $this->objSincronizacao->select($idSincronizacao);
            $nowSQL = Helper::getDiaEHoraAtualSQL();
            $this->objSincronizacao->setData_ultimo_processamento_DATETIME($nowSQL);
            $this->objSincronizacao->setData_final_DATETIME($nowSQL);
            $this->objSincronizacao->setProcessando_BOOLEAN('0');
            $this->objSincronizacao->setEstado_sincronizacao_id_INT($idEstadoErro);

            $this->objSincronizacao->setErro(Helper::substring( $msg,255));
            $this->objSincronizacao->formatarParaSQL();
            $this->objSincronizacao->update($this->objSincronizacao->getId());
            $db->commitTransacao();
        } catch (Exception $ex) {
            if ($db != null) $db->rollbackTransacao();
            SingletonLog::writeLogException($ex);
        }

    }

    private function executandoCrudMobileDaSincronizacao($idSincronizacao)
    {
        try {

            $this->objSincronizacao->select($idSincronizacao);

            $idsSincronizacaoMobile = EXTDAO_Sincronizacao_mobile::getUltimosCrudsDaSincronizacaoNoEstado(
                $this->idCorporacao,
                $idSincronizacao,
                EXTDAO_Estado_sincronizacao_mobile::PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                $this->db);

            $msg = new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            if (count($idsSincronizacaoMobile) == 0) {
                EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
                    $this->idCorporacao,
                    $idSincronizacao,
                    EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
                    $this->db);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "N�o existem mais cruds relacionados com sincronizacao_mobile para serem processados.");
            }

            $boBanco = new  BO_Banco(
                $this->idCorporacao,
                $this->corporacao,
                $this->configWeb,
                $this->configSincronizadorWeb);
            $msg = $boBanco->init();
            if ($msg != null && $msg->erro()) return $msg;
            SingletonLog::writeLogStr("executaCrudsMobileNoBancoWeb idSincronizacao[$idSincronizacao]");

            $msg = $boBanco->executaCrudsMobileNoBancoWeb($idSincronizacao);

            SingletonLog::writeLogStr("executaCrudsMobileNoBancoWeb idSincronizacao[$idSincronizacao]: resultado - " . print_r($msg, true));
            if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR) {
                return $msg;
            } else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_PROCESSAMENTO) {
                $this->setErroSincronizacao($idSincronizacao, $msg->mMensagem, $msg->mValor);
                return $msg;
            } else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
                $this->setErroSincronizacao($idSincronizacao, $msg->mMensagem);
                return $msg;
            } else {

                EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
                    $this->idCorporacao,
                    $idSincronizacao,
                    EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO,
                    $this->db);

                return $msg;
            }

        } catch (DatabaseException $dbex) {
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
        } catch (Exception $exc) {
            $this->setErroSincronizacao(
                $idSincronizacao,
                "Message: " . $exc->getMessage() . ". Stacktrace: " . $exc->getTraceAsString());
            return new Mensagem(null, null, $exc);
        }

    }

    //PROCESSO 4
    private function insereCrudsWeb($idSincronizacao)
    {

        //PARA MONTAR OS CONTAINERS DE OPERACAO: remove, edit, insert.
        //Deve-se analisar as seguintes entidades:
        //  - sincronizador_registro_sincronizador da Base Web
        //  - crud's da sincronizacao do contexto  da Base doSincronizador

        $bancoWeb = new BO_Banco_web(
            $this->idCorporacao,
            $this->corporacao,
            $this->configWeb,
            $this->configSincronizadorWeb);
        $msg = $bancoWeb->init();
        if ($msg != null && $msg->erro()) return $msg;
        $msg = $bancoWeb->insereCrudsWebDoSistemaRegistroSincronizador(
            $idSincronizacao, $this->corporacao);
        if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR) {
            //NAO FAZ NADA
        } else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
            $this->setErroSincronizacao($idSincronizacao, $msg->mMensagem);
        } else {

            EXTDAO_Sincronizacao::finalizaProcessamentoDaSincronizacao(
                $this->idCorporacao,
                $idSincronizacao,
                EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR,
                $this->db);

        }

        return $msg;
    }

    //PROCESSO 5
    public function geraArquivoCrudDaSincronizacao($idSincronizacao)
    {
        $initTransacao = false;
        try {

            $obj = new BO_Arquivo_crud_sincronizacao(
                $this->corporacao,
                $this->idCorporacao,
                $this->configWeb,
                $this->configSincronizadorWeb);
            $obj->init($idSincronizacao);

            $msg = $obj->geraArquivoCrud();

            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);

            if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR) {
                return $msg;
            } else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
                $this->setErroSincronizacao($idSincronizacao, $msg->mMensagem);
            } else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) {

                $this->db->iniciarTransacao();
                $initTransacao = true;
                $nomeArquivo = $msg->mValor;

                //$this->objSincronizacao = new EXTDAO_Sincronizacao();
                $this->objSincronizacao->select($idSincronizacao);
                $this->objSincronizacao->setCrud_ARQUIVO($nomeArquivo);
                $this->objSincronizacao->setEstado_sincronizacao_id_INT(EXTDAO_Estado_sincronizacao::FINALIZADO);
                $total = EXTDAO_Crud::getTotalCrudsDaSincronizacao(
                    $this->idCorporacao, $idSincronizacao, $this->db);
                $this->objSincronizacao->setQuantidade_crud_processado_INT($total);
                $this->objSincronizacao->setProcessando_BOOLEAN('0');
                $nowSQL = Helper::getDiaEHoraAtualSQL();
                $this->objSincronizacao->setData_ultimo_processamento_DATETIME($nowSQL);
                $this->objSincronizacao->setData_final_DATETIME($nowSQL);

                $this->objSincronizacao->formatarParaSQL();
                $msg2 = $this->objSincronizacao->update($idSincronizacao);

                $log = Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg2);

                //                print_r($msg2);
                //                exit();
                $this->db->commitTransacao();
            }

            return $msg;
        } catch (DatabaseException $dbex) {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);

            if ($this->db != null && $initTransacao)
                $this->db->rollbackTransacao();
            return $msg;
        } catch (Exception $ex) {
            $msg = new Mensagem(null, null, $ex);
            $this->setErroSincronizacao($idSincronizacao, print_r($msg, true));
            if ($this->db != null && $initTransacao)
                $this->db->rollbackTransacao();
            return $msg;
        }

    }


    public function assinaturaMigrada($idCorporacao, $idUltimaSincronizacao = null, $corporacao = null)
    {
        $db = new Database();
//        $objSincronizacao = new EXTDAO_Sincronizacao($db);
//        $objSincronizacao->setId( $idUltimaSincronizacao + 1);
//        $objSincronizacao->setCorporacao_id_INT($idCorporacao);
//        $objSincronizacao->setEstado_sincronizacao_id_INT(EXTDAO_Estado_sincronizacao::RESETADO);
//        $objSincronizacao->setData_inicial_DATETIME(Helper::getDiaEHoraAtualSQL());
//        $objSincronizacao->formatarParaSQL();
//        $objSincronizacao->insert();

        EXTDAO_Corporacao::updateNome($db, $idCorporacao, $corporacao);

        if (is_numeric($idUltimaSincronizacao)) {
            $novoId = $idUltimaSincronizacao + 1;
            $msg = $db->queryMensagem(
                "INSERT INTO sincronizacao (id, corporacao_id_INT, estado_sincronizacao_id_INT, data_inicial_DATETIME)
                VALUE ($novoId, $idCorporacao, " . EXTDAO_Estado_sincronizacao::RESETADO . ", '" . Helper::getDiaEHoraAtualSQL() . "') ");
        }
        if ($idUltimaSincronizacao == null || $msg != null && !$msg->ok()) {
            $msg = $db->queryMensagem(
                "INSERT INTO sincronizacao ( corporacao_id_INT, estado_sincronizacao_id_INT, data_inicial_DATETIME)
            VALUE ($idCorporacao, " . EXTDAO_Estado_sincronizacao::RESETADO . ", '" . Helper::getDiaEHoraAtualSQL() . "') ");

            if ($msg != null && $msg->erro())
                return $msg;
        }

        $idSincronizacao = $db->getLastInsertId();
        $dataUltimaMigracao = EXTDAO_Corporacao::updateDataUltimaMigracao($db);
        return new Mensagem(
            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
            "Sincronizacao adicionada como marco do novo banco sqlite: $idSincronizacao."
            . " Data da migracao atualizada: $dataUltimaMigracao.",
            $dataUltimaMigracao);

    }

    public static function pushCorporacaoToSync(){
        $redis = new HelperRedis();
        $redis = HelperRedis::getSingleton();

    }

}
