<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Tabela
 *
 * @author home
 */
class BO_Crud {
    //put your code here
    public $nome;
    public $idSistemaTabela;
    public $containerRemove;
    public $containerInsert;
    public $containerEdit;
    public $db;
    public $objSistemaTabela;
    public $dbWeb;
    public $idsWebRemover;
    public $idsMobileInserir;
    public $idsWebRepetidos;

    public $atributos;
    public $atributosConteudo;
    public $idSincronizacao;
    public $atributosChaveUnica;

    public $objCrudMobile;



    public function __construct( $nome, $idSistemaTabela, $objSincronizacao, $db = null){

        $this->db = new Database();

        $this->nome = $nome;
        $this->idSistemaTabela = $idSistemaTabela;
        $this->objSistemaTabela = new EXTDAO_Sistema_tabela();
        $this->objSistemaTabela->select($idSistemaTabela);
        $this->atributosChaveUnica = $this->objSistemaTabela->getAtributosChaveUnica();

        $this->idSincronizacao = $objSincronizacao->getId();
        $this->dbWeb = new Database(NOME_BANCO_DE_DADOS_WEB);

        $this->idsWebRemover = array();
        $this->idsMobileInserir = array();
        $this->idsWebRepetidos = array();


        $this->atributos = $this->db->getAtributosDaTabela('__'.$this->nome);
        $this->atributosConteudo = array();
        for($k = 0 ; $k < count($this->atributos); $k++){
            $attr = $this->atributos[$k];
            if($attr == 'id'
                || $attr == 'crud_mobile_id_INT') continue;
            $this->atributosConteudo[count($this->atributosConteudo)] = $attr;
        }


    }
    public function validaOperacaoEditar($idRegistro){

        $index = array_search($idRegistro, $this->idsWebRemover);
        if($index != false){
            unset($this->idsWebRemover[$index]);
            $this->idsWebRepetidos[count($this->idsWebRepetidos)] = $idRegistro;
//            for($j = 0 ;$j < count($this->containerRemove); $j++){
//                if($this->containerRemove[$j] != null
//                    && $this->containerRemove[$j]['id_tabela_web_INT'] == $idRegistro){
//                    
//                    //unset( $this->containerRemove[$j] );
//                }
//            }
            return false;
        }

        $index = array_search($idRegistro, $this->idsWebRepetidos);
        if($index != false){


            return false;
        }
        return true;
    }

    public function inicializaContainerInsert($primeiroCrud, $ultimoCrud){

        $container = EXTDAO_Crud::getContainerOperacaoBanco(
            $this->idCorporacao,
            EXTDAO_Tipo_operacao_banco::Inserir,
            $this->idSistemaTabela,
            $primeiroCrud, $ultimoCrud,
            $this->db);

        $limite = count($container);
        for($i = 0 ; $i < $limite; $i++){
            $this->idsMobileInserir[count($this->idsMobileInserir)] = $container[$i]['id_tabela_mobile_INT'];
        }

        $this->containerInsert = $container;
    }

    public function inicializaContainerRemove($primeiroCrud, $ultimoCrud){

        $container = EXTDAO_Crud::getContainerOperacaoBanco(
            $this->idCorporacao,
            EXTDAO_Tipo_operacao_banco::Remover,
            $this->idSistemaTabela,
            $primeiroCrud, $ultimoCrud,
            $this->db);
        for($i = 0 ; $i < count($container); $i++){
            $this->idsWebRemover[count($this->idsWebRemover)] = $container[$i]['id_tabela_web_INT'];
        }
        $this->containerRemove = $container;
    }


    public function inicializaContainerEdit($primeiroCrud, $ultimoCrud){

        $container = EXTDAO_Crud::getContainerOperacaoBanco(
            $this->idCorporacao,
            EXTDAO_Tipo_operacao_banco::Editar,
            $this->idSistemaTabela,
            $primeiroCrud,
            $ultimoCrud,
            $this->db);
        $limite = count($container);
        $removeu  = false;
        for($i = 0 ; $i < $limite; $i++){
            $idRegistro = $container[$i]['idTabelaWeb'];
            if(!$this->validaOperacaoEditar($idRegistro)) {
                unset($container[$i] );
                $removeu  = true;
                continue;
            }

        }
        $this->containerEdit = $container;
    }



    public function inicializaContainer($primeiroCrud, $ultimoCrud){

        $this->inicializaContainerRemove($primeiroCrud, $ultimoCrud);
        $this->inicializaContainerInsert($primeiroCrud, $ultimoCrud);
        $this->inicializaContainerEdit($primeiroCrud, $ultimoCrud);

        $this->containerRemove = Helper::atualizaIndiceArray($this->containerRemove);
        $this->containerInsert = Helper::atualizaIndiceArray($this->containerInsert);
        $this->containerEdit = Helper::atualizaIndiceArray($this->containerEdit);
    }
    public function geraArquivoCrud(){
        $path = ConfiguracaoSite::getPathConteudo()."sincronizacao_web/";
        $arquivo = $this->idSincronizacao . "_crud_".Helper::getDiaEHoraNomeArquivo().".json";
        $handle = fopen($path.$arquivo, "w+");
        try {

            $mensagem = $this->executarProtocoloRemover($handle);
            if($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR)
                return $mensagem;
            else{
                $mensagem =$this->executaProtocolo($handle, $this->containerInsert, EXTDAO_Tipo_operacao_banco::Inserir);
                if($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR)
                    return $mensagem;
                else{
                    $mensagem =$this->executaProtocoloEditar($handle, $this->containerEdit, EXTDAO_Tipo_operacao_banco::Editar);
                    if($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR)
                        return $mensagem;
                    else
                        $mensagem = new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Todos os cruds foram processados");
                }

            }

        }catch(DatabaseException $dbex){
            $mensagem = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        }  catch (Exception $exc) {
            $mensagem = new Mensagem(null, null, $exc);
        }
        if($handle && $handle != null )
            fclose($handle);
        if($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
            $objSincronizacao = new EXTDAO_Sincronizacao();
            $objSincronizacao->select($this->idSincronizacao);
            $objSincronizacao->setCrud_ARQUIVO($arquivo);
            $objSincronizacao->setData_final_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objSincronizacao->formatarParaSQL();
            $objSincronizacao->update($this->idSincronizacao);
        } else if($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR){
            //deleta o arquivo
        }

        return $mensagem;
    }


    private function getWhereConsultaPelaChaveUnica($valores){
        if(!count($valores) || !count($this->atributosChaveUnica))
            return null;
        $str = "";
        $count = 0;


        for($i = 0 ; $i < count($this->atributosConteudo); $i++){
            $valor = $valores[$i];


            for($j = 0; $j < count($this->atributosChaveUnica); $j++){
                if($this->atributosConteudo[$i] == $this->atributosChaveUnica[$j]){
                    if(strlen($str)) $str .= " AND  ";
                    $str .= " {$this->atributosChaveUnica[$j]} = '$valor' ";
                    $count++;
                    break;
                }
            }
        }

        if($count != count($this->atributosChaveUnica))
            return null;
        else return $str;
    }


    public function vazia(){
        if(!count($this->containerInsert) && !count($this->containerEdit) && !count($this->containerRemove))
            return true;
        else return false;
    }


    private function executaProtocolo($handle, $container, $tipoOperacaoBanco){

        if(count($container)){
            //gravar json no arquivo
            $cabecalhoProtocolo = array();
            $cabecalhoProtocolo['idSincronizacao'] = $this->idSincronizacao;
            $cabecalhoProtocolo['tabela'] = $this->nome;
            $cabecalhoProtocolo['idSistematabela'] = $this->idSistemaTabela;
            $cabecalhoProtocolo['tipoOperacaoBanco'] = $tipoOperacaoBanco;
            $cabecalhoProtocolo['atributos'] = $this->atributos;
            $jsonCabecalho = Helper::jsonEncode($cabecalhoProtocolo);
            $jsonCabecalho = substr($jsonCabecalho, 0, strlen($jsonCabecalho) - 1);

            $jsonCabecalho .= ", \"protocolos: [\"";
            fwrite($handle, $jsonCabecalho);
            for($i = 0 ; $i < count($container);){
                $ids = array();
                for($j = $i ; $j < count($container); $j++){
                    $ids[count($ids)] = $container[$j]['idTabelaWeb'];
                    if(count($ids) == 50) break;
                }
                $corpos = $this->getValoresDosRegistrosDoBancoWeb($ids);

                $iCorpos = 0;
                for(
                    $j = $i ;
                    $j < count($ids)
                    && $iCorpos < count($corpos)
                    && $i < count($container);
                    $j++, $i++){

                    if($corpos[$iCorpos]['idTabelaWeb'] == $ids[$j]){
                        $corpo = $corpos[$iCorpos++];
                        $strValues = "";

                        $valores = array();


                        for($k = 0 ; $k < count($this->atributos); $k++){
                            $attr = $this->atributos[$k];

                            if(strlen($strValues))
                                $strValues .= ", ";
                            $valor = $corpo[$attr];
                            if($valor == null){
                                $strValues .=  "null";
                            }
                            else{
                                $valorFormatado = str_replace("'", "\'", $valor);
                                $strValues .=  "'$valorFormatado'";
                            }
                            $valores[count($valores)] = $valor;
                        }
                        $protocolo = array();

                        $protocolo['crud'] = $container[$i];
                        $protocolo['valores'] = $valores;
                        $strProtocolo = Helper::jsonEncode($protocolo);
                        fwrite($handle, $strProtocolo);
                    }
                }
            }
            fwrite($handle, "]}");
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Todas as inser��es foram processadas.");
        } else
            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Container vazio");
    }


    public function getCabecalhoAtributosInsert(){
        $str = "";

        for($i = 0; $i < count($this->atributos); $i++){
            if($this->atributos[$i] == 'id'
                || $this->atributos[$i] == 'crud_mobile_id_INT')continue;

            if(strlen($str)) $str.= ", ";
            $str .= "`".$this->atributos[$i]."`";
        }
        return $str;

    }
    private function executarProtocoloRemover($handle){
        if(count($this->containerRemove)){
            $cabecalhoProtocolo = array();
            $cabecalhoProtocolo['idSincronizacao'] = $this->idSincronizacao;
            $cabecalhoProtocolo['tabela'] = $this->nome;
            $cabecalhoProtocolo['idSistematabela'] = $this->idSistemaTabela;
            $cabecalhoProtocolo['tipoOperacaoBanco'] = EXTDAO_Tipo_operacao_banco::Remover;
            $cabecalhoProtocolo['protocolos'] = $this->containerRemove;
            $json = Helper::jsonEncode($cabecalhoProtocolo);
            fwrite($handle, $json);
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        } return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
    }


    public function getCorpoSelect(){
        if(!isset($this->select)){
            $select = "";
            for($i = 0 ; $i < count($this->atributos); $i++){

                if(strlen($select) > 0)
                    $select .=  ", ";
                $select .=  $this->atributos[$i]  ;
            }
            $this->select = $select;
        }
        return $this->select;
    }


    public function getValoresDosRegistrosDoBancoWeb($ids) {
        if(!count($ids))
            return false;

        $strIn = Helper::arrayToString($ids, ", ");
        $validade = false;

//            echo "PASSO 6 </br>";
        $this->dbWeb->queryMensagemThrowException("SELECT {$this->getCorpoSelect()} 
                        FROM {$this->nome} 
                        WHERE id IN ($strIn)");
        

        $v_vetor = Helper::getResultSetToMatriz($this->db->result, 1, 0);

        return $v_vetor;
    }
}
