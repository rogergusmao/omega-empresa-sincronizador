<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Sincronizacao
 *
 * @author home
 */
class BO_Sincronizacao_mobile {
    //put your code here


    public static function retiraMobileDaFilaDeEspera(
        $idSincronizacaoMobile, $idCorporacao){

        try {
            $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile();
            $objSincronizacaoMobile->select($idSincronizacaoMobile);
            $msg = $objSincronizacaoMobile->retiraMobileDaFilaDeEspera($idCorporacao);
            return $msg;

        } catch(DatabaseException $dbex){
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        } catch (Exception $exc) {
            return new Mensagem(null,null,$exc);
        }
    }

    public static function statusSincronizacaoMobile(
        $idSistemaSihop,
        $corporacao,
        $idSincronizacaoMobile,
        $ultimoIdSincronizacao,
        $idCorporacao,
        $idMobileConectado,
        $ultimaMigracao){

        try {
            $helperProcesso = new HelperProcesso($idSistemaSihop );
            $helperProcesso->addItemProc0($idCorporacao);

            $db = new Database();
            $ret = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao);
            if($ret != null) return $ret;

            //O mobile estava desligado por exemplo e depois de uma no ligou
            //ainda pendente da conclusao de uma sincronizacao
            //ele ent�o envia o id da sincronizacao_mobile daquela epoca

            //pode ter havido a troca de pacote, e consequentemente do banco de dados
            //e o id sincronizacao_mobile que o mobile guardava, n�o ser mais quem ele era.

//            $objMI = new EXTDAO_Mobile_conectado($db);
//            $objMI->select($idMobileConectado);
//            $idMI = $objMI->getMobile_identificador_id_INT();
//            BO_Sincronizacao_mobile::mobileResetado($idMI, null, $idCorporacao);



            $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($db);
            $objSincronizacaoMobile->select($idSincronizacaoMobile);

            $ret = array();
            $idEstadoSM = $objSincronizacaoMobile->getEstado_sincronizacao_mobile_id_INT();
            if($idEstadoSM == EXTDAO_Estado_sincronizacao_mobile::INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO){
                $db = new Database();
                $idEstadoS = EXTDAO_Sincronizacao::getEstadoDaSincronizacao(
                    $objSincronizacaoMobile->getSincronizacao_id_INT(),
                    $db);
                if($idEstadoS == EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO){
                    $msg = EXTDAO_Sincronizacao_mobile::updateEstadoSincronizacaoMobile(
                        $idSincronizacaoMobile,
                        $idCorporacao, 
                        EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL, 
                        $db);
                    if($msg != null && $msg->erro()) return $msg;
                    $idEstadoSM = EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL;
                }
            }
            
            $ret['estado'] = $idEstadoSM;
            $idsUltimasSincronizacoes = null;
            if($objSincronizacaoMobile->getEstado_sincronizacao_mobile_id_INT() == EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE){
                $idsUltimasSincronizacoes = $objSincronizacaoMobile->getUltimasSincronizacoes(
                    $idCorporacao, 
                    $ultimoIdSincronizacao, 
                    $objSincronizacaoMobile->database);
            }
            $ret['possuiArquivoCrudWebParaMobile'] = strlen($objSincronizacaoMobile->getCrud_web_para_mobile_ARQUIVO()) ? true : false;
            $ret['possuiArquivoCrudMobile'] = strlen($objSincronizacaoMobile->getCrud_mobile_ARQUIVO()) ? true : false;
            $ret['idSincronizacaoAtual'] = $objSincronizacaoMobile->getSincronizacao_id_INT();
            $ret['isPrimeiraVez'] = $objSincronizacaoMobile->getPrimeira_vez_BOOLEAN();
            if(strlen($objSincronizacaoMobile->getSincronizacao_id_INT())){
                $idSincronizacao = $objSincronizacaoMobile->getSincronizacao_id_INT();

                $ret['estadoSincronizacao'] = EXTDAO_Sincronizacao::getEstadoSincronizacao(
                    $idSincronizacao, $objSincronizacaoMobile->database);
            }

            $ret['idSincronizacaoMobileAtual'] = $objSincronizacaoMobile->getId();
            if($idsUltimasSincronizacoes != null && count($idsUltimasSincronizacoes)){
                $ret['idsUltimasSincronizacoes'] = Helper::arrayToString($idsUltimasSincronizacoes, ",") ;
            } else
                $ret['idsUltimasSincronizacoes'] = null;

            $msg = new Mensagem_generica($ret);
            return $msg;

        }catch(DatabaseException $dbex){
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        } catch (Exception $exc) {
            return new Mensagem(null,null,$exc);
        }
    }

    public static function downloadDadosSincronizacaoMobile($idSM, $idCorporacao, $corporacao, $ultimaMigracao){

        try {
            $db = new Database();
            $ret = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao);
            if($ret != null) return $ret;

            $objSM = new EXTDAO_Sincronizacao_mobile($db);
            $objSM->select($idSM) ;
            if($objSM->getEstado_sincronizacao_mobile_id_INT() == EXTDAO_Estado_sincronizacao_mobile::FINALIZADO){
                return new Mensagem(
                    PROTOCOLO_SISTEMA::SINCRONIZACAO_JA_FINALIZADA_PELO_MOBILE,
                    I18N::getExpression("Ocorreu um erro na sincroniza��o dos dados do mobile {0}.", $idSM));
            }
            else
                if($objSM->getEstado_sincronizacao_mobile_id_INT() != EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE){
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_SINCRONIZACAO,
                    I18N::getExpression("Ocorreu um erro na sincroniza��o dos dados do mobile {0}.", $idSM));
            }
            else if($objSM->getEstado_sincronizacao_mobile_id_INT() != EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE
            && $objSM->getEstado_sincronizacao_mobile_id_INT() != EXTDAO_Estado_sincronizacao_mobile::FINALIZADO){
                throw new Exception("A sincroniza��o mobile $idSM n�o est�
                 no estado AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE, nem FINALIZADO.
                  O estado atual �: ". $objSM->getEstado_sincronizacao_mobile_id_INT());
            } else {
                $msg = $objSM->downloadDadosSincronizacaoMobile($corporacao);
                return $msg;
            }
        }catch(DatabaseException $dbex){
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        } catch (Exception $exc) {
            return new Mensagem(null,null,$exc);
        }
    }
    public static function downloadDadosSincronizacao(
        $idSincronizacaoMobile, $idSincronizacao, $idCorporacao, $corporacao, $ultimaMigracao){

        try {
            $db = new Database();
            $ret = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao);

            if($ret != null) return $ret;

            $objSM = new EXTDAO_Sincronizacao_mobile($db);
            $objSM->select($idSincronizacaoMobile);
            if($objSM->getEstado_sincronizacao_mobile_id_INT() == EXTDAO_Estado_sincronizacao_mobile::FINALIZADO) {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::SINCRONIZACAO_JA_FINALIZADA_PELO_MOBILE,
                    I18N::getExpression("A sincroniza��o mobile {0} ja havia sido finalizada.",
                        $idSincronizacaoMobile));
            }
            else if($objSM->getEstado_sincronizacao_mobile_id_INT() == EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL) {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_SINCRONIZACAO,
                    I18N::getExpression("A sincroniza��o mobile {0} n�o"
                    ." est� no estado AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE, nem FINALIZADO."
                    ." O estado atual �: {1}",
                        $idSincronizacaoMobile,
                        $objSM->getEstado_sincronizacao_mobile_id_INT()));
            }
            else if($objSM->getEstado_sincronizacao_mobile_id_INT() != EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE
                && $objSM->getEstado_sincronizacao_mobile_id_INT() != EXTDAO_Estado_sincronizacao_mobile::FINALIZADO){
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    I18N::getExpression( "A sincroniza��o mobile {0} n�o
                 est� no estado AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE, nem FINALIZADO.
                  O estado atual �: {1}",
                        $idSincronizacaoMobile,
                        $objSM->getEstado_sincronizacao_mobile_id_INT()));
            } else {

                $objSincronizacao = new EXTDAO_Sincronizacao();

                $objSincronizacao->select($idSincronizacao);
                $msg = $objSincronizacao->downloadDadosSincronizacao($corporacao);

                return $msg;
            }
        } catch(DatabaseException $dbex){
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        }catch (Exception $exc) {
            return new Mensagem(null,null,$exc);
        }
    }

    public static function finalizaSincronizacaoMobile(
        $idSincronizacaoMobile, $corporacao, $idCorporacao, $ultimaMigracao){
        $db = null;
        try {

            $db = new Database();
            $ret = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao);
            if($ret != null) return $ret;
            $db->iniciarTransacao();
            $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile();
            $objSincronizacaoMobile->select($idSincronizacaoMobile);
            $objSincronizacaoMobile->setEstado_sincronizacao_mobile_id_INT(EXTDAO_Estado_sincronizacao_mobile::FINALIZADO);
            $objSincronizacaoMobile->setData_final_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objSincronizacaoMobile->setCorporacao_id_INT($idCorporacao);
            $objSincronizacaoMobile->formatarParaSQL();
            $objSincronizacaoMobile->update($objSincronizacaoMobile->getId());
            
            $msg = EXTDAO_Sincronizacao_mobile_iteracao::inserir($db, $objSincronizacaoMobile->getId(), EXTDAO_Estado_sincronizacao_mobile::FINALIZADO);
            if($msg != null && $msg->erro())
                return $msg;
            
            $db->commitTransacao();
            if(isset($_FILES["logfile"])){
                $nomeArquivo = "{$corporacao}_{$objSincronizacaoMobile->getSincronizacao_id_INT()}_{$objSincronizacaoMobile->getId()}_{$objSincronizacaoMobile->getMobile_identificador_INT()}_sucesso.log";
                $objUpload = new Upload();
                $objUpload->arrPermitido = "";
                $objUpload->tamanhoMax = "";
                $objUpload->file = $_FILES["logfile"];
                $objUpload->nome = $nomeArquivo;
                $configuracaoSite = Registry::get('ConfiguracaoSite');
                $objUpload->uploadPath = $configuracaoSite->PATH_CONTEUDO."sincronizacao_mobile/$corporacao/";
                if(!file_exists($objUpload->uploadPath))
                    Helper::mkdir($objUpload->uploadPath, 0777, true);
                
                $success = $objUpload->uploadFile();
                if (!$success ) {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_UPLOAD,
                        "Falha durante o envio para o servidor do arquivo de sincroniza��o do mobile. Erro: " . $objUpload->erro);
                }
            }

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "A sincroniza��o mobile teve o estado atualizado.");

        }catch(DatabaseException $dbex){
            if($db != null)$db->rollbackTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        } catch (Exception $exc) {
            if($db != null)$db->rollbackTransacao();
            return new Mensagem(null,null,$exc);
        }
    }

    public static function erroSincronizacaoMobile(
        $idSistemaSihop ,
        $mobileIdentificador,
        $idSincronizacao,
        $idSincronizacaoMobile,
        $corporacao,
        $idCorporacao){
        $db = null;
        try {
            $helperProcesso = new HelperProcesso($idSistemaSihop );
            $helperProcesso->addItemProc0($idCorporacao);
            $db = new Database();
            $db->iniciarTransacao();

            $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile();
            $objSincronizacaoMobile->select($idSincronizacaoMobile);
            $objSincronizacaoMobile->setEstado_sincronizacao_mobile_id_INT(EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL);
            SingletonLog::writeLogStr(
                                "Setando estado sincronizador mobile[{$idSincronizacaoMobile}]"
                                . " da sincronizacao[$idSincronizacao]"
                                . " a ordem partiu do mobile_identificador[{$mobileIdentificador}]"
                                . " OCORREU_UM_ERRO_FATAL. Motivo: ");
            $objSincronizacaoMobile->setData_final_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objSincronizacaoMobile->setCorporacao_id_INT($idCorporacao);
            $objSincronizacaoMobile->formatarParaSQL();
            $objSincronizacaoMobile->update($objSincronizacaoMobile->getId());

            EXTDAO_Sincronizacao_mobile_iteracao::inserir($db, $objSincronizacaoMobile->getId(), EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL);
            
            if(isset($_FILES["logfile"])){
                $nomeArquivo = "{$corporacao}_{$idSincronizacao}_{$idSincronizacaoMobile}_MI{$mobileIdentificador}_erro.log";
                $objUpload = new Upload();
                $objUpload->arrPermitido = "";
                $objUpload->tamanhoMax = "";
                $objUpload->file = $_FILES["logfile"];
                $objUpload->nome = $nomeArquivo;
                $configuracaoSite = Registry::get('ConfiguracaoSite');
                $objUpload->uploadPath = $configuracaoSite->PATH_CONTEUDO."sincronizacao_mobile/{$corporacao}/";
                if(!file_exists($objUpload->uploadPath))
                    Helper::mkdir($objUpload->uploadPath, 0777, true);
                $success = $objUpload->uploadFile();
                if (!$success) {
                    $db->commitTransacao();
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        "[w��klmdcfwe]Falha durante o download do arquivo: ".print_r($objUpload->file,true));
                }
            }
            $db->commitTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "A sincroniza��o mobile teve o estado atualizado.");

        }catch(DatabaseException $dbex){
            if($db != null) $db->rollbackTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        } catch (Exception $exc) {
            if($db != null) $db->rollbackTransacao();
            return new Mensagem(null,null,$exc);
        }
    }

    public static function mobileResetado(
        $mobileIdentificador,
        $idUltimaSincronizacao,
        $idCorporacao,
        $ultimaMigracao,
        $idSistemaSihop ){
        $db = null;
        try {
            $helperProcesso = new HelperProcesso($idSistemaSihop );
            $helperProcesso->addItemProc0($idCorporacao);
            $db = new Database();
            $msg = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao);
            if($msg != null)
                return $msg;
            $idsSincronizacaoMobile = EXTDAO_Sincronizacao_mobile::getIdsNosEstados(
                $idCorporacao,
                $mobileIdentificador,
                array(
                    EXTDAO_Estado_sincronizacao_mobile::INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO
                ),
                $db
            );
            if(!empty($idsSincronizacaoMobile)){
                $str = Helper::arrayToString($idsSincronizacaoMobile, ",");
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "Aguarde at� o processo aberto do mobile de sincroniza��o ser finalizado. $str");
            }

            $idsSincronizacaoMobile = EXTDAO_Sincronizacao_mobile::getIdsNosEstados(
                $idCorporacao,
                $mobileIdentificador,
                array(
                    EXTDAO_Estado_sincronizacao_mobile::PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB,
                    EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO,
                    EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE,
                    EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE,
                    EXTDAO_Estado_sincronizacao_mobile::MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO),
                $db
            );
            $db->iniciarTransacao();
            $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($db);
            $nowSQL = Helper::getDiaEHoraAtualSQL();
            for($i = 0 ; $i < count($idsSincronizacaoMobile); $i++){
                $idSincronizacaoMobile = $idsSincronizacaoMobile[$i];

                $objSincronizacaoMobile->select($idSincronizacaoMobile);
                //Se nao tiver enviado arquivo, ent�o nao cancela
                if(!strlen($objSincronizacaoMobile->getCrud_mobile_ARQUIVO() )){

                    $objSincronizacaoMobile->setEstado_sincronizacao_mobile_id_INT(
                        EXTDAO_Estado_sincronizacao_mobile::CANCELADO);
//                $objSincronizacaoMobile->setSincronizacao_id_INT( $idUltimaSincronizacao);
                    $objSincronizacaoMobile->setData_final_DATETIME($nowSQL);
                    $objSincronizacaoMobile->setCorporacao_id_INT($idCorporacao);
                    $objSincronizacaoMobile->formatarParaSQL();
                    $objSincronizacaoMobile->update($idSincronizacaoMobile);

                    EXTDAO_Sincronizacao_mobile_iteracao::inserir(
                        $db,
                        $idSincronizacaoMobile,
                        EXTDAO_Estado_sincronizacao_mobile::CANCELADO,
                        $nowSQL);
                }
            }
            $objSMResetado  = new EXTDAO_Sincronizacao_mobile();
            $objSMResetado->setData_inicial_DATETIME($nowSQL);
            $objSMResetado->setData_final_DATETIME($nowSQL);
            $objSMResetado->setEstado_sincronizacao_mobile_id_INT(EXTDAO_Estado_sincronizacao_mobile::RESETADO);
            $objSMResetado->setMobile_identificador_INT($mobileIdentificador);
            $objSMResetado->setCorporacao_id_INT($idCorporacao);
            $objSMResetado->setSincronizacao_id_INT( $idUltimaSincronizacao);
            $objSMResetado->formatarParaSQL();
            $objSMResetado->insert();
            $idSMResetado = $objSMResetado->getIdDoUltimoRegistroInserido($idCorporacao);
            EXTDAO_Sincronizacao_mobile_iteracao::inserir($db, $idSMResetado, EXTDAO_Estado_sincronizacao_mobile::RESETADO, $nowSQL);
            
            $db->commitTransacao();
            return new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                "A sincroniza��o teve o seu estado atualizado com sucesso.");
        }catch(DatabaseException $dbex){
            if($db != null)$db->rollbackTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
        } catch (Exception $exc) {
            if($db != null)$db->rollbackTransacao();
            return new Mensagem(null,null,$exc);
        }
    }
}
