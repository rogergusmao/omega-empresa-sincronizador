<?php
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  Database_ponto_eletronico
    * DATA DE GERA��O: 26.08.2017
    * ARQUIVO:         Database_ponto_eletronico.php
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARA��O DA CLASSE
    // **********************
    class Database_ponto_eletronico {

        // *************************
        // DECLARA��O DE TODAS AS TABELAS DO BANCO DE DADOS
        // *************************
        public static $tabelas = array (
            "area_menu",
            "corporacao",
            "empresa_equipe",
            "empresa_produto_unidade_medida",
            "empresa_servico_unidade_medida",
            "estado_civil",
            "operadora",
            "perfil",
            "permissao",
            "pessoa_empresa_equipe",
            "servico",
            "sexo",
            "sistema_estado_lista",
            "sistema_parametro_global",
            "sistema_projetos_versao",
            "sistema_sequencia",
            "sistema_tabela",
            "sistema_tipo_campo",
            "sistema_tipo_download_arquivo",
            "sistema_tipo_operacao_banco",
            "tela_estado",
            "tela_tipo",
            "tela_tipo_componente",
            "tipo_anexo",
            "tipo_cardinalidade",
            "tipo_ponto",
            "usuario",
            "categoria_permissao",
            "empresa_produto_tipo",
            "empresa_servico_tipo",
            "forma_pagamento",
            "modelo",
            "veiculo",
            "pais",
            "uf",
            "cidade",
            "bairro",
            "profissao",
            "rede",
            "rotina",
            "sistema_parametro_global_corporacao",
            "tipo_documento",
            "tipo_empresa",
            "usuario_tipo",
            "usuario_tipo_menu",
            "usuario_tipo_privilegio",
            "empresa",
            "empresa_produto",
            "empresa_produto_foto",
            "empresa_servico",
            "empresa_servico_foto",
            "empresa_tipo_venda",
            "empresa_tipo_venda_parcela",
            "rede_empresa",
            "empresa_perfil",
            "permissao_categoria_permissao",
            "pessoa",
            "pessoa_empresa",
            "horario_trabalho_pessoa_empresa",
            "pessoa_empresa_rotina",
            "wifi",
            "sistema_atributo",
            "acesso",
            "empresa_compra",
            "empresa_compra_parcela",
            "empresa_produto_compra",
            "empresa_servico_compra",
            "empresa_venda",
            "empresa_produto_venda",
            "empresa_servico_venda",
            "empresa_venda_parcela",
            "pessoa_usuario",
            "ponto",
            "ponto_endereco",
            "relatorio",
            "relatorio_anexo",
            "sistema_lista",
            "sistema_campo",
            "sistema_campo_atributo",
            "sistema_registro_sincronizador",
            "tela",
            "tela_sistema_lista",
            "tela_componente",
            "usuario_categoria_permissao",
            "usuario_corporacao",
            "usuario_empresa",
            "usuario_foto",
            "usuario_foto_configuracao",
            "usuario_menu",
            "usuario_privilegio",
            "usuario_servico",
            "usuario_tipo_corporacao",
            "veiculo_usuario",
            "tarefa",
            "usuario_posicao",
            "veiculo_registro",
            "wifi_registro");
        public static $chavesUnicas = null;

        public static function getChaveUnica($tabela){
            if(Database_ponto_eletronico::$chavesUnicas == null){
                Database_ponto_eletronico::$chavesUnicas = array();
                Database_ponto_eletronico::$chavesUnicas['area_menu'] = null;
                Database_ponto_eletronico::$chavesUnicas['corporacao'] = array('nome_normalizado');
                Database_ponto_eletronico::$chavesUnicas['empresa_equipe'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_produto_unidade_medida'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_servico_unidade_medida'] = null;
                Database_ponto_eletronico::$chavesUnicas['estado_civil'] = array('nome');
                Database_ponto_eletronico::$chavesUnicas['operadora'] = array('nome_normalizado');
                Database_ponto_eletronico::$chavesUnicas['perfil'] = array('nome');
                Database_ponto_eletronico::$chavesUnicas['permissao'] = array('tag');
                Database_ponto_eletronico::$chavesUnicas['pessoa_empresa_equipe'] = null;
                Database_ponto_eletronico::$chavesUnicas['servico'] = array('nome');
                Database_ponto_eletronico::$chavesUnicas['sexo'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_estado_lista'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_parametro_global'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_projetos_versao'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_sequencia'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_tabela'] = array('nome');
                Database_ponto_eletronico::$chavesUnicas['sistema_tipo_campo'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_tipo_download_arquivo'] = array('nome');
                Database_ponto_eletronico::$chavesUnicas['sistema_tipo_operacao_banco'] = array('nome');
                Database_ponto_eletronico::$chavesUnicas['tela_estado'] = null;
                Database_ponto_eletronico::$chavesUnicas['tela_tipo'] = null;
                Database_ponto_eletronico::$chavesUnicas['tela_tipo_componente'] = null;
                Database_ponto_eletronico::$chavesUnicas['tipo_anexo'] = null;
                Database_ponto_eletronico::$chavesUnicas['tipo_cardinalidade'] = null;
                Database_ponto_eletronico::$chavesUnicas['tipo_ponto'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario'] = array('email');
                Database_ponto_eletronico::$chavesUnicas['categoria_permissao'] = array('nome_normalizado', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['empresa_produto_tipo'] = array('nome', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['empresa_servico_tipo'] = array('nome', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['forma_pagamento'] = null;
                Database_ponto_eletronico::$chavesUnicas['modelo'] = array('nome_normalizado', 'ano_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['veiculo'] = array('placa', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['pais'] = array('nome_normalizado', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['uf'] = array('nome_normalizado', 'corporacao_id_INT', 'pais_id_INT');
                Database_ponto_eletronico::$chavesUnicas['cidade'] = array('nome_normalizado', 'uf_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['bairro'] = array('nome_normalizado', 'cidade_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['profissao'] = array('nome_normalizado', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['rede'] = array('nome', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['rotina'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_parametro_global_corporacao'] = null;
                Database_ponto_eletronico::$chavesUnicas['tipo_documento'] = null;
                Database_ponto_eletronico::$chavesUnicas['tipo_empresa'] = array('nome', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['usuario_tipo'] = array('nome', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['usuario_tipo_menu'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_tipo_privilegio'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa'] = array('email', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['empresa_produto'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_produto_foto'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_servico'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_servico_foto'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_tipo_venda'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_tipo_venda_parcela'] = null;
                Database_ponto_eletronico::$chavesUnicas['rede_empresa'] = array('rede_id_INT', 'empresa_id_INT');
                Database_ponto_eletronico::$chavesUnicas['empresa_perfil'] = array('empresa_id_INT', 'perfil_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['permissao_categoria_permissao'] = null;
                Database_ponto_eletronico::$chavesUnicas['pessoa'] = array('email', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['pessoa_empresa'] = array('pessoa_id_INT', 'empresa_id_INT', 'profissao_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['horario_trabalho_pessoa_empresa'] = null;
                Database_ponto_eletronico::$chavesUnicas['pessoa_empresa_rotina'] = null;
                Database_ponto_eletronico::$chavesUnicas['wifi'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_atributo'] = array('nome', 'sistema_tabela_id_INT');
                Database_ponto_eletronico::$chavesUnicas['acesso'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_compra'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_compra_parcela'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_produto_compra'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_servico_compra'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_venda'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_produto_venda'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_servico_venda'] = null;
                Database_ponto_eletronico::$chavesUnicas['empresa_venda_parcela'] = null;
                Database_ponto_eletronico::$chavesUnicas['pessoa_usuario'] = array('pessoa_id_INT', 'usuario_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['ponto'] = null;
                Database_ponto_eletronico::$chavesUnicas['ponto_endereco'] = null;
                Database_ponto_eletronico::$chavesUnicas['relatorio'] = null;
                Database_ponto_eletronico::$chavesUnicas['relatorio_anexo'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_lista'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_campo'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_campo_atributo'] = null;
                Database_ponto_eletronico::$chavesUnicas['sistema_registro_sincronizador'] = null;
                Database_ponto_eletronico::$chavesUnicas['tela'] = null;
                Database_ponto_eletronico::$chavesUnicas['tela_sistema_lista'] = null;
                Database_ponto_eletronico::$chavesUnicas['tela_componente'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_categoria_permissao'] = array('usuario_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['usuario_corporacao'] = array('usuario_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['usuario_empresa'] = array('usuario_id_INT', 'empresa_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['usuario_foto'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_foto_configuracao'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_menu'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_privilegio'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_servico'] = array('servico_id_INT', 'usuario_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['usuario_tipo_corporacao'] = array('usuario_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['veiculo_usuario'] = array('usuario_id_INT', 'veiculo_id_INT', 'corporacao_id_INT');
                Database_ponto_eletronico::$chavesUnicas['tarefa'] = null;
                Database_ponto_eletronico::$chavesUnicas['usuario_posicao'] = null;
                Database_ponto_eletronico::$chavesUnicas['veiculo_registro'] = null;
                Database_ponto_eletronico::$chavesUnicas['wifi_registro'] = null;
            }

            return Database_ponto_eletronico::$chavesUnicas[$tabela];
        }


        // *************************
        // CONSTRUTOR
        // *************************

        public function __construct(){


        }


        // *************************
        // FACTORY
        // *************************
        public function factory(){
            return new Database_ponto_eletronico();

        }


        public function getVetorTable() {

            return Database_ponto_eletronico::$tabelas;
        }

    } // classe: fim
    

    ?>