<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BOBanco
 *
 * @author home
 */
class BO_Banco_web {
    const LIMITE_REGISTROS_POR_SINCRONIZACAO = 400;
    public $tabelasUpload = array();
    
    public $db = null;
    public $dbWeb = null;
    public $idCorporacao = null;
    public $corporacao = null;
    public $configWeb = false;
        public $configSincronizadorWeb = false;
    //put your code here
    public function __construct(
        $idCorporacao, 
        $corporacao,
        $configWeb = false,
        $configSincronizadorWeb = false) {
        
        $this->configWeb = $configWeb;
        $this->configSincronizadorWeb = $configSincronizadorWeb;
        $this->corporacao=$corporacao;
        $this->idCorporacao= $idCorporacao;    
    }
    public function init(){
        
        $boBancoEspelho = new BO_Banco(
            $this->idCorporacao, 
            $this->corporacao,
            $this->configWeb ,
            $this->configSincronizadorWeb);
        
        $msg = $boBancoEspelho->init();
        if($msg != null && $msg->erro()) return $msg;
        
        $this->tabelasUpload = $boBancoEspelho->tabelasUpload;
        $this->dbWeb = new Database(
            $this->configWeb != false
            ? $this->configWeb 
            : NOME_BANCO_DE_DADOS_WEB);
        
        $this->db = new Database($this->configSincronizadorWeb);
        
        //print_r($this->dbWeb);
        
        return null;
        
    }
    
    public function close(){
        if($this->db != null)
            $this->db->close();
        if($this->dbWeb != null)
            $this->dbWeb->close();
        
        $this->db = null;
        $this->dbWeb= null;
    }

    public static function getTabelasOrdenadas($idsTabelas, $db = null){
        $ret = array();
        for($k = 0 ; $k < count(Database_ponto_eletronico::$tabelas); $k++) {
            for($i = 0 ; $i < count($idsTabelas); $i++){
                $idSistemaTabela = $idsTabelas[$i];
                $nomeTabela = EXTDAO_Sistema_tabela::getNomeDaTabela(
                    $idSistemaTabela, 
                    $db );
                if($nomeTabela == Database_ponto_eletronico::$tabelas[$k]){
                    $ret[count($ret)] = $idSistemaTabela;
                    break;
                }
            }
        }
        return $ret;
    }

    public function getRegistrosDoSelect( $nomeTabela, $idsWeb, $select = "*"){
        if(!count($idsWeb))
            return null;
        $clausaIn = Helper::arrayToString($idsWeb, ", ");
        //if($nomeTabela == "pessoa_empresa"){
        //echo  "IN: $clausaIn";
        //}

        $q = "SELECT $select "
            . " FROM $nomeTabela "
            . " WHERE id IN ($clausaIn)";
//        SingletonLog::writeLogException("Consulta Banco Web: $q");
        $this->dbWeb->queryMensagemThrowException($q);

        $objs = Helper::getResultSetToMatriz($this->dbWeb->result, 0 , 1);

        return $objs;
    }
    public function getRegistros( $nomeTabela, $idsWeb){
        if(!count($idsWeb))
            return null;
        $clausaIn = Helper::arrayToString($idsWeb, ", ");
        //if($nomeTabela == "pessoa_empresa"){
        //echo  "IN: $clausaIn";
        //}
        
        $q = "SELECT * "
            . " FROM $nomeTabela "
            . " WHERE id IN ($clausaIn)";
//        SingletonLog::writeLogException("Consulta Banco Web: $q");
        $this->dbWeb->queryMensagemThrowException($q);

        $objs = Helper::getResultSetToMatriz($this->dbWeb->result, 0 , 1);
        
        return $objs;
    }
    
    
    public function getUltimoIdDaTabelaSistemaRegistroSincronizadorWeb(){
        $q = "SELECT MAX(s.id)"
            . " FROM sistema_registro_sincronizador s "
             . " WHERE s.corporacao_id_INT = {$this->idCorporacao}";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $ultimoId = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
        return $ultimoId ;
    }
    
    public function getPrimeiroIdDaTabelaSistemaRegistroSincronizadorWeb(){
        $q = "SELECT MIN(s.id)"
            . " FROM sistema_registro_sincronizador s "
            . " WHERE s.corporacao_id_INT = {$this->idCorporacao}";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $ultimoId = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
        return $ultimoId ;
    }
    
    public function getUltimoIdSistemaRegistroSincronizadorWebParaAndroidDaSincronizacao($idSincronizacao){
        
        $id = EXTDAO_Sincronizacao::getUltimoIdSistemaRegistroSincronizadorDaWeb($idSincronizacao, $this->db);
        
        if(!strlen($id)){
            $ultimoId = $this->getUltimoIdDaTabelaSistemaRegistroSincronizadorWeb();
            if(!strlen($ultimoId)){
                $ultimoId = EXTDAO_Sincronizacao::getUltimoIdSistemaRegistroSincronizadorWebParaAndroidDeUmaSincronizacao(
                    $this->idCorporacao, $idSincronizacao, $this->db);
            } else {
                $primeiroId = $this->getPrimeiroIdDaTabelaSistemaRegistroSincronizadorWeb();
                if($ultimoId - $primeiroId > BO_Banco_web::LIMITE_REGISTROS_POR_SINCRONIZACAO){
                    $ultimoId = $primeiroId + BO_Banco_web::LIMITE_REGISTROS_POR_SINCRONIZACAO;
                }
            }
            $objSinc = new EXTDAO_Sincronizacao($this->db);
            $objSinc->select($idSincronizacao);
            $ultimoId = !(strlen($ultimoId)) ? null : $ultimoId;
            $objSinc->setId_ultimo_sistema_registro_sincronizador_INT($ultimoId);
            $objSinc->formatarParaSQL();
            $objSinc->update($idSincronizacao);
            return $ultimoId;
            
        } else return $id;

    }

    public function getTodasAsTabelas(){
        $q="SHOW TABLES";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $arrTabelas = Helper::getResultSetToArrayDeUmCampo($this->dbWeb->result);
        return $arrTabelas;
    }
    public function getTabelasDoAndroid(){
        $q = "SELECT DISTINCT nome 
                FROM sistema_tabela 
                WHERE (frequencia_sincronizador_INT = -1 OR frequencia_sincronizador_INT > 0) 
                    AND transmissao_web_para_mobile_BOOLEAN = 1 
                    AND banco_mobile_BOOLEAN = 1
                    AND (excluida_BOOLEAN IS NULL OR excluida_BOOLEAN = '0')";
        
         $this->dbWeb->queryMensagemThrowException($q);
        
        $arrTabelas = Helper::getResultSetToArrayDeUmCampo($this->dbWeb->result);
        
        return $arrTabelas;
    }

    public function getTabelasASeremSincronizadas($idUltimoRegistroIncluso){
        $q = "SELECT DISTINCT(sistema_tabela_id_INT) "
            . " FROM sistema_registro_sincronizador ";
        if($idUltimoRegistroIncluso != null)
            $q .= " WHERE id <= $idUltimoRegistroIncluso ";
        
        $this->dbWeb->queryMensagemThrowException($q);
        
        $ids = Helper::getResultSetToArrayDeUmCampo($this->dbWeb->result);
//        echo "entrou<br/>";
//        echo $q;
//        print_r($ids);
//        exit();
        return $ids;
    }
    
    public function existemCrudsWebParaSincronizar(){
        $q = "SELECT s.id"
            . " FROM sistema_registro_sincronizador s"
            . " WHERE s.corporacao_id_INT = ".$this->idCorporacao
             . " LIMIT 0,1";
        $msg = $this->dbWeb->queryMensagem($q);
        if($msg != null && $msg->erro()) return $msg;
        if($this->dbWeb->rows() > 0){
            $ultimoId = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
            return new Mensagem_token(null,null,$ultimoId);
        }else return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        
    }
    
    public function gravaHistoricoSincronizadorRegistroWebCsv($idSincronizacao, $idUltimo, $corporacao){
        if(!MODO_DE_DEPURACAO) return;
        $query = "SELECT * "
            . " FROM sistema_registro_sincronizador s"
            . " WHERE id <= $idUltimo";
        $nomeArquivo = "{$corporacao}_".$idSincronizacao ."_HistoricoRegistroSincronizadorWeb.csv";
        $path = ConfiguracaoSite::getPathConteudo()."sincronizacao_mobile/{$corporacao}/"  ;
        if(!file_exists($path))
            Helper::mkdir($path, 0777, true);
        $path .=    $nomeArquivo;
        $msg = $this->dbWeb->queryMensagemThrowException($query);
        
        $registros = Helper::getResultSetToMatriz($this->dbWeb->result, 1, 0);
        if(file_exists($path)){
            if(count($registros) == 0 ) 
                return;
            else                
                unlink ($path);
        }
        $handle = fopen($path, "w+");
        $cont = 0;
        
        foreach ($registros as $reg) {
            
            if($cont == 0){
                
                foreach ($reg as $key => $value) {
                    fwrite($handle,$key.";" );
                }
                fwrite($handle,$value."\n" );
            }
            
            foreach ($reg as $key => $value) {
                fwrite($handle,$value.";" );
            }
            fwrite($handle,$value."\n" );
            $cont++;
            if($cont % 15 == 0) fflush ($handle);
        }
        fflush ($handle);
        fclose($handle);
//        exit();
       
    }
    public function gravaHistoricoSincronizadorRegistroWebJson($idSincronizacao, $idUltimo, $corporacao){
        if(!MODO_DE_DEPURACAO) return;
        if(strlen($idUltimo)){
            $query = "SELECT * "
            . " FROM sistema_registro_sincronizador s"
            . " WHERE id <= $idUltimo";
            
            $msg = $this->dbWeb->queryMensagemThrowException($query);
            
        }
        
        $nomeArquivo = "{$corporacao}_".$idSincronizacao ."_HistoricoRegistroSincronizadorWeb.json";
        $path = ConfiguracaoSite::getPathConteudo()."sincronizacao_mobile/{$corporacao}/";
        if(!file_exists($path))
            Helper::mkdir($path, 0777, true);
        $path .= $nomeArquivo;
        
        $registros = Helper::getResultSetToMatriz($this->dbWeb->result, 1, 0);
        if(file_exists($path)){
            if(count($registros) == 0 ) 
                return;
            else                
                unlink ($path);
        }
        $handle = fopen($path, "w+");
        $cont = 0;
        fwrite($handle,"{\"registros\":[" );
        foreach ($registros as $reg) {
            
            fwrite($handle, Helper::jsonEncode($reg)."," );
            $cont++;
            if($cont % 15 == 0) fflush ($handle);
        }
        fwrite($handle,"]}" );
        fflush ($handle);
        fclose($handle);
//        exit();
       
    }
    public function insereCrudsWebDoSistemaRegistroSincronizador($idSincronizacao, $corporacao){
        try{
            SingletonLog::writeLogStr("insereCrudsWebDoSistemaRegistroSincronizador");
            $ultimoId = $this->getUltimoIdSistemaRegistroSincronizadorWebParaAndroidDaSincronizacao($idSincronizacao);
            $primeiroId = $this->getPrimeiroIdDaTabelaSistemaRegistroSincronizadorWeb($idSincronizacao);
            SingletonLog::writeLogStr("getUltimoIdSistemaRegistroSincronizadorAndroiParaWebDaSincronizacao($idSincronizacao) = $ultimoId");
            //$this->gravaHistoricoSincronizadorRegistroWebCsv($idSincronizacao, $ultimoId, $corporacao);
            $this->gravaHistoricoSincronizadorRegistroWebJson($idSincronizacao, $ultimoId, $corporacao);
            SingletonLog::writeLogStr("registros inseridos");
            if(!strlen($ultimoId)){
                
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            } else {

                $idsTabelas = $this->getTabelasASeremSincronizadas($ultimoId);
    //            print_r($this->tabelasUpload);
    //            exit();
                if(!count($idsTabelas)){
                    
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "N�o existem tabelas a serem sincronizadas.");
                }
                try{
                    $this->dbWeb->iniciarTransacao();
                    $this->db->iniciarTransacao();
                    //            echo "ENTROU 1";
                    $cont = 0;
                    for($j = 0 ; $j < count($this->tabelasUpload); $j++){
        //                echo "2";
                        $idTabelaUpload = $this->tabelasUpload[$j]['id'];
                        for($i = 0 ; $i < count($idsTabelas); $i++){
        //                    echo "3";

                            $idTabelaSinc = $idsTabelas[$i];
                            if($idTabelaSinc == $idTabelaUpload){
        //                    echo "4";    
                                $boTabela = new BO_Tabela_web(
                                    $this->tabelasUpload[$j]['nome'], 
                                    $idTabelaSinc, 
                                    $ultimoId,
                                    $this->dbWeb, 
                                    $this->db,
                                    $idSincronizacao,
                                    $this->idCorporacao,
                                    $this->configWeb ,
                                    $this->configSincronizadorWeb);
                                $boTabela->inicializaContainer();                        
                                $boTabela->gravaDadosDaTabelaUpload();
                                $cont++;
                                break;
                            }
                        }
                        if($cont == count($idsTabelas))break;
                    }
                    $qDelete =  "DELETE FROM sistema_registro_sincronizador WHERE id <= $ultimoId AND corporacao_id_INT ={$this->idCorporacao}";
                    $msg = $this->dbWeb->queryMensagem($qDelete, Database::OPCAO_LANCAR_EXCECAO_BANCO_FORA_DO_AR);
                    if($msg != null && $msg->erroConsultaSql()){
                        $this->dbWeb->rollbackTransacao();
                        $this->db->rollbackTransacao();
                        return $msg;
                    } else {
                        $this->dbWeb->commitTransacao();
                        $this->db->commitTransacao();
                        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
                    }
                }catch(DatabaseException $dbex){
                    $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
                    $this->dbWeb->rollbackTransacao();
                    $this->db->rollbackTransacao();
                    return $msg;
                } catch (Exception $ex) {
                    $this->dbWeb->rollbackTransacao();
                    $this->db->rollbackTransacao();
                    return new Mensagem(null, null, $ex);
                }
            }
       } catch(DatabaseException $dbex){
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null,$dbex);
            return $msg;
        }catch (Exception $ex) {
           return new Mensagem(null, null, $ex);
       }
    }

    public function getIdSistemaRegistroSincronizadorParaArquivoDeControleBancoSQLite($idSincronizacao){
        $ultimoId = $this->getUltimoIdDaTabelaSistemaRegistroSincronizadorWeb();
        if(!is_numeric($ultimoId)){
            $ultimoId = EXTDAO_Sincronizacao::getUltimoIdSistemaRegistroSincronizadorWebParaAndroidDeUmaSincronizacao(
                $this->idCorporacao, $idSincronizacao, $this->db);
        }
        return $ultimoId;
    }
    
    public function getUltimoIdDoRegistroSincronizadorWebNoBancoWebOuNoSincronizadorWeb($idSincronizacao){
        $ultimoId = $this->getUltimoIdDaTabelaSistemaRegistroSincronizadorWeb();
        if(!is_numeric($ultimoId)){
            $ultimoId = $this->getUltimoIdSistemaRegistroSincronizadorWebParaAndroidDaSincronizacao($idSincronizacao);
        }
        return $ultimoId;
    }
}
