<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BOBanco
 *
 * @author home
 */
class BO_Arquivo_crud_sincronizacao {
    public $tabelasUpload = array();

    public $db = null;
    public $boBancoWeb = null;
    public $handle = null;

    public $objSincronizacao = null;
    public $corporacao = null;
    
    public $idCorporacao =null;
    public $configWeb = false;
    public  $configSincronizadorWeb = false;
    //put your code here
    public function __construct(
            $corporacao, 
            $idCorporacao, 
            $configWeb = false, 
            $configSincronizadorWeb = false) {
        
        $this->configWeb = $configWeb;
        $this->configSincronizadorWeb = $configSincronizadorWeb;
        $this->corporacao = $corporacao;
       $this->idCorporacao = $idCorporacao;
    }
    public function init($idSincronizacao){
        $this->db = new Database($this->configSincronizadorWeb);
        $this->boBancoWeb = new BO_Banco_web(
            $this->idCorporacao, 
            $this->corporacao, 
            $this->configWeb, 
            $this->configSincronizadorWeb);
        $msg = $this->boBancoWeb->init();
        if($msg != null && $msg->erro()) return $msg;
        
        $this->objSincronizacao = new EXTDAO_Sincronizacao($this->db);
        $msg = $this->objSincronizacao->select($idSincronizacao);
        
        return $msg;
    }
    private function getNomeArquivo(){
        return "{$this->corporacao}_{$this->objSincronizacao->getId()}_sincronizacao.json";
    }
    private function getPathArquivo(){
        $nomeArquivo = $this->getNomeArquivo();
        $path = ConfiguracaoSite::getPathConteudo()."sincronizacao_mobile/{$this->corporacao}/";
        if(!file_exists($path))  Helper::mkdir($path, 0777, true);
        $path .= $nomeArquivo;
        return $path;
    }
    private function open(){
        $path = $this->getPathArquivo();

        if(file_exists($path))
            unlink ($path);
        $this->handle = fopen($path, "w+");
        return $this->handle;
    }
    private function close(){

        fclose($this->handle);
    }

    public function geraArquivoCrud(){
        try{
            SingletonLog::writeLogStr("Gerando arquivo sincronizacao ");
            //$objSincronizacao = new EXTDAO_Sincronizacao();
            $hd = $this->open();
            if(!$hd){
                $strMsgErro = "Erro ao tentar abrir o arquivo: ".$this->getPathArquivo();
                SingletonLog::writeLogStr($strMsgErro);
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, $strMsgErro);
            }
            $tabelas = $this->objSincronizacao->getTabelasDaSincronizacao();
            SingletonLog::writeLogStr("Tabelas da sinc: ".print_r($tabelas, true));
            //        echo "ID: {$this->objSincronizacao->getId()}";
            //        print_r($tabelas);

            $idsTabelasOrdenadas = BO_Banco_web::getTabelasOrdenadas(
                $tabelas,
                $this->db);

            $nomesTabelas = array();
            for($i = 0 ; $i < count($idsTabelasOrdenadas); $i++){

                $nomesTabelas[$i] = EXTDAO_Sistema_tabela::getNomeDaTabela(
                    $idsTabelasOrdenadas[$i],
                    $this->db);
            }
            $strTabelas = Helper::arrayToJson($nomesTabelas);
            $data = $this->objSincronizacao->getData_inicial_DATETIME();
            fwrite($this->handle, "{\"sincronizacao\":\"{$this->objSincronizacao->getId()}\",\"data\":\"$data\",  \"tabelas\":[$strTabelas], \"cruds\":[" );

            //escreve as operacoes de forma ordenada pronta para ser executada no mobile

            for($i = 0 ; $i < count($idsTabelasOrdenadas); $i++){
                $idSistemaTabela = $idsTabelasOrdenadas[$i];
                $tabela = $nomesTabelas[$i];
                if($i > 0)
                    fwrite($this->handle, ",");
                fwrite($this->handle, "{\"tabela\":\"$tabela\",\"operacoes\":[" );
                $validade = $this->escreveOperacaoRemover($idSistemaTabela);
                if($validade)
                    fwrite($this->handle, "," );
                $validade = $this->escreveOperacaoInserirOuEditar(
                    $idSistemaTabela,
                    EXTDAO_Tipo_operacao_banco::Inserir);
                if($validade)
                    fwrite($this->handle, "," );

                $validade = $this->escreveOperacaoInserirOuEditar(
                    $idSistemaTabela,
                    EXTDAO_Tipo_operacao_banco::Editar);

                if($validade)
                    fwrite($this->handle, "," );
                fwrite($this->handle, "]}");
            }


            fwrite($this->handle, "]}");
            $this->close();

            $idsMobile=  EXTDAO_Sincronizacao_mobile::getMobilesDaSincronizacaoQuePossuemArquivos(
                $this->idCorporacao,
                $this->objSincronizacao->getId(),
                $this->db);

            if(count($idsMobile)){
                for($i = 0 ; $i < count($idsMobile); $i++){
                    SingletonLog::writeLogStr("Gerando arquivo sincronizacao mobile ".$idsMobile[$i]);
                    $objArquivoMobile = new BO_Arquivo_sincronizacao_mobile(
                        $this->corporacao, 
                        $idsMobile[$i], 
                        $this->db, 
                        $this->idCorporacao, 
                        $this->configWeb, 
                        $this->configSincronizadorWeb);
                    $msg = $objArquivoMobile->init();
                    if($msg != null && $msg->erro()) return $msg;
                    $msg = $objArquivoMobile->geraArquivo();
                    if($msg != null && $msg->erro()) return $msg;
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                "Arquivos cruds da sincroniza��o foram gerados com sucesso.",
                $this->getNomeArquivo());
        }
        catch(DatabaseException $dbex){
            $msgBF = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);

            $log= Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msgBF);
            return $msgBF;
        }
        catch (Exception $ex) {
            $msgEx = new Mensagem(null, null, $ex);
            $log= Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msgEx);
            return $msgEx;
        }
    }


    private function escreveOperacaoRemover($idSistemaTabela){
        //$this->objSincronizacao = new EXTDAO_Sincronizacao();
        $cruds = $this->objSincronizacao->getCrudsRemocao($idSistemaTabela);
        if(!count($cruds)) return false;
        $jsonCruds = Helper::jsonEncode($cruds);
        //inicio do termo do identificador da opera��o do banco na tabela
        fwrite($this->handle, "{\"tipoOperacao\":\"".EXTDAO_Tipo_operacao_banco::Remover."\",\"cruds\": $jsonCruds }");
        return true;
    }
    private function escreveOperacaoInserirOuEditar($idSistemaTabela, $idTipoOperacao){

        $q = "SELECT DISTINCT c.id idCrud, "
            . "     c.crud_origem_id_INT idCrudOrigem, "
            . "     c.id_tabela_web_INT idTabelaWeb,"
            . "     sm.mobile_identificador_INT idMobileIdentificador,"
            . "     c.id_sistema_registro_sincronizador_INT idSistemaRegistroSincronizador "
            . " FROM crud c "
            . "     LEFT JOIN crud_mobile cm "
            . "         ON c.id = cm.crud_id_INT "
            . "     LEFT JOIN sincronizacao_mobile sm "
            . "         ON sm.id = cm.sincronizacao_mobile_id_INT "
            . " WHERE c.sincronizacao_id_INT = {$this->objSincronizacao->getId()} "
            . "     AND c.sistema_tabela_id_INT = $idSistemaTabela "
            . "     AND c.tipo_operacao_banco_id_INT = $idTipoOperacao "
            . "     AND c.corporacao_id_INT =  {$this->idCorporacao}"
            . " ORDER BY c.id ";

        $this->db->queryMensagemThrowException($q);
        
        $objs = Helper::getResultSetToMatriz($this->db->result, 1, 0);

        if(!count($objs)) return false;

        $nomeTabela = EXTDAO_Sistema_tabela::getNomeDaTabela(
            $idSistemaTabela, 
            $this->db);
        $colunas = $this->boBancoWeb->dbWeb->getAtributosDaTabela($nomeTabela);

        if(count($colunas) == 0) {
            throw new Exception("N�o foi poss�vel identificar as colunas da tabela $nomeTabela. No banco: "
                . $this->boBancoWeb->dbWeb->getJsonConfiguracaoDatabase());
        }
        $indexSenha = false;
        $select = "*";
        if($nomeTabela == "usuario"){
            //$indexSenha = array_search("senha", $colunas);
            //if($indexSenha != false) {
                $temp = array();
                foreach ($colunas as $col){
                    if($col == "senha") continue;
                    $temp[count($temp)] = $col;
                }
                $colunas = $temp;
              //  unset($colunas[$indexSenha]);
                $select = Helper::arrayToString($colunas, ',');
            //}
        }

        $jsonColunas = Helper::jsonEncode($colunas);
        //inicio do termo do identificador da opera��o do banco na tabela
        fwrite($this->handle, "{\"tipoOperacao\":\"$idTipoOperacao\",\"campos\": $jsonColunas ");
        //inicio dos protocolos
        fwrite($this->handle, ",\"protocolos\":[" );
        $imprimiuLinha = false;
        $i = 0;
        while($i < count($objs)){
            $idsWeb = array();
            $inicio = $i;
            $fim = $i + 100 ;
            $idsWebPorObj = array();
            //Monta a matriz idTabelaWeb, Lista de cruds da mesma tabela web
            for(;$i < $fim && $i < count($objs); $i++){
                $obj = &$objs[$i];

                $idTabelaWeb = $obj['idTabelaWeb'];
                if(!strlen($idTabelaWeb))
                    continue;
                else if(isset($idsWebPorObj[$idTabelaWeb])){
                    $tempObjs = &$idsWebPorObj[$idTabelaWeb];
                    $tempObjs[count($tempObjs)] = &$obj;
                } else {
                    $idsWeb[count($idsWeb)] = $idTabelaWeb;
                    $idsWebPorObj[$idTabelaWeb] = array(&$obj);
                }
            }
            $fim = $i;
            $registros = null;

            $registros = $this->boBancoWeb->getRegistrosDoSelect($nomeTabela, $idsWeb, $select);


            for($k = 0; $k < count($registros); $k++){
                $idRegistro = $registros[$k][0];
                if(isset($idsWebPorObj[$idRegistro])){
                    $tempObjs = &$idsWebPorObj[$idRegistro];
                    for($o = 0 ; $o < count($tempObjs); $o++){

                        $tempObjs[$o]['valores'] = &$registros[$k];
                    }
                }
            }
            for($k = $inicio; $k < $fim; $k++){
                $obj = &$objs[$k];


                if(!isset($obj['valores']) || !count($obj['valores'])){
                    $obj['valores'] = null;
                }
                $str = json_encode($obj);

                if($imprimiuLinha){ //divisor dos termos do vetor do protocolos
                    fwrite($this->handle, ",");
                } else{ $imprimiuLinha = true; }

                fwrite($this->handle, $str);
                
            }
        }
        //fim dos protocolos
        //fim do identificador da operacao de banco da tabela
        fwrite($this->handle, "]}");
//      exit();
        return true;
    }


}
