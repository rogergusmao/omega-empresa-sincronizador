<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Ta1424
 * bela
 *
 * @author home
 */
class BO_Tabela {

    const MAXIMO_TENTATIVA_CORRECAO_FK = 3;
    
    //put your code here
    public $nome;
    public $idSistemaTabela;
    public $containerRemove;
    public $containerInsert;
    public $containerEdit;
    public $db;
    public $objSistemaTabela;
    public $dbWeb;
    public $idsWebRemover;
    public $idsMobileInserir;
    public $idsWebRepetidos;
    public $atributos;
    public $idSincronizacao;
    public $atributosChaveUnica;
//    public $objCrudMobile;
    public $atributosFk;
    public $boTabelaWeb;
    public $atributosFormatadosFk;
    public $ultimoIdCrudMobile;
    public $idCorporacao;
    static public $idsTipoOperacaoInserirEEditar = array(EXTDAO_Tipo_operacao_banco::Editar, EXTDAO_Tipo_operacao_banco::Inserir);
    public  $configWeb = false;
    public  $configSincronizadorWeb = false;
    public $objSS = null;

    public function __construct(
        $nome, 
        $idSistemaTabela, 
        $idSincronizacao, 
        $db = null, 
        $dbWeb = null, 
        $idCorporacao = null,
        $configWeb = false,
        $configSincronizadorWeb = false) {
        $this->idCorporacao = $idCorporacao;
        $this->objSS = Registry::get('EXTDAO_Sistema_sequencia');
        $this->configWeb = $configWeb ;
        $this->configSincronizadorWeb = $configSincronizadorWeb ;
        
        if ($db == null)
            $this->db = new Database($configSincronizadorWeb);
        else
            $this->db = $db;
        $this->nome = $nome;
        $this->idSistemaTabela = $idSistemaTabela;
        $this->objSistemaTabela = new EXTDAO_Sistema_tabela($this->db);
        $this->objSistemaTabela->select($idSistemaTabela);
        $this->atributosChaveUnica = $this->objSistemaTabela->getAtributosChaveUnica();
//        $this->objCrudMobile = new EXTDAO_Crud_mobile();
        $this->idSincronizacao = $idSincronizacao;
        if ($dbWeb == null)
            $this->dbWeb = new Database($configWeb != null ? $configWeb : NOME_BANCO_DE_DADOS_WEB);
        else
            $this->dbWeb = $dbWeb;

        $this->idsWebRemover = array();
        $this->idsMobileInserir = array();
        $this->idsWebRepetidos = array();


        $this->atributos = $this->db->getAtributosDaTabela('__' . $this->nome);
        $this->atributosConteudo = array();
        for ($k = 0; $k < count($this->atributos); $k++) {
            $attr = $this->atributos[$k];
            if ($attr == 'id' || $attr == 'crud_mobile_id_INT')
                continue;
            $this->atributosConteudo[count($this->atributosConteudo)] = $attr;
        }
        $this->atributosFk = EXTDAO_Sistema_atributo::getAtributosFkDaTabela(
                $this->idSistemaTabela, $this->db);
        
        $this->atributosFormatadosFk = EXTDAO_Sistema_atributo::formataAtributosFk(
            $this->idSistemaTabela,
                $this->atributos,
                $this->atributosFk,
                $this->db->getIdentificador());
        
        $this->boTabelaWeb = new BO_Tabela_web(
            $this->nome, 
            $this->idSistemaTabela, 
            null, 
            $this->dbWeb, 
            $this->db, 
            $idSincronizacao,
            $idCorporacao,
            $configWeb ,
            $configSincronizadorWeb );
        
        
    }

    public static function edicaoNaoRealizadaDevidoARemocao($container) {
        $idRegistro = $container['id_tabela_web_INT'];
    }

    public function validaOperacaoEditar($container) {
        $idRegistro = $container['id_tabela_web_INT'];
        $index = array_search($idRegistro, $this->idsWebRemover);
        if ($index != false) {
            SingletonLog::writeLogStr("O id relacionado a edicao ja foi "
                . "removido", $container);
            return false;
        }

        $index = array_search($idRegistro, $this->idsWebRepetidos);
        if ($index != false) {
            SingletonLog::writeLogStr("O id relacionado a edicao foi removido "
                . "pois o mesmo ja esta relacionado a outro crud nessa mesma "
                . "sincronizacao", $container);
            return false;
        } else {
            $this->idsWebRepetidos[count($this->idsWebRepetidos)] = $idRegistro;
        }
        return true;
    }

    public function inicializaContainerInsert() {

        $containers = EXTDAO_Crud_mobile::getContainerInsert(
            $this->idSistemaTabela, 
            $this->idCorporacao,
            $this->idSincronizacao, 
            $this->db);
        SingletonLog::writeLogStr("inicializaContainerInsert - containers", $containers);
        $limite = count($containers);
        for ($i = 0; $i < $limite; $i++) {
            $this->idsMobileInserir[count($this->idsMobileInserir)] = $containers[$i]['id_tabela_mobile_INT'];
        }

        $this->boTabelaWeb = new BO_Tabela_web(
            $this->nome, 
            $this->idSistemaTabela, 
            null, 
            $this->dbWeb, 
            $this->db,
            $this->idSincronizacao,
            $this->idCorporacao,
            $this->configWeb ,
            $this->configSincronizadorWeb );

        if (count($this->atributosFk)) {

            $tabelasContainerRemoverWeb = $this->boTabelaWeb->getTabelasContainerRemover();
            
            if (count($tabelasContainerRemoverWeb)) {
                SingletonLog::writeLogStr("Temos alguns atributos fk na tabela "
                    . "{$this->idSistemaTabela} e cruds remove na fila das tabelas "
                    . "correspondentes. Logo temos que analisar o contexto.");

                //Para cada atributo que possui chave extrangeira para o registro,
                //e verificado as tabelas extrangeiras
                //TABELA ATUAL: pessoa_empresa
                for ($i = 0; $i < count($this->atributosFk); $i++) {
                    //ATRIBUTO pessoa_id_INT 
                    $attrFk = $this->atributosFk[$i];
                    $atributoFk = $attrFk["atributo_fk"];
                    $onDelete = $attrFk["delete_tipo_fk"];
                    //$onUpdate = $attrFk["update_tipo_fk"];
                    $idSistemaTabelaFk = $attrFk["fk_sistema_tabela_id_INT"];
                    
                    if (in_array($idSistemaTabelaFk, $tabelasContainerRemoverWeb)) {
                        
                        $tabelaFk = EXTDAO_Sistema_tabela::getNomeDaTabela($idSistemaTabelaFk, $this->db);
                        SingletonLog::writeLogStr(
                            "[324klfm] A tabela FK '$tabelaFk' possui registros na fila de remocao de cruds web. ".print_r($containers,true), 
                            $attrFk);
                        
                        $idsRemoverAsc = $this->boTabelaWeb->getIdsWebRemover($idSistemaTabelaFk);
                        
                        SingletonLog::writeLogStr(
                            "[324k�ro] Ids web remove. ".print_r($idsRemoverAsc,true));
                        
                        for ($j = 0; $j < count($containers); ) {
                            $strIdsCrudMobile = "";
                            for($k = 0; 
                                $j < count($containers) && $k < 100; 
                                $j++, $k++){
                                if($k > 0)
                                    $strIdsCrudMobile += ", ";
                                $strIdsCrudMobile += $containers[$j]['id'];
                            }
                            SingletonLog::writeLogStr(
                                "[edf2438] StrIdsCrudMobile: ".$strIdsCrudMobile);
                            $q = "SELECT {$atributoFk} "
                                . " FROM __{$this->nome} "
                                . " WHERE crud_mobile_id_INT IN ( $strIdsCrudMobile )"
                                    . " ORDER BY $atributoFk";
                            $this->db->queryMensagemThrowException($q);
                            
                            $idsAtributoFk = Helper::getResultSetToArrayDeUmCampo($this->db->result);
                            SingletonLog::writeLogStr(
                                "[23rfwfg34] idsAtributoFk: $idsAtributoFk");
                            $u = 0;
                            for($k = 0 ; $k < count($idsAtributoFk); $k++){
                                $idAtributoFk = $idsAtributoFk[$k];
                                //se for um id local do mobile ent�o seu valor ser� adpatado no momento do insert.
                                if($idAtributoFk < 0) continue;
                                for( ;$u < count($idsRemoverAsc) && $idsRemoverAsc[$u] <= $idAtributoFk; $u++){
                                    SingletonLog::writeLogStr(
                                        "[234484] Verificando se $idAtributoFk == {$idsRemoverAsc[$u]} ...");
                                    
                                    if($idAtributoFk == $idsRemoverAsc[$u]){
                                        
                                        SingletonLog::writeLogStr(
                                            "O atributo {$this->nome}::$atributoFk ($idAtributoFk) $onDelete  esta "
                                            . "relacionado a um registro que possui crud de remocao.", 
                                                $attrFk);
                                        $this->propagaRemocaoAtributo($attrFk, $idAtributoFk);
                                        if($msg == null && $onDelete == "CASCADE"){
                                            
                                            unset($containers[$j]);
                                        }
                                        
                                    } 
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->containerInsert = $containers;
    }
    public function propagaRemocaoAtributo($attrFk, $idAtributoFk){
        $idSistemaTabelaFk = $attrFk["fk_sistema_tabela_id_INT"];
        $tabelaFk = EXTDAO_Sistema_tabela::getNomeDaTabela($idSistemaTabelaFk, $this->db);
        $atributoFk = $attrFk["atributo_fk"];
        $onDelete = $attrFk["delete_tipo_fk"];
        if ($onDelete == "SET NULL") {      
            $q = "UPDATE __{$this->nome} t1 "
                . " SET t1.{$atributoFk} = NULL "
                . " WHERE  ("
                . "SELECT COUNT(t.id) "
                . " FROM __{$this->nome} t "
                . "     JOIN crud_mobile cm "
                . "         ON t.crud_mobile_id_INT = cm.id "
                . " WHERE (cm.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Inserir . "' "
                . "   OR cm.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Editar . "' ) "
                . "     AND t1.id = t.id"
                . " AND cm.sincronizacao_id_INT = {$this->idSincronizacao} "
                . " AND t.{$atributoFk} = {$idAtributoFk} ) > 0 ";
            SingletonLog::writeLogStr("[okmsefoier]Setando para nulo o atributo $atributoFk [$idAtributoFk] -> $q");
            $this->db->queryMensagemThrowException($q);
            
        } else if ($onDelete == "CASCADE") {
            SingletonLog::writeLogStr("Removendo o registro on cascade {$tabelaFk}::{$idAtributoFk}.");
            
            $q = "UPDATE crud_mobile cm "
                . " SET cm.processada_BOOLEAN = '1', "
                . "     cm.data_processamento_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',"
                . "     cm.erro_sql_INT = '" . Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE . "' "
                . " WHERE  ("
                . "SELECT COUNT(t.id) "
                . " FROM crud_mobile cm1 "
                . "        JOIN __{$this->nome} t "
                . "         ON t.crud_mobile_id_INT = cm1.id "
                . " WHERE (cm1.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Inserir . "' "
                . "   OR cm1.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Editar . "' ) "
                . "     AND cm1.id = cm.id "
                . " AND cm1.sincronizacao_id_INT = {$this->idSincronizacao} "
                . " AND t.{$atributoFk} = {$idAtributoFk} ) > 0 ";
            SingletonLog::writeLogStr("Setando para nulo o atributo[fd234dsf] $q");
            $this->db->queryMensagemThrowException($q);
            
        } else {
            throw new Exception("[qwer23] CONDICIONAL NAO PROGRAMADA");
        }
        return null;
    }
   

    public function inicializaContainerRemove() {

        $containers = EXTDAO_Crud_mobile::getContainerRemove(
                $this->idSistemaTabela, $this->idCorporacao,
                $this->idSincronizacao, $this->db);
        SingletonLog::writeLogStr("Container remove. ", $containers);

        //Caso existam atributos FK CASCADE([TABELA]::PESSOA_ID_INT) relacioinados
        //com o registro da operacao que est� sendo removido [OP DELETAR]::PESSOA(1)
        //ent�o esses atributos tamb�m ser�o registros
        //
        //Caso existam atributos FK SET NULL([TABELA]::PESSOA_ID_INT) relacioinados
        //com o registro da operacao que est� sendo removido [OP DELETAR]::PESSOA(1)
        //ent�o esses atributos ser�o setados para nulo
        //
        //
        //Caso existam atributos FK SET NULL([TABELA]::PESSOA_ID_INT) relacioinados
        //com o registro da operacao que est� sendo removido [OP RESTRICT]::PESSOA(1)
        //ent�o esses atributos tamb�m ser�o registros
        //
        //Antes de remover eh verificado o impacto que pode
        //causar a todo atributo filho que possue chave para o registro
        
        if(!empty($containers)){
            SingletonLog::writeLogStr("Existem cruds remove, logo ser� recuperado"
                . " atributos de outras tabelas que apontam para as que sofreream "
                . "remocao.");
            $objs = EXTDAO_Sistema_atributo::getAtributosQueApontamParaATabela(
                $this->idSistemaTabela, $this->dbWeb);
            if (count($objs)) {
                SingletonLog::writeLogStr("Existem atributos dependentes.");
                $tabelasContainerInsert = EXTDAO_Crud_mobile::getTabelasContainerInserirEditar(
                        $this->idSincronizacao, $this->idCorporacao, $this->db);

                //Para cada atributo que possui chave extrangeira para o registro
                for ($i = 0; $i < count($objs); $i++) {
                    //ATRIBUTO pessoa_id_INT
                    $obj = $objs[$i];
                    $atributoFk = $obj["atributo_fk"];
                    $onDelete = $obj["delete_tipo_fk"];
                    $onUpdate = $obj["update_tipo_fk"];
                    $idSistemaTabelaFk = $obj["fk_sistema_tabela_id_INT"];
                    SingletonLog::writeLogStr("Verificando o obj", $obj);
                    if (in_array($idSistemaTabelaFk, $tabelasContainerInsert)) {
                        SingletonLog::writeLogStr("Existe crud insert na fila do obj");
                        $tabelaFk = EXTDAO_Sistema_tabela::getNomeDaTabela($idSistemaTabelaFk);
                        for ($j = 0; $j < count($containers); $j++) {
                            $container = $containers[$j];
                            SingletonLog::writeLogStr("Crud insert", $container);
                            $idTabelaWeb = $container['id_tabela_web_INT'];
                            //Opera��o de remo��o da PESSOA::ID = 1;
                            //verifica se PESSOA_EMPRESA::pessoa_id_INT = 1
                            //SELECT crud_mobile
                            //PESSOA_ID_INT = 1 AND (OP.INSERT OU OP.EDIT)
                            //SELECT SINCRONIZADOR_WEB 
                            //PESSOA_ID_INT = 1 AND (OP.INSERT OU OP.EDIT)
                            //CASO Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI

                            if ($onDelete == "SET NULL") {
                                $q = "UPDATE __$tabelaFk t1 "
                                    . " SET t1.{$atributoFk} = NULL "
                                    . " WHERE  ("
                                    . "SELECT COUNT(t.id) "
                                    . " FROM __$tabelaFk t "
                                    . "     JOIN crud_mobile cm "
                                    . "         ON t.crud_mobile_id_INT = cm.id "
                                    . " WHERE (cm.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Inserir . "' "
                                    . "   OR cm.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Editar . "' ) "
                                    . "     AND t1.id = t.id "
                                    . "     AND t.{$atributoFk} = $idTabelaWeb "
                                    . "     AND cm.sincronizacao_id_INT = {$this->idSincronizacao} ) > 0";

                                $msg = $this->db->queryMensagem($q);
                                SingletonLog::writeLogStr("SET NULL � o relacionamento atual entre o atributo, realizando a atualizacao para nulo dos regs dependentes.", $msg);

                            } else if ($onDelete == "CASCADE") {

                                $q = "UPDATE __$tabelaFk t1 "
                                    . " SET t1.processada_BOOLEAN = '1', "
                                    . "     t1.data_processamento_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',"
                                    . "     t1.erro_sql_INT = '" . Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE . "' "
                                    . " WHERE  ("
                                    . "SELECT COUNT(t.id) "
                                    . " FROM __$tabelaFk t "
                                    . "     JOIN crud_mobile cm "
                                    . "         ON t.crud_mobile_id_INT = cm.id "
                                    . " WHERE (cm.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Inserir . "' "
                                    . "   OR cm.tipo_operacao_banco_id_INT = '" . EXTDAO_Tipo_operacao_banco::Editar . "' ) "
                                    . "     AND t1.id = t.id "
                                    . "     AND cm.sincronizacao_id_INT = {$this->idSincronizacao} ) "
                                    . "     AND t.{$atributoFk} = $idTabelaWeb ) > 0";
                                $msg = $this->db->queryMensagem($q);
                                SingletonLog::writeLogStr("CASCADE � o relacionamento atual entre o atributo, realizando a atualizacao dos cruds dizendo que eles foram removidos devido ao cascade.", $msg);

                            } else {

                                throw new NotImplementedException("CONDICIONAL NAO PROGRAMADA");
                            }
                        }
                    }
                }
            } else {
                SingletonLog::writeLogStr("N�o existem atributos dependentes.");
            }
            foreach ($containers as $c) {
                $this->idsWebRemover[count($this->idsWebRemover)] = $c['id_tabela_web_INT'];
            }
        } else {
            SingletonLog::writeLogStr("N�o possuem cruds de remo��o na tabela {$this->nome}.");
        }
        
        $this->containerRemove = $containers;
    }

    public function inicializaContainerEdit() {
        SingletonLog::writeLogStr("inicializaContainerEdit");
        $containers = EXTDAO_Crud_mobile::getContainerEdit(
            $this->idSistemaTabela, 
            $this->idCorporacao, 
            $this->idSincronizacao,
            $this->db);
        $limite = count($containers);
        $removeu = false;

        for ($i = 0; $i < $limite; $i++) {
            if (!$this->validaOperacaoEditar($containers[$i])) {
                SingletonLog::writeLogStr("registro invalidado e foi removido da lista", $containers[$i]);
                unset($containers[$i]);
                $removeu = true;
                continue;
            } else {
                SingletonLog::writeLogStr("registro validado", $containers[$i]);
            }
        }
        $this->containerEdit = $containers;
        unset($this->idsWebRemover);
    }

    public function inicializaContainer() {

        $this->inicializaContainerRemove();
        $this->inicializaContainerInsert();
        $this->inicializaContainerEdit();

        $this->containerRemove = Helper::atualizaIndiceArray($this->containerRemove);
        $this->containerInsert = Helper::atualizaIndiceArray($this->containerInsert);
        $this->containerEdit = Helper::atualizaIndiceArray($this->containerEdit);
    }

    public function executaCrudsDoBancoEspelhoNoBancoWeb() {
        try {
            SingletonLog::writeLogStr("executaCrudsDoBancoEspelhoNoBancoMobile");
//            echo "asdf3: <br/>";
            $msg = $this->executarProtocoloRemover();
//            echo "asdf4: <br/>";

            if ($msg != null 
                && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR 
                || $msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR))
                return $msg;

            $msg = $this->executaProtocoloInserir();
            
            if ($msg != null 
                && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR 
                    || $msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR))
                return $msg;

            $msg = $this->executaProtocoloEditar();
//echo "PAOSDJF";
//exit();   
//            print_r($msg);
//            exit();
            if ($msg != null 
                && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR 
                    || $msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR))
                return $msg;
            else
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Todos os cruds foram processados");
        } catch(DatabaseException $dbex){
            $msg = new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
            SingletonLog::writeLogStr("dbex::executaCrudsDoBancoEspelhoNoBancoWeb", $msg);
            return $msg ;
        }   catch (Exception $exc) {
            $msg = new Mensagem(null, null, $exc);
            SingletonLog::writeLogStr("executaCrudsDoBancoEspelhoNoBancoWeb", $msg);
            return $msg ;
        }
    }

    private function getWhereConsultaPelaChaveUnica($valores) {
        if (!count($valores) || !count($this->atributosChaveUnica))
            return null;
        $str = "";
        $count = 0;


        for ($i = 0; $i < count($this->atributosConteudo); $i++) {
            $valor = $valores[$i];


            for ($j = 0; $j < count($this->atributosChaveUnica); $j++) {
                if ($this->atributosConteudo[$i] == $this->atributosChaveUnica[$j]) {
                    if (strlen($str))
                        $str .= " AND  ";
                    $str .= " {$this->atributosChaveUnica[$j]} = '$valor' ";
                    $count++;
                    break;
                }
            }
        }

        if ($count != count($this->atributosChaveUnica))
            return null;
        else
            return $str;
    }

    private function processaDuplicada($idCrudMobile, $valores) {

        if (empty($valores)) {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Os valores para descobrir a duplicata do crud esta vazio: {$idCrudMobile}");
        } else if (empty($this->atributosChaveUnica)) {

            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Chave indefinida para a tabela: . {$this->nome} .Crud mobile: {$idCrudMobile}");
        }
        SingletonLog::writeLogStr("Tabela {$this->nome} - Erro leve durante o processamento do crud dependente B, registro duplicado de acordo com a chave unica. Realizando tentativa de correcao...");
        $where = $this->getWhereConsultaPelaChaveUnica($valores);
        if (!strlen($where))
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "A consulta WHERE para tratar a duplicata est� nula. N�o foi poss�vel tratar a duplicata do crud_mobile: {$idCrudMobile}. Valores: " . print_r($valores, true));
        $q = "SELECT id "
            . "  FROM {$this->nome} "
            . "  WHERE $where "
            . " LIMIT 0,1 ";

        $msg = $this->dbWeb->queryMensagem($q);
        SingletonLog::writeLogStr("$q . Resultado: ".print_r($msg, true));
        
        $id = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);

        if (strlen($id)) {

            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $id);
        } else {
            return new Mensagem_token(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
                "[546657] N�o foi encontrado nenhum registro com a busca: $q. Msg: ".print_r($msg,true), 
                $id);
        }
    }

    private function processaOperacaoRemocaoDosCrudsDependentes($idCrudMobile) {
        //$this->objCrudMobile = new EXTDAO_Crud_mobile();
        $idSincronizadorMobile = EXTDAO_Crud_mobile::getIdSincronizacaoMobile(
            $idCrudMobile, $this->idCorporacao, $this->db);


        $crudsDependentes = EXTDAO_Crud_mobile_dependente::getCrudsDependentes(
            $this->idCorporacao,
            $idSincronizadorMobile, 
            $idCrudMobile, 
            $this->db, 
            $this->idsTipoOperacaoInserirEEditar);

        for ($i = 0; $i < count($crudsDependentes); $i++) {
            $crud = $crudsDependentes[$i];
            switch ($crud['tipoOperacao']) {
                case EXTDAO_Tipo_operacao_banco::Editar:
                case EXTDAO_Tipo_operacao_banco::Inserir:
                    //verifica o comportamento do registro dependente no caso de ON DELETE
                    //TODO implementar modo on cascade
                    //CASCADE
                    //SET NULL
                    $queryUpdate = "UPDATE __{$crud['tabela']} SET {$crud['atributo']} = null WHERE crud_mobile_id_INT = '{$crud['idCrudMobile']}'";

                    $this->db->queryMensagem($queryUpdate, Database::OPCAO_LANCAR_EXCECAO);

                    break;

                default:
                    break;
            }
        }

        return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }

    private function processaCrudsDependentes($idCrudMobile, $novoIdWeb) {
        SingletonLog::writeLogStr("Atualizando todos os registros dependentes do crud_mobile[{$idCrudMobile}] apontam para o registro {$this->nome}[$novoIdWeb]");
        //$this->objCrudMobile = new EXTDAO_Crud_mobile();
        $idSincronizadorMobile = EXTDAO_Crud_mobile::getIdSincronizacaoMobile(
            $idCrudMobile, $this->idCorporacao, $this->db);

        $crudsDependentes = EXTDAO_Crud_mobile_dependente::getCrudsDependentes(
            $this->idCorporacao,
            $idSincronizadorMobile, 
            $idCrudMobile, 
            $this->db, 
            self::$idsTipoOperacaoInserirEEditar);
        if (!count($crudsDependentes)) {
            SingletonLog::writeLogStr("Nao existem cruds dependentes");
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }
        SingletonLog::writeLogStr("Processando " . count($crudsDependentes)
            . " cruds dependentes");
        for ($i = 0; $i < count($crudsDependentes); $i++) {
            $crud = $crudsDependentes[$i];

            switch ($crud['tipoOperacao']) {
                case EXTDAO_Tipo_operacao_banco::Editar:
                case EXTDAO_Tipo_operacao_banco::Inserir:
                    $queryUpdate = "UPDATE __{$crud['tabela']} "
                        . " SET {$crud['atributo']} = '$novoIdWeb' "
                        . " WHERE crud_mobile_id_INT = '{$crud['idCrudMobile']}'";
                    SingletonLog::writeLogStr("Processando o crud dependente", $crud);
                    $this->db->queryMensagem($queryUpdate, Database::OPCAO_LANCAR_EXCECAO);
                    SingletonLog::writeLogStr("Crud dependente processado com sucesso");
                    break;

                default:
                    SingletonLog::writeLogStr("Crud dependente de remocao "
                        . "nao tem necessidade de ser processado");
                    break;
            }
        }
        return null;
    }

    public function vazia() {
        if (!count($this->containerInsert) && !count($this->containerEdit) && !count($this->containerRemove))
            return true;
        else
            return false;
    }
    private function formataValoresCrudInsertMobile(
        $corpo, $idCrudMobile, $atributosChaveUnica, &$valores, &$idMI, &$strChaveUnica, &$strValues, &$possuiValorNulo){
        $valores = array();
        for ($k = 0; $k < count($this->atributos); $k++) {
            $attr = $this->atributos[$k];
            if ($attr == 'id' || $attr == 'crud_mobile_id_INT')
                continue;
            $valor = $corpo[$attr];
            //se o registro continua com o valor negativo e for uma FK, significa
            //que ainda persiste o registro local do mobile enviado ao banco espelho
            //logo temos que fazer um tratamento especial nesse caso,
            //  ou o registro foi processado e temos um id definitvo
            //  ou foi removido
            if($this->atributosFormatadosFk[$k] != null && ($valor != null && $valor < 0)){
                if($idMI == null ){
                    $idMI = EXTDAO_Crud_mobile::getIdMobileIdentificador(
                        $idCrudMobile, $this->idCorporacao, $this->db);
                    if($idMI == null) {

                        $msg =EXTDAO_Crud_mobile::updateProcessada(
                            $this->idCorporacao,
                            $idCrudMobile,
                            null,
                            Database::ERRO_FALTA_DADOS_HISTORICO_SINCRONIZACAO,
                            $this->db) ;
                        if($msg != null && Interface_mensagem::checkErro($msg))
                            return $msg;

                        return new Mensagem(
                            PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                            "N�o temos dados historicos do crud mobile $idCrudMobile para identificar o mobile identificador da opera��o.");
                    }
                }

                $valorNovo = EXTDAO_Crud_mobile::getIdTabelaWebCorrelacionado(
                    $idMI,
                    $this->atributosFormatadosFk[$k] ['fk_sistema_tabela_id_INT'],
                    $valor,
                    $this->db,
                    $this->idCorporacao);

                $valor = $valorNovo;
            }

            $valorFormatado=null;
            if($atributosChaveUnica != null){
                $indiceChaveUnica = array_search($attr, $atributosChaveUnica);
                if($indiceChaveUnica >= 0){
                    if(strlen($strChaveUnica))
                        $strChaveUnica .= " AND ";
                    if($valor == null){
                        $strChaveUnica .= " $attr is null ";
                        $possuiValorNulo = true;
                    }
                    else {
                        $valorFormatado = "'".$this->db->realEscapeString($valor)."'";
                        $strChaveUnica .= " $attr  = $valorFormatado ";
                    }
                }
            }

            if (!empty($strValues))
                $strValues .= ", ";

            if ($valor == null) {
                $strValues .= "null";
            } else {
                if($valorFormatado == null) {
                    $valorFormatado = "'".$this->db->realEscapeString($valor)."'";
                }
                $strValues .= $valorFormatado ;
            }
            $valores[count($valores)] = $valor;
        }
        return new Mensagem_generica($valores);
    }
    private function executaCrudMobileInserir(
        $container, 
        $cabecalho, 
        $corpo = null, 
        $verificaFkPais = true) {
        
        SingletonLog::writeLogStr("BO_Tabela::executaCrudMobileInserir ".print_r($container, true));
        $idCrudMobile = $container['id'];
        $this->ultimoIdCrudMobile = $idCrudMobile;
        if($corpo == null) {
            $corpo = $this->getValores($idCrudMobile);
            SingletonLog::writeLogStr("BO_Tabela::executaCrudMobileInserir - [A] - ".print_r($corpo, true));
        } else {
            SingletonLog::writeLogStr("BO_Tabela::executaCrudMobileInserir - [B] - ".print_r($corpo,true));
        }
        $atributosChaveUnica = Database_ponto_eletronico::getChaveUnica($this->nome);
        $erroSql = null;
        $idCrud = null;
        $valores = null;
        $idWebDuplicada = null;
        if ($corpo != null) {
            $possuiValorNulo = false;
            $strValues = "";
            $strChaveUnica="";
            $idMI = null;
            $valores = array();
            SingletonLog::writeLogStr("BO_Tabela::executaCrudMobileInserir - formataValoresCrudInsertMobile ... ");
            $msg = $this->formataValoresCrudInsertMobile(
                $corpo, $idCrudMobile, $atributosChaveUnica,$valores,
                $idMI, $strChaveUnica, $strValues, $possuiValorNulo);

            SingletonLog::writeLogStr("BO_Tabela::executaCrudMobileInserir - formataValoresCrudInsertMobile: "
                .print_r($msg, true));

            if(!Interface_mensagem::checkOk($msg )) return $msg;

            $valores = $msg->mObj;

            SingletonLog::writeLogStr("[ASDF51ASD] - executaCrudMobileInserir");
            //TODO testar if abaixo 21/11/2016
            //se nao possuir valor nulo verifica se encontra alguma duplicada
            if(!$possuiValorNulo && !empty($strChaveUnica ) ){
                SingletonLog::writeLogStr("[ASDF51ASD] - 2 - Chave unica: $strChaveUnica");
                $queryUnique = "SELECT id FROM {$this->nome} WHERE $strChaveUnica ";
                $msg = $this->dbWeb->queryMensagem($queryUnique);
                if($msg != null && Interface_mensagem::checkErro($msg))
                    return $msg;
                $idExistente = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
                if (!empty($idExistente)) {
                    SingletonLog::writeLogStr("[oisefioj]Correcao realizada com sucesso,"
                       . " o id da duplicada que ja existia � $idExistente."
                       . " Processaremos os cruds dependentes do registro que falhou a insercao, para aponta-los para o id relacionado a duplicada $idExistente");
                   //Todo crud que apontava para esse registro ir� apontar para o registro
                   //que j� possuia a chave duplicada
                   $msg = $this->processaCrudsDependentes($idCrudMobile, $idExistente);
                   if (Interface_mensagem::checkOk($msg)) {

                       //$msg = new Mensagem(PROTOCOLO_SISTEMA::DUPLICADA_SOLUCIONADA);
//                       $this->objCrudMobile->setId_tabela_web_duplicada_INT($idExistente);
                       $idWebDuplicada = $idExistente;
                   } else {
                       throw new SyncException("[lkmfdnj]Erro durante o processamento "
                       . "dos cruds dependentes da duplicada {$this->nome}[$idExistente]");
                   }
                }
            }

            //COLOCAR O VERIFICADOR SE HA DUPLICADA PARA CONSIDERAR VALORES NULOS
            //$novoId = EXTDAO_Sistema_sequencia::gerarId($this->nome);
            $novoId = $this->objSS->gerarIdInterna($this->nome);
            SingletonLog::writeLogStr("[ASDF51ASD] - 2 - Novo id: $novoId");
            if($novoId == EXTDAO_Sistema_sequencia::VALOR_INVALIDO){
                return new Mensagem(
                    PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, 
                    "Falha ao tentar gerar o identificador da tabela: {$this->nome}");
            }
            $query = $cabecalho . " ( '$novoId', $strValues ) ";
            SingletonLog::writeLogStr("[ASDF51ASD] - 3 - executaCrudMobileInserir: $query");
            SingletonLog::writeLogStr("[ASDF51ASD] - 3 - executaCrudMobileInserir: ".$this->dbWeb->getIdentificador());
            $msg = $this->dbWeb->queryMensagem($query, Database::OPCAO_LANCAR_EXCECAO_BANCO_FORA_DO_AR);
            $registrarCrud = true;
//            if($msg != null && $msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL){
//                throw new Exception(json_encode($msg).$this->dbWeb->getJsonConfiguracaoDatabase().". Consulta: $query");
//            }
//            $this->objCrudMobile->select($idCrudMobile);
            if ($msg == null || Interface_mensagem::checkOk($msg)) {
                SingletonLog::writeLogStr("Crud realizado com sucesso tendo novo id web: $novoId");
                $idWeb = $novoId;
                //Atualiza os registros do banco espelho que possui FK para esse registro
                $this->processaCrudsDependentes($idCrudMobile, $novoId);

//                $objCrud = new EXTDAO_Crud();
//                $objCrud->setId_tabela_web_INT($idWeb);
//                $objCrud->setSistema_tabela_id_INT($this->idSistemaTabela);
//                $objCrud->setCrud_origem_id_INT(EXTDAO_Crud_origem::MOBILE);
//                $objCrud->setSincronizacao_id_INT($this->idSincronizacao);
//                $objCrud->setTipo_operacao_banco_id_INT(EXTDAO_Tipo_operacao_banco::Inserir);
//                $objCrud->formatarParaSQL();
//                $msg = $objCrud->insert();
                $msg =EXTDAO_Crud::insertCrud(
                    $this->idCorporacao,
                    $this->idSistemaTabela, 
                    $idWeb, 
                    EXTDAO_Tipo_operacao_banco::Inserir, 
                    EXTDAO_Crud_origem::MOBILE,
                    $this->idSincronizacao, 
                    null, 
                    $this->db);
                
                if($msg != null && Interface_mensagem::checkErro($msg))
                    return $msg;
                $idCrud = $this->db->getLastInsertId();
//                $this->objCrudMobile->setCrud_id_INT($idCrud);
                $msg = new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "Todas as inser��es foram processadas.");
            } else if ($msg != null 
                && $msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL) {
                SingletonLog::writeLogStr("Erro durante a execucao da consulta do crud mobile $idCrudMobile. Msg: ".  print_r($msg, true));
                //$msgToken = new Mensagem_token();
                $msgToken = $msg;
                $erroSql = $msgToken->mValor;
//                $this->objCrudMobile->setErro_sql_INT($erroSql);

                if ($erroSql == Database::CHAVE_DUPLICADA) {
                    
                    
                    SingletonLog::writeLogStr("Erro leve durante o processamento do crud dependente A. Query: $query, registro duplicado de acordo com a chave unica. Realizando tentativa de correcao...");
                    $msg = $this->processaDuplicada($idCrudMobile, $valores);
                    if (Interface_mensagem::checkOk($msg)) {

                        $idWebDuplicada = $msg->mValor;
                        SingletonLog::writeLogStr("Correcao realizada com sucesso,"
                            . " o id da duplicada que ja existia � $idWebDuplicada."
                            . " Processaremos os cruds dependentes do registro que falhou a insercao, para aponta-los para o id relacionado a duplicada $idWebDuplicada");
                        //Todo crud que apontava para esse registro ir� apontar para o registro
                        //que j� possuia a chave duplicada
                        $msg = $this->processaCrudsDependentes($idCrudMobile, $idWebDuplicada);
                        if (Interface_mensagem::checkOk($msg)) {

                            //$msg = new Mensagem(PROTOCOLO_SISTEMA::DUPLICADA_SOLUCIONADA);
//                            $this->objCrudMobile->setId_tabela_web_duplicada_INT($idWebDuplicada);
                        } else {
                            throw new SyncException("Erro durante o processamento "
                            . "dos cruds dependentes da duplicada {$this->nome}[$idWebDuplicada]");
                        }
                    }
                } else {
                    $corrigido = false;
                    //VERIFICANDO O CRUD DO REGISTRO PAI 
                    if ($verificaFkPais) {
                        $registrarCrud = false;
                        SingletonLog::writeLogStr("Ocorreu o erro sql $erroSql. "
                            . "Tentando corrigir os valores dos atributos fk para tentar inserir novamente.");
                        
                        while(true){
                            $idSincronizacaoMobile = EXTDAO_Crud_mobile::getIdSincronizacaoMobile(
                                $idCrudMobile, $this->idCorporacao, $this->db);
                            $verificado = $this->verificaDependenciaDaEdicaoOuInsercaoDoCrudMobile(
                                $idSincronizacaoMobile, $idCrudMobile, $corpo);
                        
                        
                            if ($verificado == PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE) {
                                $corrigido = true;
                                $erroSql = Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE;

                                SingletonLog::writeLogStr("Um registro pai foi "
                                    . "removido e o relacionamento com o atual "
                                    . "registro era ON_CASCADE, logo o mesmo tambem "
                                    . "foi removido. ");
                                //removido on cascade
                                $msg = new Mensagem(
                                    PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE, "Registro removido on cascade");
                                break;
                            } else if ($verificado != PROTOCOLO_SISTEMA::SEM_ALTERACAO) {
                                SingletonLog::writeLogStr("[TENTATIVA] Bem vamos tentar inserir novamente o registro. ", $container);
                                //chamada recursiva
                                $msg = $this->executaCrudMobileInserir(
                                    $container, $cabecalho, null, false);
                                if($msg != null ){
                                    if($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR){
                                        SingletonLog::writeLogStr("Parando o procedimento de sincornizacao devido a um erro de acesso ao banco", $msg);
                                        return $msg;
                                    }
                                    else if (Interface_mensagem::checkOk($msg)
                                        || $msg->mCodRetorno == PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE) {
                                        SingletonLog::writeLogStr("Segue o que ocorreu com a [TENTATIVA]", $msg);
                                        $corrigido = true;
                                        break;
                                    } else if(Interface_mensagem::checkErro($msg))
                                        return $msg;
                                }
                                
                                //Retorna para nao atualizar o estado do crud_mobile pois o mesmo
                                //ja foi atualizado na chamada interna
    //                            return $msg;
                            } else {
                                SingletonLog::writeLogStr("[N�O HAVER� TENTATIVA] N�o houve nenhuma modifica��o na verifica��o de fk. ", $container);
                                //Ocorreu um erro no dia 16/07/2016 que uma operacao de remocao
                                //que ocorria no exato momento do fluxo aqui, impossibilitou a insercao
                                //se tivess esperado mais um pouco, nao teria dado erro pois a chave
                                //teria se adaptado para NULL ou DELETE ON CASCADE.
                                $msg = $msgToken;
                                break;
                            }
                        }
                    }
                    if (!$corrigido) {
                        if (strlen($erroSql)) {
                            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "BO_Tabela::executaProtocoloInserir 1 - Erro SQL: $erroSql. "
                                . "Consulta: '$query'");
                        } else {
                            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "BO_Tabela::executaProtocoloInserir 2 - Erro SQL: N�o identificado. "
                                . "Consulta: '$query'");
                        }
                    } 

                }
            } else {
                $jsonErro = print_r($msg, true);
                $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "Erro ao processar o crud_mobile = {$idCrudMobile}. JSON1: $jsonErro");
            }

            if($registrarCrud){
                EXTDAO_Crud_mobile::updateProcessada($this->idCorporacao,
                    $idCrudMobile, $idCrud, 
                    $erroSql, 1, $this->db, $idWebDuplicada);
                SingletonLog::writeLogStr(
                    "Resultado final do crud insert." . print_r($msg, true). ". Id Crud: $idCrud");
            }
            else {
                SingletonLog::writeLogStr(
                    "N�o � para registrar o prcessamento do crud porque o mesmo j� foi registrado na itera��o de corre��o dos fks pais. "
                    . "Resultado final do crud insert." . print_r($msg, true). ". Id Crud: $idCrud");    
            }
            return $msg;
        } else {
//            $this->objCrudMobile->setErro_sql_INT(Database::ERRO_EXECUCAO);
//            $this->objCrudMobile->setProcessada_BOOLEAN('1');
//            $this->objCrudMobile->setData_processamento_DATETIME(Helper::getDiaEHoraAtualSQL());
//            $this->objCrudMobile->formatarParaSQL();
//            $this->objCrudMobile->update($idCrudMobile);

            EXTDAO_Crud_mobile::updateProcessada($this->idCorporacao,
                $idCrudMobile, null, 
                Database::ERRO_EXECUCAO, 1, $this->db, $idWebDuplicada);
            
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "O crud mobile n�o estava no banco espelho.");
        }
    }

    private function executaProtocoloInserir() {
        try {
            if (count($this->containerInsert)) {
                SingletonLog::writeLogStr("executaProtocoloInserir - executando o total de " . count($this->containerInsert) . " cruds de insercao");
                $cabecalho = $this->getCabecalhoAtributosInsert();
                $cabecalho = "INSERT INTO " . $this->nome . " ( `id`, $cabecalho ) VALUES ";
                //gravar json no arquivo

//                $this->objCrudMobile = new EXTDAO_Crud_mobile();;
                
                $this->db->iniciarTransacao();
                $this->dbWeb->iniciarTransacao();
                $idsToUnset = array();
                for ($i = 0; $i < count($this->containerInsert); ) {
                    $idsCrudMobileIt = array();
                    $i2 = $i;
                    for($k = 0 ; 
                        $i < count($this->containerInsert) && $k < 100; 
                        $k++, $i++){
                        if(!isset($this->containerInsert[$i])) continue;
                        $crudInserir = $this->containerInsert[$i];
                        if($crudInserir == null) continue;
                        $idsCrudMobileIt[count($idsCrudMobileIt)] = $crudInserir['id'];
                    }
                    $corpos = $this->getCorposInsert($idsCrudMobileIt);
                    unset($idsCrudMobileIt);
                    for($k = 0;
                        $i2  < $i && $k < count($corpos); 
                        $i2++){
                        
                        $crudInserir = $this->containerInsert[$i2];
                        if($crudInserir == null) continue;
                        
                        $corpo = $corpos[$k];
                        if($corpo['crud_mobile_id_INT'] != $crudInserir['id'])
                            continue;
                        else $k++;
                        
                        SingletonLog::writeLogStr("Execuntando crud inserir", $crudInserir);
                        SingletonLog::writeLogStr("Corpo[ASD]: ", $corpo);
                        $msg = $this->executaCrudMobileInserir($crudInserir, $cabecalho, $corpo, true);
                        if ($msg != null) {
                            if($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR){
                                SingletonLog::writeLogStr("[ertgdgbh]Parando a sincornizacao deivdo a um problema na conexao com o banco de dados", $msg);
                                return $msg;
                            }
                            else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
                                SingletonLog::writeLogStr("Erro grave durante o processamento do crud de insercao", $msg);
                                return $msg;
                            } else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE) {
                                SingletonLog::writeLogStr("Insercao cancelada pois o registro havia sido REMOVIDO_ON_CASCADE", $msg);
                                unset($this->containerInsert[$i2]);
                            }else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL) {
                                SingletonLog::writeLogStr("Erro durante o processamento do crud de insercao", $msg);
                                unset($this->containerInsert[$i2]);
                            } else {
                                SingletonLog::writeLogStr("Insercao realizado com sucesso");
                            }
                        }
                    }
                }
                
                $this->db->commitTransacao();
                $this->dbWeb->commitTransacao();
                
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, 
                    "Todas as inser��es foram processadas.");
            } else
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Container vazio");
        }  catch(DatabaseException $dbex){
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
        }  catch (Exception $exc) {
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            return new Mensagem(null, null, $exc);
        }
    }

    private function executaCrudMobileEditar($container, $cabecalho, $corpo = null, $verificarFkPais = true) {
        if($corpo == null)
            $corpo = $this->getValores($container['id']);

        $idCrudMobile = $container['id'];
        $this->ultimoIdCrudMobile = $idCrudMobile;
        $idCrud = null;
        $erroSql = null;
        $idMI = null;
//        $this->objCrudMobile->select($idCrudMobile);
        if ($corpo != null) {
            SingletonLog::writeLogStr("[f34fr]executaCrudMobileEditar", $container);
            $strValues = "";
//            $valores = array();
            for ($k = 0; $k < count($this->atributos); $k++) {

                $attr = $this->atributos[$k];
                if ($attr == 'id'
                    || $attr == 'crud_mobile_id_INT'
                    || $attr == 'senha' )
                    continue;

                if (strlen($strValues))
                    $strValues .= ", ";
                $valor = $corpo[$attr];
                if($this->atributosFormatadosFk[$k] != null && ($valor != null && $valor < 0)){
                    if($idMI == null ){
                        $idMI = EXTDAO_Crud_mobile::getIdMobileIdentificador(
                            $idCrudMobile, $this->idCorporacao, $this->db);
                        if($idMI == null) {
                            $msg =EXTDAO_Crud_mobile::updateProcessada(
                                $this->idCorporacao,
                                $idCrudMobile,
                                null,
                                Database::ERRO_FALTA_DADOS_HISTORICO_SINCRONIZACAO,
                                $this->db) ;
                            if($msg != null && Interface_mensagem::checkErro($msg))
                                return $msg;

                            return new Mensagem(
                                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                "N�o temos dados historicos do crud mobile $idCrudMobile para identificar o mobile identificador da opera��o.");
                        }
                    }

                    $valorNovo = EXTDAO_Crud_mobile::getIdTabelaWebCorrelacionado(
                        $idMI,
                        $this->atributosFormatadosFk[$k] ['fk_sistema_tabela_id_INT'],
                        $valor,
                        $this->db,
                        $this->idCorporacao);

                    $valor = $valorNovo;
                }


                if ($valor == null) {
                    $strValues .= "$attr = null";
                } else {

                    $valorFormatado = "'".$this->db->realEscapeString($valor)."'";
                    $strValues .= "$attr = $valorFormatado";
                }
//                $valores[count($valores)] = $valor;
            }

            //DEVERA SER VERIFICADA A EXISTENCIA DO REGISTRO
            $query = "SELECT id FROM {$this->nome} WHERE id = {$container['id_tabela_web_INT']}";
            $msg = $this->dbWeb->queryMensagem($query);
            if($msg != null && Interface_mensagem::checkErro($msg))
                return $msg;
            $idExistente = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
            $houverCorrecao = null;
            $registrarCrud = true;
            if (!strlen($idExistente)) {
                SingletonLog::writeLogStr("O registro � inexistente {$this->nome}[{$container['id_tabela_web_INT']}] no banco web");
//                $this->objCrudMobile->setErro_sql_INT(Database::CHAVE_INEXISTENTE);
                $erroSql = Database::CHAVE_INEXISTENTE;
                    
                $msg = new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "O registro � inexistente {$this->nome}[{$container['id_tabela_web_INT']}] no banco web, ja deve ter sido removido.");
            } else {
                SingletonLog::writeLogStr("[f34fr]executaCrudMobileEditar executando...");
                //realizando o UPDATE
                $query = $cabecalho . " $strValues WHERE id = {$container['id_tabela_web_INT']} ";

                $msg = $this->dbWeb->queryMensagem($query, Database::OPCAO_LANCAR_EXCECAO_BANCO_FORA_DO_AR);
                
                SingletonLog::writeLogStr("[f34fr]executaCrudMobileEditar, resultado da edi��o: ", $msg);
                
                if($msg != null && Interface_mensagem::checkErro($msg)){
                    SingletonLog::writeLogStr("[954ger]executaCrudMobileEditar erro com o servidor!");
                    return $msg;
                }
                if (Interface_mensagem::checkOk($msg)) {
                    SingletonLog::writeLogStr("A edicao foi realizada com sucesso!");
//                    $objCrud = new EXTDAO_Crud();
//                    $objCrud->setId_tabela_web_INT($container['id_tabela_web_INT']);
//                    $objCrud->setSincronizacao_id_INT($this->idSincronizacao);
//                    $objCrud->setSistema_tabela_id_INT($this->idSistemaTabela);
//                    $objCrud->setCrud_origem_id_INT(EXTDAO_Crud_origem::MOBILE);
//                    $objCrud->setTipo_operacao_banco_id_INT(EXTDAO_Tipo_operacao_banco::Editar);
//                    $objCrud->formatarParaSQL();
//                    $objCrud->insert();
                    $msg =EXTDAO_Crud::insertCrud(
                        $this->idCorporacao ,
                        $this->idSistemaTabela, 
                        $container['id_tabela_web_INT'], 
                        EXTDAO_Tipo_operacao_banco::Editar, 
                        EXTDAO_Crud_origem::MOBILE,
                        $this->idSincronizacao, 
                        null, 
                        $this->db);
                    if($msg != null && Interface_mensagem::checkErro($msg))
                        return $msg;
                    $idCrud = $this->db->getLastInsertId();

//                    $this->objCrudMobile->setCrud_id_INT($idCrud);

                    $msg = new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        "Edicao mobile realizada com sucesso [0d203948r4]");
                } else if ($msg != null && $msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL) {
                    
                    //$msgToken = new Mensagem_token();
                    $msgToken = $msg;
                    $erroSql = $msgToken->mValor;
                    SingletonLog::writeLogStr("[f234t3]falha durante a execu��o da consulta de edi��o. Erro: $erroSql");
//                    $this->objCrudMobile->setErro_sql_INT($erroSql);
                    
                    if ($erroSql == Database::CHAVE_DUPLICADA) {
                        
                        $msg = new Mensagem(PROTOCOLO_SISTEMA::CRUD_PENALIZADO);
                        SingletonLog::writeLogStr("Duplicada ocorrida. Crud penalizado");
                        
                        //CODIGO ANTIGO
//                        $msg = $this->processaDuplicada($valores);
//                        if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) {
//                            $this->objCrudMobile->setErro_sql_INT(Database::CHAVE_DUPLICADA);
//                            $idWebDuplicada = $msg->mValor;
//                            SingletonLog::writeLogStr("Encontrada a duplicada que ocasionou o bloqueio da edicao $idWebDuplicada .");
////                             $msg = $this->processaCrudsDependentes( $idWebDuplicada);
////                             if($msg->mCodRetorno != PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
//                            $this->objCrudMobile->setId_tabela_web_duplicada_INT($idWebDuplicada);
////                             $msg = new Mensagem(PROTOCOLO_SISTEMA::DUPLICADA_SOLUCIONADA);
////                             }
//                        } else if (strlen($erroSql)) {
//                            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "BO_Tabela::executaProtocoloInserir 9 - Erro SQL: $erroSql. "
//                                . "Consulta: '$query'");
//                        } else {
//                            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "BO_Tabela::executaProtocoloInserir 10 - Erro SQL: N�o identificado. "
//                                . "Consulta: '$query'");
//                        }
                    } 
                    else {
                        $houverCorrecao = false;
                        if ($verificarFkPais) {
                            $registrarCrud = false;
                            SingletonLog::writeLogStr("Verificandos os atributos fks do registro...");
                            while(true){
                                $verificado = $this->verificaDependenciaDaEdicaoOuInsercaoDoCrudMobile(
                                    EXTDAO_Crud_mobile::getIdSincronizacaoMobile(
                                        $idCrudMobile,
                                        $this->idCorporacao,
                                        $this->db),
                                    $idCrudMobile, 
                                    $corpo);
                                SingletonLog::writeLogStr("Verificado? $verificado.");
                                if ($verificado == PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE) {
                                    $houverCorrecao = true;
                                    //$this->objCrudMobile->setErro_sql_INT(Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE);
                                    $erroSql = Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE;
                                      
                                    //removido on cascade
                                    $msg = new Mensagem(
                                        PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE, "Registro removido on cascade");
                                    SingletonLog::writeLogStr(
                                        "Corrigido removido on cascade devido ao atributo fk possuir relacionmento cascade.", $msg);
                                    break;
                                } else if ($verificado != PROTOCOLO_SISTEMA::SEM_ALTERACAO) {

                                    SingletonLog::writeLogStr(
                                        "[TENTATIVA sdf] verificamos os atributos fks e vamos tentar editar novamente...");
                                    $msg = $this->executaCrudMobileEditar(
                                        $container, $cabecalho, null, false);
                                    SingletonLog::writeLogStr(
                                        "Resultado da [TENTATIVA sdf]." . print_r($msg, true));
                                    if($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO ){
                                        $houverCorrecao = true;
                                        //$this->objCrudMobile->setErro_sql_INT(null);
                                        $erroSql = null;
                                        break;
                                    } else {
                                        $houverCorrecao = true;
                                        SingletonLog::writeLogStr(
                                        "Penalizando o crud mobile, obrigando que o mesmo retorne seu valor");
                                        $msg = new Mensagem(PROTOCOLO_SISTEMA::CRUD_PENALIZADO );
                                    }
                                    
                                    //Retorna para nao atualizar o registro crud_mobile
    //                                return $msg;
                                } else {
                                    SingletonLog::writeLogStr(
                                        "[N�O HAVER� NOVA TENTATIVA]." . print_r($msg, true));
                                    $houverCorrecao = true;
                                        SingletonLog::writeLogStr(
                                        "Penalizando o crud mobile, obrigando que o mesmo retorne seu valor");
                                        $msg = new Mensagem(PROTOCOLO_SISTEMA::CRUD_PENALIZADO );
                                    break;
                                }
                            }
                        }
                        if (!$houverCorrecao) {
                            if (strlen($erroSql)) {
                                $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                    "BO_Tabela::executaCrudMobileEditar 7 - Erro SQL: $erroSql. "
                                    . "Consulta: '$query'");
                            } else {
                                $erroSql = Database::ERRO_EXECUCAO;
                                $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
                                    "BO_Tabela::executaCrudMobileEditar 8 - Erro SQL: N�o identificado. "
                                    . "Consulta: '$query'");
                            }
                        } 
                    }
                } else {

                    $erroSql = Database::ERRO_EXECUCAO;
                    $jsonErro = print_r($msg, true);
                    $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
                        "Erro ao processar o crud_mobile = {$idCrudMobile}. JSON: $jsonErro");
                }
            }
            if($registrarCrud){
                EXTDAO_Crud_mobile::updateProcessada($this->idCorporacao, 
                    $idCrudMobile, $idCrud, $erroSql, 1, $this->db);
                SingletonLog::writeLogStr(
                    "Resultado final do crud edit." . print_r($msg, true). ". Id Crud: $idCrud");
            }
            else {
                SingletonLog::writeLogStr(
                    "N�o � para registrar o prcessamento do crud porque o mesmo j� foi registrado na itera��o de corre��o dos fks pais. "
                    . "Resultado final do crud edit." . print_r($msg, true). ". Id Crud: $idCrud");    
            }
                
            return $msg;
        } else {
            SingletonLog::writeLogStr(
                "Corpo do crud esta nulo. Crud Mobile $idCrudMobile");

//            $this->objCrudMobile->setErro_sql_INT(Database::ERRO_EXECUCAO);
//            $this->objCrudMobile->setProcessada_BOOLEAN('1');
//            $this->objCrudMobile->setData_processamento_DATETIME(Helper::getDiaEHoraAtualSQL());
//            $this->objCrudMobile->formatarParaSQL();
//            $this->objCrudMobile->update($idCrudMobile);
            EXTDAO_Crud_mobile::updateProcessada($this->idCorporacao, 
                $idCrudMobile, null, Database::ERRO_EXECUCAO, 1, $this->db);
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "O crud mobile n�o estava no banco espelho.");
        }
    }

    private function executaProtocoloEditar() {
        try{
            if (count($this->containerEdit)) {
//                $msgErroFatalSoParaOMobile = false;
                $this->db->iniciarTransacao();   
                $this->dbWeb->iniciarTransacao();
                $cabecalho = $this->getCabecalhoAtributosInsert();
                $cabecalho = "UPDATE " . $this->nome . " SET ";
                //gravar json no arquivo
//                $this->objCrudMobile = new EXTDAO_Crud_mobile();
                for ($i = 0; $i < count($this->containerEdit); ) {
                    $idsCrudMobileIt = array();
                    $i2 = $i;
                    for($k = 0 ; 
                        $i < count($this->containerEdit) && $k < 100; 
                        $k++, $i++){
                        if(!isset($this->containerEdit[$i])) continue;
                        $crudInserir = $this->containerEdit[$i];
                        if($crudInserir == null) continue;
                        $idsCrudMobileIt[count($idsCrudMobileIt)] = $crudInserir['id'];
                    }
                    $corpos = $this->getCorposInsert($idsCrudMobileIt);
                    unset($idsCrudMobileIt);
                    for($k = 0;
                        $i2  < $i && $k < count($corpos); 
                        $i2++){
                        
                        $crudEditar = $this->containerEdit[$i2];
                        if($crudEditar == null) continue;
                        
                        $corpo = $corpos[$k];
                        if($corpo['crud_mobile_id_INT'] != $crudEditar['id'])
                            continue;
                        else $k++;
                        
                        $msg = $this->executaCrudMobileEditar($crudEditar, $cabecalho, $corpo, true);
                        if ($msg != null) {
//                            if($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_POR_FALTA_DE_HISTORICO_SINCRONIZACAO){
////                                $msgErroFatalSoParaOMobile = $msg;
//                                unset($this->containerEdit[$i2]);
//                                continue;
//                            }
                            if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR
                                || $msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR)
                                return $msg;
                            else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE
                                || $msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL){
                                unset($this->containerEdit[$i2]);
                            }
                            else if($msg->mCodRetorno == PROTOCOLO_SISTEMA::CRUD_PENALIZADO)
                                continue;
                        }
                    }
                }
                $this->db->commitTransacao();   
                $this->dbWeb->commitTransacao();
            }
//            if($msgErroFatalSoParaOMobile != null)
//                return $msgErroFatalSoParaOMobile;
//            else
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }  catch(DatabaseException $dbex){
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
        } catch (Exception $ex) {
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            return new Mensagem(null, null, $ex);
        }
    }

    // false - nao conseguiu verificar
    // true - conseguiu verificar
    public function verificaDependenciaDaEdicaoOuInsercaoDoCrudMobile(
        $idSincronizacaoMobile, $idCrudMobile, $corpo) {
        
        if (count($this->atributosFk)) {
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            for($tentativa = 0 ; $tentativa < BO_Tabela::MAXIMO_TENTATIVA_CORRECAO_FK; $tentativa++){
                try {
                        SingletonLog::writeLogStr("verificaDependenciaDaEdicaoOuInsercaoDoCrudMobile - "
                            . count($this->atributosFk) . " atributos FK a serem verificados ");
                        $modificado = false;
                        //Para cada atributo que possui chave extrangeira para o registro
                        for ($i = 0; $i < count($this->atributosFk); $i++) {
                            $attrFk = $this->atributosFk[$i];
                            SingletonLog::writeLogStr(
                                "Atributo da FK a ser verificado" .
                                print_r($attrFk, true));
                            //pais_id_INT
                            $atributoFk = $attrFk["nome"];
                            //ATRIBUTO id
        //                    $atributoFk = $this->atributosFk["atributo_fk"];
                            $onDelete = $attrFk["delete_tipo_fk"];

                            $idSistemaTabelaFk = $attrFk["fk_sistema_tabela_id_INT"];
                            if ($idSistemaTabelaFk == null || !strlen($idSistemaTabelaFk)) {
                                SingletonLog::writeLogStr(
                                    "Atencao: id sistema tabela FK do atributo � nulo." .
                                    print_r($attrFk, true));
                                continue;
                            }
                            //Opera��o de remo��o da PESSOA::ID = 1;
                            //verifica se PESSOA_EMPRESA::pessoa_id_INT = 1
                            //SELECT crud_mobile
                            //PESSOA_ID_INT = 1 AND (OP.INSERT OU OP.EDIT)
                            //SELECT SINCRONIZADOR_WEB 
                            //PESSOA_ID_INT = 1 AND (OP.INSERT OU OP.EDIT)
                            //CASO Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI
                            $tabelaFk = EXTDAO_Sistema_tabela::getNomeDaTabela(
                                    $idSistemaTabelaFk, $this->db);

                            $idAtributoFk = $corpo[$atributoFk];
                            if (!is_numeric($idAtributoFk)) {
                                SingletonLog::writeLogStr(
                                    "Atributo ignorado pois seu valor � nulo $atributoFk::$idAtributoFk." .
                                    print_r($attrFk, true));
                                continue;
                            }
                            //                $this->dbWeb->queryMensagem($q);
                            //                $q = "SELECT {$atributoFk} "
                            //                    . " FROM __{$this->nome} "
                            //                    . " WHERE crud_mobile_id_INT = $idCrudMobile ";
                            //                $this->db->queryMensagem($q);
                            //                $idAtributoFk = $this->db->getPrimeiraTuplaDoResultSet($q);
                            $q = "SELECT 1 "
                                . " FROM $tabelaFk "
                                . " WHERE id = $idAtributoFk ";
                            $msg = $this->dbWeb->queryMensagem($q);
                            
                            SingletonLog::writeLogStr("Consulta: $q. Resultado: " . print_r($msg, true));
                            $idWeb = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
                            SingletonLog::writeLogStr("1 se encontrou, null caso contrario. Resultado: '$idWeb'");
                            //Se o registro PESSOA.id = 1 nao foi encontrado
                            if (!strlen($idWeb)) {
                                SingletonLog::writeLogStr("O registro $tabelaFk [$idAtributoFk] nao existe no banco web. Consulta realizada: $q. No banco web ");
                                $modificado = true;
                                if ($onDelete == "SET NULL") {
                                    SingletonLog::writeLogStr("Relacionamento OnDelete � $onDelete. Atualizando o valor do atributo para nulo");
        //                            $q = "UPDATE __{$this->nome} t "
        //                                . " SET t.{$atributoFk} = NULL "
        //                                . " WHERE  ("
        //                                    . " t.{$atributoFk} = $idAtributoFk AND " 
        //                                    . "SELECT COUNT(cm.id) "
        //                                    . " FROM crud_mobile cm"
        //                                    . "         ON t.crud_mobile_id_INT = cm.id "
        //                                    . "     JOIN crud c "
        //                                    . "         ON c.id = cm.crud_id_INT "
        //                                    . " WHERE (cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Inserir."' "
        //                                    . "   OR cm.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Editar."' ) "
        //                                    . " AND c.sincronizacao_id_INT = {$this->idSincronizacao} "
        //                                    . " LIMIT 0,1  ) > 0 ";
                                    $q = " UPDATE __{$this->nome} t "
                                        . "             JOIN crud_mobile cm ON t.crud_mobile_id_INT = cm.id ";
                                    $q .= "         LEFT JOIN crud c ON c.id = cm.crud_id_INT  ";
                                    $q .= " SET t.$atributoFk = NULL ";
                                    $q .= " WHERE t.$atributoFk = $idAtributoFk ";
                                    $q .= "     AND (c.id IS NOT NULL OR (c.id IS NULL AND cm.sincronizacao_mobile_id_INT = $idSincronizacaoMobile))";

                                    $this->db->queryMensagemThrowException($q);
                                    SingletonLog::writeLogStr("Registros dependentes atualizados com sucesso");
                                    
                                } else if ($onDelete == "CASCADE") {
                                    SingletonLog::writeLogStr("Relacionamento OnDelete � $onDelete. Logo sera removido.");
                                    $q = "UPDATE crud_mobile cm "
                                        . "     JOIN __{$this->nome} t "
                                        . "             ON t.crud_mobile_id_INT = cm.id "
                                        . "     LEFT JOIN crud c ON c.id = cm.crud_id_INT "
                                        . " SET cm.processada_BOOLEAN = '1', "
                                        . "     cm.data_processamento_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',"
                                        . "     cm.erro_sql_INT = '" . Database::CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE . "' "
                                        . " WHERE  t.{$atributoFk} = {$idAtributoFk}"
                                        . "     AND (c.id IS NOT NULL OR (c.id IS NULL AND cm.sincronizacao_mobile_id_INT = $idSincronizacaoMobile))";
                                    $msg = $this->db->queryMensagemThrowException($q);
                                    
                                    SingletonLog::writeLogStr("Registro removido on cascade");
                                    return PROTOCOLO_SISTEMA::REMOVIDO_ON_CASCADE;
                                } else {
                                    throw new Exception("CONDICIONAL NAO PROGRAMADA");
                                }
                            } else {
                                SingletonLog::writeLogStr("Registro existe no db web, id: $idWeb");
                            }
                        }
                        if ($modificado)
                            return PROTOCOLO_SISTEMA::CORRIGIDO;
                        else
                            continue; //return PROTOCOLO_SISTEMA::SEM_ALTERACAO;
                        //TODO-OTIMIZAR - nao � pra ficar lan�ando excecao no codigo
                } catch(DatabaseException $dbex){
                    $this->db->commitTransacao();   
                    $this->dbWeb->commitTransacao();
                    $log= Registry::get('HelperLog');
                    $log->logErro($dbex, "[DBException]Falha durante a verifica��o de dependencia do registro do crud $idCrudMobile.");

                    throw $dbex;
                } catch (Exception $ex) {

                    $str = "";
                    if ($this->atributosFk != null && count($this->atributosFk)) {
                        foreach ($this->atributosFk as $attr) {
                            if (strlen($str) > 0)
                                $str .= ", ";
                            $str .= print_r($attr, true);
                        }
                    } else
                        $str = "(vazio)";

                    $log= Registry::get('HelperLog');
                    $log->logErro(new Exception("Falha durante a verifica��o de dependencia do registro do "
                        . "crud $idCrudMobile. Atributos fk : $str. {$ex->getMessage()} {$ex->getTrace()}"));


                    $this->db->commitTransacao();
                    $this->dbWeb->commitTransacao();
                    
                    throw $ex;
                }
                $this->db->commitTransacao();   
                $this->dbWeb->commitTransacao();
                        
                sleep(1);
                
                $this->db->iniciarTransacao();   
                $this->dbWeb->iniciarTransacao();
            }
        }

        return PROTOCOLO_SISTEMA::SEM_ALTERACAO;
    }

    public function getCabecalhoAtributosInsert() {
        $str = "";

        for ($i = 0; $i < count($this->atributos); $i++) {
            if ($this->atributos[$i] == 'id' || $this->atributos[$i] == 'crud_mobile_id_INT')
                continue;

            if (strlen($str))
                $str.= ", ";
            $str .= "`" . $this->atributos[$i] . "`";
        }
        return $str;
    }

    private function executarProtocoloRemover() {
        
        try {
    //        echo "asdf3.1: <br/>";
            if (count($this->containerRemove)) {
                SingletonLog::writeLogStr("executarProtocoloRemover - Temos "
                    . count($this->containerRemove)
                    . " cruds de remocao da tabela "
                    . $this->nome);

    //        echo "asdf3.2: <br/>";     
//                $this->objCrudMobile = new EXTDAO_Crud_mobile();
                
                $this->db->iniciarTransacao();
                $this->dbWeb->iniciarTransacao();
                
                for ($i = 0; $i < count($this->containerRemove); $i++) {
                    $idCrud = null;
                    $erroSql = null;
                    if(!isset($this->containerRemove[$i])) continue;
                    
                    if ($this->containerRemove[$i] != null) {
                        SingletonLog::writeLogStr("Executa crud de remocao", $this->containerRemove[$i]);
                        $idWeb = $this->containerRemove[$i]['id_tabela_web_INT'];
                        $idCrudMobile = $this->containerRemove[$i]['id'];
                        $this->ultimoIdCrudMobile = $idCrudMobile;
                        $qSelect = "SELECT id FROM {$this->nome} WHERE id = '{$idWeb}'";
                        $msg = $this->dbWeb->queryMensagem($qSelect);
                        if($msg != null){
                            if($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR){
                                return $msg;
                            }else if ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL) {

                                //$msgToken = new Mensagem_token();
                                $msgToken = $msg;
                                $erroSql = $msgToken->mValor;
                                if (strlen($erroSql)) {
                                    SingletonLog::writeLogStr("Falha grave durante a remocao, erro sql: $erroSql");
                                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "BO_Tabela::executarProtocoloRemover 9 - Erro SQL: $erroSql. Consulta: '$qSelect'. Crud mobile: " . $idCrudMobile);
                                } else {
                                    SingletonLog::writeLogStr("Falha grave durante select, erro sql desconhecido");
                                    return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, "BO_Tabela::executarProtocoloRemover 10 - Erro SQL: N�o identificado. Consulta: '$qSelect'. Crud mobile: " . $idCrudMobile);
                                }
                            } 
                        }
                        
                        $idReg = $this->dbWeb->getPrimeiraTuplaDoResultSet(0);
                        
//                        $this->objCrudMobile->select($idCrudMobile);
//                        if (strlen($idReg)) {
                            SingletonLog::writeLogStr("Removendo do banco web");
                            $qDelete = "DELETE FROM {$this->nome} WHERE id = '{$idWeb}'";
                            $msg = $this->dbWeb->queryMensagem(
                                $qDelete);
                            if($msg == null || $msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) {
                                    SingletonLog::writeLogStr("Registro com sucesso da web");
//                                    $objCrud = new EXTDAO_Crud();
//                                    $objCrud->setSincronizacao_id_INT($this->idSincronizacao);
//                                    $objCrud->setId_tabela_web_INT($idWeb);
//                                    $objCrud->setSistema_tabela_id_INT($this->idSistemaTabela);
//                                    $objCrud->setCrud_origem_id_INT(EXTDAO_Crud_origem::MOBILE);
//                                    $objCrud->setTipo_operacao_banco_id_INT(EXTDAO_Tipo_operacao_banco::Remover);
//                                    $objCrud->formatarParaSQL();
//                                    $objCrud->insert();
                                $msg =EXTDAO_Crud::insertCrud(
                                    $this->idCorporacao,
                                    $this->idSistemaTabela,
                                    $idWeb,
                                    EXTDAO_Tipo_operacao_banco::Remover,
                                    EXTDAO_Crud_origem::MOBILE,
                                    $this->idSincronizacao,
                                    null,
                                    $this->db );
                                if($msg != null && Interface_mensagem::checkErro($msg))
                                    return $msg;
                                $idCrud = $this->db->getLastInsertId();
//                                    $this->objCrudMobile->setCrud_id_INT($idCrud);
                            } else if ($msg != null && $msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL) {

                                //$msgToken = new Mensagem_token();
                                $msgToken = $msg;
                                $erroSql = $msgToken->mValor;
                                if (strlen($erroSql)) {

                                    //caso tente remover um id que n�o existe
                                    if ($erroSql == '1064') {
                                        SingletonLog::writeLogStr("Falha leve durante a remocao, o registro ja havia sido removido");
//                                            $this->objCrudMobile->setErro_sql_INT($erroSql);
                                    } else {
                                        SingletonLog::writeLogStr("Falha grave durante a remocao, erro sql: $erroSql");
                                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "BO_Tabela::executarProtocoloRemover 9 - Erro SQL: $erroSql. Consulta: '$qDelete'. Crud mobile: " . $idCrudMobile);
                                    }
                                } else {
                                    SingletonLog::writeLogStr("Falha grave durante a remocao, erro sql desconhecido");
                                    return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, "BO_Tabela::executarProtocoloRemover 10 - Erro SQL: N�o identificado. Consulta: '$qDelete'. Crud mobile: " . $idCrudMobile);
                                }
                            } else  if ($msg != null ) {
                                return $msg;
                            }

//                        } else {
//                            SingletonLog::writeLogStr("Registro ja havia sido removido da web");
//                            $this->objCrudMobile->setErro_sql_INT(Database::CHAVE_INEXISTENTE);
//                        }

//                        $this->objCrudMobile->setProcessada_BOOLEAN('1');
//                        $this->objCrudMobile->setData_processamento_DATETIME(Helper::getDiaEHoraAtualSQL());
//                        $this->objCrudMobile->formatarParaSQL();
//                        $this->objCrudMobile->update($idCrudMobile);
                        EXTDAO_Crud_mobile::updateProcessada($this->idCorporacao, 
                            $idCrudMobile, $idCrud, $erroSql, 1, $this->db);
                    }
                }

                $this->db->commitTransacao();    
                $this->dbWeb->commitTransacao();


                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
    //       echo "asdf3.3: <br/>";
            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        } catch(DatabaseException $dbex){
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR, null, $dbex);
        } catch (Exception $exc) {            
            $this->db->commitTransacao();   
            $this->dbWeb->commitTransacao();
            return new Mensagem(null, null, $exc);
        }
    }

    public function getCorpoSelect() {
        if (!isset($this->select)) {
            $select = "";
            for ($i = 0; $i < count($this->atributos); $i++) {

                if (strlen($select) > 0)
                    $select .= ", ";
                $select .= $this->atributos[$i];
            }
            $this->select = $select;
        }
        return $this->select;
    }

    public function getValores($id) {
        $corpos = $this->getCorposInsert(array($id));
        if (count($corpos))
            return $corpos[0];
        else
            return null;
    }

    public function getCorposInsert($ids) {
        if ($ids == null || empty($ids))
            return false;

        $strIn = Helper::arrayToString($ids, ", ");
        $validade = false;
        $query = "SELECT {$this->getCorpoSelect()} 
                        FROM __{$this->nome} 
                        WHERE crud_mobile_id_INT IN ($strIn)
                        ORDER BY crud_mobile_id_INT ";
//            echo "PASSO 6 </br>";
        $this->db->queryMensagemThrowException($query);
        $v_vetor = Helper::getResultSetToMatriz($this->db->result, 1, 0);

        return $v_vetor;
    }

}
