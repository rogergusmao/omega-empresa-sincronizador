<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Tabela
 *
 * @author home
 */
class BO_Tabela_web {
    //put your code here
    public $nome;
    public $idSistemaTabela;
    public $containerRemove;
    public $containerInsert;
    public $containerEdit;
    public $dbWeb;
    public $idsRemoved;
    public $idsInserted;
    public $idsEdited;
//    public $idsRepetidos;
    public $idSincronizacao;
    public $atributos;
    public $ultimoId;
    public $db;
    public $idCorporacao;
    public  $configWeb = false;
    public  $configSincronizadorWeb = false;
    
    public function __construct( 
        $nome, 
        $idSistemaTabela, 
        $ultimoId,
        $dbWeb = null, 
        $db = null, 
        $idSincronizacao = null, 
        $idCorporacao = null,
        $configWeb = false,
        $configSincronizadorWeb = false){
        
        $this->configWeb = $configWeb ;
        $this->configSincronizadorWeb = $configSincronizadorWeb ;
        $this->nome = $nome;
        $this->idSistemaTabela = $idSistemaTabela;
        if($dbWeb == null)
            $this->dbWeb = new Database($configWeb != null ? $configWeb : NOME_BANCO_DE_DADOS_WEB);
        else $this->dbWeb = $dbWeb;
        $this->idsRemoved = array();
        $this->idsInserted = array();
        $this->idsEdited = array();
//        $this->idsRepetidos = array();
        $this->db = $db;
        $this->ultimoId = $ultimoId;
        $this->atributos = $this->dbWeb->getAtributosDaTabela($this->nome);
        $this->idSincronizacao = $idSincronizacao;
        $this->idCorporacao = $idCorporacao;
    }
    public function validaOperacaoEditar($idRegistro){
        $index = array_search($idRegistro, $this->idsRemoved);
        if($index != false){
            return false;
        }
        $index = array_search($idRegistro, $this->idsInserted);
        if($index != false){
            return false;
        }
        $index = array_search($idRegistro, $this->idsEdited);
        if($index != false){
            return false;
        }
        return true;
    }

    public function validaOperacaoInserir($idRegistro){
        $index = array_search($idRegistro, $this->idsRemoved);
        if($index != false){
            //retira a opera��o de remover uma vez que a inser��o ocorrer� na mesma sincroniza��o
//            for($j = 0 ;$j < count($this->containerRemove); $j++){
//                if($this->containerRemove[$j] != null
//                    && $this->containerRemove[$j]['id_tabela_INT'] == $idRegistro){
////                    TODO n�o iremos remover mais o crud de remo��o da web, pois bancos
////                    sqlite gerados podem conter o registro
//                    $this->containerRemove[$j] = null;
//
//                }
//            }
            return false;
        }
        $index = array_search($idRegistro, $this->idsInserted);
        if($index != false){

            return false;
        }
        return true;
    }

    public function inicializaContainerInsert(){

        $container = $this->getContainerInsert($this->idSistemaTabela);

        $limite = count($container);
        for($i = 0 ; $i < $limite; $i++){
            $idRegistro = $container[$i]['id_tabela_INT'];

            if(!$this->validaOperacaoInserir($idRegistro)) {
                $container[$i] = null;
                continue;
            }
            else{
                $this->idsInserted[count($this->idsInserted)] = $container[$i]['id_tabela_INT'];
            }
        }
        $this->containerInsert = $container;
    }

    public function inicializaContainerRemove(){

        $container = $this->getContainerRemove($this->idSistemaTabela);
        for($i = 0 ; $i < count($container); $i++){
            $this->idsRemoved[count($this->idsRemoved)] = $container[$i]['id_tabela_INT'];
        }
        $this->containerRemove = $container;
    }


    public function getTabelasContainerRemover(){

        $q = "SELECT DISTINCT s.sistema_tabela_id_INT "
            . " FROM sistema_registro_sincronizador s "
            . " WHERE (s.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Remover."') "
            . " AND s.is_from_android_BOOLEAN = '0' ";
        if(strlen($this->ultimoId))
            $q .= " AND s.id <= {$this->ultimoId} ";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $container = Helper::getResultSetToMatriz($this->dbWeb->result,1, 0);

        return $container;
    }

    public function getIdsWebRemover($idSistemaTabela){

        $q = "SELECT DISTINCT s.id_tabela_INT "
            . " FROM sistema_registro_sincronizador s "
            . " WHERE (s.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Remover."') "
            . " AND s.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND s.is_from_android_BOOLEAN = '0' "
            . " ORDER BY s.id_tabela_INT";
        if(strlen($this->ultimoId))
            $q .= " AND s.id <= {$this->ultimoId} ";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $container = Helper::getResultSetToMatriz($this->dbWeb->result,1, 0);

        return $container;
    }

    public function existeIdWebRemover($idSistemaTabela, $id){

        $q = "SELECT 1 "
            . " FROM sistema_registro_sincronizador s "
            . " WHERE (s.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Remover."') "
            . " AND s.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND s.is_from_android_BOOLEAN = '0' "
            . " AND s.id_tabela_INT = $id";
        if(strlen($this->ultimoId))
            $q .= " AND s.id <= {$this->ultimoId} ";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $container = Helper::existsResultSet($this->dbWeb->result,1, 0);

        return $container;
    }
    

    private function getContainerRemove($idSistemaTabela){

        $q = "SELECT s.id id, "
            . "     id_tabela_INT id_tabela_INT"
            . " FROM sistema_registro_sincronizador s "
            . " WHERE s.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Remover."' "
            . " AND s.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND s.corporacao_id_INT = {$this->idCorporacao}";
        if(strlen($this->ultimoId))
            $q .= " AND s.id <= {$this->ultimoId} ";
        $q .= " AND s.is_from_android_BOOLEAN = '0' ";
        $this->dbWeb->queryMensagemThrowException($q);
        
        $container = Helper::getResultSetToMatriz($this->dbWeb->result,1, 0);

        return $container;
    }

    private function getContainerInsert(
        $idSistemaTabela){

        $q = "SELECT s.id id, id_tabela_INT id_tabela_INT"
            . " FROM sistema_registro_sincronizador s "
            . " WHERE s.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Inserir."' "
            . " AND s.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND s.corporacao_id_INT = {$this->idCorporacao}";
        if(strlen($this->ultimoId))
            $q .= " AND s.id <= {$this->ultimoId} ";
        $q .= " AND s.is_from_android_BOOLEAN = '0' ";

        $msg =$this->dbWeb->queryMensagem($q);
        
        $container = Helper::getResultSetToMatriz($this->dbWeb->result,1, 0);

        return $container;
    }

    private function getContainerEdit($idSistemaTabela){

        $q = "SELECT s.id id, id_tabela_INT id_tabela_INT "
            . " FROM sistema_registro_sincronizador s "
            . " WHERE s.tipo_operacao_banco_id_INT = '".EXTDAO_Tipo_operacao_banco::Editar."' "
            . " AND s.sistema_tabela_id_INT = $idSistemaTabela"
            . " AND s.corporacao_id_INT = {$this->idCorporacao}";
        if(strlen($this->ultimoId))
            $q .= " AND s.id <= {$this->ultimoId} ";
        $q .=  " AND s.is_from_android_BOOLEAN = '0' ";
        $msg =$this->dbWeb->queryMensagem($q);
        
        $container = Helper::getResultSetToMatriz($this->dbWeb->result,1, 0);
        return $container;
    }

    public function inicializaContainerEdit(){

        $container = $this->getContainerEdit($this->idSistemaTabela);
        $limite = count($container);

        for($i = 0 ; $i < $limite; $i++){
            $idRegistro = $container[$i]['id_tabela_INT'];
            if(!$this->validaOperacaoEditar($idRegistro)) {
                $container[$i] = null;
                continue;
            } else {
                $this->idsEdited[count($this->idsEdited)] = $idRegistro;
            }

        }
        $this->containerEdit = $container;
    }



    public function inicializaContainer(){

        $this->inicializaContainerRemove();
        $this->inicializaContainerInsert();
        $this->inicializaContainerEdit();

        $this->containerRemove = Helper::atualizaIndiceArray($this->containerRemove);

        $this->containerInsert = Helper::atualizaIndiceArray($this->containerInsert);
        $this->containerEdit = Helper::atualizaIndiceArray($this->containerEdit);

    }
    public function gravaDadosDaTabelaUpload(){

        $this->gravaProtocoloRemover();
        $this->gravaProtocoloInserirEditar(EXTDAO_Tipo_operacao_banco::Inserir, $this->containerInsert);
        $this->gravaProtocoloInserirEditar(EXTDAO_Tipo_operacao_banco::Editar, $this->containerEdit);
    }

    private function gravaProtocoloRemover(){
        if($this->containerRemove != null && !empty($this->containerRemove)){
            for($i = 0 ; $i < count($this->containerRemove); ){
                $idsWeb = array();
                $idsSistemaRegistroSincronizador = array();
                for($k = 0; $k < 50 && $i < count($this->containerRemove); $k++, $i++){
                    if($this->containerRemove[$i] != null){
                        $idsWeb[$k] = $this->containerRemove[$i]['id_tabela_INT'];
                        $idsSistemaRegistroSincronizador[$k] = $this->containerRemove[$i]['id'];
                    }
                }
                if(!empty($idsWeb)){
                    $msg =EXTDAO_Crud::insertCruds(
                            $this->idCorporacao,
                            $this->idSistemaTabela, 
                            $idsWeb, 
                            EXTDAO_Tipo_operacao_banco::Remover, 
                            EXTDAO_Crud_origem::WEB,
                            $this->idSincronizacao, 
                            $idsSistemaRegistroSincronizador, 
                            $this->db );
                    if($msg != null && $msg->erro())
                        return $msg;
                }
            }
        }
    }

    private function gravaProtocoloInserirEditar($tipoOperacao, $containers){

        if($containers != null && !empty($containers)){
            for($i = 0 ; $i < count($containers); ){
                $idsWeb = array();
                $idsSistemaRegistroSincronizador = array();
                for($k = 0; $k < 50 && $i < count($containers); $k++, $i++){
                    if($containers[$i] != null){
                        $idsWeb[count($idsWeb)] = $containers[$i]['id_tabela_INT'];
                        $idsSistemaRegistroSincronizador[count($idsSistemaRegistroSincronizador)] = $containers[$i]['id'];
                    }
                }
                if(!empty($idsWeb)){
                    $msg =EXTDAO_Crud::insertCruds(
                            $this->idCorporacao,
                            $this->idSistemaTabela, 
                            $idsWeb, 
                            $tipoOperacao, 
                            EXTDAO_Crud_origem::WEB,
                            $this->idSincronizacao, 
                            $idsSistemaRegistroSincronizador, 
                            $this->db );
                    if($msg != null && $msg->erro())
                        return $msg;
                }
            }
            
        }
    }

    public function getCorpoSelect(){
        if(!isset($this->select)){
            $select = "";
            for($i = 0 ; $i < count($this->atributos); $i++){
                if($i > 0)
                    $select .=  ", ";
                $select .=  $this->atributos[$i]  ;
            }
        }
        return $this->select;
    }

}
