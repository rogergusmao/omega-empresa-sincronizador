<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Sincronizador_espelho
 *
 * @author home
 */
class BO_Sincronizador_espelho {
    //put your code here
    static function uploadDadosSincronizadorMobile(
        $idSistemaSihop,
        $mobileConectado,
        $mobileIdentificador,
        $resetando, 
        $numRegistro = '1', 
        $idPrimeiro = null, 
        $idUltimo = null, 
        $idCorporacao = null,
        $corporacao=null,
        $ultimaMigracao = null){
        $db = null;
        try {
            $helperProcesso = new HelperProcesso($idSistemaSihop );
            $helperProcesso->addItemProc0($idCorporacao, true);
            $db = new Database();
            //A migracao de base obriga a sincronizacao de todo mobile resetar perdendo os dados pendentes
            if($ultimaMigracao != null){
                $ret = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao($db, $idCorporacao, $ultimaMigracao);
                if($ret != null) return $ret;
            }
            $obj = new EXTDAO_Sincronizacao_mobile();
            $idSincronizacaoMobileExistente = null;
            $primeiraVez = null;
            $idSincronizacaoMobile = null;
            if(!$resetando){
                $idSincronizacaoMobile = EXTDAO_Sincronizacao_mobile::geUltimotIdMobile(
                    $idCorporacao, $mobileIdentificador, $db);
                if(!empty($idSincronizacaoMobile)){

                    $obj->select($idSincronizacaoMobile);
                    $estado = $obj->getEstado_sincronizacao_mobile_id_INT();
                    switch ($estado) {

                        case EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO:
                        case EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE:
                        case EXTDAO_Estado_sincronizacao_mobile::INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO:
                        case EXTDAO_Estado_sincronizacao_mobile::PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB:
                            $objDados = EXTDAO_Sincronizacao_mobile::getDadosProcessoEmAberto(
                                $idCorporacao, $idSincronizacaoMobile, $db);
//                        $idSincronizacao = EXTDAO_Sincronizacao_mobile::geIdSincronizacao($idSincronizacaoMobile);

                            return new Mensagem_protocolo(Protocolo_processo_em_aberto::constroi($objDados),
                                PROTOCOLO_SISTEMA::PROCESSO_EM_ABERTO);
                        case EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL:
                        case EXTDAO_Estado_sincronizacao_mobile::RESETADO:
                            $primeiraVez = true;
                        case EXTDAO_Estado_sincronizacao_mobile::FINALIZADO:
                            $idSincronizacaoMobileExistente = null;
                            break;
                        case EXTDAO_Estado_sincronizacao_mobile::MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO:
                            $idSincronizacaoMobileExistente = $idSincronizacaoMobile;
                            break;
                        default:
                            break;
                    }
                }
            }

            $configuracaoSite = Registry::get('ConfiguracaoSite');
            $path = $configuracaoSite->PATH_CONTEUDO."sincronizacao_mobile/{$corporacao}/";
            if(!file_exists($path)){
                Helper::mkdir($path, 0777, true);
            }
            if(isset($_FILES["logfile"])){

                $nomeArquivo = $mobileIdentificador ."_".Helper::getDiaEHoraNomeArquivo().".json";
                $objUpload = new Upload();
                $objUpload->arrPermitido = "";
                $objUpload->tamanhoMax = "";
                $objUpload->file = $_FILES["logfile"];
                $objUpload->nome = $nomeArquivo;
                $objUpload->uploadPath = $path;
                $success = $objUpload->uploadFile();
                if (!$success) {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_SINCRONIZACAO,
                        I18N::getExpression( "Ocorreu uma falha durante o envio de dados do seu celular. Talvez por falha de conex�o a internet."));
                }
            }
            $db->iniciarTransacao();
            $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile();
            if(strlen($idSincronizacaoMobileExistente)){
                $objSincronizacaoMobile->select($idSincronizacaoMobileExistente);
            }
            if(strlen($mobileConectado))
                $objSincronizacaoMobile->setMobile_conectado_INT($mobileConectado);
            $objSincronizacaoMobile->setMobile_identificador_INT($mobileIdentificador);
            if(isset($nomeArquivo)){
                $objSincronizacaoMobile->setCrud_mobile_ARQUIVO($nomeArquivo);
            }
            $strNow = Helper::getDiaEHoraAtual();
            $objSincronizacaoMobile->setData_inicial_DATETIME($strNow);
            $objSincronizacaoMobile->setResetando_BOOLEAN($resetando);
            $objSincronizacaoMobile->setEstado_sincronizacao_mobile_id_INT(
                EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO);
            $objSincronizacaoMobile->setPrimeira_vez_BOOLEAN($primeiraVez);
            $objSincronizacaoMobile->setId_primeiro_sinc_android_web_INT($idPrimeiro);
            $objSincronizacaoMobile->setId_ultimo_sinc_android_web_INT($idUltimo);
            $objSincronizacaoMobile->setCorporacao_id_INT($idCorporacao);
            $objSincronizacaoMobile->formatarParaSQL();
            if(!empty($idSincronizacaoMobileExistente)){
                $objSincronizacaoMobile->update($idSincronizacaoMobileExistente);
                $idSM = $idSincronizacaoMobileExistente;
            }else{
                $objSincronizacaoMobile->insert();
                $idSM = $objSincronizacaoMobile->getIdDoUltimoRegistroInserido($idCorporacao);
            }
            
            EXTDAO_Sincronizacao_mobile_iteracao::inserir(
                $db, $idSM, EXTDAO_Estado_sincronizacao_mobile::FILA_ESPERA_PARA_SINCRONIZACAO, 
                Helper::getDiaEHoraAtualSQL());
            
            //            $objSincronizacaoMobile->select($idSM);
            $db->commitTransacao();
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $idSM);

        }  catch(DatabaseException $dbex){
            if($db !=null)$db->rollbackTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }   catch (Exception $exc) {
            if($db !=null)$db->rollbackTransacao();
            return new Mensagem(null,null,$exc);
        }
    }
}
