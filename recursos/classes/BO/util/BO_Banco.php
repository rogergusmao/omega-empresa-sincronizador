<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BOBanco
 *
 * @author home
 */
class BO_Banco {
    public $tabelasUpload = array();
    
    public $db = null;
    public $dbWeb = null;
    public $idCorporacao = null;
    public $corporacao = null;
    public $configWeb = false;
    public $configSincronizadorWeb = false;
    //put your code here
    public function __construct(
        $idCorporacao, 
        $corporacao,
        $configWeb = false,
        $configSincronizadorWeb = false) {
        
        $this->configWeb = $configWeb ;
        $this->configSincronizadorWeb = $configSincronizadorWeb ;
        $this->idCorporacao=$idCorporacao;
        $this->corporacao=$corporacao;
        
        
    }

    public function init(){
        $this->db = new Database($this->configSincronizadorWeb);
        $this->dbWeb = new Database($this->configWeb != null ? $this->configWeb : NOME_BANCO_DE_DADOS_WEB);
        
        $msg = $this->initTabelasUpload();
        if($msg != null && $msg->erro()) return $msg;
        return null;
    }
    public function close(){
        
    }
    
    private function initTabelasUpload(){
        $this->tabelasUpload = null;
        $q = "SELECT id, nome "
            . " FROM sistema_tabela"
            . " WHERE transmissao_mobile_para_web_BOOLEAN = '1' "
            . "     AND frequencia_sincronizador_INT = '-1' "
            . "     AND (excluida_BOOLEAN IS NULL OR excluida_BOOLEAN = '0')";
        
        $this->db->queryMensagemThrowException($q);
        
        $tabelas = Helper::getResultSetToMatriz($this->db->result, 1 , 0);
//        print_r($this->db);
//        print_r($tabelas);
//        exit(); 
        $ret= array();
        for($i = 0 ; $i < count(Database_ponto_eletronico::$tabelas); $i++){
            for($j = 0 ; $j < count($tabelas); $j++){
                $idSistemaTabela = $tabelas[$j]['id'];
                $nome = $tabelas[$j]['nome'];
                if($nome == Database_ponto_eletronico::$tabelas[$i]){
                    $ret[count($ret)] = $tabelas[$j];
                }
            }
        }
        $this->tabelasUpload = $ret;
        return null;
    }
    
    public function getIdDoUltimoCrudProcessadoNoBancoWeb(){
        
        $q = "SELECT max(c.id) "
        . " FROM crud c join sincronizacao s on c.sincronizacao_id_INT = s.id "
        . " WHERE s.estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::FINALIZADO;
        $this->db->queryMensagemThrowException($q);
        $id = $this->db->getPrimeiraTuplaDoResultSet(0);
        return $id;

        
    }
    
    public function getUltimoIdSistemaRegistroSincronizadorNoCrud(){
        $q = "SELECT max(id_sistema_registro_sincronizador_INT)"
            . " FROM crud "
            . " WHERE crud_origem_id_INT = " . EXTDAO_Crud_origem::WEB;
        $msg = $this->db->queryMensagemThrowException($q);
        
        return $this->db->getPrimeiraTuplaDoResultSet(0);
    }
    
    public function escreveTabela($handle, $idSistemaTabela, $idTipoOperacao, $idSincronizacao){
        $q = "SELECT c.id, "
            . "     c.tipo_operacao_banco_id_INT, "
            . "     c.crud_origem_id_INT, "
            . "     c.id_tabela_web_INT,"
            . "     sm.mobile_identificador_id_INT  "
            . " FROM crud c "
            . "     LEFT JOIN crud_mobile cm "
            . "         ON c.id = cm.crud_id_INT "
            . "     LEFT JOIN sincronizacao_mobile sm "
            . "         ON sm.id = cm.sincronizacao_mobile_id_INT "
            . " WHERE c.sincronizacao_id_INT = $idSincronizacao "
            . " AND c.sistema_tabela_id_INT = $idSistemaTabela "
            . " AND c.tipo_operacao_banco_id_INT = $idTipoOperacao " 
            . " ORDER BY c.sistema_tabela_id_INT, c.id ";
        $msg = $this->db->queryMensagemThrowException($q);
        $objs = Helper::getResultSetToMatriz($this->db->result, 1, 0);
        
        
        $boBancoWeb = new BO_Banco_web(
            $this->idCorporacao, 
            $this->corporacao,
            $this->configWeb ,
            $this->configSincronizadorWeb );
        $msg = $boBancoWeb->init();
        if($msg != null && $msg->erro()) return $msg;

        $path = ConfiguracaoSite::getPathConteudo()."sincronizacao_mobile/{$this->corporacao}/";
        if(!file_exists($path))
            Helper::mkdir($path, 0777, true);
        $path .= "{$idSincronizacao}_sincronizacao.json";
        $handle = fopen($path, "w+");
        fwrite($handle, "{\"sincronizacao\":\"$idSincronizacao\", ");
        for($i = 0; $i < count($objs); $i++){
            if($i > 0){
                fwrite($handle, ",");
            }
            $obj = $objs[$i];
            
            $idSistemaTabela = $obj['sistema_tabela_id_INT'];
            $nomeTabela = EXTDAO_Sistema_tabela::getNomeDaTabela(
                $idSistemaTabela, $this->db);
            $colunas = $this->db->getAtributosDaTabela();
            if(count($colunas) == 0) {
                throw new Exception("N�o foi poss�vel identificar as colunas da tabela $nomeTabela. No banco: " 
                    . $this->boBancoWeb->dbWeb->getJsonConfiguracaoDatabase());
            }
            for($j = 0; $j < 20 && $j + $i < count($objs) ;$j ++){
                $idsWeb = array();
                $idsWeb[count($idsWeb)] = $obj['id_tabela_web_INT'];
            }
            $registros = $boBancoWeb->getRegistros($nomeTabela, $idsWeb);
            $identificador = array();
            $identificador['tabela'] = $nomeTabela;
            $identificador['tipoOperacao'] = $obj["tipo_operacao_banco_id_INT"];
            $identificador['campos'] =$colunas ;
            $protocolos = array();
            for($k = 0; $k < $j; $k++){
                $protocolo = array();
                $protocolo['idCrud'] = $obj['id'];
                $protocolo['idMobileIdentificador'] = $obj['id_crud_mobile'];
                $protocolo['idTabelaWeb'] = $obj['id_tabela_web_INT'];
                $protocolo['crudOrigem'] = $obj['crud_origem_id_INT'];
                $protocolo['valores'] = $registros[$k];
                $protocolos[count($protocolos)] = $protocolo;
            }
            $identificador['protocolos'] = $protocolos;
            $str = Helper::jsonEncode($identificador);
            fwrite($handle, $str);
            fwrite($handle, "}");
            if($i % 50 == 0)
                fflush($handle);
        }
        fwrite($handle, "}");
        fclose($handle);
        
    }
    
    public function executaCrudsMobileNoBancoWeb($idSincronizacao){
        try {
            
            $msg = null;
            $idsTabelas = EXTDAO_Crud_mobile::getTabelasDaSincronizacao(
                $idSincronizacao, $this->idCorporacao, $this->db);
            SingletonLog::writeLogStr("[weroij23098]Tabelas da sincronizacao. Tabelas da sinc: ".  print_r($idsTabelas, true) );
            $executouCrud = false; 
            if(count($idsTabelas)){
                $cont = 0;
                $boTabela =  null;
                for($j = 0 ; $j < count($this->tabelasUpload); $j++){
                    $idTabelaUpload = $this->tabelasUpload[$j]['id'];

                    for($i = 0 ; $i < count($idsTabelas); $i++){
                        $idTabelaSinc = $idsTabelas[$i];
                        if($idTabelaSinc == $idTabelaUpload){
                            
                            $boTabela = new BO_Tabela(
                                $this->tabelasUpload[$j]['nome'], 
                                $idTabelaSinc, 
                                $idSincronizacao, 
                                $this->db,
                                $this->dbWeb,
                                $this->idCorporacao,
                                $this->configWeb ,
                                $this->configSincronizadorWeb);
                            SingletonLog::writeLogStr("Inicializa container da tabela ".  $this->tabelasUpload[$j]['nome'] );
                            $boTabela->inicializaContainer();

                            if(!$boTabela->vazia()){
//                                $this->db->iniciarTransacao();
//                                $this->dbWeb->iniciarTransacao();
                            
                                $executouCrud = true;
                                $msg = $boTabela->executaCrudsDoBancoEspelhoNoBancoWeb();
//                                 if(($msg != null 
                                
//                                $this->db->commitTransacao();    
//                                if($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR){
//                                    $this->dbWeb->rollbackTransacao();
//                                    break;
//                                } else {
//                                    $this->dbWeb->commitTransacao();
//                                }
                            } else {
                                SingletonLog::writeLogStr("A tabela nao possui "
                                    . "mais cruds para serem processados, todos "
                                    . "foram eliminiados de acordo com o contexto "
                                    . "atual da sincronizacao", 
                                    $boTabela->nome);
                            }

                            $cont++;

                            break;
                        }
                    }
                    if ($msg != null &&
                    ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR 
                    || $msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR))
                        break;
                    if(  $cont == count($idsTabelas))
                       break;
                }
                SingletonLog::writeMessage($msg);

                $log= Registry::get('HelperLog');
                $log->gravarLogEmCasoErro($msg);
                if($msg != null){
                    if($msg->mCodRetorno == PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR)
                        return $msg;
                    else if($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR){
                        SingletonLog::writeLogStr("Erro: ".print_r($msg,true), 
                            $boTabela->nome);
                        try{
                            $estadoErroSinc = null;
                            $this->db->iniciarTransacao();

                            if($boTabela != null ){ //&& $boTabela->objCrudMobile != null){
//                              $boTabela->objCrudMobile->getSincronizacao_mobile_id_INT();
                                if($boTabela != null && strlen($boTabela->ultimoIdCrudMobile)){
                                    $idSincronizacaoMobile = EXTDAO_Crud_mobile::getIdSincronizacaoMobile(
                                        $boTabela->ultimoIdCrudMobile, 
                                        $this->idCorporacao,
                                        $this->db);
                                    $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($this->database);
                                    $objSincronizacaoMobile->select($idSincronizacaoMobile);

                                    $json = print_r($msg, true);
                                    $objSincronizacaoMobile->setErro(Helper::substring($json,512));

                                    SingletonLog::writeLogStr(
                                        "Setando estado sincronizador mobile[{$idSincronizacaoMobile}]"
                                        . " da sincronizacao[$idSincronizacao]"
                                        . "  para "
                                        . "OCORREU_UM_ERRO_FATAL. Motivo: ".print_r($msg, true),
                                        $boTabela->nome);
                                        $nowSQL = Helper::getDiaEHoraAtualSQL();
                                    $objSincronizacaoMobile->setErro_sincronizacao_id_INT($idSincronizacao);
                                    $objSincronizacaoMobile->setEstado_sincronizacao_mobile_id_INT(EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL);
                                    $objSincronizacaoMobile->setData_final_DATETIME($nowSQL);
                                    $objSincronizacaoMobile->formatarParaSQL();
                                    $objSincronizacaoMobile->update($idSincronizacaoMobile);
                                    $estadoErroSinc = EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO_MOBILE;
                                    EXTDAO_Sincronizacao_mobile_iteracao::inserir($this->db, $idSincronizacaoMobile, EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL, $nowSQL);
                                }
                                
                            } else{
                                SingletonLog::writeLogStr(
                                    "Erro sincronizacao",
                                    $boTabela->nome);
                                $estadoErroSinc = EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO;
                            }

                            $this->db->commitTransacao();
                            return new Mensagem_token(
                                PROTOCOLO_SISTEMA::ERRO_PROCESSAMENTO,
                                $msg->mMensagem, 
                                $estadoErroSinc) ;
                        }
                        catch(DatabaseException $dbex){

                            $this->db->rollbackTransacao();
                            return new Mensagem_token(
                                 PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,
                                null, 
                                null, 
                                $dbex) ;
                        }
                        catch (Exception $ex) {
                            $this->db->rollbackTransacao();
                            return new Mensagem_token(
                                null,
                                null,
                                null, 
                                $ex) ;
                        }
                    }
                }
                
            }
            $this->db->iniciarTransacao();
            try{
                $idsSincronizacaoMobile = EXTDAO_Sincronizacao_mobile::getMobilesDaSincronizacao(
                    $this->idCorporacao,
                    $idSincronizacao, 
                    $this->db);
                
                $msg = EXTDAO_Sincronizacao_mobile::updateEstadosSincronizacoesMobiles(
                         $idsSincronizacaoMobile, 
                         $this->idCorporacao, 
                         EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE,
                         $this->db);
                if($msg != null && $msg->erro()) {
                    $this->db->rollbackTransacao();
                    return $msg;
                }
                $msg = EXTDAO_Sincronizacao_mobile_iteracao::inserirLista(
                         $this->db, 
                         $idsSincronizacaoMobile, 
                         EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE);
                if($msg != null && $msg->erro()) {
                    $this->db->rollbackTransacao();
                    return $msg;
                }
                
                $this->db->commitTransacao();
                if(!$executouCrud){
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "N�o existem crud_mobile's a serem processados.");
                }
                else {
                    return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Todos os crud_mobile's foram processados.");
                }
            } catch(DatabaseException $dbex){
                $this->db->rollbackTransacao();
                return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
            }catch (Exception $ex) {
                $this->db->rollbackTransacao();
                return new Mensagem(null,null, $ex);
            }
        }  catch(DatabaseException $dbex){
            
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }catch (Exception $exc) {
            return new Mensagem(null, null , $exc);
        }

    }

}
