<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Banco_sqlite
 *
 * @author home
 */
class BO_Banco_sqlite {
    //put your code here
    public $idCorporacao;
    public $corporacao;
    
    public $configWeb = false;
    public $configSincronizadorWeb = false;
    
    public function __construct(
        $idCorporacao, 
        $corporacao,
        $configWeb = false,
        $configSincronizadorWeb = false){

        $this->idCorporacao = $idCorporacao;
        $this->corporacao = $corporacao;
        if($configWeb == false){

            $configSincronizadorWebDb = Registry::get('ConfiguracaoDatabasePrincipal');
            $configWebDb = Registry::get('ConfiguracaoDatabaseSecundario');

            $this->configWeb = $configWebDb;
            $this->configSincronizadorWeb = $configSincronizadorWebDb;
        } else {
            $this->configWeb=$configWeb;
            $this->configSincronizadorWeb=$configSincronizadorWeb;
        }


    }
    public function status(){
        try{
            $diretorio = $this->getDiretorio();

            $fileJson = $diretorio."/estado_corporacao_$this->idCorporacao.json";
            if(!file_exists($fileJson)){
                
                $msg = $this->procedimentoGerarBancoSqlite(false);
                if($msg->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
                    return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO, "N�o existe arquivo algum de sincroniza��o. $fileJson");
                }
            }
            //Verifica se o arquivo do banco de dados criado n�o est� defazado
            //isso ocorre quando uma sincroniza��o que ocorreu depois da cria��o
            //do banco de dados ocorre.
            if(file_exists($fileJson)){
                $strJson = file_get_contents($fileJson);
                $json = json_decode($strJson, true);
                $json['pathArquivoBancoZip'] = null;
                $idUltimaSincronizacao = $json['idUltimaSincronizacao'];
                $idUltimaSincronizacaoComErro = EXTDAO_Sincronizacao::getIdDaUltimaSincronizacaoComErro(
                    $this->idCorporacao,
                    new Database($this->configSincronizadorWeb));
                if($idUltimaSincronizacao < $idUltimaSincronizacaoComErro){
                    $msg = $this->procedimentoGerarBancoSqlite(true, false);
                    if($msg->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
                        return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO, "N�o existe arquivo algum de sincroniza��o. Temos uma sincronizacao com erro que ocorreu depois da sincronizacao atual, $idUltimaSincronizacao <= $idUltimaSincronizacaoComErro");
                    }
                }
                return new Mensagem_generica($json);
            }
            if(file_exists($fileJson)){
                $strJson = file_get_contents($fileJson);
                $json = json_decode($strJson, true);
                $json['pathArquivoBancoZip'] = null;
                return new Mensagem_generica($json);
            }

        } catch(DatabaseException $dbex){
                
                return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        } catch (Exception $ex) {
            return new Mensagem(null, null, $ex);
        }


    }

    public function downloadBancoSqliteDaCorporacao(){
        try{
            $msg = $this->validaBancoSqliteDaCorporacaoAtualmenteArmazenado();
            if($msg != null && !$msg->ok())
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
                    "Aguarde o banco sqlite ser gerado");
            
            $diretorio = $this->getDiretorio();

            $fileJson = $diretorio."/estado_corporacao_$this->idCorporacao.json";
            if(file_exists($fileJson)){
                $strJson = file_get_contents($fileJson);
                $json = json_decode($strJson, 1);
                $pathArquivo= $json['pathArquivoBancoZip'];
                if(!file_exists($pathArquivo))
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Arquivo de sincroniza��o inexistente: $pathArquivo");
                $objDownload = new Download($pathArquivo);
                $fp = $objDownload->df_download();
                if ($fp){
                    return null;
                }
                else
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        "[cf3r4ferg]Falha durante o download do arquivo: $pathArquivo");
            } else {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Arquivo do estado da sincroniza��o inexistente: $fileJson");
            }
        }  catch(DatabaseException $dbex){
                
                return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }catch (Exception $ex) {
            return new Mensagem(null, null, $ex);
        }
    }
    public function getDiretorio(){

        $diretorio = ConfiguracaoSite::getPathConteudo()."bancos_sqlite/{$this->idCorporacao}";
        if(!file_exists($diretorio))
            Helper::mkdir ($diretorio, 0777, true);
        return $diretorio;
    }
    private function validaIdUltimaVersao( $idUltimaSinc, $db){
        $q = "SELECT MAX(id) FROM ("
            . " SELECT MAX(sincronizacao_id_INT) id "
            . " FROM sincronizacao_mobile "
            . " WHERE corporacao_id_INT = {$this->idCorporacao} "
            . "     AND estado_sincronizacao_mobile_id_INT = '".EXTDAO_Estado_sincronizacao_mobile::OCORREU_UM_ERRO_FATAL."' ";
        if(is_numeric($idUltimaSinc))
            $q .= " AND sincronizacao_id_INT > $idUltimaSinc";
        $q .= " UNION ALL "
            . " SELECT MAX(s.id) id 
                    FROM sincronizacao s 
                    WHERE s.corporacao_id_INT = {$this->idCorporacao} 
                    AND  
                    ( s.estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::RESETADO
                    . " OR s.estado_sincronizacao_id_INT = ".EXTDAO_Estado_sincronizacao::ERRO_SINCRONIZACAO. ") ";
        if(is_numeric($idUltimaSinc)) {

            $q .= " AND s.id > $idUltimaSinc ";
        }
        $q .=  ") X ";
//                echo $q;

        $msg = $db->queryMensagem($q);
        if($msg != null && ! $msg->ok()) return $msg;
        $idAuxUltimaSinc = $db->getPrimeiraTuplaDoResultSet(0);
        return is_numeric($idAuxUltimaSinc)
            ? new Mensagem_token(PROTOCOLO_SISTEMA::BANCO_SQLITE_DEFAZADO, null, $idAuxUltimaSinc)
            : null;
    }
    ///Se retornar o numero, � porque o banco � invalido. O numero corresponde 
    ////ao id da ultima sincronizacao com problema
    public function  validaBancoSqliteDaCorporacaoAtualmenteArmazenado(){
        $diretorio = $this->getDiretorio();
        $fileJson = $diretorio."/estado_corporacao_{$this->idCorporacao}.json";

        if(file_exists($fileJson)){
            $json = file_get_contents($fileJson);
            $obj = json_decode($json);

            $idUltimaSinc = $obj->idUltimaSincronizacao;
            $db = new Database($this->configSincronizadorWeb);
            $msg = $this->validaIdUltimaVersao($idUltimaSinc, $db);
//                echo "asdf:".$idAuxUltimaSinc;
            if($msg != null && !$msg->ok()){
                return $msg;
            } else {
                if(!empty($obj->ultimaMigracao)){
                    $msg = EXTDAO_Corporacao::procedimentoGetDataUltimaMigracao(
                        $db,
                        $this->idCorporacao,
                        $obj->ultimaMigracao);
                    if($msg != null) return $msg;
                } else {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ESTRUTURA_MIGRADA,
                        "A estrutura foi migrada e a atual data � nula");
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        null,
                        $idUltimaSinc);
            }
        } else return new Mensagem(
            PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
            "N�o h� bancos gerados");
    }
    public function procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao($tryLock = true, $aForca = false){
        $lock = false;
        try{
            
            if($tryLock && !LockSincronizador::semaphoreGet($this->idCorporacao))
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO, 
                    "O processo de sincroniza��o da corporacao {$this->idCorporacao} est� retido.");
            
            $lock = true;
            $diretorio = $this->getDiretorio();
            
            $fileJson = $diretorio."/estado_corporacao_{$this->idCorporacao}.json";
            
            if(file_exists($fileJson) && !$aForca){
                //$json = file_get_contents($fileJson);
                //$obj = json_decode($json);
                $msg = $this->validaBancoSqliteDaCorporacaoAtualmenteArmazenado();
              
                if($msg != null && !$msg->ok()){
                    $msg = self::procedimentoGerarBancoSqlite($aForca, false);
                    if($msg != null && $msg->ok()){
                        $msg->appendMsg(". DB Sqlite recriado com sucesso!");
                    }
                    if($tryLock)
                    LockSincronizador::semaphoreRelease($this->idCorporacao);
                    return $msg;
                }
                if($tryLock)
                LockSincronizador::semaphoreRelease($this->idCorporacao);
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    "Nao tivemos nenhuma incidencia de erro desde a ultima "
                    . "sincronizacao({$msg->mValor}) do atual banco sqlite.");
            } else {
                $ret1 = self::procedimentoGerarBancoSqlite($aForca, false);
                if($tryLock)
                LockSincronizador::semaphoreRelease($this->idCorporacao);
                return $ret1;
            }
            
        }  catch(DatabaseException $dbex){
            if($tryLock && $lock)
                LockSincronizador::semaphoreRelease($this->idCorporacao);
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }catch (Exception $ex) {
            if($tryLock && $lock)
                LockSincronizador::semaphoreRelease($this->idCorporacao);
            return new Mensagem(null, null, $ex);
        } 
            
        
    }
    
    public function procedimentoGerarBancoSqlite($aForca = false, $tryLock = true){
        
         $lock = false;
        try{
            if($tryLock && !LockSincronizador::semaphoreGet($this->idCorporacao))
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ACESSO_NEGADO, 
                    "O processo de sincroniza��o da corporacao {$this->idCorporacao} est� retido.");
            $lock = true;
            $diretorio = $this->getDiretorio();

            $fileJson = $diretorio."/estado_corporacao_{$this->idCorporacao}.json";

            $strData = Helper::getDiaEHoraNomeArquivo();
            $nomeBanco = "banco_sqlite_{$this->idCorporacao}";
            $nomeArquivoBanco = "Sqlite_corporacao_{$this->idCorporacao}.db";
            $arquivoZip = "Sqlite_corporacao_{$this->idCorporacao}.zip";
            
            if(!file_exists($diretorio))
                Helper::mkdir($diretorio, 0777, true);
            else {
                //remove os diretorios antigos e deixa apenas os 3 mais recentes
                Helper::exec("rm -rf $(ls -d  -1t $diretorio/*/ | tail -n +3)");
            }
            $files = scandir ($diretorio); // get all file names
//            print_r($files);
//            echo "entoruoasdfj";
            if(count($files) > 0){
                
                $nomeHoraArquivo = Helper::getDiaEHoraNomeArquivo();
                $newDir = "$diretorio/$nomeHoraArquivo";
                if(!file_exists($newDir))
                    Helper::mkdir ($newDir, 0777, true);
                
                foreach($files as $file){ // iterate files
//                    echo "entrou: $file. ";
                    $pathFile = "$diretorio/$file";
                    if(is_file($pathFile)){
                     // unlink($file); // delete file
                        
                        $bkpFile = "$newDir/{$file}";
                        rename($pathFile, $bkpFile);
//                        echo "rename($file, $bkpFile)";
                    }
                }
            }
//            exit();
            $dados = array();
            $boBanco = new BO_Banco(
                $this->idCorporacao, 
                $this->corporacao,
                $this->configWeb,
                $this->configSincronizadorWeb);
            $msg = $boBanco->init();
            if($msg != null && $msg->erro()) return $msg;
//                print_r($this->configWeb);
//                exit();
            $boBancoWeb = new BO_Banco_web(
                $this->idCorporacao, 
                $this->corporacao,
                $this->configWeb,
                $this->configSincronizadorWeb);
            $msg = $boBancoWeb->init();
            if($msg != null && $msg->erro()) return $msg;
            $db = new Database($this->configSincronizadorWeb);

            $idSincronizacao = EXTDAO_Sincronizacao::getIdDaUltimaSincronizacaoFinalizadaSejaSucessoOuComErro($this->idCorporacao, $db);
            $dados['idUltimoRegistroAntesDeGerarBanco'] = $boBancoWeb->getIdSistemaRegistroSincronizadorParaArquivoDeControleBancoSQLite($idSincronizacao);
            $dados['idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco'] = $boBanco->getIdDoUltimoCrudProcessadoNoBancoWeb();
            $dados['idUltimaSincronizacao'] = !strlen($idSincronizacao ) ? 0 : $idSincronizacao;

            $msg = $this->gerarBanco($nomeBanco, $diretorio,$nomeArquivoBanco, $arquivoZip );
            if($msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){

                $dados['idUltimoRegistroDepoisDeGerarBanco'] = $boBancoWeb->getIdSistemaRegistroSincronizadorParaArquivoDeControleBancoSQLite($idSincronizacao);
                $dados['idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco'] = $boBanco->getIdDoUltimoCrudProcessadoNoBancoWeb();
                $dados['ultimaMigracao'] = EXTDAO_Corporacao::getDataUltimaMigracao($db, $this->idCorporacao);
                if(!strlen($dados['ultimaMigracao'] )) $dados['ultimaMigracao'] = Helper::getDiaEHoraAtualSQL();
                $tamanhoBytes = $msg->mValor;

                $dados['tamanhoBytes'] = $tamanhoBytes;
                $dados['pathArquivoBancoZip'] = $diretorio."/".$arquivoZip;
                $dados['data'] = Helper::getDiaEHoraAtualSQL();

                $strJson = Helper::jsonEncode($dados);
                if(file_exists($fileJson)){
                    unlink($fileJson);
                }
                $fp = fopen($fileJson, "w+");
                if($fp){
                    fwrite($fp, $strJson);
                }
                fclose($fp);
                if($tryLock )
                    LockSincronizador::semaphoreRelease($this->idCorporacao);    
                return $msg;
            }
            else{
                if($tryLock )LockSincronizador::semaphoreRelease($this->idCorporacao);    
                return $msg;
            }
        }  catch(DatabaseException $dbex){
                if($tryLock && $lock)LockSincronizador::semaphoreRelease($this->idCorporacao);    
                return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }catch (Exception $ex) {
            if($tryLock && $lock)LockSincronizador::semaphoreRelease($this->idCorporacao);    
            return new Mensagem(null, null, $ex);
        }
    }

    public static function validaArquivoConfiguracao(){

        Helper::corrigirQuebrasDeLinha(Helper::acharRaiz() . "adm/db_converter.corporacao.sh");
    }

    private function gerarBanco($nomeDoBanco, $diretorio, $nomeDoArquivoDB, $nomeDoArquivoZip){
        try{

            $boBancoWeb = new BO_Banco_web(
                $this->idCorporacao, 
                $this->corporacao,
                $this->configWeb,
                $this->configSincronizadorWeb);
            $msg = $boBancoWeb->init();
            if($msg != null && $msg->erro()) return $msg;
            chmod($diretorio, 0777);

            $arrTabelas = $boBancoWeb->getTabelasDoAndroid();
//            print_r($arrTabelas);
//            exit();
            $arrTabelasUtilizar = array();

            $tabelas = $boBancoWeb->getTodasAsTabelas();

            for($i = 0 ; $i < count($tabelas); $i++){
                $tabelaExistente = $tabelas[$i];
                if(in_array($tabelaExistente, $arrTabelas)){
                    $arrTabelasUtilizar[] = $tabelaExistente;
                }
            }
            if(is_array($arrTabelasUtilizar)){
                $strTabelas = implode(" ", $arrTabelasUtilizar);
            }
            else{
                $strTabelas = "";
            }

            //$host = BANCO_DE_DADOS_HOST;
            $host =  $this->configWeb->host;
            $usuario = $this->configWeb->usuario;
            $senha = $this->configWeb->senha;
            $banco = $this->configWeb->DBName;
            $porta = $this->configWeb->porta;

            
            $pathSql = "{$diretorio}/{$nomeDoBanco}.sql";
            if(file_exists($pathSql)) unlink($pathSql);
            //-d -> dump somente a estrutura do banco
            $comando = "mysqldump -d --compatible=ansi --default-character-set=latin1 "
                . " --skip-extended-insert --compact --host={$host} --user={$usuario} "
                . " --password={$senha} -P {$porta} {$banco} {$strTabelas} > $pathSql";
//            echo "ENTROU: $comando";
//            $fp = fopen("c:/teste.txt", "w");
//            fwrite($fp, $comando);
//            fclose($fp);

            $saida = Helper::shellExec($comando);
            Helper::corrigirQuebrasDeLinha($pathSql);

//                echo "SAIDA 1 $comando: ";
//                echo $comando;
//                echo "<br/>";
//                exit();
            foreach ($arrTabelasUtilizar as $tabela) {
                
                $consultaTuplas = "SELECT *
                                FROM {$tabela}";
                $q = "SHOW COLUMNS FROM {$tabela} "
                    . " WHERE Field = 'corporacao_id_INT'";
                $msg = $boBancoWeb->dbWeb->queryMensagem($q);
                if($msg != null && $msg->erro()) return $msg;
                $coluna = $boBancoWeb->dbWeb->getPrimeiraTuplaDoResultSet("Field");
                $iSenha = null;
                if(! is_null($coluna)){
                    $consultaTuplas .= " WHERE corporacao_id_INT = {$this->idCorporacao}";
                } else if($tabela == "corporacao"){
                    $consultaTuplas .= " WHERE id = {$this->idCorporacao}";
                } else if($tabela == "usuario"){
                    $q = "SHOW COLUMNS FROM usuario ";
                    $msg = $boBancoWeb->dbWeb->queryMensagem($q);
                    if($msg != null && $msg->erro()) return $msg;
                    $rs = Helper::getResultSetToMatriz($boBancoWeb->dbWeb->result, true, false);
                    for($i = 0 ; $i < count($rs); $i++){
                        if($rs[$i]['Field'] == "senha") {
                            $iSenha = $i;
                            break;
                        }
                    }
                    $consultaTuplas = "SELECT u.* "
                        . " FROM usuario u "
                        . "     JOIN usuario_corporacao uc "
                        . "         ON u.id = uc.usuario_id_INT "
                        . " WHERE uc.corporacao_id_INT = {$this->idCorporacao}";
                }

                $msg = $boBancoWeb->dbWeb->queryMensagem(
                    $consultaTuplas, Database::OPCAO_LANCAR_EXCECAO);
                if($msg != null && $msg->erro()) return $msg;
                
                
                $handleArquivo = fopen($pathSql, "a");
                if($handleArquivo == false)
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        "[awvferfg]Falha ao tentar escrever no arquivo $pathSql");
                
                
                $primeiraVez = true;
                $j = 0;
                while($dadosTabelasExistentes = $boBancoWeb->dbWeb->fetchArray(MYSQL_NUM)){
//                    print_r($dadosTabelasExistentes);
                    if($primeiraVez) {
                        $primeiraVez = false;
                    }
                    fwrite($handleArquivo, "INSERT INTO \"{$tabela}\" VALUES (");
                    for($i = 0 ; $i < count($dadosTabelasExistentes); $i ++){
                        if($iSenha != null && $iSenha == $i)
                            $vValorAtributo = null;
                        else
                            $vValorAtributo = $dadosTabelasExistentes[$i];
                        
                        if(is_null($vValorAtributo)){
                            if($i == 0)
                                fwrite($handleArquivo, " NULL");
                            else fwrite($handleArquivo, ", NULL");
                        }else{
                            $vValorAtributo = str_replace(array("\r\n", "\r", "\n"), " ", $vValorAtributo);
                            if($i == 0)
                                fwrite($handleArquivo, " '{$vValorAtributo}'");
                            else fwrite($handleArquivo, ", '{$vValorAtributo}'");
                        }
                    }
                    fwrite($handleArquivo, " );\n");
                    if($j++ % 100 == 0){
                        fflush ($handleArquivo);
                        $j = 1;
                    }
                }
                fflush ($handleArquivo);
                fclose($handleArquivo);
                
            }
//            exit();
            
            $pathDb =  "{$diretorio}/{$nomeDoArquivoDB}";
            $comando= "";
            if(Helper::getSistemaOperacionalDoServidor() == WINDOWS){
                $comando = PATH_CYGWIN."bash ./db_converter.corporacao.sh {$pathSql} | sqlite3 {$pathDb}";
            }else{
                $comando = "sh ./db_converter.corporacao.sh {$pathSql} | sqlite3 {$pathDb}";
            }

            $saida = Helper::shellExec($comando);

            if(!file_exists($pathDb)){
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "[awsferg]Falha ao gerar o arquivo: "
                    .  print_r($msg, true).". PathDB: $pathDb.");
            }
            $dbSqlite = new Database_SQLite($pathDb, true);

            try{
                $dbSqlite->query("SELECT id FROM sistema_tabela WHERE nome='usuario' LIMIT 0,1");

            }catch (Exception $ex){
                $msg = new Mensagem(null,null,$ex);
            }
            if($msg != null && $msg->erro()) {
                unlink($pathDb);
                unlink($pathSql);
                return $msg;
            } else {
                $id = $dbSqlite->getPrimeiraTuplaDoResultSet(0);
                if(!$id){
                    unlink($pathDb);
                    unlink($pathSql);
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        "[dslkf]Nao encontrou o registro da tabela 'usuario' na tabela 'sistema_tabela'
                         contida no '.db' gerado. Msg erro: ".  print_r($msg, true));
                }
            }

            
            Helper::criarArquivoZip(
                array("{$diretorio}/{$nomeDoArquivoDB}"),
                "$diretorio/{$nomeDoArquivoZip}", true);



            $tamanhoEmBytes = filesize("{$diretorio}/{$nomeDoArquivoZip}");
            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, "Banco gerado com sucesso", $tamanhoEmBytes );
        }  catch(DatabaseException $dbex){
                
                return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }catch (Exception $ex) {
            return new Mensagem(null, null, $ex);
        }
    }

}
