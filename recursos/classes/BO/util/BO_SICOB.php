<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_SIHOP
 *
 * @author home
 */
class BO_SICOB {
    const PREFIXO = "SICOB:";
    
    public function factory(){
        return new BO_SICOB();
    }
    

    public static function getCorporacoesParaSincronizacaoDoHosting(){
         
            
        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB."getCorporacoesParaSincronizacaoDoHosting&id_empresa_hosting=".ID_EMPRESA_HOSTING;

        $json = Helper::chamadaCurlJson($url);


        if(Interface_mensagem::checkOk( $json))
        {
            $msgVetor = new Mensagem_vetor_protocolo(new Protocolo_empresa_hosting());
            $msgVetor->inicializa($json);
            return $msgVetor;
        } else  return Mensagem::factoryJson($json);

    }


}
