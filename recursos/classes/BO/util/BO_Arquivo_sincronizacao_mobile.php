<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Arquivo_sincronizacao_mobile
 *
 * @author home
 */
class BO_Arquivo_sincronizacao_mobile {
    //put your code here
    public $objSincronizacaoMobile;
    public $idSincronizacaoMobile;
    public $idSincronizacao;
    public $boBancoWeb = null;
    public $handle = null;
    public $tabelasUpload = array();
    public $db ;
    public $corporacao = null;
    public $idCorporacao;
    public $configWeb = false;
    public $configSincronizadorWeb = false;
    
    public function __construct(
        $corporacao, 
        $idSincronizacaoMobile, 
        $db = null, 
        $idCorporacao = null,
        $configWeb = false, 
        $configSincronizadorWeb = false){
        $this->corporacao = $corporacao;
        $this->idSincronizacaoMobile = $idSincronizacaoMobile;
        
        $this->configWeb = $configWeb;
        $this->configSincronizadorWeb = $configSincronizadorWeb;
        if($db != null)
            $this->db = $db;
        
        $this->idCorporacao = $idCorporacao;
        
        
    }
    public function init(){
        
        
        $this->boBancoWeb = new BO_Banco_web(
            $this->idCorporacao, 
            $this->corporacao, 
            $this->configWeb, 
            $this->configSincronizadorWeb);
        $msg = $this->boBancoWeb->init();
        if($msg != null && $msg->erro()) return $msg;
        if($this->db == null)
            $this->db = new Database($this->configSincronizadorWeb);
        $this->objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($this->db);
        $msg = $this->objSincronizacaoMobile->select($this->idSincronizacaoMobile);
        if($msg != null && $msg->erro()) return $msg;
        $this->idSincronizacao = $this->objSincronizacaoMobile->getSincronizacao_id_INT();
        return null;
    }
    private function getNomeArquivo(){

        return "{$this->corporacao}_{$this->idSincronizacao}_{$this->idSincronizacaoMobile}_sincronizacao.json";
    }
    private function getPathArquivoSincronizacaoMobile(){
        $path = ConfiguracaoSite::getPathConteudo()."sincronizacao_mobile/{$this->corporacao}";
        if(!file_exists($path))
            Helper::mkdir($path, 0777, true);
        return $path;
    }
    private function open(){
        $nomeArquivo = $this->getNomeArquivo();
        $path = $this->getPathArquivoSincronizacaoMobile()."/".$nomeArquivo;
        
        if(file_exists($path))
            unlink ($path);
        $this->handle = fopen($path, "w");
        return $this->handle;
    }
    private function close(){

        fclose($this->handle);
    }

    public function geraArquivo(){
        $hd = $this->open();
        if(!$hd){
            $strMsgErro = "Erro ao tentar abrir o arquivo mobile. Path: "
                .$this->getPathArquivoSincronizacaoMobile()
                .". Arquivo: ".$this->getNomeArquivo();
            SingletonLog::writeLogStr($strMsgErro);
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, $strMsgErro);
        }
        $tabelas = $this->objSincronizacaoMobile->getTabelasDaSincronizacao($this->idCorporacao);
        $idsTabelasOrdenadas = BO_Banco_web::getTabelasOrdenadas(
                                    $tabelas, 
                                    $this->db);
        $nomesTabelas = array();
        for($i = 0 ; $i < count($idsTabelasOrdenadas); $i++){

            $nomesTabelas[$i] = EXTDAO_Sistema_tabela::getNomeDaTabela(
                $idsTabelasOrdenadas[$i],
                $this->db);
        }
        //TODO remover log abaixo
        //SingletonLog::writeLogStr(print_r($nomesTabelas, true)."=====::=====".print_r($idsTabelasOrdenadas, true));

        $strTabelas = Helper::arrayToJson($nomesTabelas);

        fwrite($this->handle, "{\"sincronizacao\":\"{$this->objSincronizacaoMobile->getSincronizacao_id_INT()}\",\"sincronizacao_mobile\":\"{$this->objSincronizacaoMobile->getId()}\",\"tabelas\":[$strTabelas], \"cruds\":[" );
        //Gera de forma ordenada pronto para ser executada no mobile
        $linhas = 0;

        for($i = 0 ; $i < count($idsTabelasOrdenadas); $i++){
            $idSistemaTabela = $idsTabelasOrdenadas[$i];
            $nomeTabela = $nomesTabelas[$i];
           // SingletonLog::writeMessage("Nome tabela: $nomeTabela::$idSistemaTabela [asgneorgneropsgmjt]");
            fwrite($this->handle, "{\"tabela\":\"$nomeTabela\",\"operacoes\":[" );


            $linhas = $this->escreveOperacaoRemover($idSistemaTabela);
            if($linhas > 0) fwrite($this->handle, ",");

            $linhas = $this->escreveOperacaoInserirOuEditar($idSistemaTabela, EXTDAO_Tipo_operacao_banco::Inserir);
            if($linhas > 0) fwrite($this->handle, ",");

            $linhas = $this->escreveOperacaoInserirOuEditar($idSistemaTabela, EXTDAO_Tipo_operacao_banco::Editar);
            if($linhas > 0) fwrite($this->handle, ",");

            fwrite($this->handle, "]},");
        }

        fwrite($this->handle, "]}");
        $this->close();
        $objSincronizacaoMobile = new EXTDAO_Sincronizacao_mobile($this->db);
        try{
            
            $objSincronizacaoMobile->database->iniciarTransacao();
            $objSincronizacaoMobile->select($this->idSincronizacaoMobile);
            $objSincronizacaoMobile->setEstado_sincronizacao_mobile_id_INT(EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE);
            $nomeArquivo = $this->getNomeArquivo();
            $objSincronizacaoMobile->setCrud_web_para_mobile_ARQUIVO($nomeArquivo);
            $objSincronizacaoMobile->setCorporacao_id_INT($this->idCorporacao);
            $objSincronizacaoMobile->formatarParaSQL();
            $objSincronizacaoMobile->update($this->idSincronizacaoMobile);
            EXTDAO_Sincronizacao_mobile_iteracao::inserir($objSincronizacaoMobile->database, $this->idSincronizacaoMobile, EXTDAO_Estado_sincronizacao_mobile::AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE);

            $objSincronizacaoMobile->database->commitTransacao();
            return null;
        } catch (Exception $ex) {
            $objSincronizacaoMobile->database->rollbackTransacao();
            throw $ex;
        }
        
    }

    private function escreveOperacaoRemover($idSistemaTabela){
        //$this->objSincronizacao = new EXTDAO_Sincronizacao();

        $cruds = $this->objSincronizacaoMobile->getCrudsRemocao(
            $idSistemaTabela, $this->idCorporacao);
        if(count($cruds)){
            $nomeTabela = EXTDAO_Sistema_tabela::getNomeDaTabela(
                                $idSistemaTabela, 
                                $this->db);
            $jsonCruds = Helper::jsonEncode($cruds);
            //inicio do termo do identificador da opera��o do banco na tabela
            fwrite($this->handle, "{\"tipoOperacao\":\"".EXTDAO_Tipo_operacao_banco::Remover."\",\"cruds\": $jsonCruds }");
            return 1;
        } else return 0;
    }
    private function escreveOperacaoInserirOuEditar($idSistemaTabela, $idTipoOperacao){
        $linhas = 0;
        $objs = null;
        switch ($idTipoOperacao) {
            case EXTDAO_Tipo_operacao_banco::Editar:
                $objs = $this->objSincronizacaoMobile->getCrudsEdicao(
                    $idSistemaTabela, $this->idCorporacao);
                break;
            case EXTDAO_Tipo_operacao_banco::Inserir:
                $objs = $this->objSincronizacaoMobile->getCrudsInsercao(
                    $idSistemaTabela, $this->idCorporacao);
                break;
            default:
                throw new Exception("Oper��o de remo��o n�o � tratada nessa fun��o.");
        }
        if($objs == null || !count($objs))return 0;
        $nomeTabela = EXTDAO_Sistema_tabela::getNomeDaTabela(
                                $idSistemaTabela, 
                                $this->db);


        $colunas = $this->boBancoWeb->dbWeb->getAtributosDaTabela($nomeTabela);
        if(count($colunas) == 0) {
            throw new Exception("N�o foi poss�vel identificar as colunas da tabela $nomeTabela. No banco: "
                . $this->boBancoWeb->dbWeb->getJsonConfiguracaoDatabase());
        }
        //$jsonColunas = Helper::jsonEncode($colunas);
        //$jsonColunas = json_encode($colunas, JSON_UNESCAPED_UNICODE);
        $jsonColunas = json_encode($colunas);
        //inicio do termo do identificador da opera��o do banco na tabela
        fwrite($this->handle, "{\"tipoOperacao\":\"$idTipoOperacao\",\"campos\": $jsonColunas");
        //inicio dos protocolos
        fwrite($this->handle, ",\"protocolos\":[" );
        for($i = 0; $i < count($objs); ){
            $idsWeb = array();
            $tempObjs = array();
            $idsWebPorObjs = array();
            for($j = $i; $j < $i + 20 && $j < count($objs) ;$j ++, $i++){
                $obj = &$objs[$j];
                $tempObjs[count($tempObjs)] = &$obj;
                
                
                $auxIds = null;
                //No caso de insercao idTabelaWebCrud eh preenchido com o novo identificador
                if(is_numeric($obj['idTabelaWebCrud'])){
                    $auxIds = array($obj['idTabelaWebCrud']);
                }
                else if(is_numeric($obj['idTabelaWebDuplicada'])){
                    $auxIds = array($obj['idTabelaWebDuplicada']);
                 
                    if(is_numeric($obj['idTabelaWeb'])){
                        $auxIds[count($auxIds)] = $obj['idTabelaWeb'];
                    }
                }
                //No caso de edicao idTabelaWeb � utilizado
                else if(is_numeric($obj['idTabelaWeb'])){
                    $auxIds = array($obj['idTabelaWeb']);
                }
                else if(is_numeric($obj['idTabelaWebCrudMobile'])){
                    $auxIds = array($obj['idTabelaWebCrudMobile']);
                }
//                else {
//                    $strObjs = json_encode($objs[$j]);
//                    throw new Exception("Condicional n�o programada 123: ".$strObjs);
//                }

                for($o = 0 ; $o < count($auxIds); $o++){
                    $auxId = $auxIds[$o];
                    if(isset($idsWebPorObjs[$auxId])){
                        $objsIdTabelaWeb = &$idsWebPorObjs[$auxId];
                        $objsIdTabelaWeb[count($objsIdTabelaWeb)] = &$obj;
                    }
                    else {
                        $idsWebPorObjs[$auxId] = array(&$obj);
                        $idsWeb[count($idsWeb )] = $auxId;
                    }
                }
            }
//            SingletonLog::writeLogStr("idsWebs: ".print_r($idsWeb, true));
            if(count($idsWeb)){
                $registros = $this->boBancoWeb->getRegistros($nomeTabela, $idsWeb);
//                SingletonLog::writeLogStr("Registros: ".print_r($registros, true));
                for($k = 0; $k < count($registros) ;  $k++){
                    //id
                    $idTabelaWeb = $registros[$k][0];

                    $auxObjs = $idsWebPorObjs[$idTabelaWeb];
                    
                    if(count($auxObjs)){

                        for($o = 0; $o < count($auxObjs); $o++ ){
                            $obj = &$auxObjs[$o];


                            if($obj['idTabelaWebDuplicada' ]== $idTabelaWeb){
//                                echo "oiaertoi";
                                //preenche o atributo valores para que venha antes no arquivo JSON
                                if(!isset($obj['valores'])){
                                    $obj['valores'] = null;
                                }
                                
                                $obj['valores_duplicada'] = $registros[$k];
                                //SingletonLog::writeLogStr("$nomeTabela [{$obj['idTabelaWebDuplicada' ]}] valores duplicada: ".print_r($registros[$k], true));

                            } else if($obj['idTabelaWeb' ]== $idTabelaWeb 
                                || $obj['idTabelaWebCrudMobile' ]== $idTabelaWeb){
                                $obj['valores'] = $registros[$k];
                                //preenche o atributo valores_duplicada para que venha depois no arquivo JSON
                                if(!isset($obj['valores_duplicada'])){
                                    $obj['valores_duplicada'] = null;
                                }
                            } 
//                            print_r($obj);
                        }
                        
//                        echo "ACESSOU";
//                        print_r($auxObjs);
                    }
                }
                
//                SingletonLog::writeLogStr("idsWebPorObjs: ".print_r($idsWebPorObjs, true));
//                SingletonLog::writeLogStr("tempObjs: ".print_r($tempObjs, true));
                

                for($k = 0 ; $k < count($tempObjs); $k++){
                    $crudsDependetes = EXTDAO_Crud_mobile_dependente::getCrudsDependentesDoCrudMobile(
                        $this->idCorporacao,
                        $tempObjs[$k]['idCrudMobile'],
                        $this->idSincronizacaoMobile,
                        $this->db,
                        array(EXTDAO_Tipo_operacao_banco::Inserir,
                            EXTDAO_Tipo_operacao_banco::Editar)
                    );
                    if(!isset($tempObjs[$k]['valores']))
                        $tempObjs[$k]['valores'] = null;
                    
                    if(!isset($tempObjs[$k]['valores_duplicada']))
                        $tempObjs[$k]['valores_duplicada'] = null;
                    //TODO remover validacao

                    if(
                        (is_numeric($tempObjs[$k]['idTabelaWebDuplicada' ])
                        && count($tempObjs[$k]['valores_duplicada'] ) == 0)
                        &&
                        (is_numeric($tempObjs[$k]['idTabelaWeb' ]) 
                        || is_numeric($tempObjs[$k]['idTabelaWebCrudMobile' ])
                        && count(  $tempObjs[$k]['valores']) == 0)
                     ){
                        throw new Exception( "erro no crud index [$k]: ".print_r($tempObjs[$k], true).", valores do registro n�o foram preenchidos");
                    }

                    $tempObjs[$k]['cruds_dependentes'] = $crudsDependetes;
                    //$str = json_encode($tempObjs[$k]);
                    $str = json_encode($tempObjs[$k], JSON_UNESCAPED_UNICODE);
                    if($linhas > 0 ){
                        //divisor dos termos do vetor do protocolos
                        fwrite($this->handle, ",");
                    }
                    fwrite($this->handle, $str);
                    $linhas++;
                }
            }

        }
//        exit();
        //fim dos protocolos
        fwrite($this->handle, "]");
        //fim do identificador da operacao de banco da tabela
        fwrite($this->handle, "}");

        return $linhas;
    }
}
