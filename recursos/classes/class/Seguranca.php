<?php

class Seguranca extends InterfaceSeguranca
{   
    //lista de paginas que nao precisam de autenticacao
    public static $paginasExcecao = array();

    //actions que nao precisam de autenticacao
    public static $actionsExcecao = array(
        "erroFatalSincronizacaoMobile",
        "mobileResetado",
        "uploadDadosSincronizadorMobile",
        "iniciaSincronizacao",
        "processaArquivosSincronizacaoMobile",
        "downloadBancoSqliteDaCorporacao",
        "erroSincronizacaoMobile",
        "statusSincronizacaoBancoSqliteDaCorporacao",
        "procedimentoGerarBancoSqlite",
        "statusSincronizacao",
        "downloadDadosSincronizacaoMobile",
        "downloadDadosSincronizacao",
        "finalizaSincronizacaoMobile",
        "iniciaProximoProcessoDeSincronizacao",
        "processaArquivosCrudMobile",
        "executaCrudMobileDoBancoEspelhoNoBancoWeb",
        "processoGeraArquivoCrudDaSincronizacao",
        "processaArquivosCrudMobileDaSincronizacao",
        "executaCrudMobileDoBancoEspelhoNoBancoWebDaSincronizacao",
        "retiraMobileDaFilaDeEspera",
        "processoInsereOperacoesDoBancoWebNoBancoEspelho",
        "executaSincronizacaoCompleta",
        "procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao",
        "cadastraCorporacao",
        "isCorporacaoExistente",
        "assinaturaMigrada",
        "cadastraCorporacaoNaReserva",
        "daCordaNoSincronizador",
        "clearCacheBanco",
        "clearCacheBancoEEmpilhaPedidoSqlite",
        "uploadDadosSincronizadorMobileAntesDoReset");



    public function __construct()
    {
        $this->paginaExcecao = Seguranca::$paginasExcecao;
    }

    public function getAcoesLiberadasParaUsoAutenticado(){
        return array("statusSincronizacaoMobile");
    }
    

    

    
    public function factory()
    {
        return new Seguranca();
    }
    
    public static function isAutenticado(){
        $obj = Registry::get('Seguranca');
        
        return $obj->__isAutenticado();
    }
    
    public function isPaginaRestrita($pagina){
        return !in_array($pagina, Seguranca::$paginasExcecao);
    }
    public function isAcaoRestrita($classe, $acao)
    {
        if(!in_array($acao, Seguranca::$actionsExcecao)) 
            return true;
        
        return false;
    }

    public function getLabel(){
        $token = self::getEmail();
        return $token;
    }

//    public function existeUsuarioDoEmail($email, $db = null){
//        return EXTDAO_Usuario::existeUsuarioDoEmail($email, $db );
//    }


//    public function verificaSenha(
//        $dados,
//        $senha,
//        $db = null,
//        $registrarAcesso = false)
//    {
//
//        $email = $dados["email"];
//        $criptografia = Registry::get('Crypt');
//        $senhaC = $criptografia->crypt($senha);
//        $q = "SELECT DISTINCT u.id AS usuario_id,
//                                    u.nome AS usuario_nome,
//                                    u.email AS usuario_email,
//                                    u.status_BOOLEAN AS usuario_status,
//                                    t.nome AS usuario_tipo,
//                                    t.nome_visivel AS usuario_tipo_visivel,
//                                    t.id AS usuario_id_tipo,
//                                    IF(u.pagina_inicial IS NULL or u.pagina_inicial = '', t.pagina_inicial, u.pagina_inicial) as usuario_pagina_inicial
//                    FROM usuario AS u JOIN usuario_tipo AS t ON  t.id=u.usuario_tipo_id_INT
//                    WHERE u.email='$email'
//                     AND u.senha='$senhaC' ";
//
//        if(BANCO_COM_TRATAMENTO_EXCLUSAO){
//            $q .= " AND t.excluido_DATETIME IS NULL
//                AND u.excluido_DATETIME IS NULL";
//        }
//
//        $msg = $db->queryMensagem($q);
//        if($msg != null && $msg->erro())
//            return $msg;
//
//        if ($db->rows() >0 ) {
//            $us =  $db->getPrimeiraTuplaDoResultSet("usuario_status");
//
//            if ($us == 1) {
//
//                if($registrarAcesso){
//                    $objCliente = Helper::getPrimeiroObjeto($db->result, 1, 0);
//
//                    $objCliente["usuario_permissoes"] =  $this->getArrayDeFuncionalidadesDoUsuario(
//                        $objCliente["usuario_id"],
//                        $db,
//                        $dados);
//
//
//                    return new Mensagem_protocolo(
//                        $objCliente,
//                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
//                        "Login realizado com suceso.");
//                }else {
//                    return new Mensagem(
//                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
//                        "Login realizado com suceso.");
//                }
//            } else {
//                return new Mensagem(
//                    PROTOCOLO_SISTEMA::ACESSO_NEGADO,
//                    "Usu�rio foi bloqueado pelo administrador do site.");
//            }
//
//
//        }else {
//            $idUsuario = EXTDAO_Usuario::getIdUsuarioDoEmail($email, $db);
//            if (!strlen($idUsuario)) {
//                return new Mensagem(
//                    PROTOCOLO_SISTEMA::EMAIL_DO_USUARIO_INEXISTENTE,
//                    "N�o existe usu�rio cadastrado com o email $email");
//            }else {
//                return new Mensagem(
//                    PROTOCOLO_SISTEMA::SENHA_DO_USUARIO_INCORRETA,
//                    "Senha incorreta");
//            }
//        }
//    }




    public function verificaUsuarioESenha($idUsuario, $senha, $db = null){
//        if(!(strlen($idUsuario) > 0 && strlen($senha) > 0 )) return false;
//        $criptografia = Registry::get('Crypt');
//        if($db == null)$db = new Database();
//        $senhaC = $criptografia->crypt($senha);
//        $db->query("SELECT u.status_BOOLEAN AS status
//                    FROM usuario AS u
//                    WHERE u.id='$idUsuario' AND u.senha='$senhaC'");
//
//        if ($db->rows() == 1) {
//
//            if ($db->getPrimeiraTuplaDoResultSet("status") == 1) {
//
//                return true;
//            }
//        }
        return false;
    }



    public function gravarLogin($id, $db){
        return;
    }

    public function onRegistraAcesso($dados){
        return;
    }

    public function existeUsuarioDoEmail($email, $db = null){
        return null;
    }

    public function __actionLogin($dados, $senha, $db = null){

    }

    public function __actionLogout($mensagemUsuario = null){

    }

    public function verificarPermissaoEmAcoesDoUsuarioCorrente(
        $classe, $metodo){
        return $this->__isAutenticado();
    }

    protected function getSenhaDecrypt($dados, $db){
        return null;
    }

    public function verificaSenha(
        $dados,
        $senha,
        $db = null,
        $registrarAcesso = false){

    }

    public function getSenhaDoUsuarioLogado($db = null){
        $email = $this->getEmail();
        $q = "SELECT c.senha
            FROM usuario AS c
            WHERE c.email ='{$email}' ";
        if($db==null) $db = new Database();
        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }
}
