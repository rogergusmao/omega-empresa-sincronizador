<?php

class LockSincronizador {

    private static $instance = null;

    private $helperRedis = null;
    private $lockHandler = array();
    private $tempoTotalSemaforo = 0;

    const APC = 1;
    const FILE = 2;
    const REDIS = 3;

    private static $TIPO = LockSincronizador::REDIS;

    const PREFIXO = "LS_";

    private function __construct(){

        if(LockSincronizador::$TIPO == LockSincronizador::APC
            && (!function_exists("apc_add")
                    || strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        )
            LockSincronizador::$TIPO = LockSincronizador::FILE;
        else if(LockSincronizador::$TIPO == LockSincronizador::REDIS)
            $this->helperRedis = new HelperRedis(LockSincronizador::PREFIXO);
    }
    private static function getInstance(){
        if(LockSincronizador::$instance == null){
            LockSincronizador::$instance = new LockSincronizador();
        }
        return LockSincronizador::$instance ;

    }

    private function __getSemaphorePath($idCorporacao){

        $path = sys_get_temp_dir() . "/dbLock_{$idCorporacao}";
        return $path;
    }

    public static function semaphoreGet($idCorporacao){
        if(Manutencao::checkIfAssinaturaDaCorporacaoEstaEmManutencao(
            false,
            $idCorporacao,
            false))
            return false;

        $instance = LockSincronizador::getInstance();
        $inicio = microtime();

        $maxIterations = 1;
        switch (LockSincronizador::$TIPO){
            case LockSincronizador::APC:
                $maxIterations = 50;
                break;
            case LockSincronizador::REDIS:
                $maxIterations = 10;
                break;
            default:
            case LockSincronizador::FILE:
                $maxIterations = 10;
                if (!isset($instance->lockHandler[$idCorporacao])
                    || !is_resource($instance->lockHandler[$idCorporacao]))
                $instance->lockHandler[$idCorporacao] = fopen($instance->__getSemaphorePath($idCorporacao), 'w');
                break;
        }

        for($i=1; $i <= $maxIterations; $i++){

            switch (LockSincronizador::$TIPO){
                case LockSincronizador::APC:

                    $isLocked = apc_exists("dblock_{$idCorporacao}");
                    $successLock = !$isLocked;

                    if(!$isLocked){

                        $adicionadoComSucesso = apc_add("dblock_{$idCorporacao}", "L", 10);

                        //se n�o foi adicionado com sucesso, � pq outra requisi��o j� adicionou antes
                        if (!$adicionadoComSucesso) {
                            $successLock = false;
                        }
                    }
                    break;

                case LockSincronizador::$TIPO == LockSincronizador::REDIS:
                    $successLock = $instance->helperRedis->setNx($idCorporacao, 1) == 1 ? true : false;
                    break;

                case LockSincronizador::$TIPO == LockSincronizador::FILE:
                default:
                    $successLock = flock($instance->lockHandler[$idCorporacao], LOCK_EX);
                    break;
            }

            if($successLock) {

                $termino = microtime();
                $instance->tempoTotalSemaforo += (($termino - $inicio)*1000);

                return true;
            } else {
                usleep($i*10000);
            }
        }

        $termino = microtime();
        $instance->tempoTotalSemaforo += (($termino - $inicio)*1000);
        return false;
    }

    public static function semaphoreRelease($idCorporacao){
        $instance = LockSincronizador::getInstance();

        switch (LockSincronizador::$TIPO){
            case LockSincronizador::APC:
                apc_delete("dblock_{$idCorporacao}");
                break;
            case LockSincronizador::REDIS:
                $instance->helperRedis->del($idCorporacao);
                break;
            default:
            case LockSincronizador::FILE:
                if (is_resource($instance->lockHandler[$idCorporacao])) {
                    flock($instance->lockHandler[$idCorporacao], LOCK_UN);
                }
                break;
        }
    }


    function unlockFiles(){
        $instance = LockSincronizador::getInstance();
        if(is_array($instance->lockHandler) && !empty($instance->lockHandler)){

            foreach($instance->lockHandler as $idCorporacao => $fileHandler){

                if (is_resource($instance->lockHandler[$idCorporacao])) {
                    flock($instance->lockHandler[$idCorporacao], LOCK_UN);
                    fclose($instance->lockHandler[$idCorporacao]);
                }
            }
        }
    }

    function __destruct() {

        $this->unlockFiles();
    }

}

