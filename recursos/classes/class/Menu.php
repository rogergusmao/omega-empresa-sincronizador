<?php

class Menu extends InterfaceMenu
{
    public function getArrayDoConteudoDoMenuCompleto(){
     
        $arrMenu = array(

            "Blog" => array(

                "config" => new MenuConfig("Blog", MENUCONFIG_MENU_ABA),

                "Notícia"  => array(

                    "config" => new MenuConfig("Notícia", MENUCONFIG_LISTA_VERTICAL),
                    "Cadastrar Notícia" => new MenuItem("forms_noticia", "restauracao_backup.png"),
                    "Gerenciar Notícia" => new MenuItem("filters_noticia", "backup.png")

                ),
                "Categoria"  => array(

                    "config" => new MenuConfig("Categoria", MENUCONFIG_LISTA_VERTICAL),
                    //"Cadastrar Categoria" => new MenuItem("forms_categoria", "restauracao_backup.png"),
                    "Gerenciar Categoria" => new MenuItem("filters_categoria", "backup.png")

                ),

            ),
    
        );

        $menuSistema = array(
            
            "Sistema" => array(

                "config" => new MenuConfig("Sistema", MENUCONFIG_MENU_ABA),

                "Backup do Banco de Dados"  => array(

                    "config" => new MenuConfig("Backup do Banco de Dados", MENUCONFIG_LISTA_VERTICAL),
                    "Restaurar um Backup do Banco de Dados" => new MenuItem("pages_restaurar_backup_banco_dados", "restauracao_backup.png"),
                    "Fazer backup do Banco de Dados" => new MenuItem("pages_fazer_backup_banco_dados", "backup.png"),
                    "Gerenciar Rotinas de Backup Automático" => new MenuItem("lists_backup_automatico", "gerenciar_backups.png")

                ),

            ),

            "Usuários" => array(

                "config" => new MenuConfig("Usuários", MENUCONFIG_MENU_ABA),

                "Usuários" => array(

                    "config" => new MenuConfig("Usuários", MENUCONFIG_LISTA_VERTICAL),
                    "Cadastrar Usuário do Sistema"	=> new MenuItem("forms_usuario", "cadastrar_usuario.png"),
                    "Gerenciar Usuários do Sistema"	=> new MenuItem("lists_usuario", "usuarios.png"),
                    "Visualizar Operações de Usuários no sistema" => new MenuItem("lists_operacao_sistema", "operacoes_usuarios.png"),

                ),

                "Classes de Usuários" => array(

                    "config" => new MenuConfig("Classes de Usuários", MENUCONFIG_LISTA_VERTICAL),
                    "Cadastrar Classe de Usuário"	=> new MenuItem("forms_usuario_tipo", "classe_usuarios.png"),
                    "Gerenciar Classes de Usuário"	=> new MenuItem("lists_usuario_tipo", "grupos_de_usuarios.png")

                ),

                "Sugestões" => array(

                    "config" => new MenuConfig("Sugestões", MENUCONFIG_LISTA_HORIZONTAL),
                    "Cadastrar Sugestão"	=> new MenuItem("forms_sugestao_software", "cadastrar_sugestao.png"),
                    "Visualizar Sugestões"	=> new MenuItem("lists_sugestao_software", "sugestao.png")

                ),

            )

        );

        $arrMenu = array_merge($arrMenu, $menuSistema);
        return $arrMenu;

    }

}

