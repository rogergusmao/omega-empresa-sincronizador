<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 14/08/2017
 * Time: 21:16
 */
class Processo0 extends InterfaceProcesso
{

    public $lastDate;

    public function __construct($idSistemaSihop){

        parent::__construct(HelperProcesso::QUEUE_SINCRONIZACAO_P0, $idSistemaSihop, 0);

    }

    public function run()
    {
        //Web, mobile, rotina seguranca ou o proprio Processo1 - podem inserir nesse processo
        //  processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar
        while (true) {

            //blpop - redis
            $idCorporacao = $this->nextCorporacao(HelperProcesso::TEMPO_ITERACAO_P0);

            if($idCorporacao == null) continue;
            else {
                $this->log( "IdCorporacao: $idCorporacao");
                if(!$this->helperRedis->exists(HelperProcesso::HASHSET_SINCRONIZACAO_P0.$idCorporacao)){

                    $this->helperRedis->set(
                        HelperProcesso::HASHSET_SINCRONIZACAO_P0.$idCorporacao,
                        1,
                        "ex",
                        HelperProcesso::MAX_TEMPO_ADD_ORDEM_SINCRONIZACAO_DE_UMA_CORPORACAO_CONSECUTIVA_SEGUNDOS);

                    $this->helperRedis->rpush(HelperProcesso::QUEUE_SINCRONIZACAO_P1, $idCorporacao);

                }
            }

        }
    }


    public static function factory($idSistemaSihop){
        return new Processo0($idSistemaSihop);
    }
}