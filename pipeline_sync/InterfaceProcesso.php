<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 19/08/2017
 * Time: 10:43
 */
class InterfaceProcesso
{
    public $queueName = null;
    public $lastIdCorporacaoEmManutencao = null;
    public $idSistemaSihop = null;
    public $helperRedis;
    public $lockWR;
    public $idProcesso;
    public static function PATH_LOG_PROCESSOS_EM_EXECUCAO($classe, $idSistema){

        $dir = Helper::acharRaiz()."conteudo/pipeline_processos_em_execucao";
        if(!is_dir($dir))
            Helper::mkdir($dir, 0777, true);
        return "{$dir}/{$classe}_Sistema_{$idSistema}.log";
    }
    public function __construct($queueName, $idSistemaSihop, $idProcesso){

        $this->idProcesso = $idProcesso;
        $this->queueName = $queueName;
        $this->helperRedis = new HelperRedis("SIHOP_{$idSistemaSihop}", $idSistemaSihop);
        $this->idSistemaSihop = $idSistemaSihop;


        $this->lockWR= new LockWR(
            "LK_SINC",
            $idProcesso,
            HelperProcesso::TOTAL_READERS,
            HelperProcesso::TEMPO_MAXIMO_EXECUCAO_PROCESSO_MINUTOS * 60);
    }

    protected function log($msg){
        HelperLog::logProcesso(
            "{$this->idProcesso}_Sistema_Sihop_{$this->idSistemaSihop}", $msg);
    }

    public function pushCorporacao($idCorporacao){
        $this->lastIdCorporacaoEmManutencao = $idCorporacao;
        return $this->helperRedis->rpush($this->queueName, $idCorporacao);
    }
    protected function nextCorporacao($tempoIteracaoSegundos = HelperProcesso::MAX_TEMPO_ESPERA_SEGUNDOS){
        $idCorporacao = $this->helperRedis->blpop(
            $this->queueName,
            $tempoIteracaoSegundos);
        if(empty($idCorporacao)){
            return null;
        }
        $idCorporacao = $idCorporacao[1];
        if($this->lastIdCorporacaoEmManutencao == $idCorporacao || empty($idCorporacao)){
            $this->lastIdCorporacaoEmManutencao =null;
            sleep(HelperProcesso::MAX_TEMPO_ESPERA_SEGUNDOS_QUEUE_EMPTY);
            return null;
        }
        else if(Manutencao::checkIfAssinaturaDaCorporacaoEstaEmManutencao(false , $idCorporacao , false)){

            $msg = "Corporacao $idCorporacao em manutencao!\n";
            echo $msg;
            $this->log($msg);
            $this->pushCorporacao($idCorporacao);
            return null;
        } else{
            return $idCorporacao;
        }
    }



}
