<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 14/08/2017
 * Time: 21:16
 */
class Processo2 extends InterfaceProcesso
{
    //Processo1 ou Processo2 - podem inserir nesse processo
    //  EXECUTA AS OPERACOES DO BANCO ESPELHO NO WEB
    //      executaCrudMobileDoBancoEspelhoNoBancoWeb
    //      processoInsereOperacoesDoBancoWebNoBancoEspelho


    public function __construct($idSistemaSihop){
        parent::__construct(HelperProcesso::QUEUE_SINCRONIZACAO_P2, $idSistemaSihop, 2);



    }

    public function run(){
        //Processo1 ou o proprio Processo2 - podem inserir nesse processo
        //  processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar

        while(true){
            //Queue - ids corporacao - sem tempo de expiracao
            $idCorporacao = $this->nextCorporacao(HelperProcesso::TEMPO_ITERACAO_P2);
            if($idCorporacao == null) continue;
            if ($this->lockWR->tryRead($idCorporacao)) {
                try{
                    $this->log( "tryRead($idCorporacao) - true");
                    //Queue - id corporacao
                    //realiza processo 1
                    $boSincronizacao = new BO_Sincronizacao();
                    $msg = $boSincronizacao->initByIdCorporacao($idCorporacao );
                    if($msg == null || $msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
                        $this->log( "Executando...");

                        $msg = $boSincronizacao->processo2(true);

                        $this->log( "Fim exec. ". print_r($msg , true));
                        if($msg->erroBancoForaDoAr()){
                            $this->pushCorporacao($idCorporacao);
                        }else if($msg->ok() || $msg->resultadoVazio()){
                            $this->helperRedis->rpush(HelperProcesso::QUEUE_SINCRONIZACAO_P3, $idCorporacao);
                        }

                    } else {
                        $this->log( "Falha ao iniciar a confiuracao do sinc: ".print_r($msg, true));
                    }
                }catch(Exception $ex){
                    $msg = new Mensagem(null, null, $ex);
                    $this->log( "Falha durante execucao do sinc: ".print_r($msg, true));
                }

                // else descarta
                $this->lockWR->closeReader($idCorporacao);

                Database::closeAll();
            } else{
                $this->log( "tryRead($idCorporacao) - false");
                $this->pushCorporacao($idCorporacao);
            }
        }
    }

    public static function factory($idSistemaSihop){
        return new Processo2($idSistemaSihop);
    }
}