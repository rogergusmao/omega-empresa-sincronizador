<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 14/08/2017
 * Time: 21:16
 */
class Processo1 extends InterfaceProcesso
{

    public function __construct($idSistemaSihop){

        parent::__construct(HelperProcesso::QUEUE_SINCRONIZACAO_P1, $idSistemaSihop, 1);

    }


    public function run(){
        //Processo0 ou o proprio Processo1 - podem inserir nesse processo
        //  processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar
        while(true) {
            $idCorporacao = $this->nextCorporacao(HelperProcesso::TEMPO_ITERACAO_P1);
            if ($idCorporacao == null) continue;

            if ($this->lockWR->tryRead($idCorporacao)) {
                try{
                    $this->log( "tryRead($idCorporacao) - true");

                    //Queue - id corporacao
                    //realiza processo 1
                    $boSincronizacao = new BO_Sincronizacao();
                    $msg = $boSincronizacao->initByIdCorporacao($idCorporacao );
                    if($msg == null || $msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){
                        $this->log( "Executando...");
                        $msg = $boSincronizacao->processo1(true);
                        $this->log( "Fim exec. ". print_r($msg , true));
                        if($msg->mCodRetorno == PROTOCOLO_SISTEMA::SEM_DADOS_PARA_SEREM_SINCRONIZADOS){

                            $this->helperRedis->del(HelperProcesso::HASHSET_SINCRONIZACAO_P0. $idCorporacao);
                            $this->helperRedis->lrem(HelperProcesso::QUEUE_SINCRONIZACAO_P0, 0, $idCorporacao);
                        }
                        else { //OPERACAO_INCOMPLETA || ok || erro || ... ){
                            if($msg->ok() || $msg->resultadoVazio()) {

                                //$idSincronizacao = $msg->mValor;
                                //TODO deveria ser a sincronizacao, mas a fila ja esta toda programada no DB,
                                //entao deixa por enquanto. MAS FILA NO DB NAO PRESTA
                                $this->helperRedis->rpush(HelperProcesso::QUEUE_SINCRONIZACAO_P2, $idCorporacao);
                            } else if($msg->erroComServidor()){
                                $this->helperRedis->rpush(HelperProcesso::QUEUE_GERAR_BANCO_SQLITE_ERRO, $idCorporacao);
                            }
                            else {
                                //Empilha novamente
                                $this->pushCorporacao($idCorporacao);
                            }
                        }

                    } else {
                        $this->log( "Falha ao iniciar a confiuracao do sinc: ".print_r($msg, true));
                    }

                }catch(Exception $ex){
                    $msg = new Mensagem(null, null, $ex);
                    $this->log( "Falha durante execucao do sinc: ".print_r($msg, true));
                }
                $this->lockWR->closeReader($idCorporacao);

                Database::closeAll();
            } else {
                $this->log( "tryRead($idCorporacao) - false");
                $this->pushCorporacao($idCorporacao);
            }
        }
    }


    public static function factory($idSistemaSihop){
        return new Processo1($idSistemaSihop);
    }
}