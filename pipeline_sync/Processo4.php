<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 14/08/2017
 * Time: 21:16
 */
class Processo4 extends InterfaceProcesso
{
    //Processo2 ou Processo3 - podem inserir nesse processo
    //  geraArquivoCrudDaSincronizacao


    public function __construct($idSistemaSihop){
        parent::__construct(HelperProcesso::QUEUE_GERAR_BANCO_SQLITE_ERRO, $idSistemaSihop, 4);

    }
    public function run(){
        //Processo2 ou o proprio Processo3 - podem inserir nesse processo
        //  processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar
        BO_Banco_sqlite::validaArquivoConfiguracao();
        while(true){

            //Queue - ids corporacao - sem tempo de expiracao
            $idCorporacao = $this->nextCorporacao(HelperProcesso::TEMPO_ITERACAO_P4);
            if($idCorporacao == null) continue;
            try{
                $corporacao = BOSicobComum::getNomeCorporacao($idCorporacao);
                $msg = null;
                if(!empty($corporacao)) {
                    $msg = ConfiguracaoCorporacaoDinamica::factoryConfiguracaoSite($corporacao);

                    if ($msg == null || (Mensagem::checkOk($msg) && !empty($corporacao))) {
                        try {
                            $status = $this->lockWR->tryWrite($idCorporacao) ;
                            switch ($status){
                                case LockWR::GOT_LOCK_WRITE:
                                    break;
                                case LockWR::GOT_LOCK_WRITE:
                                    do {
                                        sleep(5);
                                    } while (!$this->lockWR->waitReaders($idCorporacao));
                                    break;
                                case LockWR::CANNOT_TAKE_LOCK_WRITE:
                                    $this->pushCorporacao($idCorporacao);
                                    Database::closeAll();
                                    continue;
                                default:
                                    $this->log( "Status $status n�o implementado" );
                                    Database::closeAll();
                                    continue;
                            }

                            $configWebDb = Registry::get('ConfiguracaoDatabaseSecundario');
                            $configSincronizadorWebDb = Registry::get('ConfiguracaoDatabasePrincipal');

                            $boBS = new BO_Banco_sqlite(
                                $idCorporacao,
                                $corporacao,
                                $configWebDb,
                                $configSincronizadorWebDb);
                            $msg = $boBS->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao(false);
                            if ($msg != null && $msg->erroBancoForaDoAr()) {
                                $this->pushCorporacao($idCorporacao);
                                $this->log( "Falha banco fora do ar" );
                            } else {
                                $this->log( "Resultado da geracao do banco sqlite: ".print_r($msg,true) );
                            }

                        } catch (Exception $ex) {
                            $msg = new Mensagem(null, null, $ex);
                            $this->log( "Falha fatal durante execucao do sinc: " . print_r($msg, true));
                        }

                        $this->lockWR->closeWriter($idCorporacao);
                    }
                }
            }catch(Exception $ex){
                $msg = new Mensagem(null, null, $ex);
                $this->log( "Falha durante recuperacao da corporacao: ".print_r($msg, true));
            }
            Database::closeAll();

        }

    }

    public static function factory($idSistemaSihop){
        return new Processo4($idSistemaSihop);
    }
}