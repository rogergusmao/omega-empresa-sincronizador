<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 20/08/2017
 * Time: 14:08
 */
class ProcessoSeguranca
{
    private $idSistemaSihop;
    public function __construct($idSistemaSihop)
    {
        $this->idSistemaSihop=$idSistemaSihop;
    }

    public static function factory($idSistemaSihop){
        return new ProcessoSeguranca($idSistemaSihop);
    }

    public function run(){
        while(true){

            $msg = BO_SICOB::getCorporacoesParaSincronizacaoDoHosting();

            if($msg != null && $msg->ok()){

                $corporacoes = $msg->mVetorObj;

            } else {
                HelperLog::logProcesso(
                    "Seguranca_Sistema_Sihop_{$this->idSistemaSihop}",
                    "[-1] ".print_r($msg,true));
            }

            $strDataFutura = Helper::getDiaEHoraFuturaSQL(-HelperProcesso::TEMPO_MAXIMO_EXECUCAO_PROCESSO_MINUTOS);

            $where2 = "WHERE estado_sincronizacao_id_INT = '".EXTDAO_Estado_sincronizacao::PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB."'
                AND data_ultimo_processamento_DATETIME < '{$strDataFutura}'";
            $q2Update = "UPDATE sincronizacao SET processando_BOOLEAN = '0' $where2";
            $q2 = "SELECT  corporacao_id_INT FROM sincronizacao $where2";

            $where3 = " WHERE estado_sincronizacao_id_INT IN ( '"
                . EXTDAO_Estado_sincronizacao::GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR
                ."', '".EXTDAO_Estado_sincronizacao::INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO."')
                AND data_ultimo_processamento_DATETIME < '{$strDataFutura}' ";
            $q3Update = "UPDATE sincronizacao SET processando_BOOLEAN = '0' $where3 ";
            $q3 = "SELECT  corporacao_id_INT FROM sincronizacao $where3";

            $hashBancosJaVerificados = array();
            if(!empty($corporacoes)){
                HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}", "[0] Rotina seguran�a. Total corporacoes: ".count($corporacoes));
                for($i = 0 ; $i < count($corporacoes); $i++) {
                    try{
                        $jsonObj = $corporacoes[$i];
                        $idHospedagem = $jsonObj->id_hospedagem;
                        if (array_search($idHospedagem, $hashBancosJaVerificados) != false)
                            continue;
                        else $hashBancosJaVerificados[count($hashBancosJaVerificados)] = $idHospedagem;

                        $corporacao = $jsonObj->corporacao;

                        ConfiguracaoCorporacaoDinamica::factoryConfiguracaoSite($corporacao, false);
                        $configSincronizadorWebDb = Registry::get('ConfiguracaoDatabasePrincipal');
                        $configWebDb = Registry::get('ConfiguracaoDatabaseSecundario');

                        $db = new Database($configSincronizadorWebDb);

                        $msg = $db->queryMensagem($q2);

                        if ($msg != null && $msg->erro()){
                            HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}", "[1]".print_r($msg, true));
                        }
                        else if($db->rows() > 0){
                            $idsCorporacoes = Helper::getResultSetToArrayDeUmCampo($db->result);

                            $db->queryMensagem($q2Update);

                            if(!empty($idsCorporacoes)){
                                HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}", "[1.1] - Corporacoes Processo2 - ".print_r($idsCorporacoes, true));
                                $idSistema = $jsonObj->id_sistema;
                                $helperRedis = new HelperRedis("SIHOP_{$this->idSistemaSihop}");
                                $helperRedis->rpush(HelperProcesso::QUEUE_SINCRONIZACAO_P2, $idsCorporacoes);
                                HelperRedis::closeAll();
                            }
                        }
                        //BUsca as corporacoes que estao gerando arquivos/ inserindo operacoes no banco espelho
                        $msg = $db->queryMensagem($q3);

                        if ($msg != null && $msg->erro()){
                            HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}", "[2]  ". print_r($msg, true));
                        }
                        else if($db->rows() > 0){
                            $idsCorporacoes = Helper::getResultSetToArrayDeUmCampo($db->result);
                            $db->queryMensagem($q3Update);

                            if(!empty($idsCorporacoes)){
                                HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}", "[2.1] - Corporacoes Processo3 - ".print_r($idsCorporacoes, true));
                                $idSistema = $jsonObj->id_sistema;
                                $helperRedis = new HelperRedis("SIHOP_{$this->idSistemaSihop}");
                                $helperRedis->rpush(HelperProcesso::QUEUE_SINCRONIZACAO_P3, $idsCorporacoes);
                                HelperRedis::closeAll();
                            }
                        }

                        $boBS = new BO_Banco_sqlite(
                            $jsonObj->id_corporacao,
                            $corporacao,
                            $configWebDb,
                            $configSincronizadorWebDb);

                        $msg = $boBS->validaBancoSqliteDaCorporacaoAtualmenteArmazenado();
                        if ($msg != true || (is_object($msg) && !$msg->ok())) {

                            HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}",
                                "Banco sqlite da corporacao {$jsonObj->id_corporacao} est� com problemas: ".print_r($msg,true));
                            $helperRedis = new HelperRedis("SIHOP_{$this->idSistemaSihop}");
                            $helperRedis->rpush(HelperProcesso::QUEUE_GERAR_BANCO_SQLITE_ERRO, $jsonObj->id_corporacao);
                            HelperRedis::closeAll();
                        } else {
                            HelperLog::logProcesso("Seguranca_Sistema_Sihop_{$this->idSistemaSihop}",
                                "Banco sqlite da corporacao {$jsonObj->id_corporacao} - Ok! ");

                        }

                    }catch(Exception $ex){
                        $msg = new Mensagem(null,null,$ex);
                        HelperLog::logProcesso(
                            "Seguranca_Sistema_Sihop_{$this->idSistemaSihop}",
                            "[1]".print_r($msg, true));
                    }
                    Database::closeAll();

                }

            }

            sleep(HelperProcesso::TEMPO_ESPERA_ROTINA_SEGURANCA_SEGUNDOS);
        }
    }
}