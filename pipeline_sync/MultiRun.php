<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 15/09/2017
 * Time: 21:10
 */
class MultiRun
{

    private $pathRaizPipeline;
    public function __construct() {

        //$this->pathRaizPipeline = "C:\wamp64\www\SincronizadorWeb\SW10002Corporacao\adm\pipeline.php";
        $this->pathRaizPipeline = "/var/www/sincronizador/public_html/adm/pipeline.php";
    }
    public function run(){
        $classes = array("Processo0", "Processo1", "Processo2", "Processo3", "Processo4", "ProcessoSeguranca");
        $sistemas = array(ID_SISTEMA_EMPRESA, ID_SISTEMA_EMPRESA_CORPORACAO);
        for($i = 0 ; $i < count($sistemas); $i++){
            for($j = 0 ; $j < count($classes); $j++){
                $s = $sistemas[$i];
                $c = $classes[$j];
                $this->initProccess($c, $s);
            }
        }
    }

    public function kill(){
        $classes = array("Processo0", "Processo1", "Processo2", "Processo3", "Processo4", "ProcessoSeguranca");
        $sistemas = array(ID_SISTEMA_EMPRESA, ID_SISTEMA_EMPRESA_CORPORACAO);
        for($i = 0 ; $i < count($sistemas); $i++){
            for($j = 0 ; $j < count($classes); $j++){
                $s = $sistemas[$i];
                $c = $classes[$j];
                $this->killProccess($c, $s);
            }
        }
    }

    private function initProccess($classe, $idSistema){
        if($this->necessitaStartarNovoProcesso($classe, $idSistema)){
            Helper::execInBackground( "php {$this->pathRaizPipeline} -c $classe -a run  $idSistema");
        }
    }

    private function necessitaStartarNovoProcesso($classe, $idSistema){
        $pathFile = InterfaceProcesso::PATH_LOG_PROCESSOS_EM_EXECUCAO($classe, $idSistema);
        if(file_exists($pathFile)){
            $strprocessos = file_get_contents($pathFile);
            if(strlen($strprocessos)){
                $processos = explode(';', $strprocessos);
                if(count($processos) > 1){
                    unset($processos[0]);
                    $idsRunning  = Helper::getPidsRunning($processos);
                    //$idsNotRunning = Helper::arrayNotIn($processos, $idsRunning);
                    if($idsRunning != null && count($idsRunning) == 1 && count($processos) == 1){

                        return false;
                    }else{
                        Helper::killAll($processos);
                        unlink($pathFile);
                        return true;
                    }
                }
            }
        }
        return true;
    }


    private function killProccess($classe, $idSistema){
        $pathFile = InterfaceProcesso::PATH_LOG_PROCESSOS_EM_EXECUCAO($classe, $idSistema);
        if(file_exists($pathFile)){
            $strprocessos = file_get_contents($pathFile);
            if(strlen($strprocessos)){
                $processos = explode(';', $strprocessos);
                if(count($processos)){
                    unset($processos[0]);

                    Helper::killAll($processos);
                    unlink($pathFile);
                }
            }
        }
    }

    public static function factory() {

        return new MultiRun();
    }

}
