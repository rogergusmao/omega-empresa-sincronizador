<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 14/08/2017
 * Time: 21:16
 */
class Processo3 extends InterfaceProcesso
{
    //Processo2 ou Processo3 - podem inserir nesse processo
    //  geraArquivoCrudDaSincronizacao


    public function __construct($idSistemaSihop){
        parent::__construct(HelperProcesso::QUEUE_SINCRONIZACAO_P3, $idSistemaSihop, 3);

    }
    public function run(){
        //Processo2 ou o proprio Processo3 - podem inserir nesse processo
        //  processoIniciaSincronizacaoOuDaAndamentoEmUmaQueAcabouDeIniciar

        while(true){
            //Queue - ids corporacao - sem tempo de expiracao
            $idCorporacao = $this->nextCorporacao(HelperProcesso::TEMPO_ITERACAO_P3);

            if($idCorporacao == null) continue;
            if ($this->lockWR->tryRead($idCorporacao)) {
                try{
                    //Queue - id corporacao
                    //realiza processo 1
                    $boSincronizacao = new BO_Sincronizacao();
                    $msg = $boSincronizacao->initByIdCorporacao($idCorporacao );
                    if($msg == null || $msg->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO){


                        //PROCESSO EH EXECUTADO AQUI!!!!!
                        $msg = $boSincronizacao->processo3(true);

                        if($msg->erroBancoForaDoAr()){
                            $this->pushCorporacao($idCorporacao);
                        }

                    }else {
                        $this->log( "Falha ao iniciar a confiuracao do sinc: ".print_r($msg, true));
                    }

                }catch(Exception $ex){
                    $msg = new Mensagem(null, null, $ex);
                    $this->log( "Falha durante execucao do sinc: ".print_r($msg, true));
                }

                $this->lockWR->closeReader($idCorporacao);

                Database::closeAll();
            } else $this->pushCorporacao($idCorporacao);

        }
    }

    public static function factory($idSistemaSihop){
        return new Processo3($idSistemaSihop);
    }
}