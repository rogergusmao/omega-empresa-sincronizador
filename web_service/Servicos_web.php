<?php
//Helper::includePHP(false, 'recursos/classes/class/Generic_DAO.php');
//Helper::includePHP(false, 'recursos/classes/class/SingletonLog.php');
Helper::includePHP(false, 'recursos/classes/class/LockSincronizador.php');


//Helper::includePHP(false, 'recursos/classes/DAO/DAO_Erro.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Estado_sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Estado_sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sincronizacao_mobile_iteracao.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Tipo_operacao_banco.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sistema_tabela.php');

Helper::includePHP(false, 'recursos/classes/DAO/DAO_Sistema_atributo.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Usuario.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud_origem.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud_mobile.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Crud_mobile_dependente.php');
Helper::includePHP(false, 'recursos/classes/DAO/DAO_Corporacao.php');


//Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Erro.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Estado_sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Estado_sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sincronizacao_mobile_iteracao.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Tipo_operacao_banco.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sistema_tabela.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sistema_sequencia.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Sistema_atributo.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Usuario.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud_origem.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud_mobile.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Crud_mobile_dependente.php');
Helper::includePHP(false, 'recursos/classes/EXTDAO/EXTDAO_Corporacao.php');


//Helper::includePHP(false, 'recursos/classes/class/DatabaseException.php');
//Helper::includePHP(false, 'recursos/classes/class/QueryException.php');
//Helper::includePHP(false, 'recursos/classes/class/Database.php');
Helper::includePHP(false, 'recursos/classes/class/Seguranca.php');
//Helper::includePHP(false, 'recursos/classes/class/ConfiguracaoDatabase.php');

Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Crud.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Crud_mobile.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Crud_web.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/BO/entidade/BO_Sincronizacao_mobile.php');

Helper::includePHP(false, 'recursos/classes/BO/util/BO_Arquivo_crud_sincronizacao.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Arquivo_sincronizacao_mobile.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Banco.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Banco_sqlite.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Banco_web.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Sincronizador_espelho.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Tabela.php');
Helper::includePHP(false, 'recursos/classes/BO/util/BO_Tabela_web.php');

Helper::includePHP(false, 'recursos/classes/BO/Database_ponto_eletronico.php');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Servicos_web
 *
 * @author home
 */
class Servicos_web {
    //Como chamar algum webservice da classe:
    //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=

    const PREFIXO = "SINCWEB:";
    
    
    public function __construct() {
     
    }

    public static function factory() {

        return new Servicos_web();
    }

    public static function isServidorOnline(){
        echo "TRUE";
    }
    
    public static function cadastraCorporacao(){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        
        if(strlen($idCorporacao) && strlen($corporacao) ){
            $msg = EXTDAO_Corporacao::cadastra($idCorporacao, $corporacao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                "Par�metros inv�lidos");
        }
        
        return $msg;
    }


    public static function uploadDadosSincronizadorMobileAntesDoReset(){

        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        $chave= Helper::POSTGET("chave", true);

        $campos = MCrypt::decryptGet($chave);

        if(!$campos){
            return Mensagem::factoryAcessoNegado();
        }

        $corporacao2 = $campos["hash"]["corporacao2"];
        $idCorporacao2 = $campos["hash"]["id_corporacao2"];
        if($corporacao != $corporacao2 || $idCorporacao != $idCorporacao2){
            HelperLog::logPerigo("PERIGO[2F3WFR]: $corporacao != $corporacao2 || $idCorporacao != $idCorporacao2");
            return Mensagem::factoryAcessoNegado();
        }

        $mobileConectado = Helper::POSTGET("mobile_conectado");
        $mobileIdentificador = Helper::POSTGET("mobile_identificador");
        $resetando = Helper::POSTGET("resetando");
        $numRegistro = Helper::POSTGET("num_registro");

        $idPrimeiro = Helper::POSTGET("id_primeiro");
        $idUltimo = Helper::POSTGET("id_ultimo");

        $ultimaMigracao = Helper::POSTGET("ultima_migracao");

        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");

        if(strlen($resetando)
            && !empty($mobileConectado)
            && !empty($numRegistro)
            && !empty($mobileIdentificador)
            && !empty($idSistemaSihop)){

            $msg = BO_Sincronizador_espelho::uploadDadosSincronizadorMobile(
                $idSistemaSihop, $mobileConectado, $mobileIdentificador, $resetando,
                $numRegistro, $idPrimeiro, $idUltimo, $idCorporacao, $corporacao,
                $ultimaMigracao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                "Par�metros inv�lidos");
        }

        return $msg;
    }
    public static function uploadDadosSincronizadorMobile(){
        $mobileConectado = Helper::POSTGET("mobile_conectado");
        $mobileIdentificador = Helper::POSTGET("mobile_identificador");
        $resetando = Helper::POSTGET("resetando");
        $numRegistro = Helper::POSTGET("num_registro");
        
        $idPrimeiro = Helper::POSTGET("id_primeiro");
        $idUltimo = Helper::POSTGET("id_ultimo");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");

        $ultimaMigracao = Helper::POSTGET("ultima_migracao");

        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");

        if(strlen($resetando)
            && !empty($mobileConectado)
            && !empty($numRegistro)
            && !empty($mobileIdentificador)
            && !empty($idSistemaSihop)){

            $msg = BO_Sincronizador_espelho::uploadDadosSincronizadorMobile(
                $idSistemaSihop, $mobileConectado, $mobileIdentificador, $resetando,
                $numRegistro, $idPrimeiro, $idUltimo, $idCorporacao, $corporacao,
                $ultimaMigracao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                "Par�metros inv�lidos");
        }

        return $msg;
    }

    public static function downloadDadosSincronizacao(){
        $corporacao = Helper::POSTGET("corporacao");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $idSincronizacaoMobile = Helper::POSTGET("id_sincronizacao_mobile");
        $idSincronizacao = Helper::POSTGET("id_sincronizacao");
        $ultimaMigracao = Helper::POSTGET("ultima_migracao");
        if(strlen($idSincronizacaoMobile)
            && strlen($idSincronizacao)
            && strlen($corporacao)){
            $msg = BO_Sincronizacao_mobile::downloadDadosSincronizacao(
                $idSincronizacaoMobile, $idSincronizacao, $idCorporacao,$corporacao, $ultimaMigracao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }

        return $msg;
    }

    public static function statusSincronizacaoMobile(){
        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");
        $idSincronizacaoMobile = Helper::POSTGET("id_sincronizacao_mobile");
        $ultimoIdSincronizacao = Helper::POSTGET("ultimo_id_sincronizacao");
        $corporacao = Helper::POSTGET("corporacao");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $idMobileConectado = Helper::POSTGET("mobile_conectado");
        $ultimaMigracao = Helper::POSTGET("ultima_migracao");

        if(strlen($idSincronizacaoMobile) && strlen($corporacao) && strlen($idMobileConectado)){
            $msg = BO_Sincronizacao_mobile::statusSincronizacaoMobile(
                $idSistemaSihop,
                    $corporacao,
                    $idSincronizacaoMobile,
                    $ultimoIdSincronizacao,
                    $idCorporacao,
                    $idMobileConectado,
                    $ultimaMigracao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }

        return $msg;
    }

    public static function downloadDadosSincronizacaoMobile(){

        $idSM = Helper::POSTGET("id_sincronizacao_mobile");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        $ultimaMigracao= Helper::POSTGET("ultima_migracao");
        if(strlen($idSM) 
            && strlen($idCorporacao) 
            && strlen($corporacao) ){
            
            $msg = BO_Sincronizacao_mobile::downloadDadosSincronizacaoMobile(
                $idSM, $idCorporacao, $corporacao, $ultimaMigracao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }

        return $msg;
    }


    public static function mobileResetado(){

        $mobileIdentificador = Helper::POSTGET("mobile_identificador");
        $idUltimaSincronizacao = Helper::POSTGET("id_ultima_sincronizacao");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $ultimaMigracao= Helper::POSTGET("ultima_migracao");
        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");


        if(strlen($mobileIdentificador) && strlen($idCorporacao) ){
            $msg = BO_Sincronizacao_mobile::mobileResetado(
                $mobileIdentificador, $idUltimaSincronizacao, $idCorporacao, $ultimaMigracao, $idSistemaSihop);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }

        return $msg;
    }


    public static function erroSincronizacaoMobile(){

        $mobileIdentificador = Helper::POSTGET("mobile_identificador");
        $idSincronizacaoMobile = Helper::POSTGET("id_sincronizacao_mobile");
        $idSincronizacao= Helper::POSTGET("id_sincronizacao");
        $corporacao= Helper::POSTGET("corporacao");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");

        if(strlen($mobileIdentificador) && strlen($idSincronizacaoMobile)){
            $msg = BO_Sincronizacao_mobile::erroSincronizacaoMobile(
                $idSistemaSihop ,
                $mobileIdentificador,
                $idSincronizacao,
                $idSincronizacaoMobile,
                $corporacao,
                $idCorporacao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos. $mobileIdentificador, $idSincronizacaoMobile");
        }

        return $msg;
    }
    public static function finalizaSincronizacaoMobile(){

        $idSincronizacaoMobile= Helper::POSTGET("id_sincronizacao_mobile");
        $corporacao= Helper::POSTGET("corporacao");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $ultimaMigracao = Helper::POSTGET("ultima_migracao");
        if(strlen($idSincronizacaoMobile) ){
            $msg = BO_Sincronizacao_mobile::finalizaSincronizacaoMobile(
                $idSincronizacaoMobile, 
                $corporacao,
                $idCorporacao,
                $ultimaMigracao);

        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }

        return $msg;
    }
    public static function retiraMobileDaFilaDeEspera(){
        $idSincronizacaoMobile = Helper::POSTGET("id_sincronizacao_mobile");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        return BO_Sincronizacao_mobile::retiraMobileDaFilaDeEspera(
            $idSincronizacaoMobile, $idCorporacao);
    }

    public static function teste2(){
        //echo EXTDAO_Crud_mobile::getIdTabelaWebCorrelacionado(438252, 22, -2);
        
//        echo EXTDAO_Crud_mobile::getIdTabelaWebCorrelacionado(443521, 21, -20);
        
        
//        $bo = new BO_Tabela("pessoa", 6, 1, 423);
//        print_r($bo->atributosFormatadosFk);

    }
    public static function executaSincronizacaoCompleta($idCorporacao = null, $corporacao = null){
        
        if($corporacao == null)
            $corporacao = Helper::POSTGET("corporacao");
        if($idCorporacao == null)
            $idCorporacao = Helper::POSTGET("id_corporacao");
        
        $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
        //idUltimaSincronizacao - id do SistemaRegistroSincronizadorAndroidParaWeb;
        //$ultimoIdSincronizacao = Helper::POSTGET("ultimo_id_sincronizacao");
        $msg = $obj->executaSincronizacaoCompleta();
      
        return $msg;
    }

    public static function cicloSincronizacaoCompleta($idCorporacao = null, $corporacao = null){
        if($corporacao == null)
            $corporacao = Helper::POSTGET("corporacao");
        if($idCorporacao == null)
            $idCorporacao = Helper::POSTGET("id_corporacao");
        if(strlen($corporacao) && strlen($idCorporacao)){
            
            $i = 1;
            while(true){
                //http://127.0.0.1:8080/Cobranca/10003Corporacao/adm/webservice.php?class=Servicos_web&action=getCorporacoesParaSincronizacaoDoHosting&id_empresa_hosting=1
                //BO_SICOB::getCorporacoesParaSincronizacaoDoHosting()
                $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
                
                
                
//                if($i == 2){
//                    print_r(Database::$arrLinks);
//                    exit();
//                }
                //idUltimaSincronizacao - id do SistemaRegistroSincronizadorAndroidParaWeb;
                //$ultimoIdSincronizacao = Helper::POSTGET("ultimo_id_sincronizacao");
                
                $msg = $obj->executaSincronizacaoCompleta();
                
                print_r($msg);
                
                
                $obj->close();    
                $obj = null;
                
                
                echo("\n\n\n$i\n");
                $i++;
                sleep(10);
                
                $bo = new BO_Banco_sqlite($idCorporacao, $corporacao);
                $msg = $bo->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao();
                $bo->close();
                print_r($msg);
                sleep(2);
                
            }
        } else {
            return new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, 
                "[lnkerigoer] cicloSincronizacaoCompleta - Parametros invalidos");
        }
        
        
    }


    //PROCESSO 1
    public static function iniciaProximoProcessoDeSincronizacao(){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
        $idUltimaSincronizacaoDoMobile = Helper::POSTGET("ultimo_id_sincronizacao");
        $msg = $obj->iniciaSincronizacao();
        return $msg;
    }




    //PROCESSO 3
    public static function executaCrudMobileDoBancoEspelhoNoBancoWeb(){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        
        $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
        $msg = $obj->executaCrudMobileDoBancoEspelhoNoBancoWeb();
        return $msg;
    }

    //PROCESSO 4
    public static function processoInsereOperacoesDoBancoWebNoBancoEspelho(){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao= Helper::POSTGET("corporacao");
        $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
        $msg = $obj->processoInsereOperacoesDoBancoWebNoBancoEspelho(
            $corporacao);
        return $msg;
    }


    //PROCESSO 5
    public static function processoGeraArquivoCrudDaSincronizacao($corporacao= null){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        if($corporacao == null)
            $corporacao= Helper::POSTGET("corporacao");
        $obj = new BO_Sincronizacao($idCorporacao, $corporacao);
        $msg = $obj->processo3();
        return $msg;
    }
    //Servico chamado quando o mobile acaba de instalar o app, caso exista
    //alguma sincronizacao_mobile pendente na sincronizacao, essa ter� seu
    //status alterado para EXTDAO_Estado_mobile::RESETADO
    public static function resetaMobile(){
        //TODO
    }

//        public static function teste(){
//            $q = "update __pessoa_empresa set pessoa_id_INT = null where id = 154";
//            $db= new Database();
//            $msg = $db->queryMensagem($q);
//            return $msg;
//        }
    public static function teste(){

//            $db = new Database();
//            $msg = $db->queryMensagem("delete from uf where id = 37");
        try{
            $db = new Database();
            $msg = $db->queryMensagem("SELECT id FROM cliente 'HGWRE'");
        } catch (Exception $ex) {
            $msg = new Mensagem(null,null,$ex);
        }

        $log= Registry::get('HelperLog');
        $log->gravarLogEmCasoErro($msg);

        return $msg;

//            $idSincronizacao = Helper::POSTGET("id_sincronizacao");
//            $corporacao= Helper::POSTGET("corporacao");
//            $bo = new BO_Sincronizacao();
//            $bo->idSincronizacao = $idSincronizacao;
//            $bo->geraArquivoCrudDaSincronizacao($corporacao);
//            exit();
    }
    
    
    
    public static function procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao(){
        set_time_limit(0);
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        $aForca = Helper::POSTGET("aForca");
        if($aForca=="true") $aForca = true;
        else $aForca = false;
//            echo "entrou::$idCorporacao";
//            exit();
        if(strlen($idCorporacao)){
            
            $bo = new BO_Banco_sqlite($idCorporacao, $corporacao);
            $msg = $bo->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao(true, $aForca);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }
        return $msg;
    }
    
    public static function procedimentoGerarBancoSqlite(){
        $corporacao = Helper::POSTGET("corporacao");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $aForca=Helper::POSTGET("forca") == "true" ? true : false;
//            echo "entrou::$idCorporacao";
//            exit();
        if(!empty($idCorporacao)){
            $bo = new BO_Banco_sqlite($idCorporacao, $corporacao);
            $msg = $bo->procedimentoGerarBancoSqlite($aForca);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }
        return $msg;
    }

    public static function statusSincronizacaoBancoSqliteDaCorporacao(){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        $mobileIdentificador = Helper::POSTGET("mobile_identificador");
        if( strlen($mobileIdentificador) && strlen($idCorporacao)){
            $bo = new BO_Banco_sqlite($idCorporacao, $corporacao);
            $msg = $bo->status();
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }
        return $msg;
    }

    public static function downloadBancoSqliteDaCorporacao(){

        $idCorporacao = Helper::POSTGET("id_corporacao");
        $mobileIdentificador = Helper::POSTGET("mobile_identificador");

        $corporacao = Helper::POSTGET("corporacao");
        if(strlen($mobileIdentificador) && strlen($idCorporacao) && strlen($corporacao)){
            $bo = new BO_Banco_sqlite($idCorporacao, $corporacao);
            $msg = $bo->downloadBancoSqliteDaCorporacao();
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }
        return $msg;
    }


    //ATENCAO: quando alguma operacao de um crud_mobile der problema o sincronizador
    //dever� criar um novo processo de sincronizacao contendo o restante dos mobiles
    //esse ser� inicializado no estado de EXECUTANDO_CRUD_MOBILE.
    //Todas as operacoes que haviam sido executadas no banco mobile n�o existir�o, devido ao fallback do SQL.
    //A sincroniza��o continuar� at� o final com o restante dos mobiles.
    //
    public static function assinaturaMigrada($idCorporacao = null, $corporacao = null){

        if($corporacao == null)
            $corporacao = Helper::POSTGET("corporacao");
        if($idCorporacao == null)
            $idCorporacao = Helper::POSTGET("id_corporacao");

        $idUltimaSincronizacao = Helper::POSTGET("id_ultima_sincronizacao");

        if(is_numeric($idCorporacao) && !empty($corporacao)){
            $boAM = new BO_Sincronizacao();
            $msg = $boAM->assinaturaMigrada($idCorporacao, $idUltimaSincronizacao, $corporacao);
        } else
            $msg = new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                "Parametro invalidos: $idCorporacao, $corporacao");

        return $msg;
    }


    public static function cadastraCorporacaoNaReserva(){
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");

        if(strlen($idCorporacao) && strlen($corporacao) ){
            $msg = EXTDAO_Corporacao::cadastra($idCorporacao, $corporacao);
        } else {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                "Par�metros inv�lidos");
        }

        return $msg;
    }


    public static function daCordaNoSincronizador()
    {

        $idCorporacao = Helper::POSTGET("id_corporacao");
        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");

        if (!empty($idCorporacao) && !empty($idSistemaSihop)) {
            $helperProcesso = new HelperProcesso($idSistemaSihop );
            $helperProcesso->addItemProc0($idCorporacao);
        }
    }

    public static function clearCacheBanco()
    {

        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");

        if (!empty($corporacao)) {
            return ConfiguracaoCorporacaoDinamica::clearConfiguracaoSiteFromCache($corporacao);
        }else
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Parametro invalido");
    }

    public static function empilhaPedidoGeracaoSqlite()
    {

        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");
        $idCorporacao = Helper::POSTGET("id_corporacao");


        if (!empty($corporacao)) {
            $processo4 = new Processo4($idSistemaSihop);
            if($processo4->pushCorporacao($idCorporacao) > 0 )

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            else return new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                "Falha durante o pedido de geração sqlite");
        }else
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Parametro invalido");
    }

    public static function clearCacheBancoEEmpilhaPedidoSqlite()
    {

        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");
        $idSistemaSihop = Helper::POSTGET("id_sistema_sihop");

        if (!empty($corporacao) && !empty($idSistemaSihop)) {
            $msg= ConfiguracaoCorporacaoDinamica::clearConfiguracaoSiteFromCache($corporacao);
            if($msg == null || !$msg->erro()){
                $processo4 = new Processo4($idSistemaSihop);
                if($processo4->pushCorporacao($idCorporacao) > 0 ) {

                    return new Mensagem(
                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        "Cache limpa e pedido do sqlite empilhado");
                }
                else return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "Falha durante o pedido de geração sqlite");
            } else return $msg;
        }else
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Parametro invalido");
    }

    
}
